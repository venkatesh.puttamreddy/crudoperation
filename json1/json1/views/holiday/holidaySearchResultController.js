ouaApp.controller('holidaySearchResultController', function ($scope, $http, $uibModal, $log, $document, $rootScope, $cookieStore, $cookies, $location, starService, ConstantService, ServerService) {
	sessionStorage.removeItem('fltIndex');
	sessionStorage.removeItem('HotelroomIndex');
	sessionStorage.removeItem('hotelSearchResp');
	sessionStorage.removeItem('repriceResp');
	sessionStorage.removeItem('roombean');
	sessionStorage.removeItem('selectedHotelResp');
	$scope.hotelResponse = JSON.parse(sessionStorage.getItem('hotelPlusSearchResp'));
	$scope.flightResponse = JSON.parse(sessionStorage.getItem('flightplusSearchResp'));
	$scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
	$scope.filterParams = $scope.hotelResponse.filterParams ;
	
	$rootScope.selectedflightResponse = $scope.flightResponse.flightSearchResults[0];
	var selectedFlt;
	var selectedFlt = JSON.parse(sessionStorage.getItem('fltIndex'));
	if(selectedFlt !=null){
	$rootScope.selectedflightResponse=$scope.flightResponse.flightSearchResults[selectedFlt];
	} 
	
	$scope.currency = ConstantService.currency 
	$scope.night = parseInt($scope.hotelResponse.checkOutDate) - parseInt($scope.hotelResponse.checkInDate);
	
	
	$scope.disabled = false;
		$scope.loading = false;
	/* selectedHotel function */
	$scope.hotelResults = {}
	$scope.selectedHotel = function(val){
	$('html, body').scrollTop(0);
	var hotels = $scope.hotelResponse.searchResults
	$rootScope.selectInd = false;
	$scope.flightInd = $rootScope.selectInd;
	$scope.flightPlus = {}
	$scope.flightPlus.flightSearchResults = [];
	if($scope.flightInd == false){
	$scope.flightsearchRes = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(resp){
			
			if(resp.data.isSessionValid == true){
				console.log("RESP "+resp);
				document.cookie="flightIndex="+0;
		$scope.flightPlus.frmAirprt = $scope.flightsearchRes.frmAirprt;
		$scope.flightPlus.toAirprt = $scope.flightsearchRes.toAirprt;
		$scope.flightPlus.adtCnt = $scope.flightsearchRes.adtCnt;
		$scope.flightPlus.chldCnt = $scope.flightsearchRes.chldCnt;
		$scope.flightPlus.infntCnt = $scope.flightsearchRes.infntCnt;
		$scope.flightPlus.trvlType = $scope.flightsearchRes.trvlType;
		$scope.flightPlus.srchType = $scope.flightsearchRes.srchType;
		$scope.flightPlus.deptDate = $scope.flightsearchRes.deptDate;
		$scope.flightPlus.arrDate = $scope.flightsearchRes.arrDate;
		$scope.flightPlus.curncy = 'AED';
		$scope.flightPlus.agncyConvrsnRate = $scope.flightsearchRes.agncyConvrsnRate;
		$scope.flightPlus.clasType = $scope.flightsearchRes.clasType;
		$scope.flightPlus.cnvrtdCrncyCode = $scope.flightsearchRes.cnvrtdCrncyCode;
		$scope.flightPlus.cnvrsnRate = $scope.flightsearchRes.cnvrsnRate;
		$scope.flightPlus.flightSearchResults.push($scope.flightsearchRes.flightSearchResults[0]);
		$http.post(ServerService.serverPath+'rest/flight/price',$scope.flightPlus,{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).then(function(response){
			
			
			if(response.data.status == 'SUCCESS'){
				$scope.selectedFliResp = response.data.data;
			sessionStorage.setItem('selectedFliResp',JSON.stringify($scope.selectedFliResp));
			} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
					});
				}
			
			})
		
				}
			
			})
		}
	
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
			$scope.disabled = true;
		$scope.loading = true;
			document.cookie = "hotelIndex="+val;
			var hotelIndex=getCookie("hotelIndex");
			var paramIndex = hotelIndex;
		
		$scope.hotelResults.hotelCode = hotels[val].htlCode;
			$scope.hotelResults.hotelName = 'null';
			$scope.hotelResults.isHotelOnly = true;
	 $http.post(ServerService.serverPath+'rest/hotel/search/hoteldetails',$scope.hotelResults,{
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			
			}
		} ).then(function(response){
		$scope.disabled = false;
		$scope.loading = false;
			if(response.data.data != null || response.data.data != '' && response.data.status == 'SUCCESS'){
					$scope.selectedHotResp = response.data.data;
					sessionStorage.setItem('selectedHotResp',JSON.stringify($scope.selectedHotResp));
					$location.path('/holidayDetails');
			} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
					});
				}
			}) 
			}
		})
	
	
		}
	
	/* end */
	
	/* starChange */
	$scope.starsObj = {}
	$scope.starsObj.starsList = []
	$scope.starLists = $scope.filterParams.starRatingBeans;
	$scope.luxaryList = $scope.filterParams.luxuryBeans;		
	var starValue; var luxValue;
	$scope.starChange = function(value){
		$scope.starValue = $scope.filterParams.starRatingBeans[value].value;
		starValue = $scope.starValue;
		var filteredData = starService.getStar($scope.hotelResponse, starValue, luxValue);
		$scope.hotelSearchResp1 = filteredData;
		}
   /* end */
   
   /* starToggle function */
   $scope.starsObj = {}
	$scope.starsObj.starsList = []
	$scope.starLists = $scope.filterParams.starRatingBeans;
	$scope.luxaryList = $scope.filterParams.luxuryBeans;		
	var starValue; var luxValue;
   $scope.checkAll = false;
   $scope.starToggle = function(){
	   if(!$scope.checkAll){
					$scope.checkAll = true;
					$scope.starsObj.starsList = $scope.starLists.map(function(item){
					$scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
						return item.value;
						})
				} else {
				$scope.checkAll = false;
					$scope.starsObj.starsList = [];
					$scope.hotelSearchResp1 = {};
					}
	   }
   /* end */
   
   /* luxuryChange function */
   $scope.luxuryChange = function(index){
	   /* luxValue = $scope.filterParams.luxuryBeans[index].value;
	  var filteredData = starService.getStar($scope.hotelResponse, starValue, luxValue);
		$scope.hotelSearchResp1 = filteredData; */
	   }
   /* end */
   
   /* toggleLux function */
   $scope.luxObj = {}
   $scope.luxObj.luxObjList = [];
   $scope.toggleLux = function(){
	   if(!$scope.checkAll){
		   $scope.checkAll = true;
		   $scope.luxObj.luxObjList = $scope.luxaryList.map(function(item){
			   $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
			   return item.value;
		   }) 
		   
		   } else {
			   $scope.checkAll = false;
			   $scope.luxObj.luxObjList = [];
			   $scope.hotelSearchResp1 = {}
			   }
		   }
	 //  }
   /* end */
	
	
    $http.get("json/flightReview.json")
        .then(function (response) {
            $scope.flightList = response.data;
        });
    $http.get("json/hotelResultFilter.json")
        .then(function (response) {
            $scope.filterOptions = response.data;
        });
    $http.get("json/hotelResultList.json")
        .then(function (response) {
            $scope.hotelList = response.data;
        });
    $scope.date = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
   /*  $scope.range = function (n) {
        n = Number(n);
        return new Array(n);
    };

     */

    $scope.animationsEnabled = true;

    $scope.openChangeFlight = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'changeFlight.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            appendTo: parentElem
        });
    };
    
    $scope.openFlightDetails = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightDetails.html',
            controller: 'flightDetailsCtrl',
            size: size,
            appendTo: parentElem
        });
    };
}).controller('ModalInstanceCtrl', function ($http, $scope, $uibModalInstance, $rootScope, ServerService) {
    $scope.flightsearchRes = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
	/* selectedFlight function */
		$scope.flightPlus = {}
		$scope.flightPlus.flightSearchResults = [];
		$scope.disabled = false;
		$scope.loading = false;
		$rootScope.selectInd = false;
	$scope.selectedFlight = function(index){
	$rootScope.selectInd = true;
	$rootScope.selectedflightResponse = $scope.flightsearchRes.flightSearchResults[index];
		sessionStorage.setItem('fltIndex',JSON.stringify(index));
		//$uibModalInstance.close();
		$scope.disabled = true;
		$scope.loading = true;
		/* posting data */
		 $http.get(ServerService.serverPath+'rest/validate/session').then(function(resp){
			
			if(resp.data.isSessionValid == true){
				console.log("RESP "+resp);
				document.cookie="flightIndex="+index;
		$scope.flightPlus.frmAirprt = $scope.flightsearchRes.frmAirprt;
		$scope.flightPlus.toAirprt = $scope.flightsearchRes.toAirprt;
		$scope.flightPlus.adtCnt = $scope.flightsearchRes.adtCnt;
		$scope.flightPlus.chldCnt = $scope.flightsearchRes.chldCnt;
		$scope.flightPlus.infntCnt = $scope.flightsearchRes.infntCnt;
		$scope.flightPlus.trvlType = $scope.flightsearchRes.trvlType;
		$scope.flightPlus.srchType = $scope.flightsearchRes.srchType;
		$scope.flightPlus.deptDate = $scope.flightsearchRes.deptDate;
		$scope.flightPlus.arrDate = $scope.flightsearchRes.arrDate;
		$scope.flightPlus.curncy = 'AED';
		$scope.flightPlus.agncyConvrsnRate = $scope.flightsearchRes.agncyConvrsnRate;
		$scope.flightPlus.clasType = $scope.flightsearchRes.clasType;
		$scope.flightPlus.cnvrtdCrncyCode = $scope.flightsearchRes.cnvrtdCrncyCode;
		$scope.flightPlus.cnvrsnRate = $scope.flightsearchRes.cnvrsnRate;
		$scope.flightPlus.flightSearchResults.push($scope.flightsearchRes.flightSearchResults[index]);
		$http.post(ServerService.serverPath+'rest/flight/price',$scope.flightPlus,{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).then(function(response){
			
			$scope.disabled = false;
		$scope.loading = false;
			$uibModalInstance.close();
			if(response.data.status == 'SUCCESS'){
				$scope.selectedFliResp = response.data.data;
			sessionStorage.setItem('selectedFliResp',JSON.stringify($scope.selectedFliResp));
			} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
					});
				}
			
			})
		
				}
			
			}) 
		/* end */
		
	}
	
	/* if($rootScope.selectInd == true){
		var index = sessionStorage.getItem('fltIndex');
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(resp){
			
			if(resp.data.isSessionValid == true){
				console.log("RESP "+resp);
				document.cookie="flightIndex="+index;
		$scope.flightPlus.frmAirprt = $scope.flightsearchRes.frmAirprt;
		$scope.flightPlus.toAirprt = $scope.flightsearchRes.toAirprt;
		$scope.flightPlus.adtCnt = $scope.flightsearchRes.adtCnt;
		$scope.flightPlus.chldCnt = $scope.flightsearchRes.chldCnt;
		$scope.flightPlus.infntCnt = $scope.flightsearchRes.infntCnt;
		$scope.flightPlus.trvlType = $scope.flightsearchRes.trvlType;
		$scope.flightPlus.srchType = $scope.flightsearchRes.srchType;
		$scope.flightPlus.deptDate = $scope.flightsearchRes.deptDate;
		$scope.flightPlus.arrDate = $scope.flightsearchRes.arrDate;
		$scope.flightPlus.curncy = 'AED';
		$scope.flightPlus.agncyConvrsnRate = $scope.flightsearchRes.agncyConvrsnRate;
		$scope.flightPlus.clasType = $scope.flightsearchRes.clasType;
		$scope.flightPlus.cnvrtdCrncyCode = $scope.flightsearchRes.cnvrtdCrncyCode;
		$scope.flightPlus.cnvrsnRate = $scope.flightsearchRes.cnvrsnRate;
		$scope.flightPlus.flightSearchResults.push($scope.flightsearchRes.flightSearchResults[index]);
		$http.post(ServerService.serverPath+'rest/flight/price',$scope.flightPlus,{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).then(function(response){
			
			$scope.disabled = false;
		$scope.loading = false;
			$uibModalInstance.close();
			if(response.data.status == 'SUCCESS'){
				$scope.selectedFliResp = response.data.data;
			sessionStorage.setItem('selectedFliResp',JSON.stringify($scope.selectedFliResp));
			} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
					});
				}
			
			})
		
				}
			
			})
		
	}  */
	
	/* end */
  
}).controller('flightDetailsCtrl', function ($http, $scope, $uibModalInstance) {
     $scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
	  $scope.flightsearchResdetails = $scope.flightsearchResInfo.flightSearchResults[0];
	 
	 var selectedFlt;
	var selectedFlt = JSON.parse(sessionStorage.getItem('fltIndex'));
	if(selectedFlt !=null){
	$scope.flightsearchResdetails=$scope.flightsearchResInfo.flightSearchResults[selectedFlt];
	}

    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
		}
	}
    return "";
}
ouaApp.service('starService', function(){
	this.getStar = function(listing, starItem, luxItem){
	var star = [];
	var hotelResp = listing;
	var hotelList = hotelResp.searchResults;
		for(var s = 0; s < hotelList.length; s++){
		var temp = hotelList[s]
		var starValue = hotelResp.searchResults[s].starRating;
		if(starValue == starItem){
			star.push(temp);
			}
			
			}
			console.log(star);
			return star;
		}
	
	
	})