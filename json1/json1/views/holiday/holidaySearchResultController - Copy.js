ouaApp.controller('holidaySearchResultController', function ($scope, $http, $uibModal, $log, $document) {
	$scope.hotelResponse = JSON.parse(sessionStorage.getItem('hotelPlusSearchResp'));
	$scope.flightResponse = JSON.parse(sessionStorage.getItem('flightplusSearchResp'));
	$scope.select = true;
	//console.log("$scope.flightResponse" + JSON.stringify($scope.flightResponse));
	//console.log("$scope.hotelResponse" + JSON.stringify($scope.hotelResponse));
	
	$scope.night = parseInt($scope.hotelResponse.checkOutDate) - parseInt($scope.hotelResponse.checkInDate);
	
	
     $http.get("json/flightReview.json")
        .then(function (response) {
            $scope.flightList = response.data;
        });
    $http.get("json/hotelResultFilter.json")
        .then(function (response) {
            $scope.filterOptions = response.data;
        });
    $http.get("json/hotelResultList.json")
        .then(function (response) {
            $scope.hotelList = response.data;
        });
    $scope.date = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
   /*  $scope.range = function (n) {
        n = Number(n);
        return new Array(n);
    }; */

    

    $scope.animationsEnabled = true;

    $scope.openChangeFlight = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'changeFlight.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            appendTo: parentElem
        });
    };
    
    $scope.openFlightDetails = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightDetails.html',
            controller: 'flightDetailsCtrl',
            size: size,
            appendTo: parentElem
        });
    };
}).controller('ModalInstanceCtrl', function ($http, $scope, $uibModalInstance) {
	$scope.flightsearchRes = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
	
	
   /* selectedFlight function */
	$scope.selectedFlight = function(index){
	$scope.flightsearchResIndex = $scope.flightsearchRes.flightSearchResults[index]
		sessionStorage.setItem('selectedFlightList', $scope.flightsearchResIndex);
		$scope.$parent.flightResponse = $scope.flightsearchResIndex;
		
		$uibModalInstance.close();
		$scope.$parent.select = false;
		$scope.selectList = true;
		}
	/* end */

    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}).controller('flightDetailsCtrl', function ($http, $scope, $uibModalInstance) {
   $scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
   
 
    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
