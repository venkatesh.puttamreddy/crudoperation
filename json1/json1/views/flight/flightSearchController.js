ouaApp.controller('flightSearchCtrl', function($scope, $uibModal, $http, $location, ServerService,ajaxService,$timeout, $rootScope, NationService){
	NationService.nationGet();
	$scope.cabinClassList=["Economy", "Premium Economy", "Business", "First"];
	$scope.airlineList=[{title:"All Airlines", code:""}, {title:"Emirates", code:"EK"}, {title:"Qatar Airways", code:"QR"}, {title:"Flydubai", code:"FZ"}, {title:"Egyptair", code:"MS"}
						, {title:"Jet Airways", code:"9W"}, {title:"Saudi Arabian", code:"SV"}, {title:"Etihad Airways", code:"EY"}, {title:"Turkish Airlines", code:"TK"}, {title:"Aer Lingus", code:"EI"}, {title:"Aeroflot", code:"SU"}, {title:"Aerosvit Airlines", code:"VV"}, {title:"Afriqiyah Airways", code:"8U"}, {title:"Aigle Azur", code:"ZI"}, {title:"Air Algerie", code:"AH"}, {title:"Air Arabia", code:"G9"}, {title:"Air Astana", code:"KC"}, {title:"Air Baltic", code:"BT"}, {title:"Air Berlin", code:"AB"}, {title:"Air Canada", code:"AC"}, {title:"Air China", code:"CA"}, {title:"Air France", code:"AF"}, {title:"Air India", code:"AI"}, {title:"Air Macau", code:"NX"}, {title:"Air New Zealand", code:"NZ"}, {title:"Air Pacific", code:"FJ"}, {title:"Alitalia", code:"AZ"}, {title:"American Airlines", code:"AA"}, {title:"Austrian Airlines", code:"OS"}, {title:"Azerbaijan Airlines", code:"J2"}, {title:"BMI", code:"BD"}, {title:"Bangkok Airways", code:"PG"}, {title:"Biman Bangladesh Airlines", code:"BG"}, {title:"British Airways", code:"BA"}, {title:"Cathay Pacific", code:"CX"}, {title:"China Airlines", code:"CI"}, {title:"Condor", code:"DE"}, {title:"Continental Airlines", code:"CO"}, {title:"Czech Airlines", code:"OK"}, {title:"Darwin Airlines", code:"F7"}, {title:"Delta Air Lines", code:"DL"}, {title:"Eritrean Airlines", code:"B8"}, {title:"Ethiopian Airlines", code:"ET"}, {title:"Eva Air", code:"BR"}, {title:"Finnair", code:"AY"}, {title:"Flynas", code:"XY"}, {title:"Garuda Indonesia", code:"GA"}, {title:"Gulf Air", code:"GF"}, {title:"Hahn Air", code:"HR"}, {title:"Hainan Airlines", code:"HU"}, {title:"Heliair Monaco", code:"YO"}, {title:"Hong Kong Airlines", code:"HX"}, {title:"Icelandair", code:"FI"}, {title:"Jet Lite", code:"S2"}, {title:"Kenya Airways", code:"KQ"}, {title:"Kuban Airlines", code:"GW"}, {title:"Kuwait Airways", code:"KU"}, {title:"Liat", code:"LI"}, {title:"Lufthansa", code:"LH"}, {title:"Malaysia Airlines", code:"MH"}, {title:"Middle East Airlines", code:"ME"}, {title:"Mihin Lanka", code:"MJ"}, {title:"Montenegro Airlines", code:"YM"}, {title:"Olympic Airlines", code:"OA"}, {title:"Oman Air", code:"WY"}, {title:"Pakistan International", code:"PK"}, {title:"Philippine Airlines", code:"PR"}, {title:"Precision Air", code:"PW"}, {title:"Qantas", code:"QF"}, {title:"Rossiya Airlines", code:"FV"}, {title:"Royal Air Maroc", code:"AT"}, {title:"Royal Brunei", code:"BI"}, {title:"Royal Jordanian Airlines", code:"RJ"}, {title:"Royal Phnom Penh Airways", code:"RL"}, {title:"Rwandair Express", code:"WB"}, {title:"SAS", code:"SK"}, {title:"Shanghai Airlines", code:"FM"}, {title:"Siberian Airlines", code:"S7"}, {title:"SilkAir", code:"MI"}, {title:"Singapore Airlines", code:"SQ"}, {title:"South African Airways", code:"SA"}, {title:"SriLankan Airlines", code:"UL"}, {title:"Swiss", code:"LX"}, {title:"TAAG Angola Airlines", code:"DT"}, {title:"Tarom", code:"RO"}, {title:"Thai Airways International", code:"TG"}, {title:"Thomas Cook Airlines", code:"HQ"}, {title:"Tunis Air", code:"TU"}, {title:"US Airways", code:"US"}, {title:"Ukraine International Airlines", code:"PS"}, {title:"United Airlines", code:"UA"}, {title:"Ural Airlines", code:"U6"}, {title:"Vietnam Airlines", code:"VN"}, {title:"Virgin Atlantic", code:"VS"}];

	$(document).ready(function(){
    $("select").mouseenter(function(){
        $("select").css("color", "#ed1c24");
    });
    $("select").mouseleave(function(){
        $("select").css("color", "white");
    });
});

/* $(document).ready(function(){
    $("option").mouseenter(function(){
        $("option").css("background-color", "#ed1c24");
    });
    $("option").mouseleave(function(){
        $("option").css("background-color", "white");
    });
}); */


	$scope.airline = {};
	
	$scope.airline.code = "";
	$scope.cabinClass = "";
	$scope.prefAirlineList = [];
	$scope.searchFlightObj={};
	//@author Raghava
	
	/**
	validate CabinClass
	*/
	if(localStorage.getItem("cabinClass")){
		$scope.cabinClass = localStorage.getItem("cabinClass");
	}else{
		$scope.cabinClass = "Economy";
	}	
	localStorage.setItem("cabinClass",$scope.cabinClass);
	/**
		Validate CabinClass end
	*/
	/**
	validate CabinClass
	*/
	if(localStorage.getItem("airlineTitle")){
		$scope.airline.title=localStorage.getItem("airlineTitle");
		$scope.airlinee =localStorage.getItem("airlinee");
	}else{
		$scope.airline.title = "All Airlines";
		$scope.airlinee = $scope.airline.code;
	}	
	/**
		Validate CabinClass end
	*/
	   localStorage.setItem("airlineTitle",$scope.airline.title);
		localStorage.setItem("airlinee",$scope.airlinee);
	
	
	
	$scope.changeCabinClass = function(value){
		$scope.cabinClass = value;
		$scope.searchFlightObj.classType=value;
		localStorage.setItem("cabinClass",$scope.cabinClass);
	}
	
	$scope.changeAirline = function(value){
		$scope.airline.title = value.title;
		$scope.airlinee = value.code;
		localStorage.setItem("airlineTitle",$scope.airline.title);
		localStorage.setItem("airlinee",$scope.airlinee);
	}
	
	sessionStorage.removeItem('passengerCount');
	var intitialDate = {
        startDate: moment(),
        endDate: moment(),
		minDate:new Date(),
        locale: {
            format: "D-MMM-YY"
		}
	};
	$rootScope.classData = ['Economy','Business','First'];
	//if(sessionStorage.getItem('searchFlightObj')!=undefined) {
	if(localStorage.getItem('searchFlightObj')!=undefined) {
		//$scope.searchFlightObjJson = JSON.parse(sessionStorage.getItem('searchFlightObj'));
		$scope.searchFlightObjJson = JSON.parse(localStorage.getItem('searchFlightObj'));
		$scope.searchFlightObjJson = JSON.parse(localStorage.getItem('searchFlightObjJson'));
			
		$('#from_place').val($scope.searchFlightObjJson.frmAirport);
		$('#to_place').val($scope.searchFlightObjJson.toAirport);
		var infArr = [];
		for( var k = 0; k<=$scope.searchFlightObjJson.adtCnt; k++) {
			infArr.push(k);
		}
		$scope.fInfantCount = infArr;
		//console.log($scope.searchFlightObjJson);
		} else {
		$scope.searchFlightObjJson = {};
		$scope.searchFlightObjJson.adtCnt = 1;
		$scope.searchFlightObjJson.chldCnt = 0;
		$scope.searchFlightObjJson.infCnt = 0;
		$scope.fInfantCount = [0,1];
		$scope.searchFlightObjJson.classType = $scope.classData[0];
		$scope.searchFlightObjJson.travelDate="";
		$scope.searchFlightObjJson.frmAirport = 'MUSCAT, Oman - Seeb (MCT)';
		//	$("#from_place").val('MUSCAT, Oman - Seeb (MCT)');
		$scope.searchFlightObjJson.srchType = 2;
	}
	
	
	$scope.searchFlightObjJson.deptTime = "Any Time";
	$scope.searchFlightObjJson.retTime = "Any Time";
	//$scope.searchFlightObjJson.classType = 'Economy';
	//$scope.searchFlightObjJson.prefNonStop = false;
	$scope.searchFlightObjJson.pageIndex = 1;
	$scope.searchFlightObjJson.resultPerPage = 1000;
	
    
    $scope.date = intitialDate;
    
    $scope.hotelDate = intitialDate;
	if(localStorage.getItem('totalPassenger')){
	$rootScope.totalPassenger = localStorage.getItem('totalPassenger');
	}else{
	$rootScope.totalPassenger = 1;
	}
	/* slideup function */
	/* $scope.slideup = function(){
			$("html, body").stop().animate({
				scrollTop: $('#about-us').offset().top - 10
			}, '500', 'linear');
		
		}; */
	/* end */
	
	
	/* for flight search */
	$scope.searchFlightObj = {}
	$scope.date = { startDate: null,
		endDate: null,
		minDate:new Date(),
	}
	
	$scope.states = []
	if(jQuery('#from_place').val()=='') {
		jQuery('#from_place').focus();
	}
	var fromset = false;
	var toset = false;
	var flag = false;
	var flag1 = false;
	var f;
	$( "#from_place" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
					}else{
					$( "#from_place" ).val('');
					$( "#from_place" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#from_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$( "#from_place" ).val($scope.searchFlightObjJson.frmAirport);
			if($( "#to_place" ).val()==$( "#from_place" ).val()) {
				$( "#from_place" ).val("");
				return false;
			}
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
				$scope.emptyError = false;
			},10);
			/*if(flag==false) {
				$('#from_place').val('');
				$('#from_place').focus(0);
				return false;
			}*/
		},
		//autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			if($( "#from_place" ).val()==$( "#to_place" ).val()) {
				$( "#from_place" ).val('');
				$('.destcheck').show().fadeOut(5000);
				} else {
				$( "#from_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.searchFlightObjJson.frmAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$('#to_place').focus(0);
				flag = true;
			}
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function(event, ui) {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.searchFlightObjJson.frmAirport = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	$( "#to_place" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
					}else{
					$( "#to_place" ).val('');
					$( "#to_place" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#to_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$scope.$apply();
			$( "#to_place" ).val($scope.searchFlightObjJson.toAirport);
			if($( "#to_place" ).val()==$( "#from_place" ).val()) {
				$( "#to_place" ).val("");
				$('.destcheck').show().fadeOut(10000);
				return false;
			}
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
				$scope.emptyError = false;
			},10);
			/*if(flag1==false) {
				$('#to_place').val('');
				$('#to_place').focus(0);
				return false;
			}*/
		},
		//autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			if($( "#from_place" ).val()==$( "#to_place" ).val()) {
				$( "#to_place" ).val('');
				$('.destcheck').show().fadeOut(10000);
				} else {
				flag1 = true;
				$( "#to_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.searchFlightObjJson.toAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$('#datepicker3').focus();
			}
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.searchFlightObjJson.toAirport = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	
	$scope.checkboxModel = {}
	
	$scope.oneWayChangess = function(){
			
		$scope.flightop = {
		singleDatePicker: $scope.checkboxModel.value,
		minDate:new Date(),
		eventHandlers: {
			'show.daterangepicker': function(ev, picker){
				$scope.flightop = {
					singleDatePicker: $scope.checkboxModel.value
					}
				}
			}
		}
				if($scope.checkboxModel.value==true)
				{ 
					var dateelement = angular.element('#daterange1');
					var dateval = dateelement.val();
					var dateclass = dateelement.attr("class");
				    var datevalsingle = dateval.split(" - ");
					angular.element('#daterange1').val(datevalsingle[0]);
					var datevalnew = dateelement.val();
					$scope.isOneWay=true;
					if(datevalnew == "") {
				    dateelement.removeClass("ng-valid ng-valid-required");
                    dateelement.addClass("ng-empty ng-valid-parse ng-invalid ng-invalid-required");
					
					var formelement = angular.element('#searchForm');	
					formelement.removeClass("ng-pristine ng-valid ng-valid-required");
                    formelement.addClass("ng-pristine ng-invalid ng-invalid-required");
					}
				}
				else
				{ 
					var dateelement = angular.element('#daterange1');
					dateelement.val('');
					dateelement.removeClass("ng-not-empty ng-valid ng-valid-required");
                    dateelement.addClass("ng-empty ng-valid-parse ng-invalid ng-invalid-required");
					$scope.searchFlightObjJson.travelDate = {startDate: null, endDate: null};
					var formelement = angular.element('#searchForm');	
					formelement.removeClass("ng-pristine ng-valid ng-valid-required");
                    formelement.addClass("ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required");
				   $scope.isOneWay=false;
				   $scope.searchFlightObjJson.travelDate=null;
					
				}
		
		}
		
	
	$scope.flightop = {
		minDate:new Date(),
	}
	
	$scope.loading = false;
	$scope.disabled = false;
	$scope.submitted = false;
	/* flightSearchForm function */
	$scope.mandatory = false;
	var passengerList = {}
	$scope.flightSearchForm = function(valid){
		console.log($scope.checkboxModel.value);
		$scope.submitted = true;
		if(valid){
			//passengerList = JSON.parse(sessionStorage.getItem('passengerCount'));
			passengerList = JSON.parse(localStorage.getItem('passengerCount'));
			var adult; var child; var infant
			
			if(passengerList == null || passengerList == undefined){
				adult = 1;
				infant = 0;
				child = 0;
				} else {
				adult = passengerList.adult;
				infant = passengerList.infant;
				child = passengerList.child ;
			}
			var date = $('#daterange1').val();
			var date1 = []
			date1 = date.split(' ');
			
			var toflight = $('#to_place').val();
			var fromplace = $('#from_place').val();
			
			$scope.searchFlightObj.adtCnt = adult;
			$scope.searchFlightObj.chldCnt = child;
			$scope.searchFlightObj.classType = $scope.cabinClass;
			if($scope.airlinee != ""){
			$scope.prefAirlineList.push($scope.airlinee);
			}
			$scope.searchFlightObj.prefAirlineLst = $scope.prefAirlineList;
			localStorage.setItem("cabinClass",$scope.cabinClass);
			localStorage.setItem("airlineTitle",$scope.airline.title);
			localStorage.setItem("airlinee",$scope.airlinee);
			$scope.searchFlightObj.deptTime = 'Any Time';
			$scope.searchFlightObj.retTime = 'Any Time';
			$scope.searchFlightObj.infCnt = infant;
			$scope.searchFlightObj.pageIndex = 1;
			$scope.searchFlightObj.resultPerPage = 1000;
			$scope.searchFlightObj.srchType = 2;
			/* $scope.searchFlightObj.strDeptDate = date1[0]
			$scope.searchFlightObj.strRetDate = date1[2]; */
			$scope.searchFlightObj.toAirport =toflight;
			$scope.searchFlightObj.frmAirport = fromplace;
			if($scope.checkboxModel.value == true){
				$scope.searchFlightObj.srchType = 1;
				$scope.searchFlightObj.strDeptDate = date1[0]
			
			} else {
				$scope.searchFlightObj.srchType = 2;
				$scope.searchFlightObj.strDeptDate = date1[0]
			$scope.searchFlightObj.strRetDate = date1[2];
				}
			console.log("Inside Flight "+JSON.stringify($scope.searchFlightObj));
			sessionStorage.setItem('searchFlightObj',JSON.stringify($scope.searchFlightObj));
			localStorage.setItem('searchFlightObjJson',JSON.stringify($scope.searchFlightObjJson));
			localStorage.setItem('searchFlightObj',JSON.stringify($scope.searchFlightObj));
			localStorage.setItem("tripType",$scope.isOneWay);
			$location.path('/searchingFlight');
		} 
	}
	
	/* end */
	
	$scope.openFlight = function(size, parentSelector){
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'flightModalContent.html',
			controller : 'flightPassCtrl',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		
	} 
	}).controller('flightPassCtrl', function($scope, $uibModalInstance, $rootScope){
	$scope.adults = [1, 2, 3, 4, 5, 6, 7, 8, 9];
	$scope.childs = [0,1, 2, 3, 4, 5, 6, 7, 8];
	$scope.infants = [0,1];

	//$scope.paasCount = JSON.parse(sessionStorage.getItem('passengerCount'));
	$scope.paasCount = JSON.parse(localStorage.getItem('passengerCount'));
	if($scope.paasCount == undefined){
	$scope.paasCount		={};
	$scope.paasCount.adult = 1;
	$scope.paasCount.child = 0;
	$scope.paasCount.infant = 0;
	}
	
	$rootScope.totalPassenger = parseInt($scope.paasCount.adult) + parseInt($scope.paasCount.child) + parseInt($scope.paasCount.infant);
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.passBtn = function(){
		$scope.submitted = true
		
		sessionStorage.setItem('passengerCount', JSON.stringify($scope.paasCount));
		localStorage.setItem('passengerCount', JSON.stringify($scope.paasCount));
		
		$rootScope.totalPassenger = parseInt($scope.paasCount.adult) + parseInt($scope.paasCount.child) + parseInt($scope.paasCount.infant);
		localStorage.setItem('totalPassenger', JSON.stringify($rootScope.totalPassenger));
		$uibModalInstance.close();
		
		
	}
	
	
	
});