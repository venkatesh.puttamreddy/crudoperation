ouaApp.controller('hotelSearchCtrl', function($scope, $rootScope,$uibModal, $http, $location, ajaxService, ServerService){
	
	$scope.city = [];
	
	if(sessionStorage.getItem('hotelSearchObj')!=undefined) {
		$timeout(function(){
			$scope.hotelSearchObj = JSON.parse(sessionStorage.getItem('hotelSearchObj'));
		},300);
	} else {
		$scope.hotelSearchObj = {};
		$scope.hotelSearchObj.roomBean = [{adultCount:1,childCount:0,childrens:[{childBeans:[]}]}];
	}
	var hotser = false;
	jQuery('#search').keyup(function(){
		if(hotser== true) {
			$(this).val('');
			hotser = false;
		}
		if(jQuery(this).val().length==3) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/hotel/cities','{ "queryString" : "'+jQuery(this).val()+'"}','json')
			.then(function(data,status) {
				var availableTags = [];
				for(var i=0; i< data.length; i++) {
					if(data[i].city.toLowerCase()==jQuery('#search').val().toLowerCase())
					{
						availableTags.push(data[i].city+', '+data[i].country);
					}
				}
				for(var j=0; j< data.length; j++) {
					if(data[j].city.toLowerCase()!=jQuery('#search').val().toLowerCase())
					{	
						if(data[j].city.toLowerCase().search(jQuery('#search').val().toLowerCase())==0) {
							availableTags.push(data[j].city+', '+data[j].country);
						}
						
					}
				}
				for(var k=0; k< data.length; k++) {
					if(data[k].city.toLowerCase()!=jQuery('#search').val().toLowerCase())
					{	
						if(data[k].city.toLowerCase().search(jQuery('#search').val().toLowerCase())!=0) {
							availableTags.push(data[k].city+', '+data[k].country);
						}
						
					}
				}
				jQuery( "#search" ).autocomplete({
					source: availableTags,
					select: function(event, ui) {
						hotser = true;
						reminder=ui.item.value;
						jQuery('#datepicker').focus(0);
					},
					autoFocus: true,
					focus: function (event, ui) {
						event.preventDefault();
					}
				});
				var e = jQuery.Event("keydown");
				e.keyCode = 50;                     
				$("#search").trigger(e);
				
			});
		} 
	});
	
	$scope.hotelAutoComplet=function(hotelto) {
	
	if(hotelto.length==2){
	   console.log("fromHotel"+ hotelto);
		$http.post(ServerService.serverPath+'rest/hotel/cities','{"queryString" : "'+hotelto+'"}', 
			{
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).then(function(response){
			/* $scope.hotelstates = JSON.stringify(response.data); */
			for(var i=0; i<response.data.length; i++){
					var hotelObj=	{
                        Title: response.data[i].city+","+response.data[i].country,
                      Code: response.data[i].code
					}
					$scope.city.push(hotelObj);
				}
			})
}
}
	
	/* onFormSubmit function */
	$scope.loading = false;
	$scope.disabled = false;
	var adoultCount=0;
	var childCount=0;
	$scope.hotelRequest = {}
	
	$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
	 function createRoomBean(){
    var roomBeanObject= {};
        roomBeanObject.index=1;
        roomBeanObject.adultCount="2";
        roomBeanObject.cots=0;
        roomBeanObject.childCount="0";
        roomBeanObject.roomDesc='Single';
        roomBeanObject.xtraBed=false;
        roomBeanObject.roomCount=1;
        roomBeanObject.withChildrens=false;
        roomBeanObject.childrens=[]; 
        return roomBeanObject;
}  
	if($scope.roomList!=null){
	for(i=0;i<$scope.roomList.length;i++){
			adoultCount=adoultCount+parseInt($scope.roomList[i].adultCount);
			childCount=childCount+parseInt($scope.roomList[i].childCount);
		}
	}
	else{
		$scope.roomList=[];
		  var roomBean=createRoomBean();
	   roomBean.roomCount=1;
       $scope.roomList.push(roomBean);
       adoultCount=2;
       childCount=0;
       $rootScope.totalPassengers=adoultCount+childCount;
       sessionStorage.setItem("roomList",JSON.stringify($scope.roomList));
	}


		$rootScope.passengerInfo="Adults - "+adoultCount+" + Child - "+childCount;
	
	
	$scope.onFormSubmit = function(valid){
	$('html, body').scrollTop(0);
		$scope.submitted = true;
		if(valid){
			var fromHotelPlace = $scope.hotelSearchObj.place;
		var hotelDate = $('#hotelDateRange').val();
		
		var hotelCheckIn = hotelDate.split('-')[0];
		var hotelCheckOut = hotelDate.split('-')[1]; 
		
		var country = fromHotelPlace.split(',')[1];
		var city = fromHotelPlace.split(',')[0];
		
		var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))
		
		/*$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
		$scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
		$scope.childCount = JSON.parse(sessionStorage.getItem('child'));
		$scope.adultCount = JSON.parse(sessionStorage.getItem('adult_count'));
		*
		if($scope.infantCount == null || $scope.infantCount == undefined){
			$scope.infantCount = 0;
			} else {
			$scope.infantCount = $scope.infantCount;
		}
		*/
		
		$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
		if($scope.roomList!=null){
		var tempAdultCount=1;
			  for(i=0;i<$scope.roomList.length;i++){
				switch(tempAdultCount) {
				  case 1:
				  $scope.roomList[i].roomDesc='Single';
				  break;
				  case 2:
				  $scope.roomList[i].roomDesc='Double';
				  break;
				  case 3:
				  $scope.roomList[i].roomDesc='Triple';
				  break;
				  case 4:
				  $scope.roomList[i].roomDesc='Quad';
				  break;
				 }
				 tempAdultCount++;
				adoultCount=adoultCount+parseInt($scope.roomList[i].adultCount);
				childCount=childCount+parseInt($scope.roomList[i].childCount);
			}
		}
		
		$scope.hotelRequest.checkOut = hotelCheckOut;
		$scope.hotelRequest.checkIn = hotelCheckIn;
		//$scope.hotelRequest.place = fromHotelPlace;
		$scope.hotelRequest.place = $scope.hotelSearchObj.place;
		$scope.hotelRequest.country = country;
		$scope.hotelRequest.city = city;
		$scope.hotelRequest.currency = $scope.currency;
		$scope.hotelRequest.htlName = null;
		$scope.hotelRequest.searchMode = 'search';
		$scope.hotelRequest.starRating = 0;
		$scope.hotelRequest.traveller=$rootScope.totalPassengers
		$scope.hotelRequest.roomBean = $scope.roomList;
		
		
		
		$scope.hotelRequest.binNumber = '552107';
		sessionStorage.setItem('hotelRequestObj', JSON.stringify($scope.hotelRequest));
		$location.path('/searchingHotel'); 
			}
		/* var fromHotelPlace = $('#from_hotel').val();
		var hotelDate = $('#hotelDateRange').val();
		
		var hotelCheckIn = hotelDate.split('-')[0];
		var hotelCheckOut = hotelDate.split('-')[1]; 
		
		var country = fromHotelPlace.split(',')[1];
		var city = fromHotelPlace.split(',')[0];
		
		var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))
		$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
		$scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
		$scope.childCount = JSON.parse(sessionStorage.getItem('child'));
		$scope.adultCount = JSON.parse(sessionStorage.getItem('adult'));
		
		if($scope.infantCount == null || $scope.infantCount == undefined){
			$scope.infantCount = 0;
			} else {
			$scope.infantCount = $scope.infantCount;
		}
		
		$scope.hotelRequest.checkOut = hotelCheckOut;
		$scope.hotelRequest.checkIn = hotelCheckIn;
		$scope.hotelRequest.place = fromHotelPlace;
		$scope.hotelRequest.country = country;
		$scope.hotelRequest.city = city;
		$scope.hotelRequest.currency = $scope.currency;
		$scope.hotelRequest.htlName = null;
		$scope.hotelRequest.searchMode = 'search';
		$scope.hotelRequest.starRating = 0;
		
		$scope.hotelRequest.roomBean = $scope.roombean;
		$scope.hotelRequest.traveller = $scope.roombean.length
	
		$scope.hotelRequest.binNumber = '552107';
		sessionStorage.setItem('hotelRequestObj', JSON.stringify($scope.hotelRequest));
		$location.path('/searchingHotel'); */
		/* $http.post('http://192.168.1.102:9999/care4trip/rest/hotel/search/city',$scope.hotelRequest,{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
			$scope.loading = false;
			$scope.disabled = false;
			if(response.data.status == 'SUCCESS'){
				$scope.hotelSearchResp = response.data.data;
				sessionStorage.setItem('hotelSearchResp',JSON.stringify($scope.hotelSearchResp));
				$location.path('/hotelResult')
				} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
				})
			}
		}) */
		
	}
	
	/* end onFormSubmit */
	
	$scope.open = function(size, parentSelector){
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'myModalContent.html',
			controller : 'hotelPassCtrl',
			backdrop: true,
			size : 'lg',
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		
	} 
	}).controller('hotelPassCtrl', function($scope,$rootScope, $uibModalInstance){
	
	  
   $scope.maxRooms=6;
    
  $scope.roomBean= {
      index: 1,
      adultCount: 1,
      cots: 0,
      childCount: 1,
      roomDesc: "Single",
      xtraBed: false,
      roomCount: 1,
      withChildrens: true,
      childrens: []
    };
	
	$scope.childBean={
          index: 0,
          childCount: 1,
          childBeans: [ ]
        };
		
	$scope.childInfo={
              name: null,
              age: 4
            };
	function createChildBean(){
		var childBean={};
	      childBean.index= 0;
          childBean.childCount= 1;
          childBean.childBeans= [ ];
		  return childBean;
        
	}
	function createChildInfo(){	
		var childInfo={};
              childInfo.name= null;
              childInfo.age= 1;
			  return childInfo;
            
    }			
    
	if(sessionStorage.getItem('roomList') ==undefined){
    $scope.roomList=[];
	 $scope.roomCount=1;
	   var roomBean=createRoomBean();
	   roomBean.roomCount=1;
       $scope.roomList.push(roomBean);	
	}
	else{
		$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
		$scope.roomCount=$scope.roomList.length;
	}
  function createRoomBean(){
    var roomBeanObject= {};
        roomBeanObject.index=1;
        roomBeanObject.adultCount="2";
        roomBeanObject.cots=0;
        roomBeanObject.childCount="0";
        roomBeanObject.roomDesc='Single';
        roomBeanObject.xtraBed=false;
        roomBeanObject.roomCount=1;
        roomBeanObject.withChildrens=false;
        roomBeanObject.childrens=[]; 
        return roomBeanObject;
}  
  
    $scope.addRoom=function(){
    	  var roomBean=createRoomBean();
		  roomBean.roomCount=$scope.roomList.length+1;
          $scope.roomList.push(roomBean);		  
    }

     $scope.removeRoom=function(index){
       	  $scope.roomList.splice(index, 1);		  
    }


    $scope.updateRoomList=function(){
        if($scope.roomCount>0){
        if( $scope.roomList.length<$scope.roomCount){
		for(i=$scope.roomList.length; i<$scope.roomCount;i++){	
          var roomBean=createRoomBean();
		  roomBean.roomCount=i+1;
          $scope.roomList.push(roomBean);		  
		}
        }
		else{
			$scope.roomList.splice($scope.roomCount);
		}
        
      }
      
    }
	
	$scope.updateChildList=function(index){
         if($scope.roomList[index].childrens.length==0){
			 $scope.roomList[index].childrens.push(createChildBean());
			 $scope.roomList[index].childrens[0].childCount=$scope.roomList[index].childCount;
			 $scope.roomList[index].withChildrens=true;
			 for(i=0;i<$scope.roomList[index].childCount;i++){
				  $scope.roomList[index].childrens[0].childBeans.push(createChildInfo());
			 }
		 }
		 else {
			 if($scope.roomList[index].childCount==0){
				 $scope.roomList[index].withChildrens=false;
				 $scope.roomList[index].childrens[0].childBeans=[];
			 }
			 else if( $scope.roomList[index].childrens[0].childBeans.length < $scope.roomList[index].childCount)
				$scope.roomList[index].childrens[0].childBeans.push(createChildInfo());
			else{
				$scope.roomList[index].childrens[0].childBeans.splice(1);
			}
		 }
        
      }
      
    
	$scope.ok = function(){
		sessionStorage.setItem('roomList', JSON.stringify($scope.roomList));
		$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
		var adultCount=0;
		var childCount=0;
		for(i=0;i<$scope.roomList.length;i++){
			adultCount=adultCount+parseInt($scope.roomList[i].adultCount);
			childCount=childCount+parseInt($scope.roomList[i].childCount);
		
		}
		$rootScope.totalPassengers=adultCount+childCount;
		$rootScope.passengerInfo="Adults - "+adultCount+" + Child - "+childCount;
		$uibModalInstance.close();	
		
}
$scope.cancel = function(){
	$uibModalInstance.dismiss('cancel');
}

});
