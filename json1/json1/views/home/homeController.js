ouaApp.controller('homeController', function ($scope, $http, $location, $uibModal, ConstantService, ServerService, $rootScope) {
	$scope.currency = ConstantService.currency
	var currentURL=$location.absUrl();
	$scope.relativePath = currentURL.substring(0,currentURL.lastIndexOf("/") +1);
    $scope.slideOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 2,
            },
            1170: {
                items: 3,
            },
        }
    };
    
    $scope.bannerOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 1,
            },
            1170: {
                items: 1,
            },
        }
    };
    
    $scope.tabs = [
        {
            title: '<i class="icon-flights"></i> Flight + <i class="icon-hotels"></i> Hotel',
            content: 'Dynamic content 1'
        },
        {
            title: 'Dynamic Title 2',
            content: 'Dynamic content 2',
            disabled: true
        }
	];
	
	

   /* $scope.states = [
	   {
            "Title": "Chelyabinsk (Balandino), Russian Federation, CEK",
            "Code": "CEK"
    },
        {
            "Title": "Chengdu (Shuangliu International), China, CTU",
            "Code": "CTU"
    },
	{
            "Title": "CHENNAI/MADRAS,India-Madras International(MAA)",
            "Code": "MAA"
    },
        {
            "Title": "Rochester (Monroe County), U.S.A., ROC",
            "Code": "ROC"
    },
        {
            "Title": "MANCHESTER,United Kingdom-Manchester(MAN)",
            "Code": "MAN"
    },
        {
            "Title": "Seychelles, Seychelles, SEZ",
            "Code": "SEZ"
    },
        {
            "Title": "La Rochelle, France, LRH",
            "Code": "LRH"
    },
        {
            "Title": "Port Richey, U.S.A. - All Airports",
            "Code": "MCO,SFB,TPA"
    },
        {
            "Title": "Seoul Incheon, Korea, Republic Of, ICN",
            "Code": "ICN"
    },
        {
            "Title": "Al Hoceima (Cherif El Idrissi), Morocco, AHU",
            "Code": "AHU"
    },
        {
            "Title": "General Mitchell International Airport, U.S.A., MKE",
            "Code": "MKE"
    },
        {
            "Title": "Novokuznetsk (Spichenkovo), Russian Federation, NOZ",
            "Code": "NOZ"
    },
        {
            "Title": "Praslin Island, Seychelles, PRI",
            "Code": "PRI"
    },
        {
            "Title": "Errachidia (Moulay Ali Cherif), Morocco, ERH",
            "Code": "ERH"
    },
	{
            "Title": "DUBLIN,Ireland -Dublin (DUB)",
            "Code": "DUB"
    },
	{
            "Title": "DUBAI,United Arab Emirates -Dubai (DXB)",
            "Code": "DXB"
    },
	{
            "Title": "SHARJAH,United Arab Emirates -Sharjah (SHJ)",
            "Code": "SHJ"
    },
	{
            "Title": "MANAMA,Bahrain -Bahrain International (BAH)",
            "Code": "BAH"
    }
	
  ]; */
  $scope.states = [];
  $scope.flightfromAutoComplet=function(value) {
	if(value!=null){
  if(value.length >= 2){
  var place = {'queryString': value}
  $http.post(ServerService.serverPath+'rest/repo/airport','{"queryString" : "'+value+'"}', 
  {
   headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
   }
   }).then(function(response){
  
   if(response.data!='null'&&response.data.length>0){
   $scope.states = []
   for(var i=0; i<response.data.length; i++){
     var airfrom= {
                      Code: response.data[i].code,
       Title: response.data[i].city+","+response.data[i].country+"-"+response.data[i].name+"("+response.data[i].code+")",
                      
     }
     $scope.states.push(airfrom);
     
    }
    $scope.states.sort();
  }
  })
 }
 }
};


  
    var intitialDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
    
    $scope.date = intitialDate;
    
    $scope.hotelDate = intitialDate;
	
	/* insuranceSearch function */
	$scope.travelInsurances = {};
	$scope.insurancedate = { startDate: null,
    endDate: null
	}
	$scope.travelInsurances.adultCount = 1
	$scope.travelInsurances.childCount = 0
	$scope.travelInsurances.infantCount = 0
	
	$scope.loading = false;
	$scope.disabled = false;
	$scope.insuranceSearch = function(){
	$scope.loading = true;
					$scope.disabled = true;
		var insurance_from = $('#insurancefrom_place').val();
		var insurance_to = $('#insuranceto_place').val();
		var insurance_date = $('#insurance_date').val();
		var insurance_adult = $('#insuranceAdult').val();
		var insurance_child = $('#insuranceChild').val();
		
		var startDate = insurance_date.split('-')[0];
		var endDate = insurance_date.split('-')[1];
		
		if(insurance_adult == "" || insurance_adult == undefined){
			$scope.travelInsurances.adultCount = 0
		} /* else {
			$scope.travelInsurance.adultCount = insurance_adult
			} */
		if(insurance_child == "" || insurance_child == undefined){
			$scope.travelInsurances.childCount = 0
		} /* else {
			$scope.travelInsurance.childCount = insurance_child
			} */
			
			$scope.searchInsure = {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
				}
			
			$scope.Insurance = {
			'travelInsurance' : {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
			}
				}
				sessionStorage.setItem('insuranceSearchObj', JSON.stringify($scope.searchInsure))
			sessionStorage.setItem('InsuranceSearchRequest',JSON.stringify($scope.Insurance));
			$location.path('/searchingInsurance');
			/* $http.post('http://192.168.1.102:9999/care4trip/rest/insurance/plan', JSON.stringify($scope.Insurance),{
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
				$scope.loading = false;
					$scope.disabled = false;
				sessionStorage.setItem('InsuranceSearchResult',JSON.stringify(response.data.data));
				if(response.data.status == 'SUCCESS'){
						$location.path('/insuranceResult');
					}
				
				}) */
		
		
		}
		$scope.submitted = false;
		$scope.insuranceSearchForm = function(valid){
			console.log("flightSearchForm*************"+valid);
		$scope.submitted = true;
		if(valid){
			var insurance_from = $('#insurancefrom_place').val();
		var insurance_to = $('#insuranceto_place').val();
		var insurance_date = $('#insurance_date').val();
		var insurance_adult = $('#insuranceAdult').val();
		var insurance_child = $('#insuranceChild').val();
		
		var startDate = insurance_date.split('-')[0];
		var endDate = insurance_date.split('-')[1];
		
		if(insurance_adult == "" || insurance_adult == undefined){
			$scope.travelInsurances.adultCount = 0
		} 
		if(insurance_child == "" || insurance_child == undefined){
			$scope.travelInsurances.childCount = 0
		} 
			
			$scope.searchInsure = {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
				}
			
			$scope.Insurance = {
			'travelInsurance' : {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
			}
				}
				sessionStorage.setItem('insuranceSearchObj', JSON.stringify($scope.searchInsure))
			sessionStorage.setItem('InsuranceSearchRequest',JSON.stringify($scope.Insurance));
			$location.path('/searchingInsurance');
			}
			}
	/* end */
	
	/* flight plus hotel  */
	
	/* end */
	
	$scope.CabinClass = "Economy";
	$scope.updateCabinClass = function(value) {
		$scope.CabinClass = value;
	}
	$scope.AirlineValue = "All Airline";
	$scope.updateAirline = function(value) {
		$scope.AirlineValue = value;
	}
	
	$scope.opts = {
		opens: 'left',
		eventHandlers: {
			'show.daterangepicker': function(ev, picker) { 
				//console.log('arguments', arguments);
			}
		}
	}
	$scope.checkboxModel = {
       value : false
    };
	
	$scope.flightOpts = {
		singleDatePicker: $scope.checkboxModel.value,
		minDate:new Date(),
		eventHandlers: {
			'show.daterangepicker': function(ev, picker) { 
				$scope.flightOpts = {
					singleDatePicker: $scope.checkboxModel.value
				}
			}
		}
	}
	
	$scope.oneWayChange = function() {
		$scope.date = ($scope.checkboxModel.value) ? $scope.date.startDate : $scope.date;
	}

	$rootScope.tabmenuactive = 1;

}).controller('passengerCtrl', function ($http, $scope, $uibModalInstance, $uibModal) {
    		$scope.paasCount = {}


    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
	console.log("cancel");
       // $uibModalInstance.dismiss('cancel');
	  $uibModalInstance.close();
    };
	;
	
	$scope.save = function(){
		sessionStorage.setItem('pasengerList', JSON.stringify($scope.paasCount));
		$uibModalInstance.dismiss('cancel');
		
		
		}
});