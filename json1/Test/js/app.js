var ouaApp = angular.module('ouaApp', ['ui.bootstrap', 'ngTouch', 'ngSanitize', 'ui.router', 'ui']);

ouaApp.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'views/home.html',
        controller: 'homeController'
    });

    //$httpProvider.interceptors.push('httpInterceptor');
});

ouaApp.factory('httpInterceptor', function ($q, $rootScope, $log) {
    return {
        request: function (config) {
            document.querySelector('.cp-preloader').style.display = "block";
        },
        response: function (response) {
            //            $('.cp-preloader').fadeOut('slow');
            document.querySelector('.cp-preloader').style.display = "none";
        },
        responseError: function (response) {
            document.querySelector('.cp-preloader').style.display = "none";
        }
    };
}).directive('wrapOwlcarousel', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            var options = scope.$eval($(element).attr('data-options'));
            $(element).owlCarousel(options);
        }
    };
});

ouaApp.controller('mainController', function ($scope) {
    $scope.sticky = 'sticky';
});
ouaApp.controller('homeController', function ($scope) {

    $scope.slideOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 2,
            },
            1170: {
                items: 3,
            },
        }
    };
    $scope.tabs = [
        {
            title: '<i class="icon-flights"></i> Flight + <i class="icon-hotels"></i> Hotel',
            content: 'Dynamic content 1'
        },
        {
            title: 'Dynamic Title 2',
            content: 'Dynamic content 2',
            disabled: true
        }
  ];
});
