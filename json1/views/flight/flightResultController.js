ouaApp.controller('flightResultController', function ($scope, $http, $location, $uibModal, $log, $document, $rootScope, ConstantService, stopsService, ServerService) {
	
	$scope.currency = ConstantService.currency;
	$scope.loading = false;
	$scope.disabled = false;
	$scope.result = JSON.parse(sessionStorage.getItem('flightTotalData'));
	
	$scope.flightResultList = $scope.result.data;
	$scope.flightResultList1 = $scope.flightResultList.flightSearchResults
	
	$scope.totalFlight = $scope.flightResultList.flightSearchResults;
	$scope.isCollapsed = true;
    $scope.date = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
	$scope.flightResults = {}
	$scope.flightResults.flightSearchResults = [];
	//var flightSearchResults = []
	$scope.maximum=$scope.flightResultList.filterParams.priceBean.maxPrice;
    $scope.minimum=$scope.flightResultList.filterParams.priceBean.minPrice;
	$scope.filtermodel={};
	
	 $scope.filtermodel.defaultSortType="PRICE";
	 $scope.filtermodel.selectedBound="Outbound";
	 $scope.filtermodel.isOnlySorting=false;	
	 
	 $scope.filtermodel.pageIndex= 1;
	 $scope.filtermodel.resultPerPage= 1000;
	 $scope.filtermodel.defaultSortOrder="ASC";
	 $scope.filtermodel.priceBean = {"isEnabledPrice":true,"fromRange":$scope.minimum,"toRange":$scope.maximum};
	 $scope.filtermodel.arrivalTimeBean = {"isEnabledArrival":true,"fromArrival":0*60000,"toArrival":1439*60000};
	 $scope.filtermodel.deptTimeBean = {"isEnabledDept":true,"fromDept":0*60000,"toDept":1439*60000};	
	
	 $scope.filtermodel.durationBean=null;	     

	 $scope.filtermodel.airlineBeanList=[]; 
	  
	 $scope.filtermodel.stopsBeanList=$scope.flightResultList.filterParams.stopsBeanList;	  	
	 $scope.filtermodel.providerBeanList= $scope.flightResultList.filterParams.providerBeanList;  
	 
	 
	 
	$scope.flightBook = function(index){
	$('html, body').scrollTop(0);
	sessionStorage.setItem('flightIndex',JSON.stringify(index));
	$scope.loading = true;
		$scope.disabled = true;
		
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
			$scope.flightResults.frmAirprt = $scope.flightResultList.frmAirprt;
		$scope.flightResults.toAirprt = $scope.flightResultList.toAirprt;
		$scope.flightResults.adtCnt = $scope.flightResultList.adtCnt;
		$scope.flightResults.chldCnt = $scope.flightResultList.chldCnt;
		$scope.flightResults.infntCnt = $scope.flightResultList.infntCnt;
		$scope.flightResults.trvlType = $scope.flightResultList.trvlType;
		$scope.flightResults.srchType = $scope.flightResultList.srchType;
		$scope.flightResults.deptDate = $scope.flightResultList.deptDate;
		$scope.flightResults.arrDate = $scope.flightResultList.arrDate;
		$scope.flightResults.curncy = 'AED';
		$scope.flightResults.agncyConvrsnRate = $scope.flightResultList.agncyConvrsnRate;
		$scope.flightResults.clasType = $scope.flightResultList.clasType;
		$scope.flightResults.flightSearchResults.push($scope.flightResultList.flightSearchResults[index]);
		
		
		
		
		 $http.post(ServerService.serverPath+'rest/flight/price',$scope.flightResults,{
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		} ).then(function(response){
			if(response.data.data != null || response.data.data != ''){
			$scope.loading = false;
			$scope.disabled = false;
			$scope.flightPriceData = response.data.data
			sessionStorage.setItem('flightPriceData',JSON.stringify($scope.flightPriceData));
				$location.path('/flightDetails');
				}
			}) 
		
				}
			})
		
		
		}
		

		var airlineCode; var stopIndex ;
		$scope.airlineFilter ;
		var filterDatas = {}
		
		
		 for (var index in $scope.flightResultList.filterParams.airlineBeanList) {	  	 
			 $scope.filtermodel.airlineBeanList.push(	   	  	     
						{	   		 	  
							airlineCode :$scope.flightResultList.filterParams.airlineBeanList[index].airlineCode,
						airlineName : $scope.flightResultList.filterParams.airlineBeanList[index].airlineName,
						minFare:$scope.flightResultList.filterParams.airlineBeanList[index].minFare,
						count:$scope.flightResultList.filterParams.airlineBeanList[index].count	     	    	       
						} 	  
					 );		         		
			}
 		
		
		$scope.minPrice= $scope.flightResultList.filterParams.priceBean.minPrice;
		$scope.maxPrice=$scope.flightResultList.filterParams.priceBean.maxPrice;	       
		
		
		
		
		/* stopsChange function */
		$scope.checkAll = false;
		$scope.filter1 = [];
		/*$scope.stopsChange = function(index){
			console.log(index);
			stopIndex = index;
			$scope.filter = $scope.flightResultList.filterParams.stopsBeanList[index].stopCode;
			//$scope.filter1.push($scope.filter);
			console.log("$scope.filter "+$scope.filter);
		filterDatas = stopsService.getStop($scope.flightResultList,stopIndex, airlineCode);
		
		$rootScope.sCheckId = $scope.filter;
		$scope.flightResultList1 = filterDatas;
			}*/
			
			$scope.stopsObj = {}
			$scope.stopsObj.stopsList = [];
			
				$scope.stopsChange = function(index){  

		$scope.filtermodel.stopsBeanList = [];		 
					for (var s in $scope.stopsObj.stopsList) {		 	   
					$scope.filtermodel.stopsBeanList.push(		   	  	   
							{	   		 	 
							stopCode :$scope.flightResultList.filterParams.stopsBeanList[s].stopCode,
							stopName : $scope.flightResultList.filterParams.stopsBeanList[s].stopName,
							count:$scope.flightResultList.filterParams.stopsBeanList[s].count,
							minFare:$scope.flightResultList.filterParams.stopsBeanList[s].minFare		    	       
							} 
						 );		  
					   
				}   
				
					if($scope.stopsObj.stopsList!=0) 
					$scope.filterParam($scope.filtermodel);  
		
			}
			
			
			
			$scope.airLineObj = {};
			$scope.airLineObj.airLineList = [];
			$scope.stopsLists = $scope.flightResultList.filterParams.stopsBeanList
			$scope.airLineList = $scope.flightResultList.filterParams.airlineBeanList;
			
				 	$scope.toggleStops = function(){
				if(!$scope.checkAll){
					$scope.checkAll = true;
					$scope.stopsObj.stopsList = $scope.stopsLists.map(function(value){
					$scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
						return value.stopName;
						})
				} else {
				$scope.checkAll = false;
					$scope.stopsObj.stopsList = [];
					$scope.flightResultList1 = [];
					}
				}
				/* end */
				
				/* toggleAirline function */
				
				$scope.toggleAirline = function(){
					if(!$scope.checkAll){
					$scope.checkAll = true;
					$scope.airLineObj.airLineList = $scope.airLineList.map(function(value){
					$scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
						return value.airlineName;
						})
				} else {
				$scope.checkAll = false;
					$scope.airLineObj.airLineList = [];
					$scope.flightResultList1 = [];
					}
					}
					
				
				/* $scope.airlineChange = function(index, bool){
					//airlineCode = index;
					$scope.airlineFilter = $scope.flightResultList.filterParams.airlineBeanList[index].airlineCode;
					airlineCode = $scope.airlineFilter;
					$rootScope.airlineCodes = $scope.airlineFilter;
					filterDatas = stopsService.getStop($scope.flightResultList, stopIndex, $scope.airlineFilter)
						$scope.flightResultList1 = filterDatas;
					
						} */
				$scope.airlineChange = function(index){
	 
				$scope.filtermodel.airlineBeanList = [];	
				
				for (var a in $scope.airLineObj.airLineList) {	
				var targetAirLine=$scope.airLineObj.airLineList[a];
				
				for(var i in $scope.flightResultList.filterParams.airlineBeanList){
				if($scope.flightResultList.filterParams.airlineBeanList[i].airlineName==targetAirLine){
					$scope.filtermodel.airlineBeanList.push(	   	  	     
							{	  
							airlineCode :$scope.flightResultList.filterParams.airlineBeanList[i].airlineCode,
							airlineName : $scope.flightResultList.filterParams.airlineBeanList[i].airlineName,
							minFare:$scope.flightResultList.filterParams.airlineBeanList[i].minFare,
							count:$scope.flightResultList.filterParams.airlineBeanList[i].count		
							
							} 	  
						 );		    
							}
				}
				}   
				
					if($scope.airLineObj.airLineList!=0)	   	     
					$scope.filterParam($scope.filtermodel);  
		
			}
	  $scope.checkAllStops=function(){
					$scope.checkAll = true; 
					$scope.stopsObj.stopsList = $scope.stopsLists.map(function(value){
					$scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
						return value.stopName;
						}) 
		} 	
		
		$scope.checkAllAirline= function(){	  

				$scope.checkAll = true;
				$scope.airLineObj.airLineList = $scope.airLineList.map(function(value){
				$scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
					return value.airlineName;
					}) 
		
		}
		$scope.filterParam=function(filtermodel){
					$scope.flightResultList1 = [];
					$http.post(ServerService.serverPath+'rest/flight/filter/apply',filtermodel,{	    
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' 
							}
						}).then(function(response){	 	  
							$scope.flightResultList1 = response.data.data;	
							}) 
				}
		
		
		/* end */
		
				
				function toDate(dStr,format) {	
					var now = new Date();
					if (format == "h:m") {
				 		now.setHours(dStr.substr(0,dStr.indexOf(":")));
				 		now.setMinutes(dStr.substr(dStr.indexOf(":")+1));
				 		now.setSeconds(0);
				 		return now;
					}else 
						return "Invalid Format";
				}




	/**
	support : siri end
	 */
		
			/**
		support:siri pathcs 
	*/	
		$scope.isMin=function(price){
						var min=$scope.flightResultList.filterParams.stopsBeanList[0].minFare;	   
					   for(var i=0;i<= $scope.flightResultList.filterParams.stopsBeanList.length-1;i++){
						 if(Number($scope.flightResultList.filterParams.stopsBeanList[i].minFare) < min){	  
						   min=$scope.flightResultList.filterParams.stopsBeanList[i].minFare;   
						 }
					   }
						if(min===price){
						  return true;
						}else{
						  return false;
						} 
					  }
	 $scope.isairlineMin=function(price){
						var min=$scope.flightResultList.filterParams.airlineBeanList[0].minFare;	   
					   for(var i=0;i<= $scope.flightResultList.filterParams.airlineBeanList.length-1;i++){
						 if(Number($scope.flightResultList.filterParams.airlineBeanList[i].minFare) < min){	  
						   min=$scope.flightResultList.filterParams.airlineBeanList[i].minFare;   
						 }
					   }
						if(min===price){	 
						  return true;
						}else{
						  return false;
						}	
					  }
					  
					  $scope.pricerange = {
				        minValue: $scope.flightResultList.filterParams.priceBean.minPrice,	 
				        maxValue: $scope.flightResultList.filterParams.priceBean.maxPrice,	     		  	        	
				        options: {
				            floor: $scope.flightResultList.filterParams.priceBean.minPrice,	   
				            ceil: $scope.flightResultList.filterParams.priceBean.maxPrice,	   	     	 
				            step: 1, 
				            minRange: 100,	   
				            pushRange: true,
				            showSelectionBar: true,
				            getSelectionBarColor: function (value) {
				                return '#fecb16';
				            },
				            id: 'sliderA',
				            onChange: $scope.priceChange	     
				        }
				    };  
			$("#departure-range").slider({
					    range: true,
					    min: 0,
					    max: 1440,
					    step: 5,
					    values: [0, 1440],	
					    slide: function (e, ui) { 
					    	var arhours1 = Math.floor(ui.values[0] / 60);
							var arseconds1=ui.values[0];
							var arminutes1 = ui.values[0] - (arhours1 * 60);
							if (arhours1.length == 1) arhours1 = '0' + arhours1;
							if (arminutes1.length == 1) arminutes1 = '0' + arminutes1;
							if (arminutes1 >= 0 && arminutes1 <=9) arminutes1 = '0'+arminutes1;
							if (arhours1 >= 0 && arhours1 <=9) arhours1 = '0'+arhours1;
							arhours1 = arhours1;
							arminutes1 = arminutes1;
					        $('.slider-dpt-time').html(arhours1 + ':' + arminutes1);
					        
					        var arhours2 = Math.floor(ui.values[1] / 60);
							var arseconds2=ui.values[1];
							var arminutes2 = ui.values[1] - (arhours2 * 60);
							if (arhours2.length == 1) arhours2 = '0' + arhours2;
							if (arminutes2.length == 1) arminutes2 = '0' + arminutes2;
							if (arminutes2 >= 0 && arminutes2 <=9) arminutes2 = '0'+arminutes2;
							if (arhours2 >= 0 && arhours2 <=9) arhours2 = '0'+arhours2;
							arhours2 = arhours2;
							arhours2=arhours2;

					        $('.slider-dpt-time2').html(arhours2 + ':' + arhours2);
					        
					        var min = ui.values[0], max = ui.values[1];
					        
							$scope.filtermodel.deptTimeBean = {"isEnabledDept":true,"fromDept":ui.values[0]*60000,"toDept":ui.values[1]*60000};
							if($scope.filtermodel.deptTimeBean.length==0) {
								$scope.loadmore = false; 
								$scope.filtermodel.deptTimeBean = '';
								
								} else {
									$scope.filterParam($scope.filtermodel);     
							}
					    }
					});
		  
				  $("#arrival-range").slider({		    
					    range: true,
					    min: 0,
					    max: 1440,
					    step: 5,
					    values: [0, 1440],	
					    slide: function (e, ui) { 
					    	var arhours1 = Math.floor(ui.values[0] / 60);
							var arseconds1=ui.values[0];
							var arminutes1 = ui.values[0] - (arhours1 * 60);
							if (arhours1.length == 1) arhours1 = '0' + arhours1;
							if (arminutes1.length == 1) arminutes1 = '0' + arminutes1;
							if (arminutes1 >= 0 && arminutes1 <=9) arminutes1 = '0'+arminutes1;
							if (arhours1 >= 0 && arhours1 <=9) arhours1 = '0'+arhours1;
							arhours1 = arhours1;
							arminutes1 = arminutes1;

					        $('.slider-time').html(arhours1 + ':' + arminutes1);

					        var arhours2 = Math.floor(ui.values[1] / 60);
							var arseconds2=ui.values[1];
							var arminutes2 = ui.values[1] - (arhours2 * 60);
							if (arhours2.length == 1) arhours2 = '0' + arhours2;
							if (arminutes2.length == 1) arminutes2 = '0' + arminutes2;
							if (arminutes2 >= 0 && arminutes2 <=9) arminutes2 = '0'+arminutes2;
							if (arhours2 >= 0 && arhours2 <=9) arhours2 = '0'+arhours2;
							arhours2 = arhours2;
							arhours2=arhours2;

					        $('.slider-time2').html(arhours2 + ':' + arhours2);
					        
					        var min = ui.values[0], max = ui.values[1];
					          
							$scope.filtermodel.arrivalTimeBean = {"isEnabledArrival":true,"fromArrival":ui.values[0]*60000,"toArrival":ui.values[1]*60000};
							if($scope.filtermodel.arrivalTimeBean.length==0) {
								$scope.loadmore = false;
								$scope.filtermodel.arrivalTimeBean='';
								} else {
									$scope.filterParam($scope.filtermodel);     
							}
					    }
					});
		  	  
				  $("#price-range").slider({	  		    
					    range: true,
					    min: $scope.flightResultList.filterParams.priceBean.minPrice,	 
				        max: $scope.flightResultList.filterParams.priceBean.maxPrice,	 
					    step: 5,
					    values: [$scope.flightResultList.filterParams.priceBean.minPrice, $scope.flightResultList.filterParams.priceBean.maxPrice],	
					    slide: function (e, ui) { 
					        var min = ui.values[0], max = ui.values[1];
					        $('.slider-price1').html(min);
					        $('.slider-price2').html(max); 	      	  	
					        $scope.filtermodel.priceBean = {"isEnabledPrice":true,"fromRange":ui.values[0],"toRange":ui.values[1]};	 
							if($scope.filtermodel.priceBean.length==0) {
								$scope.loadmore = false;  
								$scope.filtermodel.priceBean = '';	  
								$scope.noflight = true; 
								$rootScope.fResultsLen = 0;
								$('.overlay').show().delay(1000).fadeOut();
								} else {
									$scope.filterParam($scope.filtermodel);     
							}
					    }
					});
		  	  
	/**
	support : siri end
	 */

		/* openFlight function */
	$scope.openFlight = function(size, parentSelector, index){
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedIndex = index;
		/* sessionStorage.setItem('flightIndex',JSON.stringify(index)); */
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'flightDetailedinfoContent.html',
			controller : 'flightDetailedinfoContent',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	/* end */
	
});
ouaApp.controller('flightDetailedinfoContent', function($scope, $uibModalInstance, $rootScope, stopsService){
	$scope.flightDetails = JSON.parse(sessionStorage.getItem('flightTotalData'))
	var value = $rootScope.selectedIndex;
	$scope.flightDetails1 = $scope.flightDetails.data.flightSearchResults[value];
	$scope.flightDetails2 = $scope.flightDetails.data
	//$scope.flightDetails1 = $scope.$parent.flightResultList1
	$scope.filter = $rootScope.sCheckId
	$scope.airlineCode = $rootScope.airlineCodes;
	var flag = false;
	var filterDatas = {};
	if($scope.filter != null && $scope.airlineCode != null ){
	
		filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
	} else if(!flag){
		if($scope.filter != null || $scope.filter != undefined){
			filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
		} else if($scope.airlineCode != null || $scope.airlineCode != undefined){
			filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
			}
		}
	/* if($scope.filter != null || $scope.filter != undefined){
		var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
	} */
	
	$scope.waitingDuration=[];	
	var count=0;
	$scope.getWaitingDuration = function(a,b,ind,x,key){		 
		//$scope.waitingDuration;	      
		if(a!=undefined && b!==undefined ){
			var startDate = new Date(a);   
			var endDate   = new Date(b);
			var d =(endDate.getTime() - startDate.getTime()) / 1000;	
			 d = Number(d);
			    var h = Math.floor(d / 3600);  	  
			    var m = Math.floor(d % 3600 / 60);
			    var hDisplay = h +"h";	    
			    var mDisplay = m +"m";
			    $scope.waitingDuration[parseInt(ind+""+x+""+key)]= hDisplay +" "+ mDisplay;	   	 	  	  	       	                
			
		} 
		
	}		
	
	
	$scope.getTotalDuration =function(a,b){
		$scope.totalDuration  = timeConvert(parseInt(a)+parseInt(b));  		
		
	} 
	
	function timeConvert(n) {
		var num = n;
		var hours = (num / 60);
		var rhours = Math.floor(hours);
		var minutes = (hours - rhours) * 60;
		var rminutes = Math.round(minutes);
		return rhours + "h " + rminutes + " m";	      
		}

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});
function formatNumber(n) {
	return Math.round(n);
}
 /* ouaApp.filter('stopFilter', function(){
	return function(listing, filter){
		var filtered  = [];
		for(var i =0; i<listing.length; i++){
		var tempData = listing[i]
		var oubound = listing[i].outboundFilter.stops
		
			if(oubound == filter){
				filtered.push(tempData);
				}
			//stops = listing; 
			}
		console.log(filtered);
		return filtered 
		}
	}) */
/* ouaApp.service('stopsService', function(){
	this.getStop = function(listing, filter){
		var stops = [];
		var filterlength = listing.flightSearchResults.length
		for(var i =0; i<filterlength; i++){
			var tempData = listing.flightSearchResults[i]
		var oubound = listing.flightSearchResults[i].outboundFilter.stops;
		if(oubound == filter){
			stops.push(tempData);
			}
			}
			console.log(stops);
		return stops 
		}
})	 */

 ouaApp.service('stopsService', function(){
	this.getStop = function(listing, filter, airlineFilter){
		var stops = [];
		var flage=false;
		var filterlength = listing.flightSearchResults.length
		for(var i =0; i<filterlength; i++){
					var tempData = listing.flightSearchResults[i]
					var oubound = listing.flightSearchResults[i].outboundFilter.stops;
					var outBoundAirline = listing.flightSearchResults[i].outboundFilter.airlineCode;
					if(filter!=null&&airlineFilter!=null){
					if(outBoundAirline==airlineFilter&&filter==oubound){
						stops.push(tempData);
						flage=true;
					
					}
					}else{
					if(!flage){
							if(filter==oubound){
								stops.push(tempData);
							}
							else if(outBoundAirline==airlineFilter){
								stops.push(tempData);
							}
					}
					}
		}
			console.log(stops);
		return stops 
		}
		
	
		
}) 	