ouaApp.controller('flightItenaryController', function($scope, $http, ServerService){


	$scope.flightconfirmJson = sessionStorage.getItem("flightconfirmJson");//Retrieve the stored data
	$scope.confirmJson =  JSON.parse($scope.flightconfirmJson);
	if($scope.confirmJson) {
	$scope.fareInfo = $scope.confirmJson.fareInfoLst;
	$scope.discount=$scope.confirmJson.discnt;
	$scope.servicecharge=$scope.confirmJson.servChrg;
	$scope.grandtotal=$scope.confirmJson.grandTot;
	$scope.flightPriceData = sessionStorage.getItem("flightPriceData");//Retrieve the stored data
	$scope.searchPasflights =  JSON.parse($scope.flightPriceData);
	$scope.passengListArray = $scope.confirmJson.psgrLst;
	for(var i=0; i<$scope.passengListArray.length; i++){
		$scope.splSerDataLst = $scope.passengListArray[i].ssrDetailList;
	}
	
	$scope.tripsDetail = function() {
		$('body').addClass("loading");
		$location.path('/user-trips');
		$timeout(function() {
			$('body').removeClass("loading");
		}, 1000);
	};
	
	$scope.printRequest = {};
	$scope.printTkt = function(i){
		sessionStorage.removeItem('hotelBookJson');
		$scope.printRequest.bookingRefNo = $scope.confirmJson.pnrNo;
		$scope.printRequest.serviceType = $scope.confirmJson.serviceOrderType;
		$scope.printRequest.clientRefNo = $scope.confirmJson.clientRefNo;
		$scope.printRequest.type = "DB";
		$http({
			method:'POST',
			data:$scope.printRequest,
			url:ServerService.serverPath+'rest/retrieve/itinerary'
			}).success(function(data,status){
			if(data.status=="SUCCESS") {
				data.data.tIndex = i;
				var jsondata=data.data;
				//sessionStorage.setItem("eTicketFlight",JSON.stringify(jsondata));
				console.log(jsondata.serviceOrderType);
				if(jsondata.serviceOrderType=='FLT'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
				}
				if (jsondata.serviceOrderType=='FNH'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
				}
				window.open("e-ticketFlight.jsf",'_blank');
				} else {
				Lobibox.alert('error',
				{
					msg: data.errorMessage
				});
			}
			
		})
	}
	} else {
		$location.path('/');
	}
});