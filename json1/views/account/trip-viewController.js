ouaApp.controller('userTripsCtrl', function($scope, $http, ServerService, $location, $uibModal){
	console.log("Test");
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	/* $scope.rolename = document.getElementById('rolename').value; */
	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords1 = true;
	$scope.noRecords = false;
	$scope.travelClick = false;
	
	// prepare traveller search request
	
	$scope.travelReq = function(page,count) {
		$scope.userProfileReq.docketNo = null;
		$scope.userProfileReq.paxsName = null;
		$scope.userProfileReq.status = null;
		$scope.userProfileReq.service = null;
		$scope.userProfileReq.docketDate = null;
		$scope.userProfileReq.category = "SELF";
		$scope.userProfileReq.createBy = null;
		$scope.userProfileReq.orgin = null;
		$scope.userProfileReq.destination = null;
		$scope.userProfileReq.gridMode = "child";
		$scope.userProfileReq.searchMode = "grid";
		$scope.userProfileReq.fromIndex = 0;
		$scope.userProfileReq.toIndex = 0;
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	
	$scope.getTraveller = function (params, paramsObj) {
	$scope.loading = true;
		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/docket/search',$scope.userProfileReq).then(function (response) {
			$scope.loading = false;
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.recentBookingData = response.data.data.docketChildList;
				console.log('recentBookingData '+JSON.stringify($scope.recentBookingData))
				$scope.totTrvCount = response.data.data.totalCount;
				//$location.path('/user-trips');
				$location.path('/user');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
			} else {
				$scope.recentBookingData = [];
				$scope.totTrvCount = 0;
				$scope.noRecords = true;
			//	$location.path('/user-trips');
				$location.path('/user');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
			}
		});	
	};
	
	$scope.tripsModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	$scope.viewDocPnr = function(clientRefNo, pnrNumber, prodType){
		$('body').addClass("loading");
		var docPnrRequest = new Object();
		docPnrRequest.clientRefNo = clientRefNo;
		docPnrRequest.bookingRefNo = pnrNumber;
		docPnrRequest.type = 'DB';
		if(prodType=='FLT'){
			docPnrRequest.serviceType = 'FLT';
		}
		if(prodType=='HTL'){
			docPnrRequest.serviceType = 'HTL';
		}
		if(prodType=='INSRNC'){
			docPnrRequest.serviceType = 'INSRNC';
		}
		if(prodType=='VISA'){
			docPnrRequest.serviceType = 'VISA';
		}
		var docPnrRequestJson = JSON.stringify(docPnrRequest);	
		 $http.post(ServerService.serverPath+'rest/retrieve/itinerary', docPnrRequestJson).then(function(response){
			console.log(response +"itinerary");
			if(response.data.status == "SUCCESS"){
				var jsondata= response.data.data;
				if(jsondata.serviceOrderType == 'FLT'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
					$location.path('/flightItenary');
					}
					if(jsondata.serviceOrderType == 'HTL'){
						sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata));
					$location.path('/hotelItenary');
						}
						if(jsondata.serviceOrderType == 'INSRNC'){
						sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insurance-itinerary');
						}
						if(jsondata.serviceOrderType == 'VISA'){
						sessionStorage.setItem("visaItineraryJson",JSON.stringify(jsondata));
					$location.path('/visa-itinerary');
						}
						if(jsondata.serviceOrderType == 'FNH'){
						sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
					$location.path('/flight-hotel-itinerary');
						}
			} else {
			$location.path('/user');
				
				Lobibox.alert('error',
				{
					msg: response.data.errorMessage
				});
			}
			$timeout(function(){
				$('body').removeClass("loading");
				$("html,body").scrollTop(0);
			},1000);
			}) 
		 
	}
	
	$scope.logOut = function(){
	/* sessionStorage.removeItem('loginName');
		sessionStorage.removeItem('userBookingdetails'); */
		sessionStorage.clear();
		}
		$scope.changePwd = function(size, parentElem){
			  var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'changePwd.html',
			controller : 'passwordChangeController',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
			}
});
ouaApp.controller('passwordChangeController', function($scope, $uibModalInstance, $http, ServerService, $timeout){
	$scope.changePasReq = {}
	$scope.userName = JSON.parse(sessionStorage.getItem('loginName'));
	$scope.changePasReq.loginid = $scope.userName.email;
	$scope.close = function(){
		$uibModalInstance.close();
		}
		$scope.reset = function(){
			console.log("passwordChangeController");
			$scope.changePasReq.userid = 0;
			$scope.changePasReq.oldpassword = null;
			$scope.changePasReq.profiledata = null;
			$http.post(ServerService.serverPath+'rest/password/change', $scope.changePasReq, {
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
				console.log("RRRRR "+response);
				
				if(response.data.status == "SUCCESS"){
				$scope.dispsuccMsg = true
					$scope.successMsg = response.data.message
				} else if(response.data.status == "Failure") {
				$scope.dispErrMsg = true
					$scope.errorMsg = response.data.errorMessage
					}
					$timeout(function(){
						$scope.dispsuccMsg = false;
						$scope.dispErrMsg = false;
						$uibModalInstance.dismiss();
						}, 2000)
				})
			
			}
})