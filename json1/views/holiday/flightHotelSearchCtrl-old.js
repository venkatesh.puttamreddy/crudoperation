ouaApp.controller('flightHotelCtrl', function($scope, ConstantService, $uibModal, $http, $location, $interval, $timeout, ServerService, ajaxService, $rootScope){
	$scope.currency = ConstantService.currency1
	//	$scope.passengerCount = 0
	
	$scope.cabinClassList=["Economy", "Premium Economy", "Business", "First"];
	$scope.airlineList=[{title:"All Airlines", code:""}, {title:"Emirates", code:"EK"}, {title:"Qatar Airways", code:"QR"}, {title:"Flydubai", code:"FZ"}];
	$scope.cabinClass = "Economy";
	$scope.airline = {};
	$scope.airline.title = "All Airlines";
	$scope.airline.code = "";
	$scope.airlinee = $scope.airline.code;
	$scope.prefAirlineList = [];
	
	$scope.changeCabinClass = function(value){
		$scope.cabinClass = value;
	}
	
	$scope.changeAirline = function(value){
		$scope.airline.title = value.title;
		$scope.airlinee = value.code;
	}
	
	$rootScope.classData = ['Economy','Business','First'];
	if(sessionStorage.getItem('searchFlightJson')!=undefined && $scope.urlPath=='/flight') {
		$scope.searchFlightObjJson = JSON.parse(sessionStorage.getItem('searchFlightJson'));
		$('#from__place').val($scope.searchFlightObjJson.fromAirport);
		$('#to__place').val($scope.searchFlightObjJson.toAirport);
		var infArr = [];
		for( var k = 0; k<=$scope.searchFlightObjJson.adtCnt; k++) {
			infArr.push(k);
		}
		$scope.fInfantCount = infArr;
		//console.log($scope.searchFlightObjJson);
		} else {
		$scope.searchFlightObjJson = {};
		$scope.searchFlightObjJson.adtCnt = 1;
		$scope.searchFlightObjJson.chldCnt = 0;
		$scope.searchFlightObjJson.infCnt = 0;
		$scope.fInfantCount = [0,1];
		$scope.searchFlightObjJson.classType = $scope.classData[0];
		$scope.searchFlightObjJson.frmAirport = 'MUSCAT, Oman - Seeb (MCT)';
		$("#from__place").val('MUSCAT, Oman - Seeb (MCT)');
	}
	
	$scope.searchFlightObjJson.srchType = 2;
	$scope.searchFlightObjJson.deptTime = "Any Time";
	$scope.searchFlightObjJson.retTime = "Any Time";
	//$scope.searchFlightObjJson.classType = 'Economy';
	//$scope.searchFlightObjJson.prefNonStop = false;
	$scope.searchFlightObjJson.pageIndex = 1;
	$scope.searchFlightObjJson.resultPerPage = 1000;
	
	if(jQuery('#from__place').val()=='') {
		jQuery('#from__place').focus();
	}
	var fromset = false;
	var toset = false;
	var flag = false;
	var flag1 = false;
	var f;
	$( "#from__place" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST','http://localhost:7777/Oua/rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
					}else{
					$( "#from__place" ).val('');
					$( "#from__place" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#from__place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$( "#from__place" ).val($scope.searchFlightObjJson.frmAirport);
			if($( "#to__place" ).val()==$( "#from__place" ).val()) {
				$( "#from__place" ).val("");
				return false;
			}
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
				$scope.emptyError = false;
			},10);
			/*if(flag==false) {
				$('#from__place').val('');
				$('#from__place').focus(0);
				return false;
			}*/
		},
		//autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			if($( "#from__place" ).val()==$( "#to__place" ).val()) {
				$( "#from__place" ).val('');
				$('.destcheck').show().fadeOut(5000);
				} else {
				$( "#from__place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.searchFlightObjJson.frmAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$('#to__place').focus(0);
				flag = true;
			}
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function(event, ui) {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.searchFlightObjJson.frmAirport = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	$( "#to__place" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST','http://localhost:7777/Oua/rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
					}else{
					$( "#to__place" ).val('');
					$( "#to__place" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#to__place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$scope.$apply();
			$( "#to__place" ).val($scope.searchFlightObjJson.toAirport);
			if($( "#to__place" ).val()==$( "#from__place" ).val()) {
				$( "#to__place" ).val("");
				$('.destcheck').show().fadeOut(10000);
				return false;
			}
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
				$scope.emptyError = false;
			},10);
			/*if(flag1==false) {
				$('#to__place').val('');
				$('#to__place').focus(0);
				return false;
			}*/
		},
		//autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			if($( "#from__place" ).val()==$( "#to__place" ).val()) {
				$( "#to__place" ).val('');
				$('.destcheck').show().fadeOut(10000);
				} else {
				flag1 = true;
				$( "#to__place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.searchFlightObjJson.toAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$('#datepicker3').focus();
			}
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.searchFlightObjJson.toAirport = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	
	/* if(sessionStorage.getItem('hotelSearchObj')!=undefined) {
		$timeout(function(){
			$scope.hotelSearchObj = JSON.parse(sessionStorage.getItem('hotelSearchObj'));
		},300);
	} else {
		$scope.hotelSearchObj = {};
		$scope.hotelSearchObj.roomBean = [{adultCount:1,childCount:0,childrens:[{childBeans:[]}]}];
	}
	var hotser = false;
	jQuery('#searchCity').keyup(function(){
		if(hotser== true) {
			$(this).val('');
			hotser = false;
		}
		if(jQuery(this).val().length==3) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/hotel/cities','{ "queryString" : "'+jQuery(this).val()+'"}','json')
			.then(function(data,status) {
				var availableTags = [];
				for(var i=0; i< data.length; i++) {
					if(data[i].city.toLowerCase()==jQuery('#searchCity').val().toLowerCase())
					{
						availableTags.push(data[i].city+', '+data[i].country);
					}
				}
				for(var j=0; j< data.length; j++) {
					if(data[j].city.toLowerCase()!=jQuery('#searchCity').val().toLowerCase())
					{	
						if(data[j].city.toLowerCase().search(jQuery('#searchCity').val().toLowerCase())==0) {
							availableTags.push(data[j].city+', '+data[j].country);
						}
						
					}
				}
				for(var k=0; k< data.length; k++) {
					if(data[k].city.toLowerCase()!=jQuery('#searchCity').val().toLowerCase())
					{	
						if(data[k].city.toLowerCase().search(jQuery('#searchCity').val().toLowerCase())!=0) {
							availableTags.push(data[k].city+', '+data[k].country);
						}
						
					}
				}
				jQuery( "#searchCity" ).autocomplete({
					source: availableTags,
					select: function(event, ui) {
						hotser = true;
						reminder=ui.item.value;
						jQuery('#datepicker').focus(0);
					},
					autoFocus: true,
					focus: function (event, ui) {
						event.preventDefault();
					}
				});
				var e = jQuery.Event("keydown");
				e.keyCode = 50;                     
				$("#searchCity").trigger(e);
				
			});
		} 
	}); */
	
	/* $scope.states = [];
	$scope.flightfromAutoComplet=function(value) {
	if(value!=null){
  if(value.length >= 2){
  var place = {'queryString': value}
  $http.post(ServerService.serverPath+'rest/repo/airport','{"queryString" : "'+value+'"}', 
  {
   headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
   }
   }).then(function(response){
  
   if(response.data!='null'&&response.data.length>0){
   $scope.states = []
   for(var i=0; i<response.data.length; i++){
     var airfrom= {
                      Code: response.data[i].code,
       Title: response.data[i].city+","+response.data[i].country+"-"+response.data[i].name+"("+response.data[i].code+")",
                      
     }
     $scope.states.push(airfrom);
     
    }
    $scope.states.sort();
  }
  })
 }
 }
}; */
$scope.city = []
$scope.cityAutoComplet = function(totalcitys) {
	 console.log("HotelInCity"+ totalcitys);
	if(totalcitys.length==2){
	   console.log("HotelInCity"+ totalcitys);
		$http.post(ServerService.serverPath+'rest/hotel/cities','{"queryString" : "'+totalcitys+'"}', 
			{
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).then(function(response){
			/* $scope.hotelstates = JSON.stringify(response.data); */
			for(var i=0; i<response.data.length; i++){
					var hotelcity=	{
                        Title: response.data[i].city+","+response.data[i].country,
                      Code: response.data[i].code
					}
					$scope.city.push(hotelcity);
				}
			})
}
} 
	
	
    
    var intitialDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D/MM/YYYY"
		}
	};
    
    $scope.date = intitialDate;
	
	$scope.hoteldate = {
		startDate: null,
		endDate: null
	}
	
	/* search function */
	$scope.flightRequest = {}
	$scope.hotelRequest = {}
	$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
	/* if($scope.roombean != null){
		$scope.passengerCount = $scope.roombean.length	
	} */
	
	$scope.search = function(){
		
	}
	/* end */
	
	/* holidaySearch function */
	
	$scope.holidaySearch = function(valid){
	$scope.submitted = true;
		
		if(valid){
			var fromPlace = $scope.searchFlightObjJson.frmAirport;
		var toPlace = $scope.searchFlightObjJson.toAirport;
		var flightDate1 = $scope.date;
		var fromFlightDate = flightDate1.startDate.format('D/MM/YYYY')
		var toFlightDate = flightDate1.endDate.format('D/MM/YYYY')
		
		/* var flightDate = $('#daterange1').val();
		var fromFlightDate = flightDate.split('-')[0];
		var toFlightDate = flightDate.split('-')[1]; */
		
		var hotelDate = $('#hoteldaterange').val();
		var hotelCheckIn = hotelDate.split('-')[0];
		var hotelCheckOut = hotelDate.split('-')[1];
		
		var fromHotelPlace = $scope.fromHotel.Title;
		var country = fromHotelPlace.split(',')[1];
		var city = fromHotelPlace.split(',')[0];
		
		var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))
		$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
		$scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
		$scope.childCount = JSON.parse(sessionStorage.getItem('child'));
		$scope.adultCount = JSON.parse(sessionStorage.getItem('adult'));
		
		if($scope.infantCount == null || $scope.infantCount == undefined){
			$scope.infantCount = 0;
			} else {
			$scope.infantCount = $scope.infantCount;
		}
		
		$scope.flightRequest.frmAirport = fromPlace;
		$scope.flightRequest.toAirport = toPlace;
		$scope.flightRequest.adtCnt = $scope.adultCount;
		$scope.flightRequest.chldCnt = $scope.childCount;
		$scope.flightRequest.strDeptDate = fromFlightDate;
		$scope.flightRequest.strRetDate = toFlightDate;
		$scope.flightRequest.binNumber = '552107';
		
		$scope.flightRequest.classType = $scope.cabinClass;
			if($scope.airlinee != ""){
			$scope.prefAirlineList.push($scope.airlinee);
			}
			$scope.flightRequest.prefAirlineLst = $scope.prefAirlineList;
		$scope.flightRequest.deptTime = 'Any Time';
		$scope.flightRequest.retTime = 'Any Time';
		$scope.flightRequest.infCnt = $scope.infantCount;
		$scope.flightRequest.onlyFlight = false;
		$scope.flightRequest.pageIndex = 1;
		$scope.flightRequest.prefNonStop = false;
		$scope.flightRequest.resultPerPage = 1000;
		$scope.flightRequest.srchType = 2;
		$scope.hotelRequest.checkIn = hotelCheckIn;
		$scope.hotelRequest.checkOut = hotelCheckOut;
		$scope.hotelRequest.place = fromHotelPlace;
		$scope.hotelRequest.country = country;
		$scope.hotelRequest.city = city;
		$scope.hotelRequest.currency = $scope.currency;
		$scope.hotelRequest.htlName = null;
		$scope.hotelRequest.searchMode = 'search';
		$scope.hotelRequest.starRating = 0;
		
		$scope.hotelRequest.roomBean = $scope.roombean;
		$scope.hotelRequest.traveller = $scope.roombean.length
		
		$scope.hotelRequest.binNumber = '552107';
		sessionStorage.setItem('hotelPlusRequest', JSON.stringify($scope.hotelRequest));
		sessionStorage.setItem('flightPlusRequest', JSON.stringify($scope.flightRequest));
		$location.path('/searchingHoliday');
			}
		}
	
	
	/* open modal function */
	
	$scope.animationEnabled = true;
	$scope.open = function(size, parentSelector){
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'modalContent.html',
			controller : 'modalContentCtrl',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
	}
	/* end */
	
});

ouaApp.controller('modalContentCtrl', function($uibModalInstance, $scope){
	$scope.adults = [1, 2, 3, 4, 5];
	$scope.childs = [0,1, 2];
	$scope.childrenAge = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,11];
	$scope.rooms = [1,2,3,4,5,6];
	
	/* roomChange function */
	$scope.roomChange = function(values){
		$scope.roomCount = [];
		for(var r=0; r<values; r++){
			$scope.roomCount.push(values);
		}
		
	}
	/* end */
	
	/* childAge function */
	$scope.childAge = function(value){
		$scope.child = [];
		for(var c=0; c<value; c++){
			$scope.child.push(value);
		}
	} 
	/* end */
	$scope.submitted = false;
	$scope.passengers = function(valid){
	$scope.submitted = true;
	if(!valid){
		$scope.flightChild = 0; $scope.flightInfant = 0; $scope.adultNo = 0;
		var roomBean = new Array();
		$('.passenger').each(function(index){
			jQuery(this).attr('id', 'passenger_'+(index+1));
		});
		$('.roomlist').each(function(ind){
			
			var id = jQuery(this).attr('id');
			var roomBeanObject = new Object();
			roomBeanObject.index = parseInt(ind)+1;
			var roomId = id.split('_');
			//if(roomId[0] == 'passenger'){
			console.log('passenger_passenger_passenger_')
			roomBeanObject.adultCount = parseInt($(this).find('.adult').val());
			$scope.adultNo += roomBeanObject.adultCount
			sessionStorage.setItem('adult',JSON.stringify($scope.adultNo));
			roomBeanObject.childCount = parseInt($(this).find('.child').val());
			roomBeanObject.cots = 0;
			
			if(roomBeanObject.childCount > 0){
				roomBeanObject.withChildrens=true;
				var childrens = new Object();
				childrens.index = 0;
				childrens.childCount = roomBeanObject.childCount;
				var child_beans = new Array();
				jQuery(this).find('.child-age').each(function(){
					var child_object = new Object();
					child_object.name = null;
					child_object.age = $(this).find('.age').val();
					child_beans.push(child_object);
					if(child_object.age == 1){
						$scope.flightInfant += 1
						sessionStorage.setItem('infant',JSON.stringify($scope.flightInfant));
						
						} else {
						$scope.flightChild += 1
						sessionStorage.setItem('child',JSON.stringify($scope.flightChild));
					}
				})
				childrens.childBeans = child_beans;
				var children_beans = new Array();
				children_beans.push(childrens);
				roomBeanObject.childrens = children_beans
				} else {
				roomBeanObject.withChildrens=false;
				roomBeanObject.childrens = null
			}
		//}
		switch(roomBeanObject.adultCount){
			case 1:
			roomBeanObject.roomDesc='Single';
			break;
			case 2:
			roomBeanObject.roomDesc='Double';
			break;
			case 3:
			roomBeanObject.roomDesc='Triple';
			break;
			case 4:
			roomBeanObject.roomDesc='Quad';
			break;
		}
		roomBeanObject.xtraBed=false;
		roomBeanObject.roomCount = roomId[1]
		
		if(roomBeanObject.childCount == null || roomBeanObject.childCount == undefined || roomBeanObject.childCount == NaN){
			roomBeanObject.childCount = 0;
			}
		$scope.$parent.passengerCount = parseInt(roomBeanObject.adultCount) + parseInt(roomBeanObject.childCount)
		
		$scope.roombeanList = roomBean.push(roomBeanObject);
		sessionStorage.setItem('roombean', JSON.stringify(roomBean));
	});
	$uibModalInstance.close();
		}
		
}
$scope.cancel = function(){
	$uibModalInstance.dismiss('cancel');
}

})