ouaApp.controller('hotelReviewController', function ($scope, $http, ConstantService, $interval, Month, Year, ServerService, $location, $timeout, NationService) {
	$('html, body').scrollTop(0);
	var nation = NationService.nationGet();
	$scope.repriceResp = JSON.parse(sessionStorage.getItem('repriceResp'));
	$scope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp'));
	$scope.selectedHotelRooms = JSON.parse(sessionStorage.getItem('selectedHotels'));
	$scope.selectedHotelResp1 = $scope.selectedHotelResp.searchResults[0];
	$scope.starRating=$scope.selectedHotelResp1.starRating;
	$scope.currency = ConstantService.currency;
	$scope.user=JSON.stringify(sessionStorage.getItem("user"));
	if($scope.user ==undefined){
	$scope.user={};	
	}
	$scope.nights = parseInt($scope.selectedHotelResp.checkOutDate) - parseInt($scope.selectedHotelResp.checkInDate);
	$scope.HotelroomIndex = JSON.parse(sessionStorage.getItem('HotelroomIndex'));
	var parentIndex = $scope.HotelroomIndex.parentIndex;
	var childIndex = $scope.HotelroomIndex.childIndex;
	var totalFare=0.00;
	$scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
	$scope.nationality = JSON.stringify($scope.nationality1);
	$scope.salutations=["Mr","Mrs","Miss"];
	var personaldata=function(){
									this.gender= "M";
     								this.salutation="Mr";
    								this.fname= '';
    								this.lname= '';
     								this.dob= '';
      								this.mobcntrycode= 0;
      								this.mobile='';
      								this.email= '';
      								this.nationalityname= '';
      								this.passportNo= null;
							}
	var traveller = function(){
		 this.personaldata=new personaldata();
		 this.agntid= 0;
	     this.profileid= 0;
	     this.travellerRefRPH= 'A1';
	}

	var travellerByRoom=function(){
		this.adultTravellersList=[];
		this.childTravellersList=[];
	}
	
	$scope.month = Month;
	$scope.year = Year;
	/* signIn function */
	$scope.showSignIn = false;
	$scope.travellersListByRoom=JSON.parse(sessionStorage.getItem("travellersListByRoom"));
	if($scope.travellersListByRoom==undefined){
		$scope.travellersListByRoom = [];
			for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
				var travellerByRoomObj=new travellerByRoom();
				for(j=0;j<$scope.selectedHotelResp1.occGroups[i].roomGrpBean.adultCount;j++){
					var travellerObj = new traveller();
					travellerByRoomObj.adultTravellersList.push(travellerObj);
				}
				for(j=0;j<$scope.selectedHotelResp1.occGroups[i].roomGrpBean.childCount;j++){
					var travellerObj = new traveller();
					travellerByRoomObj.childTravellersList.push(travellerObj);
				}
				$scope.travellersListByRoom.push(travellerByRoomObj);
			}
			console.log(JSON.stringify($scope.travellersListByRoom));
	}
			for (i=0;i<$scope.selectedHotelRooms.length;i++){
					totalFare=totalFare+parseInt($scope.selectedHotelRooms[i].rawRoomFare);
				}				
			$scope.repriceResp.repriceResponse.totalFare=totalFare;
		
	/**
	@author Raghava Muramreddy
	to showing payment error message
	*/
	if(sessionStorage.getItem("paymentError")){
		var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
			Lobibox.alert('error',
			{
				msg: paymentError.errorMessage,
				callback: function(lobibox, type){
					if (type === 'ok'){
						sessionStorage.removeItem("paymentError");
					}
				}
			});
		
	}
	$scope.signIn = function(){
		$scope.showSignIn = $scope.showSignIn ? false : true;
		}
	$scope.goBack=function(path){
		if(path=="/hotelResult"){
			sessionStorage.removeItem("selectedHotels");
			sessionStorage.removeItem("selectedHotelRoomsIndex");
			sessionStorage.removeItem("repriceResp");
		}
		$location.path(path);
	}
	/* end */
	/* multiple date */
	var depDate = $scope.selectedHotelResp.checkInDate;
		var depDateSplt = depDate.split('/');
		var minDepDate = new Date(depDateSplt[1]+'/'+depDateSplt[0]+'/'+depDateSplt[2]);
		var dt = daysBetween(new Date(),minDepDate)-1;
		var chldMinDate = daysBetween(new Date(),minDepDate)+2;
		
		
			$timeout(function() {
			$('.adultTrvlr').each(function(index){
				jQuery(this).attr('id','adultTrvlr_'+(index+1));
			});
			$('.childTrvlr').each(function(index){
				jQuery(this).attr('id','childTrvlr_'+(index+1));
			});
			$('.infantTrvlr').each(function(index){
				jQuery(this).attr('id','infantTrvlr_'+(index+1));
			});
			$('.dateOfBirth').each(function(index){
				jQuery(this).attr('id','dateOfBirth'+(index+1));
			});
			$('.cdatepicker').each(function(index){
				jQuery(this).attr('id','cdatepicker'+(index+1));
			});
			$('.idatepicker').each(function(index){
				jQuery(this).attr('id','idatepicker'+(index+1));
			});
			
			$('[id*=dateOfBirth]').daterangepicker({ 
			locale: {
					format: 'DD/MM/YYYY'
				},
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
	$scope.age=years;
	//getAge(jQuery(this).val(), 3, $scope.selectedHotelResp.checkInDate);
  jQuery(this).attr('data-age',years);
			});
			
			$('[id*=cdateOfBirth]').daterangepicker({ 
				locale: {
					format: 'DD/MM/YYYY'
				},
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   $scope.age=years;
  // getAge(jQuery(this).val(), 3, $scope.selectedHotelResp.checkInDate);
	jQuery(this).attr('data-age',years);
			});
			
			
			
			}, 2000 );
		
		
	/* end */
		
		/* $('[id*=adateOfBirth]').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'dd/mm/yyyy',
		minYear: 1901,
		maxYear: parseInt(moment(),10)
		}, function(start, end, label) {
		var years = moment().diff(start, 'years');
		alert("You are " + years + " years old!");
		})
		}, 2000)   */ 
		
	/* end */
		/* payNow function */
		
	$scope.payment = {}
	$scope.message = false;
	$scope.showCard = false;
	$scope.payNow = function(){
		var cardId = $('#cardNo').val();
		var firstDigit = cardId.charAt(0)
		var secondDigit = cardId.charAt(1)
		if(firstDigit == 3 && between(secondDigit,4,7) && cardId.length == 5){
			$scope.path = 'american';
			$scope.message = false;
			$scope.showCard = true;
			} else if(firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length ==5){
			$scope.path = 'dinners';
			$scope.message = false;
			$scope.showCard = true;
			} else if(((firstDigit == 5 && between(secondDigit,1,5)) || (firstDigit == 2 && between(secondDigit,2,7)))&&cardId.length== 5){
			$scope.path = 'master';
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 5 || firstDigit == 6) && (cardId.length == 5) ){
			$scope.path = "metro";
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 4) && (cardId.length == 5) ){
			$scope.path = 'visa';
			$scope.message = false;
			$scope.showCard = true;
			} else if(cardId.length == 0){
			$scope.message = true;
			$scope.showCard = false;
		}
	}
	/* end */
	
	/* payment function*/
	$scope.paymentDetail = {}
	var expireDate = {}
	$scope.selectDoc = {}
	$scope.payment = function(bookingWay, valid){
	expireDate = $scope.paymentDetail.expireYear+$scope.paymentDetail.expireMonth
	$scope.submitted = true;
	var age = $(this).find('.adateOfBirth').attr('data-age');
	
	var cardId = $('#cardNo').val();
	if(cardId.length!=16){
		Lobibox.alert('error',
								{
									msg: "Please enter 16 digit card number"
							});
		 $('#cardNo').focus();
		valid=false;
	}
	
	if(valid){
		$scope.travellerdatalist = [];
		$('.adultTrvlr').each(function(index){
			jQuery(this).attr('id', 'adultTrvlr_'+(index+1))
			});
			$('.childTrvlr').each(function(index){
			jQuery(this).attr('id', 'childTrvlr_'+(index+1))
			});
			for(i=0;i<$scope.travellersListByRoom.length;i++){
				for(j=0;j<$scope.travellersListByRoom[i].adultTravellersList.length;j++){
					var dob = moment($scope.travellersListByRoom[i].adultTravellersList[j].personaldata.dob,"DD/MM/YYYY"); // another date
					var dep =moment(depDate,"DD-MMM-YYYY");
					var age= Math.floor(moment.duration(dep.diff(dob)).asYears());
					if($scope.travellersListByRoom[i].adultTravellersList[j].personaldata.salutation != "Mr"){
						$scope.travellersListByRoom[i].adultTravellersList[j].personaldata.gender="F";
					}
					if(age<18){
						Lobibox.alert('error',
								{
									msg: "Adult age should be greater than 18"
							});
						return;
					}
					
				}
				for(j=0;j<$scope.travellersListByRoom[i].childTravellersList.length;j++){
					var dob = moment($scope.travellersListByRoom[i].childTravellersList[j].personaldata.dob,"DD/MM/YYYY"); // another date
					var dep =moment(depDate,"DD-MMM-YYYY");
					var age= Math.floor(moment.duration(dep.diff(dob)).asYears());
						if($scope.travellersListByRoom[i].childTravellersList[j].personaldata.salutation != "Mr"){
						$scope.travellersListByRoom[i].childTravellersList[j].personaldata.gender="F";
					}
					if(age>=18){
						Lobibox.alert('error',
								{
									msg: "Child age should be less than 18 at the time of checkIn"
							});
						return;
					}					
					
				}
			}
			
			/*$('.traveller').each(function(ind){
				
				var id = $(this).attr('id');
				var passId = id.split('_');
				console.log("Test "+id);
				if(passId[0] == 'adultTrvlr'){
					var salutationVal = $(this).find('#asalutation_'+ind).val();
					//var salutationVal = $('#asalutation_'+ind).val();
				var fname = $(this).find('#afname_'+ind).val();
				var lnameVal = $(this).find('#alname_'+ind).val();
				var datepickerVal = $(this).find('.adateOfBirth').val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('#acontact_'+ind).val();
				var emailid = $(this).find('#amail_'+ind).val();
				var nationalityVal = $(this).find('#anationalityVal_'+ind).val();
				var age = $(this).find('.adateOfBirth').attr('data-age');
				var passport = $(this).find('#apassport_'+ind).val();
				} else if(passId[0] == 'childTrvlr'){
					var salutationVal = $(this).find('.csalutation').val();
				var fname = $(this).find('.cfname').val();
				var lnameVal = $(this).find('.clname').val();
				var datepickerVal = $(this).find('.cdateOfBirth').val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('.ccontact').val();
				var emailid = $(this).find('.cmail').val();
				var nationalityVal = $(this).find('.cnationalityVal').val();
				var passport = $(this).find('.cpassport').val();
					}
				
					var personalObj = new Object();
					var travellerdataObj = new Object();
					if(salutationVal == 'Mr'){
					personalObj.gender = 'M'
				} else {
				personalObj.gender = 'F'
				}
				
				personalObj.salutation = salutationVal;
				personalObj.fname = fname;
				personalObj.lname = lnameVal;
				personalObj.dob = datepickerVal;
				personalObj.mobcntrycode = c_codeVal;
				personalObj.mobile = c_noVal;
				personalObj.email = emailid;
				personalObj.nationalityname = nationalityVal;
				personalObj.passportNo = passport;
				
				travellerdataObj.personaldata = personalObj;
				travellerdataObj.agntid = 0;
				travellerdataObj.profileid = "-0";
				travellerdataObj.travellerRefRPH = "A1";
				$scope.travellerdatalist.push(travellerdataObj)
				});*/
				console.log(JSON.stringify($scope.travellerdatalist));
				var bookingReqObj = new Object();
				if($scope.nonRefundFlag == false){
					bookingReqObj.reqCommand = 3;
				} else if($scope.nonRefundFlag == true){
					bookingReqObj.reqCommand = 1;
			}
			
			bookingReqObj.htlName = $scope.selectedHotelResp1.name;
			bookingReqObj.htlCode = $scope.selectedHotelResp1.htlCode;
			bookingReqObj.zcode = $scope.selectedHotelResp1.zcode;
			bookingReqObj.address = $scope.selectedHotelResp1.address;
			bookingReqObj.city = $scope.selectedHotelResp.city;
			bookingReqObj.cityCode = $scope.selectedHotelResp1.cityCode;
			bookingReqObj.country = $scope.selectedHotelResp.country;
			bookingReqObj.phNo = $scope.selectedHotelResp1.phNo;
			bookingReqObj.htlDesc = $scope.selectedHotelResp1.htlDesc;
			bookingReqObj.htlName = $scope.selectedHotelResp1.starRating;
			bookingReqObj.provider = $scope.selectedHotelResp1.provider;
			bookingReqObj.latitude = $scope.selectedHotelResp1.latitude;
			bookingReqObj.longitude = $scope.selectedHotelResp1.longitude;
			bookingReqObj.currency = $scope.selectedHotelResp.currency;
			bookingReqObj.comments = $scope.selectedHotelResp1.comments;
			bookingReqObj.status = $scope.selectedHotelResp1.status;
			bookingReqObj.checkInTime = $scope.selectedHotelResp1.checkInTime;
			bookingReqObj.checkOutTime = $scope.selectedHotelResp1.checkOutTime;
			bookingReqObj.totalRooms = $scope.selectedHotelResp.totalRooms;
			bookingReqObj.totalPax = $scope.selectedHotelResp.totalPaxs;
			bookingReqObj.providerCur = $scope.selectedHotelResp1.providerCur;
			bookingReqObj.convRate = $scope.selectedHotelResp1.convRate;
			bookingReqObj.grossFare = $scope.repriceResp.repriceResponse.grandTotal;
			//bookingReqObj.location = $scope.selectedHotel.latitude;
			bookingReqObj.checkIn = $scope.selectedHotelResp.checkInDate;
			bookingReqObj.checkOut = $scope.selectedHotelResp.checkOutDate;
			bookingReqObj.offers = $scope.selectedHotelResp1.offer;
			bookingReqObj.offerDesc = $scope.selectedHotelResp1.offerDesc;
			bookingReqObj.offerDis = $scope.selectedHotelResp1.offerDis;
			//bookingReqObj.grossWithOutDiscount = $scope.selectedHotelResp1.grsWithOutDiscount;
			bookingReqObj.nationality = $scope.selectedHotelResp1.nationality;
			bookingReqObj.totalFare = $scope.repriceResp.repriceResponse.totalFare;
			bookingReqObj.duration = $scope.selectedHotelResp1.duration;
			bookingReqObj.servChrg = $scope.selectedHotelResp1.servChrg;
		//	bookingReqObj.rawGrandTotal = $scope.repriceResp.repriceResponse.rawGrandTotal;
			bookingReqObj.discount = $scope.selectedHotelResp1.discount;
			bookingReqObj.markupAmount = $scope.selectedHotelResp1.markupAmount;
			bookingReqObj.appMode = 2;
			
			var roomCategories = new Array();
			for(i=0;i<$scope.selectedHotelRooms.length;i++){
					var roomObj = new Object();
					roomObj.status = $scope.selectedHotelRooms[i].status;
					roomObj.roomRefNo = $scope.selectedHotelRooms[i].roomRefNo;
					roomObj.roomCode = $scope.selectedHotelRooms[i].roomCode;
					roomObj.roomName = $scope.selectedHotelRooms[i].roomName;
					roomObj.roomCheckIn = $scope.selectedHotelRooms[i].roomCheckIn;
					roomObj.roomCheckOut = $scope.selectedHotelRooms[i].roomCheckOut;
					roomObj.roomType = $scope.selectedHotelRooms[i].roomType;
					roomObj.roomTypeChar = $scope.selectedHotelRooms[i].roomTypeChar;
					roomObj.rateBasis = $scope.selectedHotelRooms[i].rateBasis;
					roomObj.rateBasisCode = $scope.selectedHotelRooms[i].rateBasisCode;
					roomObj.mealBoard = $scope.selectedHotelRooms[i].mealBoard;
					roomObj.mealBoardDesc = $scope.selectedHotelRooms[i].mealBoardDesc;
					roomObj.adultCount = $scope.selectedHotelRooms[i].adultCount;
					roomObj.childCount = $scope.selectedHotelRooms[i].childCount;
					roomObj.infantCount = $scope.selectedHotelRooms[i].infantCount;
					roomObj.xtrBeds = $scope.selectedHotelRooms[i].xtrBeds;
					roomObj.roomFare = $scope.selectedHotelRooms[i].roomFare;
					roomObj.servChrg = $scope.selectedHotelRooms[i].servChrg;
					roomObj.discount = $scope.selectedHotelRooms[i].discount;
					roomObj.childAges = $scope.selectedHotelRooms[i].childAges;
					roomObj.cancellationPolicy = $scope.selectedHotelRooms[i].cancellationPolicy;
					if($scope.repriceResp.repriceResponse.roomDetails[i].psngrNamesRequird!=0) {
						roomObj.psngrNamesRequird = $scope.repriceResp.repriceResponse.roomDetails[i].psngrNamesRequird;
					}
			
					var validRoomOccupancy = new Object();
					if ($scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy != null) {					
							validRoomOccupancy.runno = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.runno;
							validRoomOccupancy.roomCode = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.roomCode;
							validRoomOccupancy.adult = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.adult;
							validRoomOccupancy.children = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.children;
							validRoomOccupancy.childrenAges = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.childrenAges;
							validRoomOccupancy.extraBed = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.extraBed;
							validRoomOccupancy.extraBedOccupant = $scope.repriceResp.repriceResponse.roomDetails[i].validRoomOccupancy.extraBedOccupant;
							roomObj.validRoomOccupancy = validRoomOccupancy;
					} 
					else if($scope.selectedHotelRooms[i].validRoomOccupancy != null){										
				
							validRoomOccupancy.runno = $scope.selectedHotelRooms[i].validRoomOccupancy.runno;
							validRoomOccupancy.roomCode = $scope.selectedHotelRooms[i].validRoomOccupancy.roomCode;
							validRoomOccupancy.adult = $scope.selectedHotelRooms[i].validRoomOccupancy.adult;
							validRoomOccupancy.children = $scope.selectedHotelRooms[i].validRoomOccupancy.children;
							validRoomOccupancy.childrenAges = $scope.selectedHotelRooms[i].validRoomOccupancy.childrenAges;
							validRoomOccupancy.extraBed = $scope.selectedHotelRooms[i].validRoomOccupancy.extraBed;
							validRoomOccupancy.extraBedOccupant = $scope.selectedHotelRooms[i].validRoomOccupancy.extraBedOccupant;
							roomObj.validRoomOccupancy = validRoomOccupancy;					
					} 
			
					roomObj.allocations = $scope.repriceResp.repriceResponse.roomDetails[i].allocations;
					roomObj.amendmentPolicy = $scope.selectedHotelRooms[i].amendmentPolicy;
					roomObj.uniqueId = $scope.selectedHotelRooms[i].uniqueId;
					roomObj.alertMsg = $scope.selectedHotelRooms[i].alertMsg;
					roomObj.shrdBdng = $scope.selectedHotelRooms[i].sharedBdng;
					roomObj.grossWithOutDis = $scope.selectedHotelRooms[i].grsWithOutDiscount;
					roomObj.offerCode = $scope.selectedHotelRooms[i].offerCode;
					roomObj.comments = $scope.selectedHotelRooms[i].roomComments;
					roomObj.bkngId = $scope.selectedHotelRooms[i].bkngId;
					roomObj.cancellationChrgCode = $scope.selectedHotelRooms[i].cancellationChrgCode;
					roomObj.priceCode = $scope.selectedHotelRooms[i].priceCode;
					roomObj.offerDis = $scope.selectedHotelRooms[i].offerDiscount;
					roomObj.pymntGrndBy = $scope.selectedHotelRooms[i].paymentGuaranteedBy;
					roomObj.srvChrgType = $scope.selectedHotelRooms[i].servChrgType;
					roomObj.discChrgType = $scope.selectedHotelRooms[i].discChrgType;
					roomObj.markupAmount = $scope.selectedHotelRooms[i].markupAmount;
					roomObj.seqNo = $scope.selectedHotelRooms[i].seqNo;
					if($scope.selectedHotelRooms[i].servChrgKeys!=null) {
						var servChrgKeys = new Array();
						var servChrgKeyLength = $scope.selectedHotelRooms[i].servChrgKeys.length;
						for(var s = 0; s<servChrgKeyLength; s++) {
							servChrgKeys.push($scope.selectedHotelRooms[i].servChrgKeys[s]);
						}
						roomObj.servChrgKeys = servChrgKeys;
					}
					roomObj.paxDtls = $scope.travellersListByRoom[i].adultTravellersList.concat($scope.travellersListByRoom[i].childTravellersList);
								
					roomCategories.push(roomObj);
		}
		bookingReqObj.docketNo = $scope.selectDoc.dockNo;
		bookingReqObj.roomCategories = roomCategories;
		
		/* bookingReqObj.cardNo = $scope.paymentDetail.cardNo;
		bookingReqObj.expireMonth = $scope.paymentDetail.expireMonth;
		bookingReqObj.expireYear = $scope.paymentDetail.expireYear;
		bookingReqObj.cvv = $scope.paymentDetail.cvv;
		bookingReqObj.cardName = $scope.paymentDetail.cardName; */
		
		bookingReqObj.payfortTxnData = {
			"3ds_url": null,
			"access_code": null,
			"amount": null,
			"authorization_code": null,
			"card_bin": null,
			"card_number": $scope.paymentDetail.cardNo,
			"card_security_code": $scope.paymentDetail.cvv,
			"command": null,
			"currency": 'OMR',
			"customer_email": null,
			"customer_ip": null,
			"customer_name": $scope.paymentDetail.cardName,
			"eci": null,
			"expiry_date": expireDate,
			"fort_id": null,
			"language": null,
			"merchant_identifier": null,
			"merchant_reference": null,
			"order_description": null,
			"payment_option": $scope.path,
			"response_code": null,
			"response_message": null,
			"return_url": null,
			"service_command": "TOKENIZATION",
			"device_fingerprint": $('#device_fingerprint').val(),
			"signature": null,
			"status": null,
			"token_name": null,
			"postUrl": null
		};	
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
				sessionStorage.setItem("travellersListByRoom",JSON.stringify($scope.travellersListByRoom)) ;
				sessionStorage.setItem("user",$scope.user);
				$http.post(ServerService.serverPath+'rest/hotel/booking/confirm',JSON.stringify(bookingReqObj),{
					headers: {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
				}).then(function(response){
					if(response.data.status=='SUCCESS'){
					window.location.href=response.data.returnUrl;
			    }else if(response.data.status=='FAILURE') {
							$scope.loading = false;
							$scope.disabled = false;
							Lobibox.alert('error',
								{
									msg: response.data.message
							});
				}else{
						$scope.loading = false;
						$scope.disabled = false;
							Lobibox.alert('error',
						{
							msg: 'Unexpected Error Occurred, Please Try Again Later'
						});
					}
					})
				}
			})
		
		}
		}
	function daysBetween( date1, date2 ) {
			//Get 1 day in milliseconds
			var one_day=1000*60*60*24;
			
			// Convert both dates to milliseconds
			var date1_ms = date1.getTime();
			var date2_ms = date2.getTime();
			
			// Calculate the difference in milliseconds
			var difference_ms = date2_ms - date1_ms;
			
			// Convert back to days and return
			return Math.round(difference_ms/one_day); 
		}
		
		var years;
		function getAge(dateString,dateType,deptDate) {
			//var now = new Date();
			var depdate = deptDate;
			var depdate2exp = depdate.split('/');
			var now = new Date(depdate2exp[2],depdate2exp[1]-1,depdate2exp[0]);
			var today = new Date(now.getYear(),now.getMonth(),now.getDate());
			
			var yearNow = now.getYear();
			var monthNow = now.getMonth();
			var dateNow = now.getDate();
			
			if (dateType == 1)
			var dob = new Date(dateString.substring(0,4),
			dateString.substring(4,6)-1,
			dateString.substring(6,8));
			else if (dateType == 2)
			var dob = new Date(dateString.substring(0,2),
			dateString.substring(2,4)-1,
			dateString.substring(4,6));
			else if (dateType == 3)
			var dob = new Date(dateString.substring(6,10),
			dateString.substring(3,5)-1,
			dateString.substring(0,2));
			else if (dateType == 4)
			var dob = new Date(dateString.substring(6,8),
			dateString.substring(3,5)-1,
			dateString.substring(0,2));
			else
			return '';
			
			var yearDob = dob.getYear();
			var monthDob = dob.getMonth();
			var dateDob = dob.getDate();
			
			var yearAge = yearNow - yearDob;
			
			if (monthNow >= monthDob)
			var monthAge = monthNow - monthDob;
			else {
				yearAge--;
				var monthAge = 12 + monthNow -monthDob;
			}
			
			if (dateNow >= dateDob)
			var dateAge = dateNow - dateDob;
			else {
				monthAge--;
				var dateAge = 31 + dateNow - dateDob;
				
				if (monthAge < 0) {
					monthAge = 11;
					yearAge--;
				}
			}
			years=yearAge;
			//return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days';
		}
	
	/* end */
	
  
    $scope.getNumber = function (num) {
        return new Array(num);
    }
});
