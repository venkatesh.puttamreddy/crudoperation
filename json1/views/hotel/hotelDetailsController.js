ouaApp.controller('hotelDetailsController', function ($scope, $http, $document, ConstantService, $location, renderHTMLFactory, ServerService) {
	$('html, body').scrollTop(0);
	$scope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp'));
	$scope.selectedHotelResp1 = $scope.selectedHotelResp.searchResults[0];
	console.log(JSON.stringify($scope.selectedHotelResp1));
	$scope.selectedHotelRooms = JSON.parse(sessionStorage.getItem('selectedHotels'));
	
	if($scope.selectedHotelRooms==undefined){
		$scope.selectedHotelRooms=[];
	}

	$scope.starRating=$scope.selectedHotelResp1.starRating;
	$scope.currency = ConstantService.currency;
	$scope.renderHTMLFactory = renderHTMLFactory;
	$scope.totalRoomFares=0.00;
	$scope.selectedHotelRoomsIndex=JSON.parse(sessionStorage.getItem('selectedHotelRoomsIndex'));
	if($scope.selectedHotelRoomsIndex==undefined){
		$scope.selectedHotelRoomsIndex=[];
	}
	else{
	for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
				for (j=0;j<$scope.selectedHotelResp1.occGroups[i].roomInfos.length;j++){
					$scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected=false;				
					$scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare=Math.round($scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare);
				}
			}
			$scope.selectedHotelRooms=[];
			for(i=0;i<$scope.selectedHotelRoomsIndex.length;i++){
					$scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].isSelected=true;
					$scope.totalRoomFares=$scope.totalRoomFares+parseInt($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].rawRoomFare);
					$scope.selectedHotelRooms.push($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]]);
				
			}
	}
	/* onRoomSelect function */
	$scope.loading = false;
		$scope.disabled = false;
		var roonList = [];
	var repriceObj = new Object();
	$scope.clsObj = {}
	$scope.goBack=function(path){
		sessionStorage.removeItem("selectedHotels");
		sessionStorage.removeItem("selectedHotelRoomsIndex");
		sessionStorage.removeItem("repriceResp");
		
		$location.path(path);
	}
		var noNight =  parseInt($scope.selectedHotelResp.checkOutDate) -  parseInt($scope.selectedHotelResp.checkInDate)
		$scope.roomFare = [];
		if($scope.selectedHotelRooms.length==0){
			for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
				for (j=0;j<$scope.selectedHotelResp1.occGroups[i].roomInfos.length;j++){
					if(j==0){
						$scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected=true;
						$scope.selectedHotelRooms.push($scope.selectedHotelResp1.occGroups[i].roomInfos[j]);
						$scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare=Math.round($scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare);
						$scope.totalRoomFares=$scope.totalRoomFares+parseInt($scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare);
						$scope.selectedHotelRoomsIndex.push(j);

					}
					else{
						$scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare=Math.round($scope.selectedHotelResp1.occGroups[i].roomInfos[j].rawRoomFare);
						$scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected=false;	
					}
				}
			}
		}

   

	$scope.onRoomSelectList = function(value, item){
		
		 $("input:checked").each(function(){
			console.log("onRoomSelectList "+value+" "+item);
		 });
		
		}
		/* defaultSelect function */
		$scope.defaultSelect = function(){
		
			$('.roomset').each(function(index){
				  $('input[type=radio]:first', this).prop("checked", true) 
				})
			
			}

			$scope.calculateTotal = function(parentIndex,index){
			$scope.totalRoomFares=0.00;	
			$scope.selectedHotelRoomsIndex[parentIndex]=index;
			for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
				for (j=0;j<$scope.selectedHotelResp1.occGroups[i].roomInfos.length;j++){
					$scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected=false;				
				}
			}
			$scope.selectedHotelRooms=[];
			for(i=0;i<$scope.selectedHotelRoomsIndex.length;i++){
					$scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].isSelected=true;
					$scope.totalRoomFares=$scope.totalRoomFares+parseInt($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].rawRoomFare);
					$scope.selectedHotelRooms.push($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]]);
				
			}
			

			/*for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
			for (j=0;j<$scope.selectedHotelResp1.occGroups[i].roomInfos.length;j++){
				if($scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected){
					$scope.selectedHotelRooms.push($scope.selectedHotelResp1.occGroups[i].roomInfos[j]);
					$scope.totalRoomFares=$scope.totalRoomFares+parseInt($scope.selectedHotelResp1.occGroups[i].roomInfos[j].roomFare);
					break;
				}
				
			}
		}*/
			}
		/* end */

		
		$scope.onRoomSelect = function(){
		$scope.loading = true;
		$scope.disabled = true;
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
			for(i=0;i<$scope.selectedHotelRoomsIndex.length;i++){
				child=$scope.selectedHotelRoomsIndex[i];
				parent = i;
				var clsObj = {};
							var cls = $(this).attr('class');
							$scope.clsObj.parentIndex = parent;
				$scope.clsObj.childIndex = child;
				sessionStorage.setItem('HotelroomIndex', JSON.stringify($scope.clsObj));
				repriceObj.city = $scope.selectedHotelResp1.cityCode;
				repriceObj.hotelCode = $scope.selectedHotelResp1.htlCode;
				repriceObj.checkIn = $scope.selectedHotelResp.checkInDate;
				repriceObj.checkOut = $scope.selectedHotelResp.checkOutDate;
				repriceObj.duration = noNight;
				repriceObj.provider = $scope.selectedHotelResp1.provider;
				repriceObj.currency = $scope.selectedHotelResp.currency; 
				repriceObj.convRate = $scope.selectedHotelResp1.convRate; 
				repriceObj.servChrg = $scope.selectedHotelResp1.servChrg; 
				repriceObj.discount = $scope.selectedHotelResp1.discount; 
				repriceObj.markupAmount = $scope.selectedHotelResp1.markupAmount;
				
				var rmdtlsObj = {};
				rmdtlsObj.roomId = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].roomRefNo;
				rmdtlsObj.roomCode = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].roomCode;
				rmdtlsObj.noOfRooms = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.roomCount;
				rmdtlsObj.adultCount = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.adultCount;
				rmdtlsObj.childCount = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childCount+$scope.selectedHotelResp1.occGroups[parent].roomGrpBean.infantCount;
				rmdtlsObj.xtraBed = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].xtraBeds;
				rmdtlsObj.rateBasis = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].rateBasisCode;
				rmdtlsObj.allocations = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].allocations;
				rmdtlsObj.roomFare = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].roomFare;
				rmdtlsObj.cancellationChrgCode = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].cancellationChrgCode;
				
				if($scope.selectedHotelResp1.occGroups[parent].roomInfos[child].servChrgKeys){
					var servChrgKeys = new Array();
					var servChrgKeyLength = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].servChrgKeys.length
					for(var s=0; s<servChrgKeyLength; s++){
						servChrgKeys.push($scope.selectedHotelResp1.occGroups[parent].roomInfos[child].servChrgKeys[i]);
					}
					rmdtlsObj.servChrgKeys = servChrgKeys;
				}
				
				if($scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges!=null){
					var childAges = new Array();
					var childAgeLength;
					if($scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.length){
						childAgeLength = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.split(',').length;
						}else {
						childAgeLength = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.length;
					}
					for(var c=0; c<childAgeLength; c++){
						childAges.push($scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.split(',')[c]);
					}
					rmdtlsObj.childAges = childAges;
				}
				
				roonList.push(rmdtlsObj);
				repriceObj.roomDtls = roonList;
				$scope.repriceObj = repriceObj;
		
			}
			$http.post(ServerService.serverPath+'rest/hotel/reprice',JSON.stringify($scope.repriceObj),{
					headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
				}).then(function(response){
					console.log(response);
					$scope.loading = false;
		$scope.disabled = false;
					if(response.data.status == 'SUCCESS'){
						$scope.repriceResp = response.data.data;
						console.log($scope.repriceResp);
						sessionStorage.setItem('repriceResp',JSON.stringify($scope.repriceResp));
						sessionStorage.setItem('selectedHotels',JSON.stringify($scope.selectedHotelRooms));
						sessionStorage.setItem('selectedHotelRoomsIndex',JSON.stringify($scope.selectedHotelRoomsIndex))	;
						$location.path('/hotelReview');
					} else {
						Lobibox.alert('error', {
							msg: response.data.errorMessage
							})
						}
					
					
					})
			}
			})
		
		
		/* $http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
				
				repriceObj.city = $scope.selectedHotelResp1.cityCode;
				repriceObj.hotelCode = $scope.selectedHotelResp1.htlCode;
				repriceObj.checkIn = $scope.selectedHotelResp.checkInDate;
				repriceObj.checkOut = $scope.selectedHotelResp.checkOutDate;
				repriceObj.duration = noNight;
				repriceObj.provider = $scope.selectedHotelResp1.provider;
				repriceObj.currency = $scope.selectedHotelResp.currency; 
				repriceObj.convRate = $scope.selectedHotelResp1.convRate; 
				repriceObj.servChrg = $scope.selectedHotelResp1.servChrg; 
				repriceObj.discount = $scope.selectedHotelResp1.discount; 
				repriceObj.markupAmount = $scope.selectedHotelResp1.markupAmount;
				
				var rmdtlsObj = {};
				rmdtlsObj.roomId = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].roomRefNo;
				rmdtlsObj.roomCode = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].roomCode;
				rmdtlsObj.noOfRooms = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.roomCount;
				rmdtlsObj.adultCount = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.adultCount;
				rmdtlsObj.childCount = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childCount+$scope.selectedHotelResp1.occGroups[parent].roomGrpBean.infantCount;
				rmdtlsObj.xtraBed = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].xtraBeds;
				rmdtlsObj.rateBasis = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].rateBasisCode;
				rmdtlsObj.allocations = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].allocations;
				rmdtlsObj.roomFare = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].roomFare;
				rmdtlsObj.cancellationChrgCode = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].cancellationChrgCode;
				
				if($scope.selectedHotelResp1.occGroups[parent].roomInfos[child].servChrgKeys){
					var servChrgKeys = new Array();
					var servChrgKeyLength = $scope.selectedHotelResp1.occGroups[parent].roomInfos[child].servChrgKeys.length
					for(var s=0; s<servChrgKeyLength; s++){
						servChrgKeys.push($scope.selectedHotelResp1.occGroups[parent].roomInfos[child].servChrgKeys[i]);
					}
					rmdtlsObj.servChrgKeys = servChrgKeys;
				}
				
				if($scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges!=null){
					var childAges = new Array();
					var childAgeLength;
					if($scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.length){
						childAgeLength = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.split(',').length;
						}else {
						childAgeLength = $scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.length;
					}
					for(var c=0; c<childAgeLength; c++){
						childAges.push($scope.selectedHotelResp1.occGroups[parent].roomGrpBean.childAges.split(',')[c]);
					}
					rmdtlsObj.childAges = childAges;
				}
				
				roonList.push(rmdtlsObj);
				repriceObj.roomDtls = roonList;
				$scope.repriceObj = repriceObj;
				
				$http.post(ServerService.serverPath+'rest/hotel/reprice',JSON.stringify($scope.repriceObj),{
					headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
				}).then(function(response){
					console.log(response);
					$scope.loading = false;
		$scope.disabled = false;
					if(response.data.status == 'SUCCESS'){
						$scope.repriceResp = response.data.data;
						sessionStorage.setItem('repriceResp',JSON.stringify($scope.repriceResp));
						$location.path('/hotelReview');
					} else {
						Lobibox.alert('error', {
							msg: response.data.errorMessage
							})
						}
					
					
					})
				
				}
			}) */
		
		}
	
	/* end */
	
    $scope.myInterval = 5000;
    $scope.thumbnailSize = 5;
    $scope.thumbnailPage = 1;
    $scope.slides = [{
        image: "images/hotel/h1.jpeg"
     }, {
        image: "images/hotel/h2.jpeg"
     }, {
        image: "images/hotel/h3.jpeg"
     }, {
        image: "images/hotel/h4.jpeg"
     }, {
        image: "images/hotel/h5.jpeg"
     }, {
        image: "images/hotel/h6.jpeg"
     }, {
        image: "images/hotel/h7.jpeg"
     }, {
        image: "images/hotel/h8.jpeg"
     }];

    $scope.prevPage = function () {
        if ($scope.thumbnailPage > 1) {
            $scope.thumbnailPage--;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.nextPage = function () {
        if ($scope.thumbnailPage <= Math.floor($scope.slides.length / $scope.thumbnailSize)) {
            $scope.thumbnailPage++;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    $scope.setActive = function (idx) {
        $scope.slides[idx].active = true;
    }

    $scope.reviews = [{
        "comment": "Good in general",
        "rating": "7.8",
        "rateValue": "Very Good",
        "reviewer": "Hisham",
        "location": "Shj"
    }, {
        "comment": "Good in general",
        "rating": "7.2",
        "rateValue": "Very Good",
        "reviewer": "Nijamuddin",
        "location": "Chennai"
    }];

    $scope.roomsLists = [{
        "title": "Luxury Room",
        "img": "images/hotel/13461.jpg",
        "description": ["Luxury Rooms are warm and elegant accommodations boasting views of the city.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "1,888.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "1,935.00"
        }]
    }, {
        "title": "Luxury Room Park View",
        "img": "images/hotel/13462.jpg",
        "description": ["Luxury Rooms are warm and elegant high floor accommodations showcasing views of the park.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "2,165.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "2,211.00"
        }]
    }];
    //hotelRoomsList
    var hotelRoomsList = angular.element(document.getElementById('hotelRoomsList'));
    $scope.toHotelRoomsList = function () {
        $document.scrollTo(hotelRoomsList, 30, 1000);
    }
});
