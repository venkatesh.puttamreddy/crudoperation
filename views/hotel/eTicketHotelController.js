var eTicketModule = angular.module('eTicketHotel', ['ngSanitize']);
eTicketModule.service('ServerService',function($location){
	this.serverPath = $location.absUrl().split('home.jsf')[0];
});
eTicketModule.controller( 'eTicketHotelCtrl' ,function( $scope, $location, $http, $rootScope, $timeout,$sce, ServerService) {
	$scope.eTicketHotelJson = sessionStorage.getItem("eTicketHotelJson");//Retrieve the stored data
	$scope.ticketJson =  JSON.parse($scope.eTicketHotelJson);
	$scope.reference=$scope.ticketJson.itinerary;
	$scope.docket=$scope.ticketJson.docket;
	$scope.phoneNo=$scope.ticketJson.phoneNo;
	$scope.bookingDate=$scope.ticketJson.bkngDate;
	$scope.bkngStatus=$scope.ticketJson.bkngStatus;
	$scope.totalNights=$scope.ticketJson.totalNights;
	$scope.description=$scope.ticketJson.htlDesc;
	$scope.hotelname=$scope.ticketJson.htlName;
	$scope.stars=$scope.ticketJson.htlType;
	$scope.checkIn=$scope.ticketJson.checkInDate;
	$scope.checkOut=$scope.ticketJson.checkOutDate;
	$scope.checkInTime=$scope.ticketJson.checkInTime;
	$scope.checkOutTime=$scope.ticketJson.checkOutTime;
	$scope.address=$scope.ticketJson.address;
	$scope.totalRooms=$scope.ticketJson.totalRooms;
	$scope.grandTotal=$scope.ticketJson.grandTotal;
	$scope.currency=$scope.ticketJson.currency;
	$scope.convert=$scope.ticketJson.convRate;
	$scope.rooms=$scope.ticketJson.roomOccGrps;
	$scope.agent=$scope.ticketJson.agentInfoBean;
	$scope.credit = $scope.agent.creditInfoBean;
	$scope.policyDetails=$scope.ticketJson.roomOccGrps[0].roomInfos;
	$scope.roomItems=$scope.ticketJson.roomOccGrps[0].roomInfos;
	//$scope.roomItems=$scope.ticketJson.roomOccGrps[0].roomInfos[0].roomItems;
	$scope.salutations=$scope.ticketJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.salutation;
	$scope.trvtype1=$scope.ticketJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.trvtype;
	$scope.fname=$scope.ticketJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.fname;
	$scope.lname=$scope.ticketJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.lname;
	
	$scope.capitalizeFirstLetter = function(rName) {
		var t = rName.toLowerCase();
		return t.replace(/(?:^|\s)\S/g, function(a) {
			return a.toUpperCase();
		});
	}
	
}).filter('unique', function () {

  return function (items, filterOn) {

    if (filterOn === false) {
      return items;
    }

    if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
      var hashCheck = {}, newItems = [];

      var extractValueToCompare = function (item) {
        if (angular.isObject(item) && angular.isString(filterOn)) {
          return item[filterOn];
        } else {
          return item;
        }
      };

      angular.forEach(items, function (item) {
        var valueToCheck, isDuplicate = false;

        for (var i = 0; i < newItems.length; i++) {
          if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
            isDuplicate = true;
            break;
          }
        }
        if (!isDuplicate) {
          newItems.push(item);
        }

      });
      items = newItems;
    }
    return items;
  };
});