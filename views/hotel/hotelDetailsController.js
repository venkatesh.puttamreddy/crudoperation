ouaApp.controller('hotelDetailsController', function ($scope,$sce,refreshService, $http,$timeout, $document, ConstantService,$rootScope, $location,$uibModal, renderHTMLFactory, ServerService, customSession) {
	$('html, body').scrollTop(0);
	$scope.promoCodeDiscount=0;
	$scope.sessionId=JSON.parse(sessionStorage.getItem('selectedHotelResp')).sessionId;
	$scope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp')).data;
	$scope.selectedHotelResp1 = $scope.selectedHotelResp;
	$scope.latitude=$scope.selectedHotelResp1.latitude;
    $scope.longitude=$scope.selectedHotelResp1.longitude;
    $scope.hotellocation=$sce.trustAsResourceUrl('https://maps.google.com/maps?q='+  $scope.latitude+','+$scope.longitude+'&z=15&output=embed');
	$scope.selectedHotelRespcombination = $scope.selectedHotelResp.occGroupsForFixedRoomCombination;
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname = $scope.tokenresult.data;
	$scope.checkval = 0;
	$scope.selected = 0;
	
	$scope.artloading = false;
	$scope.myInterval = 5000;


	  $('.carousel').carousel()
	
  $('.carousel').carousel({
	 	interval: 2000
	   })

	   $scope.thambnailChange = function(index){
        $scope.galleryImgIndex = index;
        $timeout(function(){
            $scope.galleryImgIndex = null;
        },1000)
    }
	   $scope.carouselFunction=function(){
        $('.carousel').carousel({
            interval: 2000
          })
		  $(this).addClass('carousel-item active');
		  
    }

	var headersOptions = {
	  'Content-Type': 'application/json',
	  'Authorization': 'Bearer ' + tokenname.authToken
	}
	$scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
        //    test(); 
           $('#load').hide();
        }, 500);
    }
	$scope.backloader=function() {
		$scope.timeload();
		$location.path('/hotelResult');
	}

	$('#myTab a').on('click', function (e) {
		e.preventDefault()
		$(this).tab('show')
	  })
	
	
		if($scope.selectedHotelResp.fixedFormat==true){
			$totalHotalCombination=$scope.selectedHotelResp.hotelRoomOccGroup.combinations;
			$scope.selectedHotelResp.occGroupsForFixedRoomCombination=[];
			for(var r=0; r<$totalHotalCombination.length; r++){
			var hotelRoomOccGrp=[];
			var totalRooms=[];
for(var c=0; c<$totalHotalCombination[r].length; c++){
	var index=$scope.selectedHotelResp.hotelRoomOccGroup.combinations[r][c];
	
	
	 
	 var tempRoomInfo=[];
	 tempRoomInfo.push($scope.selectedHotelResp.hotelRoomInfos[index]);
	
	totalRooms.push({"roomInfos":tempRoomInfo});
}

$scope.selectedHotelResp.occGroupsForFixedRoomCombination.push(totalRooms);
				for(var fare=0; fare<$scope.selectedHotelResp.occGroupsForFixedRoomCombination.length; fare++){
					$scope.combinationfirstRoomFare=[];
					for(var farevalue=0; farevalue<$scope.selectedHotelResp.occGroupsForFixedRoomCombination[0].length; farevalue++){
						$scope.combinationfirstRoomFare.push($scope.selectedHotelResp.occGroupsForFixedRoomCombination[0][farevalue].roomInfos[0]);
					}
				}
				
			}
			$scope.selectedHotelFixedFormatTruedata=$scope.selectedHotelResp.occGroupsForFixedRoomCombination;
			$scope.combinationTotalfare=[];

			
			for(var sd=0; sd<$scope.selectedHotelFixedFormatTruedata.length;sd++) {
				var tempfareVar=0;
				  for(var f=0; f<$scope.selectedHotelFixedFormatTruedata[sd].length;f++) {
				
						  var roomcombofare=$scope.selectedHotelFixedFormatTruedata[sd][f].roomInfos[0].roomFare;
					
					  
					tempfareVar+=roomcombofare;
					 
				  }
			
				   var totalroomAmount=(tempfareVar);
				   $scope.combinationTotalfare.push(totalroomAmount);
				
				  
				  
			  }
			
		}else{
			$scope.fixedFormat=false;
			$totalHotalCombination=$scope.selectedHotelResp.hotelRoomOccGroup.combinations;
			$scope.selectedHotelResp.occGroupsForFixedRoomCombination=[];
			for(var r=0; r<$totalHotalCombination.length; r++){
				
	
			var hotelRoomOccGrp=[];
			var totalRooms=[];
for(var c=0; c<$totalHotalCombination[r].length; c++){
	var index=$scope.selectedHotelResp.hotelRoomOccGroup.combinations[r][c];
	
	
	 
	 var tempRoomInfo=[];
	 tempRoomInfo.push($scope.selectedHotelResp.hotelRoomInfos[index]);
	
	totalRooms.push({"roomInfos":tempRoomInfo});
}

$scope.selectedHotelResp.occGroupsForFixedRoomCombination.push(totalRooms);

			}
			$scope.selectedHotelFixedFormatfalsedata=$scope.selectedHotelResp.occGroupsForFixedRoomCombination;
		
		}

	
	
		setTimeout(function(){
					var totroomfare = 0;
					$( "input:checked" ).each(function(){
						totroomfare += parseFloat($(this).parent('div').prev('div').children('span').text());
						$('.roomtotfare').text(totroomfare.toFixed(0));
					});
				}, 2000);


	if($scope.selectedHotelRooms==undefined){
		$scope.selectedHotelRooms=[];
	}

	$scope.roundUp=function (value, mult, dir) {
					dir = dir || 'nearest';
					mult = mult || 1;
					value = !value ? 0 : Number(value);
					if (dir === 'up') {
						return Math.ceil(value / mult) * mult;
					} else if (dir === 'down') {
						return Math.floor(value / mult) * mult;
					} else {
						return Math.round(value / mult) * mult;
					}
				}
				   
$scope.inprecise_round=function (value, decPlaces) {
  return Math.ceil(value * Math.pow(10, decPlaces)) / Math.pow(10, decPlaces);
}
	
	$scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));


	var formatDateone=$scope.hotelSearch.checkOut;
	var d=formatDateone.split("/");
	var month=d[1];
	var day=d[0];
	var year=d[2];
	var formatDate1= [month,day,year].join('/');
  
    var formatDatetwo= $scope.hotelSearch.checkIn;
	var d2=formatDatetwo.split("/");
	var month=d2[1];
	var day=d2[0];
	var year=d2[2];
	var formatDate2= [month,day,year].join('/');
	const finalCheckInDate = new Date(formatDate1);
	const finalCheckOutDate = new Date(formatDate2);
	const diffTime = Math.abs(finalCheckInDate - finalCheckOutDate);
	const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
	$scope.nights=diffDays;
	
	$scope.starRating=$scope.selectedHotelResp.starRating;
	$scope.currency = $scope.selectedHotelResp.xmlCurrency;
	$scope.renderHTMLFactory = renderHTMLFactory;
	$scope.totalRoomFares=$scope.selectedHotelResp.totalFare;
	$scope.totalRawFares=$scope.selectedHotelResp.rawFare
	$scope.selectedHotelRoomsIndex=JSON.parse(sessionStorage.getItem('selectedHotelRoomsIndex'));
	if($scope.selectedHotelRoomsIndex==undefined){
		$scope.selectedHotelRoomsIndex=[];
	}
	else{
		if($scope.selectedHotelResp.occGroups){
	for (i=0;i<$scope.selectedHotelResp.occGroups.length;i++){
				for (j=0;j<$scope.selectedHotelResp.occGroups[i].roomInfos.length;j++){
					$scope.selectedHotelResp.occGroups[i].roomInfos[j].isSelected=false;				
					$scope.selectedHotelResp.occGroups[i].roomInfos[j].roomFare=$scope.selectedHotelResp.occGroups[i].roomInfos[j].roomFare;
				}
			}
		}
			$scope.selectedHotelRooms=[];
			if($scope.selectedHotelRoomsIndex){
			for(i=0;i<$scope.selectedHotelRoomsIndex.length;i++){
					$scope.selectedHotelResp.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].isSelected=true;
					var tempRoomFare=($scope.selectedHotelResp.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].roomFare);//($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].roomFare);
					if($scope.selectedHotelResp.promoCodeDetails.AmountType!=undefined){
			
						if($scope.selectedHotelResp.promoCodeDetails.AmountType=='PERCENTAGE'){
							
							$scope.totalRoomFares=($scope.totalRoomFares+tempRoomFare)//-parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount/100);
							$scope.promoCodeDiscount=$scope.inprecise_round(($scope.totalRoomFares*parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount/100)),1);
						}else{
							
							$scope.totalRoomFares=$scope.totalRoomFares+tempRoomFare//-parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount);;
							// $scope.promoCodeDiscount=$scope.inprecise_round(($scope.totalRoomFares*parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount)),1);
							$scope.promoCodeDiscount=$scope.inprecise_round(($scope.selectedHotelResp.occGroups.length*parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount)),1);
						}
								
					}else{
						$scope.totalRoomFares=($scope.totalRoomFares+tempRoomFare)//-$scope.selectedHotelResp1.promoCodeDiscount;
						$scope.promoCodeDiscount=$scope.selectedHotelResp.promoCodeDiscount
					}
					// $scope.totalRoomFares=$scope.totalRoomFares+($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].roomFare);
					$scope.selectedHotelRooms.push($scope.selectedHotelResp.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]]);
				
			}
		}
	}

	$scope.checkValue = function(val, size, parentSelector){
		var cancelPolicyInfo=[];
		if(val=="Non Refundable"){
			var tempCancelValue={string:"A non-refundable rate is usually the hotel’s/provider’s best rate. Full payment is needed at the time of booking and if you decide to cancel, it will be a 100% loss, so no refund would be due. As this is a promotional rate, no changes at all can be made to your booking once it is confirmed."};
			cancelPolicyInfo[0]=tempCancelValue;
				
		}else{
			cancelPolicyInfo=val;
		}
			sessionStorage.setItem("hypeMsg", JSON.stringify(cancelPolicyInfo));
		
	
		
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'myHypeMessage.html',
			controller: 'myHypeMessageCtrl',
			backdrop: true,
			size: 'lg',
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}
	
	$scope.checkMsg = function(val){
		if(val.startsWith("If cancelled")){
			return false;
		}else if(val=="Non Refundable"){
			return true;
		}
	}
	
	/* onRoomSelect function */
	$scope.loading = false;
		$scope.disabled = false;
		var roonList = [];
	var repriceObj = new Object();
	$scope.clsObj = {}
	$scope.goBack=function(path){
		sessionStorage.removeItem("selectedHotels");
		sessionStorage.removeItem("selectedHotelRoomsIndex");
		sessionStorage.removeItem("repriceResp");
		
		$location.path(path);
	}
	
	
		var noNight =  parseFloat($scope.selectedHotelResp.checkOutDate) -  parseFloat($scope.selectedHotelResp.checkInDate)
		$scope.roomFare = [];
	

		$scope.roomselect = function(event){
					
			if($scope.provider=='GTA') {
				$scope.checkval = jQuery(event.target).val();
			}
			setTimeout(function(){
				var totroomfare = 0;
				$( "input[type=radio]:checked" ).each(function(){
					totroomfare += parseFloat($(this).parent('div').prev('div').children('span').text());
					$('.roomtotfare').text(totroomfare.toFixed(0));
				
				});
			}, 100);
			
		}

	
		/* defaultSelect function */
		$scope.defaultSelect = function(){
		
			$('.roomset').each(function(index){
				  $('input[type=radio]:first', this).prop("checked", true) 
				})
			
			}
			
			$scope.calculateTotal = function(parentIndex,index){
			$scope.totalRoomFares=0.00;	
			$scope.selectedHotelRoomsIndex[parentIndex]=index;
			for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
				for (j=0;j<$scope.selectedHotelResp1.occGroups[i].roomInfos.length;j++){
					$scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected=false;				
				}
			}
			$scope.selectedHotelRooms=[];
			for(i=0;i<$scope.selectedHotelRoomsIndex.length;i++){
					$scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].isSelected=true;
					var tempRoomFare=($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].roomFare);
					if($scope.selectedHotelResp1.promoCodeDetails.AmountType!=undefined){
				
					if($scope.selectedHotelResp1.promoCodeDetails.AmountType=='PERCENTAGE'){
						// $scope.promoCodeDiscount=parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount/100);
						$scope.totalRoomFares=($scope.totalRoomFares+tempRoomFare);
							$scope.promoCodeDiscount=$scope.inprecise_round(($scope.totalRoomFares*parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount/100)),1);
					}else{
						// $scope.promoCodeDiscount=parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount);
						$scope.totalRoomFares=$scope.totalRoomFares+tempRoomFare;
							// $scope.promoCodeDiscount=$scope.inprecise_round(($scope.totalRoomFares*parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount)),1);
							$scope.promoCodeDiscount=$scope.inprecise_round(($scope.selectedHotelResp1.occGroups.length*parseFloat($scope.selectedHotelResp1.promoCodeDetails.Amount)),1);
					}
							
				}else{
					$scope.totalRoomFares=($scope.totalRoomFares+tempRoomFare)
					$scope.promoCodeDiscount=$scope.selectedHotelResp1.promoCodeDiscount
				}
					$scope.selectedHotelRooms.push($scope.selectedHotelResp1.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]]);
				
			}
			

			/*for (i=0;i<$scope.selectedHotelResp1.occGroups.length;i++){
			for (j=0;j<$scope.selectedHotelResp1.occGroups[i].roomInfos.length;j++){
				if($scope.selectedHotelResp1.occGroups[i].roomInfos[j].isSelected){
					$scope.selectedHotelRooms.push($scope.selectedHotelResp1.occGroups[i].roomInfos[j]);
					$scope.totalRoomFares=$scope.totalRoomFares+parseFloat($scope.selectedHotelResp1.occGroups[i].roomInfos[j].roomFare);
					break;
				}
				
			}
		}*/
			}
		/* end */

/* var roomList = []; */
	$scope.totalRooms=[];
  $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
  var tokenname = $scope.tokenresult.data;
  var headersOptions = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + tokenname.authToken
  }

		
			
  hotelcply =  function(){
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname = $scope.tokenresult.data;
	var headersOptions = {
	  'Content-Type': 'application/json',
	  'Authorization': 'Bearer ' + tokenname.authToken
      }
	  
	  var insurenceReq={
				"sessionId":	 $scope.sessionvalue,
	"resultIndex":$scope.selectedHotelResp1.index,
	"roomDtls":$scope.totalRooms
      }
	  
	  
	  	   $http.post(ServerService.serverPath+'hotel/cpolicy', insurenceReq,{ 
			   
			 	headers: headersOptions
		}).then(function successCallback(response){

     
				console.log(response);
				 $scope.cancellationPolicy = response.data
			
						sessionStorage.setItem('cancellationPolicy',JSON.stringify($scope.cancellationPolicy));
					
						
						
						
						
				
				
		   }, function errorCallback(response) {
			   if(response.status == 403){
				   var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
				   status.then(function(greeting) {
					hotelcply();
				   });
			   }
			 });
  }
	  
  hotelreprice = function(){
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname = $scope.tokenresult.data;
	var headersOptions = {
	  'Content-Type': 'application/json',
	  'Authorization': 'Bearer ' + tokenname.authToken
	}
	var repriceDetails={
		"sessionId":$scope.sessionId,
"roomDtls":$scope.totalRooms
}
	  
	   $http.post(ServerService.serverPath+'hotel/reprice', repriceDetails,{ 
 
			 	headers: headersOptions
		}).then(function successCallback(response){
					$scope.artloading = false;
     $scope.totalRooms=[];
				// console.log(response);
				if(response.data.success){

				 $scope.repricesession = response.data
				 $scope.repriceResp = response.data.data;
						console.log($scope.repriceResp);
						sessionStorage.setItem('repriceResp',JSON.stringify($scope.repriceResp));
						sessionStorage.setItem('repriceRespSessionId',JSON.stringify($scope.repricesession));
						sessionStorage.setItem('selectedHotels',JSON.stringify($scope.selectedHotelRooms));
						
						
						
						
						$location.path('/hotelReview'); 
						
				}else{
					Lobibox.alert('error',
					{
					  msg: response.data.errorMessage
				  });
				}
				
   }, function errorCallback(response) {
	   if(response.status == 403){
		   var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
		   status.then(function(greeting) {
			hotelreprice();
			 }); 
	   }
	 }); 
  }
	$scope.onRoomSelect = function(roomList){
		for(var rlist=0; rlist<roomList.length; rlist++){
			$scope.totalRooms.push(roomList[rlist].roomInfos[0]);
		}
			roominfos=[];
			roominfos.push($scope.totalRooms)
	  hotelcply();
	  $scope.artloading = true;
	  hotelreprice();



			 
	  
	}



	if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
		$scope.loggedvalue = true;
		$rootScope.bookingHide = false;
		$rootScope.guestLogin = true;
		$rootScope.bookingHide1 = false;
		$rootScope.dispName = "";
		sessionStorage.removeItem("guestLoginDatails");
		localStorage.removeItem("loginToken");
		
	}

	
	$scope.roomBook = function(value, item){
		
		$scope.tempArr=[];
		var roomInfo = [];
		jQuery('#jalert_box_cont_infoboxid').hide();
		var clsArray = [];
		$( "input[type=radio]:checked" ).each(function(index){
			var clsObj = {};
			var cls = $(this).attr('class');
			var roomindex = cls.split('_');
			var parentIndex = parseInt(roomindex[1]);
			var rindex = parseInt(roomindex[2]);
			clsObj.parentIndex = parentIndex;
			clsObj.rindex = rindex;
			clsArray.push(clsObj);
			roomInfo.push($scope.selectedHotelFixedFormatfalsedata[parentIndex][rindex].roomInfos[0]);
	
		})
		sessionStorage.setItem("rindexarray",JSON.stringify(clsArray));
		var repriceDeta={
			"sessionId":$scope.sessionId,

"roomDtls":roomInfo
  }
  $http.post(ServerService.serverPath+'hotel/reprice', repriceDeta,{ 
 
	headers: headersOptions
	}).then(function successCallback(response){
	   $scope.artloading = false;
$scope.totalRooms=[];
   // console.log(response);
   if(response.data.data.availableForBook==true){

	$scope.repricesession = response.data
	$scope.repriceResp = response.data.data;
		   console.log($scope.repriceResp);
		   sessionStorage.setItem('repriceResp',JSON.stringify($scope.repriceResp));
		   sessionStorage.setItem('repriceRespSessionId',JSON.stringify($scope.repricesession));
		   sessionStorage.setItem('selectedHotels',JSON.stringify($scope.selectedHotelRooms));
		   
		   
		   
		   
		   $location.path('/hotelReview'); 
		   
   }else{
	   Lobibox.alert('error',
	   {
		 msg: "Rooms Not Available."
	 });
   }
   
}); 


		}

	
	$scope.hotelRate = function (size, parentSelector, roomInfo) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'hotelRate.html',
			controller: 'hotelRateCommentController',
			backdrop: true,
			size: size,
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}
	$scope.hotelInclusions = function (size, parentSelector, roomInfo) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'hotelInclusions.html',
			controller: 'hotelRateCommentController',
			backdrop: true,
			size: size,
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}

	$scope.hotelAmenties = function (size, parentSelector, roomInfo) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'hotelAmenties.html',
			controller: 'hotelRateCommentController',
			backdrop: true,
			size: size,
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}
	
	$scope.hotelcancelationPolicy = function (size, parentSelector, roomInfo) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'hotelCancelation.html',
			controller: 'hotelRateCommentController',
			backdrop: true,
			size: size,
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}

	$scope.mealBoard = function (size, parentSelector, roomInfo) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'hotelmealBoard.html',
			controller: 'hotelRateCommentController',
			backdrop: true,
			size: size,
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}


	$scope.roomDiscription = function (size, parentSelector, roomInfo) {
		var parentElem = parentSelector ?
			angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'roomdiscription.html',
			controller: 'hotelRateCommentController',
			backdrop: true,
			size: size,
			appendTo: parentElem,
			resolve: {
				items: function () {
					return $scope.items
				}
			}
		})
	}
	

	  



	/* end */
	/*ReadMore About Hotel */
	$scope.hotelDetailInfo = function(obj) {
		var currntEl = obj.target.attributes.id.value;
		
		if(jQuery('#'+currntEl).text()=='More Info'){
			jQuery('#'+currntEl).text('Less Info');
			jQuery('#'+currntEl).prev().css('height','auto');
			
			}	else {
			jQuery('#'+currntEl).text('More Info');
			jQuery('#'+currntEl).prev().css('height','80px');
			jQuery('#'+currntEl).prev().css('overflow','hidden');
		}
		
	}
	

	
	$scope.hotelDetailInfoInit = function() {
		// var currntEl = $('#hotelIndex').innerHTML;
		var hotelmoreInfo="#hotelMoreInfo";
		var currntEl = $(hotelmoreInfo).text();
		
		if(currntEl=='More Info'){
			jQuery(hotelmoreInfo).text('Less Info');
			jQuery(hotelmoreInfo).prev().css('height','auto');
			
			}	else {
			jQuery(hotelmoreInfo).text('More Info');
			jQuery(hotelmoreInfo).prev().css('height','80px');
			jQuery(hotelmoreInfo).prev().css('overflow','hidden');
		}
		
	}
	/*ReadMore About Hotel End */
    $scope.myInterval = 5000;
    $scope.thumbnailSize = 5;
    $scope.thumbnailPage = 1;
    $scope.slides = [{
        image: "images/hotel/h1.jpeg"
     }, {
        image: "images/hotel/h2.jpeg"
     }, {
        image: "images/hotel/h3.jpeg"
     }, {
        image: "images/hotel/h4.jpeg"
     }, {
        image: "images/hotel/h5.jpeg"
     }, {
        image: "images/hotel/h6.jpeg"
     }, {
        image: "images/hotel/h7.jpeg"
     }, {
        image: "images/hotel/h8.jpeg"
     }];

    $scope.prevPage = function () {
        if ($scope.thumbnailPage > 1) {
            $scope.thumbnailPage--;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.nextPage = function () {
        if ($scope.thumbnailPage <= Math.floor($scope.slides.length / $scope.thumbnailSize)) {
            $scope.thumbnailPage++;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    $scope.setActive = function (idx) {
        $scope.slides[idx].active = true;
    }

    $scope.reviews = [{
        "comment": "Good in general",
        "rating": "7.8",
        "rateValue": "Very Good",
        "reviewer": "Hisham",
        "location": "Shj"
    }, {
        "comment": "Good in general",
        "rating": "7.2",
        "rateValue": "Very Good",
        "reviewer": "Nijamuddin",
        "location": "Chennai"
    }];

    $scope.roomsLists = [{
        "title": "Luxury Room",
        "img": "images/hotel/13461.jpg",
        "description": ["Luxury Rooms are warm and elegant accommodations boasting views of the city.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "1,888.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "1,935.00"
        }]
    }, {
        "title": "Luxury Room Park View",
        "img": "images/hotel/13462.jpg",
        "description": ["Luxury Rooms are warm and elegant high floor accommodations showcasing views of the park.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "2,165.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "2,211.00"
        }]
    }];
    //hotelRoomsList
    var hotelRoomsList = angular.element(document.getElementById('hotelRoomsList'));
    $scope.toHotelRoomsList = function () {
        $document.scrollTo(hotelRoomsList, 30, 1000);
    }
}).controller('myHypeMessageCtrl', function ($scope, $rootScope, $uibModalInstance) {
	
	$scope.myHypeMsg = JSON.parse(sessionStorage.getItem("hypeMsg"));
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	}
	$scope.removeBR=function(string){
		string=string.replace(". <br/>",".");
		string=string.replace("<br/>",".");
		string=string.replace("</b>","");
		string=string.replace("<b>","");
		return string;

	}
	
});

ouaApp.controller('hotelRateCommentController', function ($scope, $uibModalInstance, $rootScope) {

  $scope.allInclusions=[];
	$scope.rateComment=$rootScope.selectedroomInfo.rateComments;
	$scope.hotelwisedata=$rootScope.selectedroomInfo;
	$scope.cancellationPolicy=$rootScope.selectedroomInfo.cancellationPolicy;
	$scope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp')).data;

	if(	$scope.rateComment.length>0){
		// $scope.rateComments=$scope.rateComment.replace( /(<([^>]+)>)/ig, '');
		$scope.rateComments=$scope.rateComment;
	}else{
		$scope.rateComments="No Rate Comments";
	}


	
	
	$scope.cancel = function () {
	  $uibModalInstance.dismiss('cancel');
	};
  });
