ouaApp.controller('hotelSearchCtrl', function($scope, $rootScope,refreshService, $uibModal, $http, $location, ajaxService, ServerService) {
    $scope.loading = false;
    $scope.disabled = false;
    $("#load").show();
    setTimeout(function () {
       $('#load').hide();
    }, 1000);

    $scope.hotelSearch = {};
    $scope.hotelSearch.roomBean = [{ adultCount: 1, childCount: 0, childrens: [{ childBeans: [] }] }];
    $scope.room = 1;
    $scope.roomCount = [1, 2, 3, 4, 5, 6];
    $scope.city = [];

    $("#checkIn").keypress(function(event) {event.preventDefault();});
    $("#checkOut").keypress(function(event) {event.preventDefault();});
    

    var cityMap = null;

    $scope.flightop = {
        opens: "left",
        singleDatePicker: true,
        autoApply: true,
        autoUpdateInput: true,
        minDate: new Date(),

        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                // $scope.flightop = {
                //     singleDatePicker: true //$scope.checkboxModel.value
                // }
            },
            'hide.daterangepicker': function(ev, picker) {
                $scope.checkoutdate = {
                    minDate: $scope.hotelSearch.checkIn,
                }
                if($scope.hotelSearch.checkOut != "" && new Date($scope.hotelSearch.checkIn) > new Date($scope.hotelSearch.checkOut)){
                    $scope.hotelSearch.checkOut = "";
                    $scope.hotelSearch.checkOut = new Date($scope.hotelSearch.checkIn).setDate(new Date($scope.hotelSearch.checkIn).getDate() + 1);
        
                }
            }
        }
    }

    $scope.checkoutdate = {
        opens: "left",
        singleDatePicker: true,
        autoApply: true,
        autoUpdateInput: true,
        minDate: new Date(),

        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                // $scope.checkoutdate = {
                //     singleDatePicker: true //$scope.checkboxModel.value
                // }
            }
        }
    }

    $scope.countryList = {};
    $scope.nationalityList = {};
    $scope.username = '';
    if ($location.absUrl().split('username=')[1]) {
        $scope.username = $location.absUrl().split('username=')[1];
    } else {
        $scope.username = 'guest'
    }
    var authenticationreq = {
            "username": $scope.username
        }
        if (localStorage.islogged == 'false' || localStorage.islogged == undefined) {

        /* var Serverurl="http://3.6.54.119:8091/ibp/api/v1/"; */
    // $http.post(ServerService.authPath + 'guest-login', JSON.stringify($scope.authenticationreq), {
    //     headers: { 'Content-Type': 'application/json' }
    // }).then(function successCallback(response) {
    //     var authenticationreqres = response;
    //     sessionStorage.setItem('authentication', JSON.stringify(authenticationreqres));
    //     $scope.dropdownList();
    // });
        }
    // $scope.room=1;
    var roomBeans = function() {
        // this.roomno = null,
        this.adultCount = "1",
            this.childCount = "0",
            this.childrens = []
            // this.child
    }

    // $scope.roomAvailanceInput = function(){
    // 	$scope.hotelSearch.roomBean = [];
    // 		for (i=0;i<$scope.hotelSearch.room;i++){
    // 		var roomBean = new roomBeans();
    // 		$scope.hotelSearch.roomBean.push(roomBean);
    // 		}
    // 	}
    // $scope.room=1;
    $scope.roomAvailanceInput = function() {
        $scope.hotelSearch.roomBean = [];
        for (i = 0; i < $scope.room; i++) {
            var roomBean = new roomBeans();
            roomBean.roomno=i+1;
            $scope.hotelSearch.roomBean.push(roomBean);
        }
    }

    $scope.nights = 0;

    $scope.calcnit =  function()
    {
        if($scope.hotelSearch.checkIn != "" && $scope.hotelSearch.checkOut != '')
        {
            var d = new Date($scope.hotelSearch.checkIn);
            var day = d.getDate();
            var monthIndex = d.getMonth() + 1;
            var year = d.getFullYear();
    
            var checkin =  monthIndex + '/' + day + '/' +year;
    
            var d1 = new Date($scope.hotelSearch.checkOut);
            var day1 = d1.getDate();
            var monthIndex1 = d1.getMonth() + 1;
            var year1 = d1.getFullYear();
    
            var checkout = monthIndex1 + '/' + day1 + '/' + year1;
            const finalCheckInDate = new Date(checkin);
            const finalCheckOutDate = new Date(checkout);
            const diffTime = Math.abs(finalCheckInDate - finalCheckOutDate);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
            $scope.nights=diffDays;
        }
    }
    
    $scope.calcNight = function(){
        var formatDateone=$scope.hotelSearch.checkOut;
        var d=formatDateone.split("/");
        var month=d[1];
        var day=d[0];
        var year=d[2];
        var formatDate1= [month,day,year].join('/');
      
        var formatDatetwo= $scope.hotelSearch.checkIn;
        var d2=formatDatetwo.split("/");
        var month=d2[1];
        var day=d2[0];
        var year=d2[2];
        var formatDate2= [month,day,year].join('/');
        const finalCheckInDate = new Date(formatDateone);
        const finalCheckOutDate = new Date(formatDatetwo);
        const diffTime = Math.abs(finalCheckInDate - finalCheckOutDate);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        $scope.nights=diffDays;
        
    }

    $scope.onCheckinChange = function() {
        console.log($scope.hotelSearch.checkIn)
        $scope.checkoutdate = {
            opens: "left",
            singleDatePicker: true,
            autoApply: true,
            autoUpdateInput: true,
            minDate: $scope.hotelSearch.checkIn,

            eventHandlers: {
                'show.daterangepicker': function(ev, picker) {
                    // $scope.checkoutdate = {
                    //     singleDatePicker: true //$scope.checkboxModel.value
                    // }
                }
            }
        }
        $scope.calcNight();

        if($scope.hotelSearch.checkIn > $scope.hotelSearch.checkOut){
            $scope.hotelSearch.checkOut = " ";
            $scope.hotelSearch.checkOut = new Date($scope.hotelSearch.checkIn).setDate(new Date($scope.hotelSearch.checkIn).getDate() + 1);

        }
    }

    $scope.hotelSearch = {};

    $scope.dateconvert = function(date) {
        var dateformat = date.split('/');
        var d = dateformat[0];
        var m = dateformat[1];
        var y = dateformat[2];
        return m + '/' + d + '/' + y + '/';
    }
    if (sessionStorage.getItem('hotelSearchdataobj') != undefined) {
        $scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));
        $('#from_city').val($scope.hotelSearch.city);
        // $scope.hotelSearch.city = $scope.hotelSearchList.city;
        $scope.hotelSearch.checkIn = $scope.dateconvert($scope.hotelSearch.checkIn);
        $scope.hotelSearch.checkOut = $scope.dateconvert($scope.hotelSearch.checkOut);
        $scope.room = $scope.hotelSearch.roomBean.length;
        // $scope.hotelSearch.nationality = $scope.hotelSearchList.nationality;
        $scope.calcNight();

    } else {
        $scope.hotelSearch = {
            city: "",
            checkIn: "",
            checkOut: "",
            nationality: "",
            country: "",
            countryOfResidence: "",
            roomBean: []

        };
        // $scope.hotelSearch.room = "1";
        $scope.roomAvailanceInput();
    }

    var childage = function() {
        this.age = "1";
    }

    var childrens = function() {
        this.childCount = "0";
        this.childBeans = [];
    }

    $scope.childAgeIteration = function(index, count) {
        $scope.hotelSearch.roomBean[index].childrens = [];
        var childrenBean = new childrens();

        $scope.hotelSearch.roomBean[index].childrens.push(childrenBean);
        $scope.hotelSearch.roomBean[index].childrens[0].childCount = "";
        $scope.hotelSearch.roomBean[index].childrens[0].childCount = count;
        $scope.hotelSearch.roomBean[index].childrens[0].childBeans = [];

        for (i = 0; i < count; i++) {
            var childBeans = new childage();
            $scope.hotelSearch.roomBean[index].childrens[0].childBeans.push(childBeans);
        }
        //console.log(angular.toJson($scope.hotelSearch));

    }

    $scope.passengerCount = 0;
    $scope.showError = false;



    $scope.hotelsSearchForm = function(valid) {

        // if(jQuery('#hotelSearchfrom').valid()){

        $scope.passengerCount = 0;
        for (i = 0; i < $scope.hotelSearch.roomBean.length; i++) {
            $scope.passengerCount = Number($scope.hotelSearch.roomBean[i].adultCount) + $scope.passengerCount;
            $scope.passengerCount = Number($scope.hotelSearch.roomBean[i].childCount) + $scope.passengerCount;
        }

        // var hotelSearch = {
        // 	"checkIn": $scope.hotelSearch.checkIn,
        // 	"checkOut": $scope.hotelSearch.checkOut,
        // 	"country": $scope.hotelSearch.country,
        // 	"city": "DUBAI",
        // 	"nationality": $scope.hotelSearch.nationality,
        // 	// "roomBean": $scope.hotelSearch.roomBean
        // }
        // var date = $('#daterange1').val();
        // var date1 = []
        // date1 = date.split(' ');
        if ($scope.passengerCount <= 9) {
            $scope.showError = false;
            var d = new Date($scope.hotelSearch.checkIn);
            var day = d.getDate();
            var monthIndex = d.getMonth() + 1;
            var year = d.getFullYear();

            var checkin = day + '/' + monthIndex + '/' + year;
            $scope.hotelSearch.checkIn = checkin;

            var d1 = new Date($scope.hotelSearch.checkOut);
            var day1 = d1.getDate();
            var monthIndex1 = d1.getMonth() + 1;
            var year1 = d1.getFullYear();
            var checkout = day1 + '/' + monthIndex1 + '/' + year1;
            $scope.hotelSearch.checkOut = checkout;
            // $scope.hotelSearch = []
            // $scope.hotelSearch ={
            // 	"checkIn": "27/11/2020 ",
            // 	"checkOut": "28/11/2020",
            // 	"country": "UAE",
            // 	"city": "DUBAI",
            // 	"nationality": "IN",
            // 	"countryOfResidence":"IN",

            // 	"roomBean": [
            // 	  {
            // 		"adultCount": "1" 
            // 	  }
            // 	]
            //   }

            // console.log(JSON.stringify(angular.toJson($scope.hotelSearch)));
            // console.log(angular.toJson($scope.hotelSearch));
            sessionStorage.setItem("hotelSearchdataobj", angular.toJson($scope.hotelSearch));
            $location.path('/searchingHotel');
            // $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
            // var tokenname = $scope.tokenresult.data;
            // var headersOptions = {
            // 	'Content-Type': 'application/json',
            // 	'Authorization': 'Bearer ' + tokenname.authToken
            // }
            // // sessionStorage.setItem("hotelSearchdataobj",angular.toJson($scope.hotelSearch));

            // $http.post(ServerService.serverPath + 'hotel/availability', angular.toJson($scope.hotelSearch), {

            // 	headers: headersOptions
            // }).then(function (response) {
            // 	$scope.loading = false;
            // 	$scope.disabled = false;
            // 	if (response.data.status == 'success') {
            // 		sessionStorage.setItem('hotelSearchResp', JSON.stringify(response.data.data));
            // 		sessionStorage.setItem("hotelSearchdataobj",angular.toJson($scope.hotelSearch));
            // 		$location.path('/hotelResult');
            // 	} else {
            // 		$scope.loading = false;
            // 		$scope.disabled = false;
            // 		Lobibox.alert('error', {
            // 			msg: response.data.errorMessage
            // 		})
            // 		// $timeout(function () {
            // 		// 	$location.path('/hotel')
            // 		// }, 1000);
            // 	}

            // });
        } else {
            $scope.showError = true;
            Lobibox.alert('info', {
                msg: "You can book upto 9 passengers per booking, including adults and children"
            });
            return;
            
        }
        // }
    }


    $('#from_city').val($scope.hotelSearch.city);


    // if (sessionStorage.getItem('hotelSearchObj') != undefined) {
    // 	$timeout(function () {
    // 		$scope.hotelSearchObj = JSON.parse(sessionStorage.getItem('hotelSearchObj'));
    // 	}, 300);
    // } else {
    // 	$scope.hotelSearchObj = {};
    // 	$scope.hotelSearchObj.roomBean = [{ adultCount: 1, childCount: 0, childrens: [{ childBeans: [] }] }];
    // }
    /*maintain search data in localStorage */
    // if (localStorage.getItem("hotelCity")) {
    // 	$scope.hotelSearchObj.place = localStorage.getItem("hotelCity");
    // }

    // 	$scope.hotelDate=JSON.parse(localStorage.getItem("hotelSearchDate"));

    /*maintain search data in localStorage end*/
    var hotser = false;

    $('#promoCode').keyup(function() {
        this.value = this.value.toUpperCase();
    });

    $(function() {

        'use strict';
        var f = 0;
        var flag = false;
        // FROM ARIPORT AUTOCOMPELTE
        $('#from_city').autocomplete({
            source: function(request, response) {
                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                var tokenname = $scope.tokenresult.data;
                //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokenname.authToken
                }
                // $http.get(ServerService.serverPath + 'hotel/cities/' + request.term, {
                    $http.post(ServerService.listPath + 'list/custom/data/HCD' , JSON.stringify({"searchStr":request.term}), {
                        headers: headersOptions
                }).then(function successCallback(data, status) {
                    var success = data.data.success
                    var data = data.data.data;
                    if (success&&data.length>0) {
                        f = 0;
                        response(data);
                    } else {
                        $("#from_city").val('');
                        $("#from_city").focus(0);
                        $('.ui-autocomplete').hide();
                    }
                }, function errorCallback(response) {
                    if(response.status == 403){
                        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                        status.then(function(greeting) {
                        $("#from_city").val('');
                           });
                    }
                });
            },
            // focus: function(event, ui) {
            //     $("#from_city").val(ui.item.city + ", " + ui.item.country);
            //     $scope.hotelSearch.city = ui.item.city+", " + ui.item.country;
            //         $scope.hotelSearch.country = ui.item.country;
                   
            //     return false;
            // },
            change: function(event, ui) {
                var uiCity = JSON.stringify(ui.item); 
                if(uiCity == 'null'){
                    $("#from_city").val('');
                }
                // $("#from_city").val(ui.item.city+", " + ui.item.country);
                // $scope.hotelSearch.city = ui.item.city;
                // $scope.hotelSearch.country = ui.item.country;
                
                // if ($("#to_place").val() == $("#from_city").val()) {
                //     $("#from_city").val("");
                //     return false;
                // }

                /*if(flag==false) {
                $('#from_city').val('');
                $('#from_city').focus(0);
                return false;
                }*/
            },
            autoFocus: true,
            minLength: 3,
            select: function(event, ui) {
                if ($("#from_city").val() == $("#to_place").val()) {
                    $("#from_city").val('');
                    $('.destcheck').show().fadeOut(5000);
                } else {
                    $("#from_city").val(ui.item.city+', ' + ui.item.country);
                    $scope.hotelSearch.city = ui.item.city+", " + ui.item.country;
                    $scope.hotelSearch.country = ui.item.country;
                    $scope.$apply();
                    $("#from_city").parent().removeClass('has-error');
                    $scope.errorDisp = false;
                    $('#to_place').focus(0);
                    flag = true;
                }
                return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function(event, ui) {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (f == 0) {
                f++;
                //$scope.hotelSearch.city = item.city+", " + item.country;
                return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.country + "</a>").appendTo(ul);
            } else {
                return $("<li>").append("<a>" + item.city + "," + item.country + "</a>").appendTo(ul);
            }
        };
    });

    nationalityget = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/nationality',

            headers: headersOptions
        }).then(function successCallback(response, status) {
            if (status = 'success') {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.nationalityList = response.data.data;
                countrycode();
            } else {}
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    nationalityget();
                });
            }
          });
    }

    countrycode = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.listPath + 'list/country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.loading = false;
                $scope.disabled = false;
                if (response.data.success) {
                $scope.allcountrys = response.data.data;
                $scope.countryList = $scope.allcountrys.sort((a, b) => a.name.localeCompare(b.name));
            } else {}
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    countrycode();
                });
            }
          });
    }
    $scope.dropdownList = function(){
        nationalityget();
    };

    $scope.dropdownList();

    $(".refine-search-close").click(function() {
        $(".flight-search").slideUp();
    });

    // jQuery('#search').keyup(function () {
    // 	/* if (hotser == true) {
    // 		$(this).val('');
    // 		hotser = false;
    // 	} */

    // 	$scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
    // 	var tokenname = $scope.tokenresult.data;
    // 	//  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
    // 	var headersOptions = {
    // 		'Content-Type': 'application/json',
    // 		'Authorization': 'Bearer ' + tokenname.authToken
    // 	}

    // 	if (jQuery(this).val().length == 3) {
    // 		$http.get(ServerService.serverPath + 'hotel/cities/'+jQuery(this).val().toUpperCase(), {
    //             headers: headersOptions
    //         })
    // 			.then(function (data, status) {
    // 				var availableTags = [];
    // 				cityMap = new Map();
    // 				for (var i = 0; i < data.length; i++) {
    // 					if (data[i].city.toLowerCase() == jQuery('#search').val().toLowerCase()) {
    // 						availableTags.push(data[i].city + ', ' + data[i].provinceName + ', ' +data[i].country);
    // 						cityMap.set(data[i].city + ', ' + data[i].provinceName + ', ' +data[i].country,data[i].cityId);
    // 					}
    // 				}
    // 				for (var j = 0; j < data.length; j++) {
    // 					if (data[j].city.toLowerCase() != jQuery('#search').val().toLowerCase()) {
    // 						if (data[j].city.toLowerCase().search(jQuery('#search').val().toLowerCase()) == 0) {
    // 							availableTags.push(data[j].city + ', ' + data[j].provinceName + ', ' +data[j].country);
    // 							cityMap.set(data[j].city + ', ' + data[j].provinceName + ', ' +data[j].country,data[j].cityId);
    // 						}

    // 					}
    // 				}
    // 				for (var k = 0; k < data.length; k++) {
    // 					if (data[k].city.toLowerCase() != jQuery('#search').val().toLowerCase()) {
    // 						if (data[k].city.toLowerCase().search(jQuery('#search').val().toLowerCase()) != 0) {
    // 							availableTags.push(data[k].city + ', ' + data[k].provinceName + ', ' +data[k].country);
    // 							cityMap.set(data[k].city + ', ' + data[k].provinceName + ', ' +data[k].country,data[k].cityId);
    // 						}

    // 					}
    // 				}
    // 				jQuery("#search").autocomplete({
    // 					source: availableTags,
    // 					select: function (event, ui) {
    // 						hotser = true;
    // 						reminder = ui.item.value;
    // 						jQuery('#datepicker').focus(0);
    // 					},
    // 					autoFocus: true,
    // 					focus: function (event, ui) {
    // 						event.preventDefault();
    // 					}
    // 				});
    // 				var e = jQuery.Event("keydown");
    // 				e.keyCode = 50;
    // 				$("#search").trigger(e);

    // 			});
    // 		localStorage.setItem("cityMap", JSON.stringify(cityMap))	
    // 	}
    // });
    // var date=new Date();
    // var currentDate=date.getDate()+1;
    // $scope.dateOption = {
    // 	opens: "right",
    // minDate:new Date(date.getFullYear(),date.getMonth(),currentDate),
    // }
    // $scope.hotelAutoComplet = function (hotelto) {

    // 	if (hotelto.length == 2) {
    // 		console.log("fromHotel" + hotelto);
    // 		$http.post(ServerService.serverPath + 'rest/hotel/cities', '{"queryString" : "' + hotelto + '"}',
    // 			{
    // 				headers: {
    // 					'Content-Type': 'application/json; charset=UTF-8'
    // 				}
    // 			}).then(function (response) {
    // 				/* $scope.hotelstates = JSON.stringify(response.data); */
    // 				for (var i = 0; i < response.data.length; i++) {
    // 					var hotelObj = {
    // 						Title: response.data[i].city + "," + response.data[i].country,
    // 						Code: response.data[i].code
    // 					}
    // 					$scope.city.push(hotelObj);
    // 				}
    // 			})
    // 	}
    // }

    /* onFormSubmit function */
    // $scope.loading = false;
    // $scope.disabled = false;
    // var adoultCount = 0;
    // var childCount = 0;
    // $scope.hotelRequest = {}

    // $scope.roomBean = JSON.parse(localStorage.getItem('roomBean'));
    function createRoomBean() {
        var roomBeanObject = {};
        roomBeanObject.index = 1;
        roomBeanObject.adultCount = "2";
        roomBeanObject.cots = 0;
        roomBeanObject.childCount = "0";
        roomBeanObject.roomDesc = 'Single';
        roomBeanObject.xtraBed = false;
        roomBeanObject.roomCount = 1;
        roomBeanObject.withChildrens = false;
        roomBeanObject.childrens = null;
        return roomBeanObject;
    }


    // if ($scope.roomBean != null) {
    // 	for (i = 0; i < $scope.roomBean.length; i++) {
    // 		adoultCount = adoultCount + parseInt($scope.roomBean[i].adultCount);
    // 		childCount = childCount + parseInt($scope.roomBean[i].childCount);
    // 	}
    // }
    // else {
    // 	$scope.roomBean = [];
    // 	var roomBean = createRoomBean();
    // 	roomBean.roomCount = 1;
    // 	$scope.roomBean.push(roomBean);
    // 	adoultCount = 2;
    // 	childCount = 0;
    // 	$rootScope.totalPassengers = adoultCount + childCount;
    // 	localStorage.setItem("roomBean", JSON.stringify($scope.roomBean));
    // }

    // if (childCount > 0){
    // 	$rootScope.passengerHotelInfo = "Adults - " + adoultCount + " + Child - " + childCount;
    // }else{
    // 	$rootScope.passengerHotelInfo = "Adults - " + adoultCount;
    // }
    // localStorage.setItem("passengerHotelInfo", $rootScope.passengerHotelInfo);


    // $scope.onFormSubmit = function (valid) {
    // 	// $('html, body').scrollTop(0);
    // 	$scope.submitted = true;
    // 	if (valid) {
    // 		var fromHotelPlace = $scope.hotelSearchObj.place;
    // 		if(!fromHotelPlace.includes(',')){
    // 			fromHotelPlace=$("#search").val();
    // 			$scope.hotelSearchObj.place=fromHotelPlace;
    // 		}
    // 		if(cityMap!=null&&cityMap!='undefined'&&cityMap.size>0)
    // 		var cityId=cityMap.get(fromHotelPlace);
    // 		else{
    // 			var cityId=localStorage.getItem("cityId")
    // 		}
    // 		localStorage.setItem("cityId", cityId)
    // 		localStorage.setItem("hotelCity", fromHotelPlace)
    // 		localStorage.setItem("hotelSearchDate",JSON.stringify($scope.hotelDate));
    // 		var hotelDate = $('#hotelDateRange').val();

    // 		var hotelCheckIn = hotelDate.split('-')[0];
    // 		var hotelCheckOut = hotelDate.split('-')[1];

    // 		var country = fromHotelPlace.split(',')[2];
    // 		var city = fromHotelPlace.split(',')[0];

    // 		var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))

    // 		/*$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
    // 		$scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
    // 		$scope.childCount = JSON.parse(sessionStorage.getItem('child'));
    // 		$scope.adultCount = JSON.parse(sessionStorage.getItem('adult_count'));
    // 		*
    // 		if($scope.infantCount == null || $scope.infantCount == undefined){
    // 			$scope.infantCount = 0;
    // 			} else {
    // 			$scope.infantCount = $scope.infantCount;
    // 		}
    // 		*/

    // 		$scope.roomBean = JSON.parse(localStorage.getItem('roomBean'));
    // 		if ($scope.roomBean != null) {
    // 			var tempAdultCount = 1;
    // 			for (i = 0; i < $scope.roomBean.length; i++) {
    // 				switch (tempAdultCount) {
    // 					case 1:
    // 						$scope.roomBean[i].roomDesc = 'Single'+i;
    // 						break;
    // 					case 2:
    // 						$scope.roomBean[i].roomDesc = 'Double'+i;
    // 						break;
    // 					case 3:
    // 						$scope.roomBean[i].roomDesc = 'Triple'+i;
    // 						break;
    // 					case 4:
    // 						$scope.roomBean[i].roomDesc = 'Quad'+i;
    // 						break;
    // 				}
    // 				tempAdultCount++;
    // 				adoultCount = adoultCount + parseInt($scope.roomBean[i].adultCount);
    // 				childCount = childCount + parseInt($scope.roomBean[i].childCount);
    // 			}
    // 		}

    // 		$scope.hotelRequest.checkOut = hotelCheckOut;
    // 		$scope.hotelRequest.checkIn = hotelCheckIn;
    // 		//$scope.hotelRequest.place = fromHotelPlace;
    // 		$scope.hotelRequest.place = $scope.hotelSearchObj.place;
    // 		$scope.hotelRequest.country = country;
    // 		$scope.hotelRequest.city = city;
    // 		$scope.hotelRequest.cityId = cityId;
    // 		$scope.hotelRequest.currency = $scope.currency;
    // 		$scope.hotelRequest.htlName = null;
    // 		$scope.hotelRequest.searchMode = 'search';
    // 		$scope.hotelRequest.starRating = 0;
    // 		$scope.hotelRequest.traveller = $rootScope.totalPassengers
    // 		$scope.hotelRequest.roomBean = $scope.roomBean;



    // 		$scope.hotelRequest.promoCode = $scope.hotelSearchObj.promoCode;
    // 		sessionStorage.setItem('hotelRequestObj', JSON.stringify($scope.hotelRequest));
    // 		sessionStorage.removeItem("travellersListByRoom");
    // 		$location.path('/searchingHotel');
    // 	}
    // 	// $('#promoCode').keyup(function() {
    // 	// this.value = this.value.toUpperCase();
    //   // });

    // 	/* var fromHotelPlace = $('#from_hotel').val();
    // 	var hotelDate = $('#hotelDateRange').val();

    // 	var hotelCheckIn = hotelDate.split('-')[0];
    // 	var hotelCheckOut = hotelDate.split('-')[1]; 

    // 	var country = fromHotelPlace.split(',')[1];
    // 	var city = fromHotelPlace.split(',')[0];

    // 	var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))
    // 	$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
    // 	$scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
    // 	$scope.childCount = JSON.parse(sessionStorage.getItem('child'));
    // 	$scope.adultCount = JSON.parse(sessionStorage.getItem('adult'));

    // 	if($scope.infantCount == null || $scope.infantCount == undefined){
    // 		$scope.infantCount = 0;
    // 		} else {
    // 		$scope.infantCount = $scope.infantCount;
    // 	}

    // 	$scope.hotelRequest.checkOut = hotelCheckOut;
    // 	$scope.hotelRequest.checkIn = hotelCheckIn;
    // 	$scope.hotelRequest.place = fromHotelPlace;
    // 	$scope.hotelRequest.country = country;
    // 	$scope.hotelRequest.city = city;
    // 	$scope.hotelRequest.currency = $scope.currency;
    // 	$scope.hotelRequest.htlName = null;
    // 	$scope.hotelRequest.searchMode = 'search';
    // 	$scope.hotelRequest.starRating = 0;

    // 	$scope.hotelRequest.roomBean = $scope.roombean;
    // 	$scope.hotelRequest.traveller = $scope.roombean.length

    // 	$scope.hotelRequest.binNumber = '552107';
    // 	sessionStorage.setItem('hotelRequestObj', JSON.stringify($scope.hotelRequest));
    // 	$location.path('/searchingHotel'); */
    // 	/* $http.post('http://192.168.1.102:9999/care4trip/rest/hotel/search/city',$scope.hotelRequest,{
    // 		headers: {'Content-Type': 'application/json; charset=UTF-8'}
    // 		}).then(function(response){
    // 		$scope.loading = false;
    // 		$scope.disabled = false;
    // 		if(response.data.status == 'SUCCESS'){
    // 			$scope.hotelSearchResp = response.data.data;
    // 			sessionStorage.setItem('hotelSearchResp',JSON.stringify($scope.hotelSearchResp));
    // 			$location.path('/hotelResult')
    // 			} else {
    // 			Lobibox.alert('error',{
    // 				msg: response.data.errorMessage
    // 			})
    // 		}
    // 	}) */

    // }
    // $('#promoCode').keyup(function() {
    // 	this.value = this.value.toUpperCase();
    //   });

    /* end onFormSubmit */

    // $scope.open = function (size, parentSelector) {
    // 	var parentElem = parentSelector ?
    // 		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

    // 	$scope.animationEnabled = true;
    // 	var modalInstance = $uibModal.open({
    // 		animation: $scope.animationEnabled,
    // 		ariaLabelledBy: 'modal-title',
    // 		ariaDescribedBy: 'modal-body',
    // 		templateUrl: 'myModalContent.html',
    // 		controller: 'hotelPassCtrl',
    // 		backdrop: true,
    // 		size: 'lg',
    // 		appendTo: parentElem,
    // 		resolve: {
    // 			items: function () {
    // 				return $scope.items
    // 			}
    // 		}
    // 	})

    // }
}).controller('hotelPassCtrl', function($scope, $rootScope, $uibModalInstance) {


    $scope.maxRooms = 5;

    // $scope.roomBean = {
    // 	index: 1,
    // 	adultCount: 1,
    // 	cots: 0,
    // 	childCount: 1,
    // 	roomDesc: "Single",
    // 	xtraBed: false,
    // 	roomCount: 1,
    // 	withChildrens: true,
    // 	childrens: null
    // };

    $scope.childBean = {
        index: 0,
        childCount: 1,
        childBeans: []
    };

    $scope.childInfo = {
        name: null,
        age: 4
    };

    function createChildBean() {
        var childBean = {};
        childBean.index = 0;
        childBean.childCount = 1;
        childBean.childBeans = [];
        return childBean;

    }

    function createChildInfo() {
        var childInfo = {};
        childInfo.name = null;
        childInfo.age = "1";
        return childInfo;

    }

    // if (localStorage.getItem('roomBean') == undefined) {
    // 	$scope.roomBean = [];
    // 	$scope.roomCount = 1;
    // 	var roomBean = createRoomBean();
    // 	if(localStorage.getItem("adultCount")){
    // 		roomBean.adultCount=localStorage.getItem("adultCount");

    // 	}
    // 	roomBean.roomCount = 1;
    // 	$scope.roomBean.push(roomBean);
    // }
    // else {
    // 	$scope.roomBean = JSON.parse(localStorage.getItem('roomBean'));
    // 	$scope.roomCount = $scope.roomBean.length;
    // }
    // function createRoomBean() {
    // 	var roomBeanObject = {};
    // 	roomBeanObject.index = 1;
    // 	roomBeanObject.adultCount = "2";
    // 	roomBeanObject.cots = 0;
    // 	roomBeanObject.childCount = "0";
    // 	roomBeanObject.roomDesc = 'Single';
    // 	roomBeanObject.xtraBed = false;
    // 	roomBeanObject.roomCount = 1;
    // 	roomBeanObject.withChildrens = false;
    // 	roomBeanObject.childrens = null;
    // 	return roomBeanObject;
    // }

    // $scope.addRoom = function () {
    // 	var roomBean = createRoomBean();
    // 	// roomBean.roomCount = $scope.roomBean.length + 1;
    // 	roomBean.roomCount = 1;
    // 	$scope.roomBean.push(roomBean);
    // }

    // $scope.removeRoom = function (index) {
    // 	$scope.roomBean.splice(index, 1);
    // 	if($scope.roomBean.length>0){
    // 	for(var i=0; i<$scope.roomBean.length; i++){
    // 		if($scope.roomBean[i].childrens!=null){
    // 			$scope.withChild=[];
    // 			$scope.withChild.push(0);
    // 			break;
    // 		}else{
    // 	$scope.withChild=[];
    // }
    // 	}
    // }
    // }


    // $scope.updateroomBean = function () {
    // 	if ($scope.roomCount > 0) {
    // 		if ($scope.roomBean.length < $scope.roomCount) {
    // 			for (i = $scope.roomBean.length; i < $scope.roomCount; i++) {
    // 				var roomBean = createRoomBean();
    // 				roomBean.roomCount = i + 1;
    // 				$scope.roomBean.push(roomBean);
    // 			}
    // 		}
    // 		else {
    // 			$scope.roomBean.splice($scope.roomCount);
    // 		}

    // 	}

    // }
    // if($scope.roomBean.length>0){
    // 	for(var i=0; i<$scope.roomBean.length; i++){
    // 		if($scope.roomBean[i].childrens!=null){
    // 			$scope.withChild=[];
    // 			$scope.withChild.push(0);
    // 			break;
    // 		}else{
    // 	$scope.withChild=[];
    // }
    // 	}
    // }
    // $scope.updateChildList = function (index) {
    // 	if ($scope.roomBean[index].childrens == null) {
    // 		$scope.roomBean[index].childrens = [];
    // 		$scope.roomBean[index].childrens.push(createChildBean());
    // 		$scope.roomBean[index].childrens[0].childCount = $scope.roomBean[index].childCount;
    // 		$scope.roomBean[index].withChildrens = true;
    // 		$scope.withChild.push(0);
    // 		for (i = 0; i < $scope.roomBean[index].childCount; i++) {

    // 			$scope.roomBean[index].childrens[0].childBeans.push(createChildInfo());
    // 		}
    // 	}
    // 	else {
    // 		if ($scope.roomBean[index].childCount == 0) {
    // 			$scope.withChild.pop();
    // 			$scope.roomBean[index].withChildrens = false;
    // 			$scope.roomBean[index].childrens[0].childBeans = [];
    // 			$scope.roomBean[index].childrens=null;
    // 		}
    // 		else if ($scope.roomBean[index].childrens[0].childBeans.length < $scope.roomBean[index].childCount)
    // 			$scope.roomBean[index].childrens[0].childBeans.push(createChildInfo());
    // 		else {
    // 			$scope.roomBean[index].childrens[0].childBeans.splice(1);
    // 		}
    // 	}

    // }


    // $scope.ok = function () {
    // 	localStorage.setItem('roomBean', JSON.stringify($scope.roomBean));
    // 	$scope.roomBean = JSON.parse(localStorage.getItem('roomBean'));
    // 	var adultCount = 0;
    // 	var childCount = 0;
    // 	for (i = 0; i < $scope.roomBean.length; i++) {
    // 		adultCount = adultCount + parseInt($scope.roomBean[i].adultCount);
    // 		childCount = childCount + parseInt($scope.roomBean[i].childCount);

    // 	}
    // 	$rootScope.totalPassengers = adultCount + childCount;
    // 	if (childCount > 0){
    // 		$rootScope.passengerHotelInfo = "Adults - " + adultCount + " + Child - " + childCount;
    // 	}else{
    // 		$rootScope.passengerHotelInfo = "Adults - " + adultCount;
    // 	}
    // 	localStorage.setItem("passengerHotelInfo", $rootScope.passengerHotelInfo);
    // 	$uibModalInstance.close();

    // }
    // $scope.cancel = function () {
    // 	$uibModalInstance.dismiss('cancel');
    // }

});