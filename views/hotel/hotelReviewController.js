ouaApp.controller('hotelReviewController', function ($scope, $http,$rootScope,baseUrlService, ConstantService,$filter, $window,$interval,$uibModal, Month, Year, ServerService, $location, $timeout, NationService, httpService, reuseService, $state, customSession) {
	$('html, body').scrollTop(0);
	var nation = NationService.nationGet();
	$scope.countryList = {};
	$scope.backtodetail = false;
    $scope.backtotraveler = false;
    
	$scope.repriceResp = JSON.parse(sessionStorage.getItem('repriceResp'));
	
	// $scope.cancellationPolicy=$scope.repriceResp.roomDetails[0].cancellationPolicy;
	$scope.roomWisecancellationPolicy=[];
	if($scope.repriceResp.roomDetails!=null){
		for(var r=0; r<$scope.repriceResp.roomDetails.length; r++){
			var rateStringData=$scope.repriceResp.roomDetails[r].rateComments;
			var cancellationPolicy=$scope.repriceResp.roomDetails[r].cancellationPolicy;
			$scope.roomWisecancellationPolicy.push($scope.repriceResp.roomDetails[r].cancellationPolicy);
			if(rateStringData!=null && rateStringData.length>1){
			ratecommentsvalue=rateStringData;
			
			
			$scope.repriceResp.roomDetails[r].rateComments=ratecommentsvalue;
		
			}else{
				$scope.repriceResp.roomDetails[r].rateComments="No Rate Comments";
			}
			if( cancellationPolicy!=null){
				cancelationdata=cancellationPolicy.replace( /(<([^>]+)>)/ig, '');
				$scope.repriceResp.roomDetails[r].cancellationPolicy=cancelationdata;
			}
		}
	}
     // fordateofbirth..dropdown...author nandhiniD.
     $scope.months = 12;
     $scope.getminexpirymonth = 12;
     var dobmonth = [];
     var cdobmonth = [];
     // var idobmonth = [];
     for (var i = 0; i < $scope.months; i += 1) {
         dobmonth.push(i + 1);
         cdobmonth.push(i + 1);
         // idobmonth.push(i+1);
 
     }
     $scope.dobmonths = dobmonth;
     $scope.cdobmonths = cdobmonth;


     $scope.bdates=31;
     var adate= [];
     var cdate = [];
     var idate = [];
     for (var i = 0; i < $scope.bdates; i += 1) {
        adate.push(i + 1);
        cdate.push(i + 1);
        idate.push(i + 1);
     }
     $scope.dobdates = adate;
     $scope.cdobdates = cdate;
     $scope.idobdates = idate;

     // $scope.idobmonths = idobmonth;
 
     // forchild......
     var cyear = new Date().getFullYear() - 1;
     var crange = [];
     crange.push(cyear);
     for (var c = 1; c < 11; c++) {
         crange.push(cyear - c);
     }
     $scope.cyears = crange;
 
     // foradult........
     var ayear = new Date().getFullYear() - 18;
     var arange = [];
     arange.push(ayear);
     for (var i = 1; i < 55; i++) {
         arange.push(ayear - i);
     }
     $scope.ayears = arange;
     // forinfantYear......fordob..
     $scope.imonths = 12;
     var idobmonth = [];
     for (var i = 0; i < $scope.imonths; i += 1) {
         idobmonth.push(i + 1);
     }
     $scope.idobmonths = idobmonth;
     var iyear = new Date().getFullYear();
     var irange = [];
         irange.push(iyear);
         for (var i = 1; i < 2; i++) {
             irange.push(iyear - i);
         }
         $scope.iyears = irange;
     $scope.changeinfantDate = function(selectedYear) {
         console.log("selectedYear", selectedYear)
         var iyear = new Date().getFullYear();
         var infantToday = new Date();
         var getInfanntmonth = infantToday.getMonth();
 
         var selectedYear = selectedYear.infant.personaldata.birthyear || 0;
         if (selectedYear == iyear) {
             $scope.imonths = getInfanntmonth + 1;
         } else {
             $scope.imonths = 12;
         }
 
         var idobmonth = [];
         for (var i = 0; i < $scope.imonths; i += 1) {
             idobmonth.push(i + 1);
         }
         $scope.idobmonths = idobmonth;
 
         var irange = [];
         irange.push(iyear);
         for (var i = 1; i < 2; i++) {
             irange.push(iyear - i);
         }
         $scope.iyears = irange;
     }
        // forinfantYear......for..passportexpiry..
        $scope.ipemonths = 12;
        var ipemonth = [];
        for (var i = 0; i < $scope.imonths; i += 1) {
         ipemonth.push(i + 1);
        }
        $scope.ipemonths = ipemonth;
        var iexpiryyear = new Date().getFullYear();
        var aexpiryyear = new Date().getFullYear();
        var cexpiryyear = new Date().getFullYear();
        var iperange = [];
        iperange.push(iexpiryyear);
        for (var i = 1; i < 50; i++) {
         iperange.push(iexpiryyear - i);
        }
        $scope.iexpiryyears = iperange;
        $scope.changeinfantpeYear = function(selectedYear) {
            var iexpiryyear = new Date().getFullYear();
            var infantToday = new Date();
            var getInfanntmonth = infantToday.getMonth();
    
            var selectedYear = selectedYear.infant.personaldata.expiryyear || 0;
            if (selectedYear == iexpiryyear) {
                $scope.currentmonth = getInfanntmonth;
            } else {
                $scope.currentmonth = 0;
                $scope.imonths = 12;
            }
    
            var ipemonth = [];
            for (var i = $scope.currentmonth; i < $scope.imonths; i += 1) {
             ipemonth.push(i + 1);
            }
            $scope.ipemonths = ipemonth;
    
            var iperange = [];
            iperange.push(iexpiryyear);
            for (var i = 1; i < 50; i++) {
             iperange.push(iexpiryyear - i);
            }
            $scope.iexpiryyears = iperange;
        }
 
 
     
 //passport expiry

//  var apemonth = [];
//  var cpemonth = [];
//  for (var i = 0; i < $scope.getminexpirymonth; i += 1) {
//      apemonth.push(i + 1);
//      cpemonth.push(i + 1);
//  }
//  $scope.apemonths = apemonth;
//  $scope.cpemonths = cpemonth;

 $scope.pdates=31;
 var apdate= [];
 var cpdate = [];
 for (var i = 0; i < $scope.pdates; i += 1) {
    apdate.push(i + 1);
     cpdate.push(i + 1);
 }
 $scope.pedates = apdate;
 $scope.cpedates = cpdate;


 
     // adultmonthchange...
     $scope.adays = 31;

     $scope.changeadultMonth = function(selectedMonth) {
             var selectedMonth = selectedMonth.adult.personaldata.birthmonth || 0;
             if (selectedMonth == 2) {
                 $scope.adays = 28;
                 console.log("$scope.dobdates", $scope.dobdates)
             } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                 $scope.adays = 30;
                 console.log("$scope.dobdates", $scope.dobdates)
             } else {
                 $scope.adays = 31;
             }
             var dobdate = [];
             for (var i = 0; i < $scope.adays; i += 1) {
                 dobdate.push(i + 1);
             }
             $scope.dobdates = dobdate;
             console.log("$scope.dobdates", $scope.dobdates)
 
         }
         
           // adultYearchange..for..passportexpiry.
           var apemonth = [];
           $scope.amonths = 12;
           for (var i = 0; i < $scope.amonths; i += 1) {
               apemonth.push(i + 1);
           }
           $scope.apemonths = apemonth;
         
           var pefullyear = new Date().getFullYear();
           var arange = [];
           arange.push(pefullyear);
           for (var i = 1; i < 50; i++) {
               arange.push(pefullyear + i);
           }
           $scope.apeyears = arange
         $scope.changeadultpeYear = function(selectedYear) {
         

               var aexpiryyear = new Date().getFullYear();
               var adultToday = new Date();
               var getAdultmonth = adultToday.getMonth();
       
               var selectedYear = selectedYear.adult.personaldata.expiryyear || 0;
               if (selectedYear == pefullyear) {
                   $scope.currentmonth = getAdultmonth;
               } else {
                   $scope.currentmonth = 0;
                   $scope.amonths = 12;
               }
       
               var apemonth = [];
               for (var i = $scope.currentmonth; i < $scope.amonths; i += 1) {
                apemonth.push(i + 1);
               }
               $scope.apemonths = apemonth;
       
               var aperange = [];
               aperange.push(aexpiryyear);
               for (var i = 1; i < 50; i++) {
                aperange.push(aexpiryyear - i);
               }
               $scope.aexpiryyears = aperange;
         }


          // adultmonthchange..for..passportexpiry.
     $scope.apedays = 31;
     $scope.changeadultpeMonth = function(selectedMonth) {
             var selectedPeMonth = selectedMonth.adult.personaldata.expirymonth || 0;
             if (selectedPeMonth == 2) {
                 $scope.apedays = 28;
             } else if (selectedPeMonth == 4 || selectedPeMonth == 6 || selectedPeMonth == 9 || selectedPeMonth == 11) {
                 $scope.apedays = 30;
             } else {
                 $scope.apedays = 31;
             }
            var currentday = new Date();
            var today = new Date().getDate();
            var aexpirymonth = currentday.getMonth()+1;
            var selectedYear = selectedMonth.adult.personaldata.expiryyear;
            if (selectedPeMonth == aexpirymonth && selectedYear ==pefullyear ) {
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var pedate = [];
            for (var i = $scope.currentDate; i < $scope.apedays; i += 1) {
                pedate.push(i + 1);
            }
            $scope.pedates = pedate;
         }
         // childmonthchange....
     $scope.cdays = 31;
     $scope.changechildMonth = function(selectedMonth) {
             var selectedMonth = selectedMonth.child.personaldata.birthmonth || 0;
             if (selectedMonth == 2) {
                 $scope.cdays = 28;
             } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                 $scope.cdays = 30;
             } else {
                 $scope.cdays = 31;
             }
             var cdobdate = [];
             for (var i = 0; i < $scope.cdays; i += 1) {
                 cdobdate.push(i + 1);
             }
             $scope.cdobdates = cdobdate;
         }
   
             // childYearchange..for..passportexpiry.
             var cpemonth = [];
             $scope.cmonths = 12;
             for (var i = 0; i < $scope.cmonths; i += 1) {
                 cpemonth.push(i + 1);
             }
             $scope.cpemonths = cpemonth;
           
             var cyear = new Date().getFullYear();
             var crange = [];
             crange.push(cyear);
             for (var i = 1; i < 50; i++) {
                 crange.push(cexpiryyear + i);
             }
             $scope.cexpiryyears = crange
           $scope.changechildpeYear = function(selectedYear) {
           
  
                 var cexpiryyear = new Date().getFullYear();
                 var childToday = new Date();
                 var getChildmonth = childToday.getMonth();
         
                 var selectedYear = selectedYear.child.personaldata.expiryyear || 0;
                 if (selectedYear == cyear) {
                     $scope.currentmonth = getChildmonth;
                 } else {
                     $scope.currentmonth = 0;
                     $scope.cmonths = 12;
                 }
         
                 var cpemonth = [];
                 for (var i = $scope.currentmonth; i < $scope.cmonths; i += 1) {
                  cpemonth.push(i + 1);
                 }
                 $scope.cpemonths = cpemonth;
         
                //  var cperange = [];
                //  cperange.push(cexpiryyear);
                //  for (var i = 1; i < 50; i++) {
                //   cperange.push(cexpiryyear - i);
                //  }
                //  $scope.cexpiryyears = cperange;
           }
          // childmonthchange..for..passport..expiry..
     $scope.cpedays = 31;
     $scope.changechildpeMonth = function(selectedMonth) {
             var selectedpeMonth = selectedMonth.child.personaldata.expirymonth || 0;
             if (selectedpeMonth == 2) {
                 $scope.cpedays = 28;
             } else if (selectedpeMonth == 4 || selectedpeMonth == 6 || selectedpeMonth == 9 || selectedpeMonth == 11) {
                 $scope.cpedays = 30;
             } else {
                 $scope.cpedays = 31;
             }
            var currentday = new Date();
            var today = new Date().getDate();
            var cexpirymonth = currentday.getMonth()+1;
            // if (selectedpeMonth == cexpirymonth) {
                var selectedYear = selectedMonth.child.personaldata.expiryyear;
            if (selectedpeMonth == cexpirymonth && selectedYear ==pefullyear ) {
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var cpedate = [];
            for (var i = $scope.currentDate; i < $scope.cpedays; i += 1) {
                cpedate.push(i + 1);
            }
            $scope.cpedates = cpedate;
         }
         // infantmonthchange..
     $scope.idays = 31;
     $scope.changeinfantMonth = function(selectedMonth) {
         var selectedMonth = selectedMonth.infant.personaldata.birthmonth || 0;
         if (selectedMonth == 2) {
             $scope.idays = 28;
         } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
             $scope.idays = 30;
         } else {
             $scope.idays = 31;
         }
         var idobdate = [];
         for (var i = 0; i < $scope.idays; i += 1) {
             idobdate.push(i + 1);
         }
         $scope.idobdates = idobdate;
     }
        // infantmonthchange.for.passport...expirydate.
        $scope.ipedays = 31;
        var ipedate = [];
        for (var i = 0; i < $scope.ipedays; i += 1) {
            ipedate.push(i + 1);
        }
        $scope.ipedates = ipedate;
        $scope.changeinfantpeMonth = function(selectMonth) {
            console.log("selectedMonth",selectedMonth)
            var selectedMonth = selectMonth.infant.personaldata.expirymonth || 0;
            if (selectedMonth == 2) {
                $scope.ipedays = 28;
            } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                $scope.ipedays = 30;
            } else {
                $scope.ipedays = 31;
            }

            var currentday = new Date();
            var today = new Date().getDate();
            var iexpirymonth = currentday.getMonth()+1;
            var selectedYear = selectMonth.infant.personaldata.expiryyear;
            if (selectedMonth == iexpirymonth && selectedYear== pefullyear ) {
                // $scope.currentDate = iexpirymonth;
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var ipedate = [];
            for (var i = $scope.currentDate; i < $scope.ipedays; i += 1) {
                ipedate.push(i + 1);
            }
            $scope.ipedates = ipedate;
        }

 
 
 
    // ends.....dob and pe......
	
/*backloader   */
$scope.timeload = function(){
	$("#load").show();
	setTimeout(function () {
	//    test(); 
	   $('#load').hide();
	}, 500);
}
$scope.backloader=function() {
	$scope.timeload();
	if(localStorage.islogged == 'true'){
		$scope.$parent.bookingHide = true;
  $scope.$parent.bookingHide1 = true;
}else{
	  $scope.$parent.bookingHide = false;
  $scope.$parent.bookingHide1 = false;
}


	$location.path('/hotelDetails');
 }
 $scope.backdetail=function() {
	$scope.timeload();
	$("#trvl").removeClass("active");
	$scope.backtodetail = false;
	$scope.backtotraveler = false;
	$scope.validateguestdata = false;
	// $scope.validateservicedatapament = true;
	$scope.validatedata = false;
	$scope.loggedvalue = true;

}
$scope.backtraveler=function() {
	$scope.timeload();
	$("#serbill").removeClass("active");
	$("#serv").removeClass("active");
	$scope.backtodetail = true;
	$scope.backtotraveler = false;
	$scope.validateservicedatapament = true;
	$scope.validatedata = true;
	$scope.validateSpecialServicereq = false;


}
	
if($scope.repriceResp.roomDetails!=null){
		$scope.hotelGrandTotalFare=[];
		$scope.grandTotal=0;
		if( $scope.repriceResp.provider=='GRN'){
			$scope.grandTotal=$scope.repriceResp.grandTotal;
		}else{
			for(var sd=0; sd<$scope.repriceResp.roomDetails.length;sd++) {
	
	
			  
				  
			 
				var roomcombofare=	$scope.repriceResp.roomDetails[sd].roomFare
				
				$scope.grandTotal+=roomcombofare;
				 
		
			   
	
			  
			  
		  }
		}

	}






	

	$scope.cansalationResp = JSON.parse(sessionStorage.getItem('cancellationPolicy'));
	$scope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp'));
	$scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));
	
	
		$scope.getSearchJson = sessionStorage.getItem("hotelBookJson");//Retrieve the stored data
		$scope.myJson =  JSON.parse($scope.getSearchJson);
	
	
	$scope.selectedHotelRooms = JSON.parse(sessionStorage.getItem('selectedHotels'));
	$scope.getRindexArray = sessionStorage.getItem("rindexarray");//Retrieve the stored data
	$scope.rindexarray =  JSON.parse($scope.getRindexArray);
	$scope.selectedHotelResp1 = $scope.selectedHotelResp;
	$scope.starRating = $scope.selectedHotelResp1.data.starRating;
	$scope.currency = ConstantService.currency;
	$scope.user={};
	$scope.showSignIn = false;
	
	$scope.adultCount=0;
	$scope.childCount=0;
	
	$scope.selectedHotelRooms.forEach(function(roomCount){
		$scope.adultCount=$scope.adultCount+parseInt(roomCount.adultCount);
		$scope.childCount=$scope.childCount+parseInt(roomCount.childCount);
	
	})
	$scope.loggedvalue = JSON.parse(localStorage.getItem('islogged'));


$scope.guestuser={};
$scope.min = ($scope.hotelSearch.checkIn).split('/')[2] + '-' + ($scope.hotelSearch.checkIn).split('/')[1] + '-' + ($scope.hotelSearch.checkIn).split('/')[0]
var displayName = {};
var loginResp = {};
$scope.userBooking = {}
	$scope.proceedGuestLoginDetails = function (){
		if(document.getElementById('guestLogin').style.display=='block'){
		if($scope.guestuser.email != null && $scope.guestuser.contactNumber != null){
	
	$( "#email" ).parent().removeClass('has-error');	
	$( "#contact" ).parent().removeClass('has-error');	
	$("#ccode1").parent().removeClass('has-error');
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname = $scope.tokenresult.data;
	var headersOptions = {
	  'Content-Type': 'application/json',
	  'Authorization': 'Bearer ' + tokenname.authToken
	}
	$scope.artloading = true;
	var guestLoginData = '{ "username	" : "' + $scope.guestuser.email + '", "mobileNo" : "' +$scope.guestuser.contactNumber+ '"}'

	$http.post(ServerService.authPath+'guest-login/user', '{ "username" : "' + $scope.guestuser.email + '", "mobileNo" : "' +$scope.guestuser.contactNumber+ '"}',{
		headers: headersOptions
	}).then(function(response){
		$scope.artloading = false;
		if (response.data.authToken!=null){
            localStorage.setItem('authentication', JSON.stringify(response));
		if (response.data.alreadyRegistered==false) {
			$scope.backtodetail = true;
			
			$scope.validatedata=true;
			$scope.validateguestdata=true;
			$scope.loggedValue=true;
			$rootScope.bookingHide = true;
			$rootScope.guestLogin = true;
			
					   $rootScope.bookingHide1 = true; 
				
					   
					localStorage.setItem('loginToken', JSON.stringify(response.data));
					   localStorage.setItem('authentication', JSON.stringify(response));
					   $("#trvl").addClass("active");
					   $rootScope.dispName = $scope.guestuser.email ;
					   $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email =  $scope.guestuser.email ;
					   $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;

					   $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
					   sessionStorage.setItem('guestLoginDatails', JSON.stringify( $scope.guestuser.email));
					   
		}else{
			Lobibox.confirm({
			
				msg:'You have already registered, please enter your password to login or select No to Continue as Guest  ?!',
				callback: function ($this, type, ev) {
					if(type == 'yes') {
						$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
						var tokenname =$scope.tokenresult.data;
						var headersOptions = {
							'Content-Type': 'application/json',
							'Authorization': 'Bearer ' + tokenname.authToken
						}
						$http.get(ServerService.listPath + 'crud/list/language',{
							headers: headersOptions
						}).then(function(result){
							if(response.data.alreadyRegistered==true){
								$scope.user.emailId=$scope.guestuser.email;
							
					document.getElementById('loginContinue').style.display='block'
		
					document.getElementById('guestLogin').style.display='none';
							}
						})
		
					}else{
					    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
						var tokenname =$scope.tokenresult.data;
						var headersOptions = {
							'Content-Type': 'application/json',
							'Authorization': 'Bearer ' + tokenname.authToken
						}
						$http.get(ServerService.listPath + 'crud/list/language',{
							headers: headersOptions
						}).then(function(result){
							$scope.backtodetail = true;
						localStorage.isguestlogged = true;
						$scope.validatedata=true;
						$scope.validateguestdata=true;
						$scope.loggedValue=true;
						$rootScope.bookingHide = true;
						// $scope.$parent.guestLogin = true;
						 $rootScope.guestLogin = true;
								   $rootScope.bookingHide1 = true; 
								 
							
								   $rootScope.dispName = $scope.guestuser.email ;
								   $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email =  $scope.guestuser.email ;
								   $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;
			
								   $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
								   sessionStorage.setItem('guestLoginDatails', JSON.stringify( $scope.guestuser.email));
								   $("#trvl").addClass("active");	
		
						})


					
						
			}
		}
	});
	
} }else{
	Lobibox.alert('error', {
		msg: response.data.errorMessage
	});
}
	
		
	})
	
}else{
	// $( "#email" ).parent().addClass('has-error');	
	// $( "#contact" ).parent().addClass('has-error');	
	// $("#ccode1").parent().addClass('has-error');
}
		}else{
				document.getElementById('guestLogin').style.display='block';
				
				document.getElementById('loginContinue').style.display = 'none';
		
		}
		

}

// function datefunctioncall() {

// 	var currentDocumentTimestamp =
// 	new Date(performance.timing.domLoading).getTime();

// 	var now = Date.now();

// 	var tenSec = 10 * 1000;
	
// 	var plusTenSec = currentDocumentTimestamp + tenSec;
// 	if (now > plusTenSec) {
// 	location.reload();
// 	} else {}
// 	}
	
// datefunctioncall();

$scope.logindataReview=function(){
	var tokenname = $scope.tokenresult.data;
                var headersOptions = {
					"g-recaptcha-response" :$scope.token,
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokenname.authToken
                }
				$http.post(ServerService.authPath+'authenticate', '{ "username" : "'+ $scope.user.emailId +'", "password" : "'+ $scope.user.password +'"}',{
					headers: headersOptions
		   }).then(function successCallback(response){
			if(response != undefined){
					
            /* $http.post(ServerService.authPath + url, datas).then(function(response) { */
                $scope.loadingsignIn = false;
                $scope.loggedvalue=true;
			
				localStorage.setItem('loginToken', JSON.stringify(response.data));
					localStorage.setItem('authentication', JSON.stringify(response));

                if (response.data!=null) {
					var tokennames =response.data;
					var headersOptions = {
							'Content-Type': 'application/json',
							'Authorization': 'Bearer ' + tokennames.authToken
						}

					
					
				 $http.get(ServerService.authPath+'user-details',{
					 headers: headersOptions
			}).then(function(response){
                $scope.userId=response.data.userId;
             
                	sessionStorage.setItem('particulatUserData', JSON.stringify(response));
					$scope.loggedValue=true;
					$scope.loginsignIn = true;
						 $scope.$parent.bookingHide = true;
						 $scope.$parent.guestLogin = false;
									$scope.$parent.bookingHide1 = true; 
									 $scope.userBooking.bookingHide = true;
									$scope.userBooking.bookingHide1 = true;
									var userBookingdetails =  $scope.userBooking;
									localStorage.setItem('userBookingdetails', JSON.stringify(userBookingdetails));
									 displayName.fname = $scope.user.emailId ;	
									 $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email =  $scope.user.emailId
							
									 //displayName.email = response.data.data.profileData.personaldata.email;
									 //displayName.contactNumber = response.data.data.profileData.personaldata.mobile;
									 //displayName.mobcntrycode = response.data.data.profileData.personaldata.mobcntrycode;
									// sessionStorage.setItem('authentication', JSON.stringify(loginResp));
									localStorage.setItem('loginName', JSON.stringify(displayName))
									
									$rootScope.dispName =  response.data.sub;

									// window.location.reload();
					/* 	Lobibox.alert('success', {
						msg: "Login Successfully"
					}); */
					   $scope.validatedata = true;
			$scope.validateguestdata = true;
		
					
				 })
                } else {
                    Lobibox.alert('error', {
                        msg: response.data.errorMessage
                    })
                    $scope.showSignIn = true;
                }
			}else{
				Lobibox.alert('error', {
					msg: "Invalid Login Credential"
				})
                $scope.loadingsignIn = false;
				$scope.artloading = false;

			}
			}, function errorCallback(response) {
				if(response.status == 403){
					var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
					status.then(function(greeting) {
						$scope.logindataReview();
					});
				}
				if(response.status == 401){
					Lobibox.alert('error', {
						msg: "Invalid Login Credential"
					})
					// $scope.showSignIn = true;
					$scope.artloading = false;
				}
			  });
}


    $scope.login = function() {
		$scope.recaptchaId = JSON.parse(localStorage.getItem('configKeys'));
		$scope.backtodetail = true;
        if(document.getElementById('loginContinue').style.display=='block'){

        if ($scope.user.emailId != null && $scope.user.password != null) {
            $scope.loadingsignIn = true;
          
				var url = 'authenticate';
            var datas = '{ "username	" : "' + $scope.user.emailId + '", "password" : "' + $scope.user.password + '"}'
			grecaptcha.ready(function() {
                grecaptcha.execute($scope.recaptchaId.google_recaptcha_public_key, {})
                        .then(function(token) {
                            // sessionStorage.setItem("captcha",token);
                            $scope.token=token;
                            $scope.logindataReview();
                        });
            });
		
        } else {
            if ($("#password").val() != "") {
                $("#password").parent().removeClass('has-error');
                $("#emailId").parent().removeClass('has-error');
            } else {
                $("#password").parent().addClass('has-error');
                $("#emailId").parent().addClass('has-error');
            }
        }

    }else{
        document.getElementById('loginContinue').style.display='block'

		document.getElementById('guestLogin').style.display='none';
	}
}

// $scope.travellerToLoginPage = function() {
// 	$("#trvl").removeClass("active");

// 	$scope.validatedata = false;
// 	$scope.validateguestdata = false;
// }

var arrivalDateTime = $scope.hotelSearch.checkIn;

var depDateSplt = arrivalDateTime.split('/');
var minDepDate = new Date(depDateSplt[1] + '/' + depDateSplt[0] + '/' + depDateSplt[2]);
var minAdultYear = parseInt(minDepDate.getFullYear()) - 18;

var maxAdultEndYear = parseInt(minDepDate.getFullYear()) - 76;


if( $scope.hotelSearch.checkOut){
	$scope.min=( $scope.hotelSearch.checkOut).split('/')[2]+'-'+( $scope.hotelSearch.checkOut).split('/')[1]+'-'+( $scope.hotelSearch.checkOut).split('/')[0]
	var arrivalDateTime = $scope.hotelSearch.checkOut;
	var depDateSplt = arrivalDateTime.split('/');
		$scope.adultmaxDate = parseInt(depDateSplt[2]) - 18;
		
		$scope.adultmaxyear = $scope.adultmaxDate.toString()+'-'+ parseInt(depDateSplt[1])+'-'+parseInt(depDateSplt[0])
		$scope.adultminDate = parseInt(depDateSplt[2]) - 76;
		$scope.adultminyear = $scope.adultminDate.toString()+'-'+ parseInt(depDateSplt[1])+'-'+parseInt(depDateSplt[0])

		}else{
	// $scope.min=($scope.searchReq.onwardDateTime)
	$scope.min=( $scope.hotelSearch.checkIn).split('/')[2]+'-'+( $scope.hotelSearch.checkIn).split('/')[1]+'-'+( $scope.hotelSearch.checkIn).split('/')[0]

	var arrivalDateTime = $scope.hotelSearch.checkIn;
	var depDateSplt = arrivalDateTime.split('/');
	$scope.adultmaxDate = parseInt(depDateSplt[2]) - 18;
		
	$scope.adultmaxyear = $scope.adultmaxDate.toString()+'-'+ parseInt(depDateSplt[1])+'-'+parseInt(depDateSplt[0])
	$scope.adultminDate = parseInt(depDateSplt[2]) - 76;
	$scope.adultminyear = $scope.adultminDate.toString()+'-'+ parseInt(depDateSplt[1])+'-'+parseInt(depDateSplt[0])

		 }

		 if( $scope.hotelSearch.checkOut){
			$scope.min=( $scope.hotelSearch.checkOut).split('/')[2]+'-'+( $scope.hotelSearch.checkOut).split('/')[1]+'-'+( $scope.hotelSearch.checkOut).split('/')[0]
			var arrivalDateTime = $scope.hotelSearch.checkOut;
			var depDateSplt = arrivalDateTime.split('/');
				$scope.adultmaxDate = parseInt(depDateSplt[2]);
				
				$scope.childmaxyear = $scope.adultmaxDate.toString()+'-'+ parseInt(depDateSplt[1])+'-'+parseInt(depDateSplt[0])
				$scope.adultminDate = parseInt(depDateSplt[2]) - 18;
				$scope.chaildminyear = $scope.adultminDate.toString()+'-'+ parseInt(depDateSplt[1])+'-'+parseInt(depDateSplt[0])
		
				}


// $scope.adultDOB = {
// 	opens: "left",
// 	opens:"right",
// 	singleDatePicker: true,
// 	showDropdowns: true,
// 	autoApply: true,
// 	autoUpdateInput: true,

// 	startDate:new Date(minAdultYear,minDepDate.getMonth(),minDepDate.getDate()), 
		
// 		  endDate:new Date(maxAdultEndYear,minDepDate.getMonth(),minDepDate.getDate()),

// 	minDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate()),
// 	maxDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),

// 	eventHandlers: {
// 		'show.daterangepicker': function(ev, picker) {
// 			$scope.flightop = {
// 				singleDatePicker: true 
// 			}
// 		}
// 	},function (start, end, label) {
// 		var years = moment().diff(start, 'years');
// 		$scope.age = years;
	
// 		jQuery(this).attr('data-age', years);
// 	}
// }
// var childMaxYear = parseInt(minDepDate.getFullYear());
// var childMinYear = parseInt(minDepDate.getFullYear()) - 18;
// $scope.childDOB = {
// 	opens: "left",
// 	singleDatePicker: true,
// 	showDropdowns: true,
// 	autoApply: true,
// 	autoUpdateInput: true,
// 	minDate: new Date(childMinYear, minDepDate.getMonth(), minDepDate.getDate() + 1),
// 	maxDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),

// 	eventHandlers: {
// 		'show.daterangepicker': function(ev, picker) {
// 			$scope.flightop = {
// 				singleDatePicker: true 
// 			}
// 		}
// 	}
// }
// $scope.infantDOB = {
// 	opens: "left",
// 	singleDatePicker: true,
// 	showDropdowns: true,
// 	autoApply: true,
// 	autoUpdateInput: true,
// 	minYear: 1901,
// 	maxYear: parseInt(moment().format('YYYY'), 10),

// 	eventHandlers: {
// 		'show.daterangepicker': function(ev, picker) {
// 			$scope.flightop = {
// 				singleDatePicker: true 
// 			}
// 		}
// 	}
// }





	
	

	$scope.travelby = {}

	$scope.childTravelData=[];
	// $scope.adultTravelData=[];
// $scope.addTravel = function(value,indexdata) {

// $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
// 	sessionStorage.setItem('isUserCheck', value)
	
// 	if (value == true) {
	 
// 		$scope.adultTravelData=[];
// 				$scope.userDetails =$scope.allTravellerData [indexdata];
// 				$scope.adultTravelData.push($scope.userDetails);
				
		  
		   
		
// 	} else if (value == 'confirmTravelData') {
// 		if($scope.travellersListByRoom[0].adultTravellersList!=null){
// 			$scope.travellersListByRoom[0].adultTravellersList[0].personaldata= $scope.adultTravelData;
			
		
		
		 
// 		}if($scope.travellersListByRoom[0].childTravellersList!=null){
// 			$scope.travellersListByRoom[0].childTravellersList[0].personaldata = $scope.childTravelData;
			
// 		}
	
	   
// 	}
// }





	$scope.loginAndContinue=function(){
		// 	$scope.backtodetail = false;
    // $scope.backtotraveler = false;
	$scope.backtodetail = true;
	$scope.timeload();
		$("#trvl").addClass("active");
		$("#serv").removeClass("active");
		$("#serbill").removeClass("active");
		
		$scope.validatedata=true;
		$scope.validateguestdata=true;
		$scope.validateSpecialServicereq=false
		$window.scrollTo(0, 0);
		$scope.termsError=false
		$scope.validateservicedatapament=true;
	}
	
	    $scope.travelby = {}
    // $scope.addTravel = function(value,indexdata) {
	
	// $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
    //     sessionStorage.setItem('isUserCheck', value)
    //     if (value == true) {
         
        
    //                 $scope.userDetails =$scope.allTravellerData [indexdata].personaldata
					
	// 					if( $scope.userDetails.age>=12) {
	// 				     $scope.travellersListByRoom[0].adultTravellersList[0].personaldata = $scope.userDetails;
    //                 $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname = $scope.userDetails.nationalitycode;
	// 					}else if($scope.userDetails.age>=2 && age<12) {
						
	// 					  $scope.travellersListByRoom[0].childTravellersList[0].personaldata = $scope.userDetails;
    //                 $scope.travellersListByRoom[0].childTravellersList[0].personaldata.nationalityname = $scope.userDetails.nationalitycode;
						
						
	// 					}
              
               
            
    //     } else if (value == false) {
    //         $scope.travellersListByRoom[0].adultTravellersList[0].personaldata = null;
           
    //     }
    // }
	
	
	

    $scope.handler = function(event) {
        //   console.log("min date", $scope.min);
        var date = new Date($scope.min);

        // var month = date.getMonth() + 7; //months from 1-12
        // var day = date.getDate();
        // var year = date.getFullYear();
        date.setMonth(date.getMonth() + 6);
        // var newDate = year + '-' + month + '-' + day;
        //   console.log("date", date);
        var selectedDate = event.target.value;
        console.log('selected date', event.target.value);
        var date1 = new Date(selectedDate);
        if (date1 <= date) {
            //	Lobibox.notify('info', { size: 'mini', delay: 1500, msg: "Loreum ipsum asdfa asdfklj asdlkfj; ljadsf" });
            Lobibox.alert('info', //AVAILABLE TYPES: "error", "info", "success", "warning"
                {
                	msg: "Please note, if you travel to any country  other than your passport issued country, then as a general rule passports should have at least six months of validity.Please contact us for more information."
			});
	}
    }

	$scope.onCredit=function(){
		
		// $scope.cardType='CREDIT'
		 $scope.msg=" Please click on the below PAY NOW button to continue"
		//  $scope.paynw=""
	}
	$scope.onDebit=function(){
		
		//$scope.cardType='DEBIT'
		$scope.msg="Please click on the below PAY NOW button to continue"
	}
	var loginCredential ;
	$scope.isValidUser = false;
	var booking = {};
	
	
	/* $scope.user = JSON.stringify(sessionStorage.getItem("user"));
	if ($scope.user == undefined) {
		$scope.user = {};
	} */
	
	
	$scope.outdate =$scope.selectedHotelResp.data.checkOutDate;
	$scope.indate=$scope.selectedHotelResp.data.checkInDate;

 
 
 
 
 	

 
 	var formatDateone = $scope.selectedHotelResp.data.checkOutDate,
	 d=formatDateone.split("/");
	 month=d[1];
	 day=d[0];
	 year=d[2];
	 formatDate1= [month,day,year].join('/');
       

    

  
    
      var formatDatetwo = $scope.selectedHotelResp.data.checkInDate;
	 
      	 d2=formatDatetwo.split("/");
	 month=d2[1];
	day=d2[0];
	 year=d2[2];
	 formatDate2= [month,day,year].join('/');
  
	
	const finalCheckInDate = new Date(formatDate1);
	const finalCheckOutDate = new Date(formatDate2);
	const diffTime = Math.abs(finalCheckInDate - finalCheckOutDate);
	const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
	$scope.nights=diffDays;
	/* $scope.HotelroomIndex = JSON.parse(sessionStorage.getItem('HotelroomIndex'));
	var parentIndex = $scope.HotelroomIndex.parentIndex;
	var childIndex = $scope.HotelroomIndex.childIndex; */
	
	// $scope.nationality1 = JSON.parse(localStorage.getItem('nationality')).data;
	// $scope.nationality = JSON.stringify($scope.nationality1);
	$scope.salutations = ["Mr", "Mrs", "Miss"];
	$scope.childsalutations = ["Miss", "Mstr"];
	
	var personaldata = function () {
		this.gender = "M";
		this.salutation = "";
		this.fname = '';
		this.lname = '';
		this.dob = '';
		this.mobcntrycode = '';
		this.mobile = null;;
		this.email = null;
		this.nationalityname = $scope.hotelSearch.nationality;
		this.passportNo = null;
		this.trvtype="ADT";
		this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
		this.birthmonth="";
        this.birthdate="";
        this.birthyear="";
	}
	var childPersonalData = function () {
		this.gender = "M";
		this.salutation = "";
		this.fname = '';
		this.lname = '';
		this.dob = '';
		this.mobcntrycode = '';
		this.mobile = null;;
		this.email = null;
		this.nationalityname =  $scope.hotelSearch.nationality;
		this.passportNo = null;
		this.trvtype="CNN";
		this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
		this.birthmonth="";
        this.birthdate="";
        this.birthyear="";
	}
	var traveller = function () {
		this.personaldata = new personaldata();
		this.agntid = 0;
		this.profileid = 0;
		this.travellerRefRPH = 'A1';
	}
	var childTraveller = function () {
		this.personaldata = new childPersonalData();
		this.agntid = 0;
		this.profileid = 0;
		this.travellerRefRPH = 'C1';
	}
	var travellerByRoom = function () {
		this.adultTravellersList = [];
		this.childTravellersList = [];
	}
$("#contact").on("keyup", function(e){
			if($(this).val() === '0'){
					$(this).val('');
				}
		});
		
		$("#acontact_0").on("keyup", function(e){
			if($(this).val() === '0'){
					$(this).val('');
				}
		});
	$scope.month = Month;
	$scope.year = Year;
	/* signIn function */
	/* $scope.showSignIn = false; */
	/**We have to maintain same Order below until "update Traveller Details end" */
	var travellerByRoomObj = new travellerByRoom();
	$scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
	$scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))

	if($scope.selectedHotelResp1.data.fixedFormat==true){

		$totalHotalCombination=$scope.selectedHotelResp1.data.hotelRoomOccGroup.combinations;
		$scope.selectedHotelResp1.data.occGroupsForFixedRoomCombination=[];
		for(var r=0; r<$totalHotalCombination.length; r++){
		var hotelRoomOccGrp=[];
		var totalRooms=[];
for(var c=0; c<$totalHotalCombination[r].length; c++){
var index=$scope.selectedHotelResp1.data.hotelRoomOccGroup.combinations[r][c];

// totalRooms={};
 
 var tempRoomInfo=[];
 tempRoomInfo.push($scope.selectedHotelResp1.data.hotelRoomInfos[index]);

totalRooms.push({"roomInfos":tempRoomInfo});
}
// hotelRoomOccGrp.push(tempHotelRoomOccGrp);
$scope.selectedHotelResp1.data.occGroupsForFixedRoomCombination.push(totalRooms);
			
		}
		$scope.selectedHotelRooms=$scope.selectedHotelResp1.data.occGroupsForFixedRoomCombination;
		for(r=0; r<$scope.selectedHotelResp1.data.occGroupsForFixedRoomCombination.length; r++){
	
			$scope.selectedHotelResp1.data.occGroups=$scope.selectedHotelResp1.data.occGroupsForFixedRoomCombination[r]
			
			
		}
				$scope.selectedHotelRooms=$scope.selectedHotelResp1.data.occGroups;

	}else{

		$totalHotalCombination=$scope.selectedHotelResp1.data.hotelRoomOccGroup.combinations;
			$scope.selectedHotelResp1.data.occGroups=[];
			for(var r=0; r<$totalHotalCombination.length; r++){
				
		// $("#price_"+r).attr("checked", true);
			var hotelRoomOccGrp=[];
			var totalRooms=[];
for(var c=0; c<$totalHotalCombination[r].length; c++){
	var index=$scope.selectedHotelResp1.data.hotelRoomOccGroup.combinations[r][c];
	
	// totalRooms={};
	 
	 var tempRoomInfo=[];
	 tempRoomInfo.push($scope.selectedHotelResp1.data.hotelRoomInfos[index]);
	
	totalRooms.push({"roomInfos":tempRoomInfo});
}
// hotelRoomOccGrp.push(tempHotelRoomOccGrp);
$scope.selectedHotelResp1.data.occGroups.push(totalRooms);
// $("#price_"+r).prop("checked", true);
			}
			$scope.selectedHotelRoomsdata=$scope.selectedHotelResp1.data.occGroups;
			var occGroupsLength = $scope.rindexarray.length;
			for(var i=0; i<occGroupsLength; i++) {
				
					var parentIndex = $scope.rindexarray[i].parentIndex;
					var rindex = $scope.rindexarray[i].rindex;
					$scope.selectedHotelRooms.push($scope.selectedHotelRoomsdata[parentIndex][rindex].roomInfos[0]);
			}

}






	if($scope.selectedHotelResp1.data.fixedFormat==false){
	if ($scope.travellersListByRoom == undefined) {
		$scope.travellersListByRoom = [];
		for (i = 0; i < $scope.selectedHotelResp1.data.occGroups.length; i++) {
			var travellerByRoomObj = new travellerByRoom();
			for (j = 0; j < $scope.selectedHotelResp1.data.occGroups[i][i].roomInfos[0].adultCount; j++) {
				var travellerObj = new traveller();
				travellerByRoomObj.adultTravellersList.push(travellerObj);
				}
			for (j = 0; j < $scope.selectedHotelResp1.data.occGroups[i][i].roomInfos[0].childCount; j++) {
				var travellerObj = new childTraveller();
				travellerByRoomObj.childTravellersList.push(travellerObj);
			}
			$scope.travellersListByRoom.push(travellerByRoomObj);
		}

	}

}
$scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
$scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))
  $scope.guestLoginName= JSON.parse(sessionStorage.getItem("guestLoginDatails"))
    if(sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails')!="null"){
        $scope.loggedvalue=true;
        $scope.$parent.bookingHide = true;
        $scope.$parent.guestLogin = true;
        $scope.$parent.bookingHide1 = true;
        $rootScope.dispName = $scope.guestLoginName;
	}
	if ($scope.userDetails != null) {
		$scope.user = $scope.userDetails;
	}
	if(localStorage.getItem('loginToken') && localStorage.getItem('loginToken')!="null"){
		$scope.loggedvalue=true;
	}
	var loginUser="";
			/* check user Already login*/
			if(localStorage.getItem('loginName')&&localStorage.getItem('loginName')!="null"){
				$scope.isValidUser=true;
				$scope.loggedvalue=true;
				$rootScope.loginsignIn=true;
				/* $scope.user={}; */
				 loginUser= JSON.parse(localStorage.getItem('loginName'));
				$scope.user.email=loginUser.email;
				$scope.user.contactNumber = parseInt(loginUser.contactNumber);
				$scope.user.mobcntrycode = loginUser.mobcntrycode;
				
				$scope.userDetails = JSON.parse(sessionStorage.getItem('userDetails'));
		// $scope.isUserCheck = JSON.parse(sessionStorage.getItem('isUserCheck'));
				
		/*check user Already login end*/
			/**update Traveller Details */

	if ($scope.travellersListByRoom == undefined) {
		$scope.travellersListByRoom = [];
		for (i = 0; i < $scope.selectedHotelResp1.data.occGroups.length; i++) {
			var travellerByRoomObj = new travellerByRoom();
			for (j = 0; j < $scope.selectedHotelResp1.data.occGroups[i].roomInfos[0].adultCount; j++) {
				var travellerObj = new traveller();
				/* travellerByRoomObj.adultTravellersList.push(travellerObj);
				travellerObj.personaldata.email=$scope.user.email;
				travellerObj.personaldata.mobile=$scope.user.contactNumber;
				 travellerObj.personaldata.mobcntrycode=$scope.user.mobcntrycode; */  
				 
				 if(i == 0 && $scope.isUserCheck == true){
				 if(j == 0){
					 travellerByRoomObj.adultTravellersList[i] = travellerObj;
					 travellerObj.personaldata = $scope.userDetails;
					 travellerByRoomObj.adultTravellersList[0] = travellerObj;
					 
				 }else {
					 travellerByRoomObj.adultTravellersList.push(travellerObj);
				travellerObj.personaldata.email=$scope.user.email;
				travellerObj.personaldata.mobile=$scope.user.contactNumber;
				 travellerObj.personaldata.mobcntrycode=$scope.user.mobcntrycode;
					 
				 }
				 
				 } else {
					 travellerByRoomObj.adultTravellersList.push(travellerObj);
				travellerObj.personaldata.email=$scope.user.email;
				travellerObj.personaldata.mobile=$scope.user.contactNumber;
				 travellerObj.personaldata.mobcntrycode=$scope.user.mobcntrycode;
				 }
				 
			}
			for (j = 0; j < $scope.selectedHotelResp1.data.occGroups[i].roomInfos[0].childCount; j++) {
				var travellerObj = new childTraveller();
				travellerByRoomObj.childTravellersList.push(travellerObj);
				travellerObj.personaldata.email=$scope.user.email;
				travellerObj.personaldata.mobile=$scope.user.contactNumber;
				  travellerObj.personaldata.mobcntrycode=$scope.user.mobcntrycode;  
			}
			$scope.travellersListByRoom.push(travellerByRoomObj);
		}

	}
 
			} else {
				if ($scope.travellersListByRoom == undefined) {
		$scope.travellersListByRoom = [];
		for (i = 0; i < $scope.selectedHotelResp1.data.occGroups.length; i++) {
			var travellerByRoomObj = new travellerByRoom();
			for (j = 0; j < $scope.selectedHotelResp1.data.occGroups[i].roomInfos[0].adultCount; j++) {
				var travellerObj = new traveller();
				travellerByRoomObj.adultTravellersList.push(travellerObj);
				}
			for (j = 0; j < $scope.selectedHotelResp1.data.occGroups[i].roomInfos[0].childCount; j++) {
				var travellerObj = new childTraveller();
				travellerByRoomObj.childTravellersList.push(travellerObj);
			}
			$scope.travellersListByRoom.push(travellerByRoomObj);
		}

	}
				}

		
		
	$scope.selectpaxChecked = function(i,pax,paxAge) {
		document.getElementById('checkedvalue'+i).checked = true;
		$scope.addTravel('true',pax,paxAge,i);
	}
		
	document.querySelector(".guestmobilenumm").addEventListener("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });
		/* userChange function */
		
  $scope.travelby = {}
    var adultCount = $scope.travellersListByRoom[0].adultTravellersList.length;
    var chaildCount = $scope.travellersListByRoom[0].childTravellersList.length;

    $scope.addTravel = function(checked, singlePsgr, age, indexvalue) {
		let inputs = document.getElementById('checkedvalue'+indexvalue);
        $scope.particularindexvalue=indexvalue;
        $scope.checked=inputs.checked
        //  $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
        $scope.usersObj = {};
        $scope.usersObj.salutation = singlePsgr.salutation;
        $scope.usersObj.fname = singlePsgr.firstName
        $scope.usersObj.lname = singlePsgr.lastName
		$scope.usersObj.customerId = singlePsgr.customerId
        var unwantedCharacter = "0";
        var dob=singlePsgr.dateOfBirth.split("/");
        var birthdate=dob[0];
        if( birthdate.charAt( 0 ) === unwantedCharacter ){
            birthdate = birthdate.slice( 1 );

        }
        var birthmonth=dob[1];
        
       
        if( birthmonth.charAt( 0 ) === unwantedCharacter ){
            birthmonth = birthmonth.slice( 1 );

        }
        $scope.usersObj.birthdate=birthdate;

        $scope.usersObj.birthmonth=birthmonth;

        $scope.usersObj.birthyear=dob[2];
       
		var  mobcntrycodeObj=[];
		if(singlePsgr.countryCode === '' || singlePsgr.countryCode === null || singlePsgr.countryCode === undefined){
            $scope.usersObj.mobcntrycodetrobj = singlePsgr.countryCode;
        }else{
        for(var c=0; c<$scope.countryList.length; c++){
            if($scope.countryList[c].name==singlePsgr.countryCode){
                 mobcntrycodeObj.push($scope.countryList[c]);
            }
        }
		$scope.usersObj.mobcntrycode = mobcntrycodeObj[0];
	}
         $scope.usersObj.mobcntrycode = mobcntrycodeObj[0];
		$scope.usersObj.mobcntrycodetrobj = mobcntrycodeObj[0];
        $scope.usersObj.mobile = singlePsgr.phoneNumber
        $scope.usersObj.email = singlePsgr.emailId

        $scope.usersObj.nationalityname = singlePsgr.nationality
        $scope.usersObj.passportNo = singlePsgr.passportNo
        if(singlePsgr.passportExpiryDate!=null){
        var passExp=singlePsgr.passportExpiryDate.split("/");
        var expirydate=passExp[0];
        if( expirydate.charAt( 0 ) === unwantedCharacter ){
            expirydate = expirydate.slice( 1 );

        }
        var expMonth = passExp[1];
       
        if( expMonth.charAt( 0 ) === unwantedCharacter ){
            expMonth = expMonth.slice( 1 );

        }
        $scope.usersObj.expirydate=expirydate
        $scope.usersObj.expirymonth=expMonth;
        $scope.usersObj.expiryyear=passExp[2];
    }
        $scope.usersObj.nationalitycode =singlePsgr.saudiNationalityId
        $scope.usersObj.passIssueGovt =singlePsgr.passportIssueGovt
            //  var adultCount=$scope.travellersListByRoom[0].adultTravellersList.length;
        if (age >= 19) {
            $scope.usersObj.clsname = 'adultage';
            $scope.usersObj.trvtype = 'ADT';
            if ( $scope.checked) {
                if (adultCount != 0) {
                    $scope.usersObj=$scope.usersObj;
                   
                } else {
                  
                }








            } else {
                if (adultCount != $scope.travellersListByRoom[0].adultTravellersList.length)
                    adultCount++
                    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = "";

            }
        } else if (age >= 0 && age < 18) {
            $scope.usersObj.trvtype = 'CNN';
            if ( $scope.checked) {
                // if (chaildCount != 0) {
                    /* chaildCount-- */
                    $scope.childusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.usersObj


                  
                // } else {
                   
                // }








            } else {
                if (chaildCount != $scope.travellersListByRoom[0].childTravellersList.length)
                    chaildCount++
                    $scope.travellersListByRoom[0].childTravellersList[$scope.childIndex].personaldata = "";

            }


        } else {
            $scope.usersObj.trvtype = 'INF';
            if ( $scope.checked) {
                if (infantCount != 0) {
                    /* chaildCount-- */
                    $scope.usersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.usersObj


                    // Lobibox.notify('success', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
                    //     delay: 1500
                    // });
                } else {
                    // Lobibox.notify('error', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: "Infant limit Reached!",
                    //     delay: 1500
                    // });
                }








            } else {
                if (infantCount != $scope.travellersListByRoom[0].infantTravellersList.length)
                infantCount++
                    $scope.travellersListByRoom[0].infantTravellersList[$scope.infantIndex].personaldata = "";

            }
        }

    }
	
	  $scope.ConfirmTravellerData=function(){
        if($scope.TravelType=='adult'){
if($scope.TravelType=='adult' && $scope.usersObj!=undefined &&  $scope.checked==true){
    $scope.travellersListByRoom[$scope.roomIndex].adultTravellersList[$scope.adultIndex].personaldata = $scope.usersObj


    Lobibox.notify('success', {
        size: 'mini',
        position: 'top right',
        msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
        delay: 1500
    });
    $scope.modalInstance.dismiss();
}else{
    Lobibox.notify('error', {
        size: 'mini',
        position: 'top right',
        msg: "Please select at least one traveller!",
        delay: 1500
    });
}
        } else if($scope.TravelType=='child'){
            if($scope.TravelType=='child' && $scope.childusersObj!=undefined &&  $scope.checked==true){
                $scope.travellersListByRoom[$scope.roomIndex].childTravellersList[ $scope.childIndex].personaldata = $scope.childusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.childusersObj.fname + " " + $scope.childusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg: "Please select at least one traveller!",
                    delay: 1500
                });
            }
        }

    }


	$scope.adultsearchTrvlr=function(adultindexvalue,travelType,roomIndex){
        $scope.modalInstance=$uibModal.open({
            templateUrl: 'myTestModal.tmpl.html',
            backdrop: 'static',
			keyboard: true,
            scope:$scope
        });
		$scope.usersObj = undefined;
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
            $scope.allTravellerData=[];
            $scope.TravelType=travelType;
            $scope.adultIndex=adultindexvalue;
			$scope.roomIndex=roomIndex;
                 $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                     for(var t=0;  t<$scope.allTravellerDataa.length; t++){
						
    if( $scope.allTravellerDataa[t].age>= 19){
        $scope.allTravellerData.push($scope.allTravellerDataa[t])
    }
                     }
                     if($scope.allTravellerData.length==0) {
                        $scope.childText=" Traveller Data matching the age criteria given is not available."
                    }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.adultsearchTrvlr();
                });
            }
          });
    
    }




	$scope.childsearchTrvlr = function(childindexvalue,travelType,roomIndex) {
        $scope.modalInstance=$uibModal.open({
            templateUrl: 'myTestModal.tmpl.html',
            backdrop: 'static',
			keyboard: true,
            scope:$scope
        });
		$scope.childusersObj = undefined;
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
            $scope.allTravellerData=[];
        $scope.childIndex=childindexvalue;
		$scope.roomIndex=roomIndex;
        $scope.TravelType=travelType;
                 $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                 for(var t=0;  t<$scope.allTravellerDataa.length; t++){
					
 if($scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age <= 18){
    $scope.allTravellerData.push($scope.allTravellerDataa[t])
}
                 }
                if($scope.allTravellerData.length==0) {
                    $scope.childText="Traveller Data matching the age criteria given is not available."
                }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.childsearchTrvlr();
                });
            }
          });
      
    }

	$scope.close=function(){
        $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
    };
$scope.travelby = {}

$scope.userChange = function(value){
	
	sessionStorage.setItem('isUserCheck', value)
	if(value == true){
		//if ($scope.travellersListByRoom == undefined) {
			//$scope.travellersListByRoom = []
		$scope.travelby.count = 10;
				$scope.travelby.country = null;
				$scope.travelby.dateOnProcess = "15/01/2019";
				$scope.travelby.operation = "PROFILE";
				$scope.travelby.page = 1;
				$http.post(ServerService.serverPath+'rest/search/profile', $scope.travelby).then(function(response){
					
					if(response.data.status == 'FAILURE'){
						Lobibox.alert('error', {
							msg : response.data.errorMessage
							})
							$scope.isUserCheck  = false;
					} else if(response.data.status == 'SUCCESS'){
					$scope.userDetails = response.data.data.travellerBeanList[0].personaldata
					sessionStorage.setItem('userDetails', JSON.stringify($scope.userDetails));
					//$scope.userDetails = JSON.parse(sessionStorage.getItem("userDetails"));
					if($scope.travellersListByRoom=='undefined'){
						$scope.travellersListByRoom.push(travellerByRoomObj);
						if(travellersListByRoom.adultTravellersList.length==0||travellersListByRoom.adultTravellersList=='undefined'){
							var travellerObj = new traveller();
							travellersListByRoom.adultTravellersList.push(travellerObj);
						}
					}
					
					$scope.travellersListByRoom[0].adultTravellersList[0].personaldata=$scope.userDetails;
					$scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname = $scope.userDetails.nationalitycode;
							
						
					
			//	}
					//window.location.reload()
					/* sessionStorage.setItem("contactDetails", JSON.stringify($scope.userDetails))
					$scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))
					for(var a=0; a< $scope.travellersListByRoom.length; a++){
						if(a == 0){
						var travellerObj = new traveller() ;
							travellerByRoomObj.adultTravellersList[0] = travellerObj;
					travellerObj.personaldata = $scope.userDetails
					travellerObj.personaldata.salutation = $scope.userDetails.salutation
					
					$scope.salutations[0] = $scope.userDetails.salutation;
					
					
						$scope.travellersListByRoom[a] = travellerByRoomObj
						}
					} */
				
						}
					})
					
	 
	} else if(value == false){
		//window.location.reload()
		if (window.performance) {
			console.info("window.performance works fine on this browser");
		//	$scope.userDetails = JSON.parse(sessionStorage.getItem("userDetails"));
		$scope.userDetails = {}
			
			sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom));
			$scope.travellersByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
			$scope.travellersListByRoom[0].adultTravellersList[0]= $scope.userDetails;
			//$scope.travellersListByRoom.push(userDetails);
			
		  }
	
	/* var travellerObj = new traveller() ;
						$scope.userDetails = {}
						
					travellerByRoomObj.adultTravellersList[0] = travellerObj;
					
					travellerObj.personaldata = $scope.userDetails
					$scope.salutations[0] = "Mr"; */
	}
}


/* if(sessionStorage.getItem('contactDetails') != null){
	$scope.isUserCheck = JSON.parse(sessionStorage.getItem('isUserCheck'));
if($scope.isUserCheck == true){
	
	var travellerObj = new traveller() ;
						for(var a=0; a< $scope.travellersListByRoom.length; a++){
						if(a == 0){
					travellerByRoomObj.adultTravellersList[0] = travellerObj;
					travellerObj.personaldata = $scope.userDetails
					$scope.travellersListByRoom[a] = travellerByRoomObj
						}
						}
					
		}
	
} */


/* end */
		
			
			/* end */
			
/*assign guestUserDetails to Travellers */
$scope.updateGustUserDetailsToTravller=function(){
	
	$scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = jQuery('#email').val();
	if ($scope.user.emailId) {

		$scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = $scope.user.emailId;

	}
	
	$scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;

	$scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;

}
/*assign guestUserDetails to Travellers end*/




/*assign Email Id and Mobile Number same for AllTravellers end*/
	/**
	@author Raghava Muramreddy
	to showing payment error message
	*/
	if (sessionStorage.getItem("paymentError")) {
		var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
		Lobibox.alert('error',
			{
				msg: paymentError.errorMessage,
				callback: function (lobibox, type) {
					if (type === 'ok') {
						sessionStorage.removeItem("paymentError");
					}
				}
			});

	}

	$scope.showSignInText = "Sign in to book faster";
	$scope.showSignIn = false;
	$scope.signIn = function () {
		$scope.showSignIn = $scope.showSignIn ? false : true;
		var buttonText = "Sign in to book faster";
		if ($scope.showSignIn == true) { var buttonText = "For Continue As Guest"; }
		else { var buttonText = "Sign in to book faster"; }

		$scope.showSignInText = buttonText;
	}
	$scope.goBack = function (path) {
		if (path == "/hotelResult") {
			sessionStorage.removeItem("selectedHotels");
			sessionStorage.removeItem("selectedHotelRoomsIndex");
			sessionStorage.removeItem("repriceResp");
		}
		$location.path(path);
	}
	/* end */
	
	/* AgencyTermsOfBusiness*/
	$scope.openAgencyTermsOfBusiness = function(size, parentSelector){
		
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'agencyTermsOfBusiness.html',
			controller : 'flightFareRulesContent',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	
	/* AgencyTermsOfBusiness End*/
	
	/* multiple date */
	var checkOutDate = $scope.selectedHotelResp.data.checkOutDate;
	var depDateSplt = checkOutDate.split('-');
	var minDepDate = new Date(depDateSplt[1] + '-' + depDateSplt[0] + '-' + depDateSplt[2]);
	var dt = daysBetween(new Date(), minDepDate) - 1;
	var chldMinDate = daysBetween(new Date(), minDepDate) + 2;





	/* end */

	/* $('[id*=adateOfBirth]').daterangepicker({
	singleDatePicker: true,
	showDropdowns: true,
	format: 'dd/mm/yyyy',
	minYear: 1901,
	maxYear: parseInt(moment(),10)
	}, function(start, end, label) {
	var years = moment().diff(start, 'years');
	alert("You are " + years + " years old!");
	})
	}, 2000)   */

	/* end */
	/* payNow function */

	$scope.payment = {}
	$scope.message = false;
	$scope.showCard = false;
	$scope.payNow = function () {
		var cardId = $('#cardNo').val();
		var firstDigit = cardId.charAt(0)
		var secondDigit = cardId.charAt(1)
		if (firstDigit == 3 && between(secondDigit, 4, 7) && cardId.length == 5) {
			$scope.path = 'american';
			$scope.message = false;
			$scope.showCard = true;
		} else if (firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length == 5) {
			$scope.path = 'dinners';
			$scope.message = false;
			$scope.showCard = true;
		} else if (((firstDigit == 5 && between(secondDigit, 1, 5)) || (firstDigit == 2 && between(secondDigit, 2, 7))) && cardId.length == 5) {
			$scope.path = 'master';
			$scope.message = false;
			$scope.showCard = true;
		} else if ((firstDigit == 5 || firstDigit == 6) && (cardId.length == 5)) {
			$scope.path = "metro";
			$scope.message = false;
			$scope.showCard = true;
		} else if ((firstDigit == 4) && (cardId.length == 5)) {
			$scope.path = 'visa';
			$scope.message = false;
			$scope.showCard = true;
		} else if (cardId.length == 0) {
			$scope.message = true;
			$scope.showCard = false;
		}
	}
	/* end */

	/* payment function*/
	$scope.paymentDetail = {}
	var expireDate = {}
	$scope.selectDoc = {}
	if( sessionStorage.getItem('paymentParameters') != null){
		$scope.paymentDetail = JSON.parse(sessionStorage.getItem('paymentParameters'));
	}

	$scope.validateCardDetails=function(){
		var flage=true;
		if(paymentDetail.cardNo==null){
			$('#cardNo').css({'background-color': 'red'});
			flage=false;
		}else{
			$('#cardNo').css({'background-color': ''});
		}
		if(paymentDetail.expireMonth==null){
			$('#expiremonth').css({'background-color': 'red'});
			flage=false;
		}else{
			$('#expiremonth').css({'background-color': ''});
		}
		if(paymentDetail.expireYear==null){
			$('#expireYrs').css({'background-color': 'red'});
			flage=false;
		}else{
			$('#expireYrs').css({'background-color': ''});
		}
		if(paymentDetail.cvv==null){
			$('#cardName').css({'background-color': 'red'});
			flage=false;
		}else{
			$('#cardName').css({'background-color': ''});
		}
		
		if(paymentDetail.cardName==null){
			$('#cardNo').css({'background-color': 'red'});
			flage=false;
		}else{
			$('#cardNo').css({'background-color': ''});
		}
	}


/* 
 	$(document).on('change keyup', '.required', function(e){
	let Disabled = true;
	 $(".required").each(function() {
	   let value = this.value
	   if ((value)&&(value.trim() !=''))
		   {
			 Disabled = false
		   }else{
			 Disabled = true
			 return false
		   }
	 });
	
	if(Disabled){
		 $('.toggle-disabled').prop("disabled", true);
	   }else{
		 $('.toggle-disabled').prop("disabled", false);
	   }
  }) 
 */

$scope.pamentToTraveller=function(){
	$scope.timeload();
	$("#serv").removeClass("active");

	$("#serbill").removeClass("active");
	$scope.termsError=false
$scope.validateservicedatapament=true;
$scope.validateSpecialServicereq=false;
$scope.validatedata=true;
}

$scope.travellerToLoginPage=function(){
	$scope.timeload();
	$window.scrollTo(0, 0);
$scope.backtodetail=false;
$scope.backtotraveler=false;
$scope.validatedata=false;
$scope.validateguestdata=false;
$scope.loggedvalue=true;
$scope.validateSpecialServicereq=false;
$scope.validateservicedatapament=true;
$("#trvl").removeClass("active");
$("#serv").removeClass("active");
$("#serbill").removeClass("active");


}

$scope.seatToTraveller=function(){
$scope.validatedata=true;
$scope.validateservicedata=false;


}

$scope.proceedCustomDetails=function(){
	$scope.validateSpecialServicereq=true;
	$scope.validatedata=false;
	$scope.validateservicedata=true;
}
 
$scope.hotelSpecialService=function(){
	$scope.timeload();
	
	$("#serbill").addClass("active");
$scope.validateservicedatapament=false; 
	 $scope.validateSpecialServicereq=false; 
	$scope.validatedata=false;
	$window.scrollTo(0, 0);
	/* $scope.validateservicedata=true; */
}



$scope.pamentToTravellerOne=function(){
	$scope.timeload();
	$("#serv").removeClass("active");
	$("#serbill").removeClass("active");
	$scope.validateservicedatapament=true;
	
	$scope.validatedata=true;
	$window.scrollTo(0, 0);
	
}


	$scope.hotelPayment = function (bookingWay, valid,value) {
		$scope.timeload();
		window.scroll(0,0);
		/* 	 $scope.validateSpecialServicereq=false;
$scope.validateservicedata=true;
$scope.validatedata=false;
$scope.validateguestdata=true;

$scope.validateservicedatapament=false; 
 */
		
var isChecked = $('#termsCheckbox').prop('checked');
		expireDate = $scope.paymentDetail.expireYear + $scope.paymentDetail.expireMonth
		$scope.submitted = true;
		var age = $(this).find('.adateOfBirth').attr('data-age');

		var cardId = $('#cardNo').val();
		var passengerform=true;

		if (valid) {

			var trlcount = 0;
			var trvltemplst = [];
			$scope.travellersListByRoom.forEach(element => {
				if(element.adultTravellersList.length>0)
				{
					element.adultTravellersList.forEach(ele=> {
						trvltemplst.push(ele.personaldata);
					});
				}
				if(element.childTravellersList.length>0)
				{
					element.childTravellersList.forEach(elec=> {
						trvltemplst.push(elec.personaldata);
					});
				}
			});

			var valueArr = trvltemplst.map(function(item){ return item.fname });
			var isDuplicate = valueArr.some(function(item, idx){ 
				return valueArr.indexOf(item) != idx 
			});

			var valueArr1 = trvltemplst.map(function(item){ return item.passportNo });
			var isPassNoDuplicate = valueArr1.some(function(item, idx){ 
				if(item != null && item != '')
				{
					return valueArr1.indexOf(item) != idx  
				}
			});

			var valueArr2 = trvltemplst.map(function(item){ return item.nationalitycode });
			var isNationIdDuplicate = valueArr2.some(function(item, idx){ 
				if(item != null && item != '')
				{
					return valueArr2.indexOf(item) != idx
				}
			});

			if(isDuplicate)
			{
				Lobibox.alert('error',
				{
					msg: "The first name should not be same for all the guests, please try again",
				});
				return;
			}

			if(isPassNoDuplicate)
			{
				Lobibox.alert('error',
				{
					msg: "The Passport No. should not be same for all the guests, please try again",
				});
				return;
			}

			if(isNationIdDuplicate)
			{
				Lobibox.alert('error',
				{
					msg: "The National ID. should not be same for all the guests, please try again",
				});
				return;
			}

			$scope.backtodetail = false;
            $scope.backtotraveler = true;
			$scope.errMsg = "";
			
			$scope.paymentDetail.firstName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.fname;
			$scope.paymentDetail.lastName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.lname;
			$scope.paymentDetail.emailAddress = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email;
			$scope.paymentDetail.countryCode = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname;
	
			var flag=false;
			if(passengerform==true){
				for(var r=0; r<$scope.travellersListByRoom.length; r++){
					var childAgCount=0;
					var roomBean=$scope.hotelSearch.roomBean[r];
					if($scope.travellersListByRoom[r].childTravellersList.length!=0 ){
				for(var a=0; a<$scope.travellersListByRoom[r].childTravellersList.length; a++){
				
				
				var childDob= $scope.travellersListByRoom[r].childTravellersList[a].personaldata.birthmonth+"/"+$scope.travellersListByRoom[r].childTravellersList[a].personaldata.birthdate+"/"+$scope.travellersListByRoom[r].childTravellersList[a].personaldata.birthyear;
			
				var dob = new Date(childDob);

				var month_diff = Date.now() - dob.getTime();
				
				
				var age_dt = new Date(month_diff); 
				
				   
				var year = age_dt.getUTCFullYear();
				
				
				var age = Math.abs(year - 1970);
				
				$scope.childAges=[];
					for(var cbean=0; cbean<roomBean.childrens[0].childBeans.length; cbean++){
						
						$scope.childAge=parseInt(roomBean.childrens[0].childBeans[cbean].age);
						$scope.childAges.push($scope.childAge)
						if(age==$scope.childAge)
						childAgCount++;
					}

				}
			}
			if($scope.travellersListByRoom[r].childTravellersList.length!=0 ){
				if(roomBean.childrens[0].childBeans.length!=childAgCount){
					Lobibox.alert('warning',
					{
						msg: "Child age entered does not match with the age provided at the time of search. <br>Room :"+[r+1]+" Child Ages "+[$scope.childAges]+"  not matched",
					});
					return;
				}
			}
			}
			}
			if (isChecked == false) {
			
				$scope.termsError = true;
				//$timeout(function(){$('.termsError').hide();},5000);
				return ;
			}
			if(!value){
				$("#serv").addClass("active");
				$("#serbill").addClass("active");
				 $scope.validateSpecialServicereq=true;
$scope.validateservicedata=true;
$scope.validatedata=false;
$scope.validateguestdata=true;

$scope.validateservicedatapament=false; 
				return;
			}
			$scope.loading = true;
		$scope.disabled = true;
		$scope.errMsg="";
			$scope.travellerdatalist = [];
			$('.adultTrvlr').each(function (index) {
				jQuery(this).attr('id', 'adultTrvlr_' + (index + 1))
			});
			$('.childTrvlr').each(function (index) {
				jQuery(this).attr('id', 'childTrvlr_' + (index + 1))
			});
			sessionStorage.setItem("contactDetails", JSON.stringify($scope.user))

			
			var bookingReqObj = new Object();
			if ($scope.nonRefundFlag == false) {
				bookingReqObj.reqCommand = 3;
			} else if ($scope.nonRefundFlag == true) {
				bookingReqObj.reqCommand = 1;
			}

		

			var roomCategories = new Array();
			for (i = 0; i < $scope.selectedHotelRooms.length; i++) {
				var roomObj = new Object();
				roomObj.status = $scope.selectedHotelRooms[i].status;
				roomObj.roomRefNo = $scope.selectedHotelRooms[i].roomRefNo;
				roomObj.roomCode = $scope.selectedHotelRooms[i].roomCode;
				roomObj.roomName = $scope.selectedHotelRooms[i].roomName;
				roomObj.roomCheckIn = $scope.selectedHotelRooms[i].roomCheckIn;
				roomObj.roomCheckOut = $scope.selectedHotelRooms[i].roomCheckOut;
				roomObj.roomType = $scope.selectedHotelRooms[i].roomType;
				roomObj.roomTypeChar = $scope.selectedHotelRooms[i].roomTypeChar;
				roomObj.rateBasis = $scope.selectedHotelRooms[i].rateBasis;
				roomObj.rateBasisCode = $scope.selectedHotelRooms[i].rateBasisCode;
				roomObj.mealBoard = $scope.selectedHotelRooms[i].mealBoard;
				roomObj.mealBoardDesc = $scope.selectedHotelRooms[i].mealBoardDesc;
				roomObj.isBindingPrice =$scope.selectedHotelRooms[i].isBindingPrice;
				roomObj.netCost = $scope.selectedHotelRooms[i].netCost;
				roomObj.grossCost = $scope.selectedHotelRooms[i].grossCost;
				roomObj.adultCount = $scope.selectedHotelRooms[i].adultCount;
				roomObj.childCount = $scope.selectedHotelRooms[i].childCount;
				roomObj.infantCount = $scope.selectedHotelRooms[i].infantCount;
				roomObj.xtrBeds = $scope.selectedHotelRooms[i].xtrBeds;
				roomObj.roomFare = $scope.selectedHotelRooms[i].roomFare;
				roomObj.servChrg = $scope.selectedHotelRooms[i].servChrg;
				roomObj.discount = $scope.selectedHotelRooms[i].discount;
				roomObj.childAges = $scope.selectedHotelRooms[i].childAges;
				roomObj.cancellationPolicy = $scope.selectedHotelRooms[i].cancellationPolicy;
				if ($scope.repriceResp.roomDetails[i].psngrNamesRequird != 0) {
					roomObj.psngrNamesRequird = $scope.repriceResp.roomDetails[i].psngrNamesRequird;
				}

			

			/* 	roomObj.allocations = $scope.repriceResp.roomDet ails[i].allocations;
				roomObj.amendmentPolicy = $scope.selectedHotelRooms[i].amendmentPolicy;
				roomObj.uniqueId = $scope.selectedHotelRooms[i].uniqueId;
				roomObj.alertMsg = $scope.selectedHotelRooms[i].alertMsg;
				roomObj.shrdBdng = $scope.selectedHotelRooms[i].sharedBdng;
				roomObj.grossWithOutDis = $scope.selectedHotelRooms[i].grsWithOutDiscount;
				roomObj.offerCode = $scope.selectedHotelRooms[i].offerCode;
				roomObj.comments = $scope.selectedHotelRooms[i].roomComments;
				roomObj.bkngId = $scope.selectedHotelRooms[i].bkngId;
				roomObj.cancellationChrgCode = $scope.selectedHotelRooms[i].cancellationChrgCode;
				roomObj.priceCode = $scope.selectedHotelRooms[i].priceCode;
				roomObj.offerDis = $scope.selectedHotelRooms[i].offerDiscount;
				roomObj.pymntGrndBy = $scope.selectedHotelRooms[i].paymentGuaranteedBy;
				roomObj.srvChrgType = $scope.selectedHotelRooms[i].servChrgType;
				roomObj.discChrgType = $scope.selectedHotelRooms[i].discChrgType;
				roomObj.markupAmount = $scope.selectedHotelRooms[i].markupAmount;
				roomObj.seqNo = $scope.selectedHotelRooms[i]. seqNo */
			roomObj = $scope.repriceResp.roomDetails[i];
				for(var a=0; a<$scope.travellersListByRoom[i].adultTravellersList.length; a++){
					if($scope.travellersListByRoom[i].adultTravellersList[a].personaldata.nationalitycode == undefined){
						$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.nationalityname;
					
				}
				// var adultPassExp=new Date($scope.travellersListByRoom[i].adultTravellersList[a].personaldata.passExp).toLocaleDateString();
				// var adultPassExp=$scope.travellersListByRoom[0].adultTravellersList[a].personaldata.passExp.toLocaleDateString();
				if($scope.travellersListByRoom[i].adultTravellersList[a].personaldata.expirydate!=null && $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.expirymonth!=null && $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.expiryyear!=null){
					$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.passExp=$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.expirydate+"/"+$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.expirymonth+"/"+$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.expiryyear;
				}else{
					$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.passExp=null;
				}
			
				// var adultDob=new Date($scope.travellersListByRoom[i].adultTravellersList[a].personaldata.dob).toLocaleDateString();
				$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.dob= $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.birthdate+"/"+$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.birthmonth+"/"+$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.birthyear;
				// adultDob.split('/')[1]+'/'+adultDob.split('/')[0]+'/'+adultDob.split('/')[2];
				// delete $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthdate;
                // delete $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthmonth;
                // delete $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthyear;
				$scope.travellersListByRoom[i].adultTravellersList[a].personaldata.mobcntrycode = $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.mobcntrycodetrobj.name;
			}
				for(var a=0; a<$scope.travellersListByRoom[i].childTravellersList.length; a++){
					if($scope.travellersListByRoom[i].childTravellersList[a].personaldata.nationalitycode == undefined){
						$scope.travellersListByRoom[i].childTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[i].childTravellersList[a].personaldata.nationalityname;
					
				}
				// var chaildPassExp=new Date($scope.travellersListByRoom[i].childTravellersList[a].personaldata.passExp).toLocaleDateString();
				
				if($scope.travellersListByRoom[i].childTravellersList[a].personaldata.expirydate!=null && $scope.travellersListByRoom[i].childTravellersList[a].personaldata.expirymonth!=null && $scope.travellersListByRoom[i].childTravellersList[a].personaldata.expiryyear!=null){
					$scope.travellersListByRoom[i].childTravellersList[a].personaldata.passExp=$scope.travellersListByRoom[i].childTravellersList[a].personaldata.expirydate+"/"+$scope.travellersListByRoom[i].childTravellersList[a].personaldata.expirymonth+"/"+$scope.travellersListByRoom[i].childTravellersList[a].personaldata.expiryyear;
				}else{
					$scope.travellersListByRoom[i].childTravellersList[a].personaldata.passExp=null;
				}
			
				// var childDob=new Date($scope.travellersListByRoom[i].childTravellersList[a].personaldata.dob).toLocaleDateString();
				$scope.travellersListByRoom[i].childTravellersList[a].personaldata.dob= $scope.travellersListByRoom[i].childTravellersList[a].personaldata.birthdate+"/"+$scope.travellersListByRoom[i].childTravellersList[a].personaldata.birthmonth+"/"+$scope.travellersListByRoom[i].childTravellersList[a].personaldata.birthyear;
				// childDob.split('/')[1]+'/'+childDob.split('/')[0]+'/'+childDob.split('/')[2];
				// delete $scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthdate;
                // delete $scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthmonth;
                // delete $scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthyear;
				if( $scope.travellersListByRoom[i].childTravellersList[a].personaldata.mobcntrycode==undefined ||$scope.travellersListByRoom[i].childTravellersList[a].personaldata.mobcntrycode=='' ||$scope.travellersListByRoom[i].childTravellersList[a].personaldata.mobcntrycode==null){
					$scope.travellersListByRoom[i].childTravellersList[a].personaldata.mobcntrycode=null; 
				}else{
					$scope.travellersListByRoom[i].childTravellersList[a].personaldata.mobcntrycode = $scope.travellersListByRoom[i].childTravellersList[a].personaldata.mobcntrycodetrobj.name;
				}
			    
				
				}
				
				roomObj.paxDtls = $scope.travellersListByRoom[i].adultTravellersList.concat($scope.travellersListByRoom[i].childTravellersList);

				roomCategories.push(roomObj);
			}
		


			
				
			
			bookingReqObj.sessionId=JSON.parse(sessionStorage.getItem('repriceRespSessionId')).sessionId,
						bookingReqObj.paymentMode = "PAYMENT";
						    bookingReqObj.paymentParameters=$scope.paymentDetail
                        
                //     bookingReqObj.paymentParameters.successUrl="http://3.6.54.119:8091/art/paymentRedirector.html",
                //    bookingReqObj.paymentParameters.failureUrl="http://3.6.54.119:8091/art/paymentRedirector.html"
                //    bookingReqObj.paymentParameters.cancelUrl="http://http://3.6.54.119:8091/art/#!/hotelReview"
						
				
				
				bookingReqObj.paymentParameters.successUrl="paymentRedirector.html",
				bookingReqObj.paymentParameters.failureUrl="paymentRedirector.html",
				bookingReqObj.paymentParameters.cancelUrl="#!/hotelReview",
	
				
				
				bookingReqObj.specialRequests = $scope.specialRequest; 
										bookingReqObj.roomCategories = roomCategories;
			
			
										
		/* $location.path('/hotelItenary'); */
		$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
			var tokenname =$scope.tokenresult.data;
			var headersOptions = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + tokenname.authToken
			}
	
  $scope.artloading = true;
  sessionStorage.setItem("paymentParameters", JSON.stringify(bookingReqObj.paymentParameters));
 localStorage.setItem('hotel/book', JSON.stringify(bookingReqObj))
			 		$http.post(ServerService.serverPath + 'hotel/book', JSON.stringify(bookingReqObj), {
						headers:headersOptions
					}).then(function (response) {
						sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom));
			
						$scope.artloading = false;
						if (response.data.status == 'success') {

						

								var data = response.data;
					var expiry_date = $scope.paymentDetail.expireYear + $scope.paymentDetail.expireMonth
					/* $('#hdn_access_code').attr('name', 'access_code');
					$('#hdn_access_code').attr('value', data.data.access_code);
					$('#hdn_language').attr('name', 'language');
					$('#hdn_language').attr('value', data.data.language);
					$('#hdn_merchant_id').attr('name', 'merchant_identifier');
					$('#hdn_merchant_id').attr('value', data.data.merchant_identifier);
					$('#hdn_merchant_ref').attr('name', 'merchant_reference');
					$('#hdn_merchant_ref').attr('value', data.data.merchant_reference);
					$('#hdn_merchant_ref').attr('value', data.data.merchant_reference);
					$('#hdn_return_url').attr('name', 'return_url');
					$('#hdn_return_url').attr('value', data.data.return_url);
					$('#hdn_serv_command').attr('name', 'service_command');
					$('#hdn_serv_command').attr('value', data.data.service_command);
					$('#hdn_signature').attr('name', 'signature');
					$('#hdn_signature').attr('value', data.data.signature);
					$('#hdn_tok_name').attr('name', 'token_name');
					$('#hdn_tok_name').attr('value', data.data.token_name);
					$('#paymentPage').attr('action', data.data.postUrl);
					$('#hdn_card_security_code').attr('name', 'card_security_code');
					$('#hdn_card_security_code').attr('value', $scope.paymentDetail.cvv);
					$('#hdn_card_holder_name').attr('name', 'card_holder_name');
					$('#hdn_card_holder_name').attr('value', $scope.paymentDetail.cardName);
					$('#hdn_card_number').attr('name', 'card_number');
					$('#hdn_card_number').attr('value', $scope.paymentDetail.cardNo);
					$('#hdn_expiry_date').attr('name', 'expiry_date');
					$('#hdn_expiry_date').attr('value', expiry_date);
					$('#paymentPage').submit(); */
					     window.open(data.data.hostedPageLink, "_self")
							
						} else if (response.data.status == 'FAILURE') {
							$scope.loading = false;
							$scope.disabled = false;
							Lobibox.alert('error',
								{
									msg: response.data.message
								});
						} else {
							$scope.loading = false;
							$scope.disabled = false;
							Lobibox.alert('error',
								{
									msg: 'Unexpected Error Occurred, Please Try Again Later'
								});
						}
					}) 
				
				
		

		}else{
			$('html,body').scrollTop(240);
			$scope.errMsg = "Enter All * (Mandatory) Fields";
			}
	}
	function daysBetween(date1, date2) {
		//Get 1 day in milliseconds
		var one_day = 1000 * 60 * 60 * 24;

		// Convert both dates to milliseconds
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();

		// Calculate the difference in milliseconds
		var difference_ms = date2_ms - date1_ms;

		// Convert back to days and return
		return Math.round(difference_ms / one_day);
	}

	var years;
	function getAge(dateString, dateType, deptDate) {
		//var now = new Date();
		var depdate = deptDate;
		var depdate2exp = depdate.split('/');
		var now = new Date(depdate2exp[2], depdate2exp[1] - 1, depdate2exp[0]);
		var today = new Date(now.getYear(), now.getMonth(), now.getDate());

		var yearNow = now.getYear();
		var monthNow = now.getMonth();
		var dateNow = now.getDate();

		if (dateType == 1)
			var dob = new Date(dateString.substring(0, 4),
				dateString.substring(4, 6) - 1,
				dateString.substring(6, 8));
		else if (dateType == 2)
			var dob = new Date(dateString.substring(0, 2),
				dateString.substring(2, 4) - 1,
				dateString.substring(4, 6));
		else if (dateType == 3)
			var dob = new Date(dateString.substring(6, 10),
				dateString.substring(3, 5) - 1,
				dateString.substring(0, 2));
		else if (dateType == 4)
			var dob = new Date(dateString.substring(6, 8),
				dateString.substring(3, 5) - 1,
				dateString.substring(0, 2));
		else
			return '';

		var yearDob = dob.getYear();
		var monthDob = dob.getMonth();
		var dateDob = dob.getDate();

		var yearAge = yearNow - yearDob;

		if (monthNow >= monthDob)
			var monthAge = monthNow - monthDob;
		else {
			yearAge--;
			var monthAge = 12 + monthNow - monthDob;
		}

		if (dateNow >= dateDob)
			var dateAge = dateNow - dateDob;
		else {
			monthAge--;
			var dateAge = 31 + dateNow - dateDob;

			if (monthAge < 0) {
				monthAge = 11;
				yearAge--;
			}
		}
		years = yearAge;
		//return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days';
	}



	$(document).ready(function () {  
		$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
			var tokenname =$scope.tokenresult.data;
			var headersOptions = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + tokenname.authToken
			}
	
		$http({
			method:'GET',
			url:ServerService.listPath+'list/nationality',		
		
			headers:headersOptions
			}).then(function(data,status){
			if(status ='success') {
				$scope.nationalityResult = data;
				$scope.allNationalitys=$scope.nationalityResult.data.data;
				$scope.nationality1 =	$scope.allNationalitys;
	$scope.nationality = JSON.stringify($scope.nationality1);
				} else {
				alert('error');
			}
			
		}); 
		$http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;
				for(var a=0; a<  $scope.countryList.length;a++ ){
                    if( $scope.countryList[a].name==='971'){
                        $scope.guestuser.ccode=$scope.countryList[1];
                    }
                }
                // $scope.allcountrys = response.data.data;
                // $scope.countryList = $scope.allcountrys.sort((a, b) => a.name.localeCompare(b.name));
                //     } else {
            }
        })

    // });
	});  

	/* end */

$scope.removeBR=function(string){
		string=string.replace(". <br/>",".");
		string=string.replace("<br/>",".");
		string=string.replace("</b>","");
		string=string.replace("<b>","");
		return string;

	}
	$scope.getNumber = function (num) {
		return new Array(num);
	}
	$scope.flightTermsDetails = function(size, parentSelector){
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        
        $http.get(ServerService.profilePath + 'company/latest/tandc/1', {
            headers: headersOptions
        }).then(function(response) {
            $scope.termsresp = response.data.data.content;
            // $scope.termsresp = "response.data.data.content;";
    
            //$window.location.href = '/terms_conditions';
            // $location.path('/terms_conditions');
        });
        $("#termsandConditions").modal({show:true});
		// var parentElem = parentSelector ? 
		// angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		// $scope.animationEnabled = true;
		// var modalInstance = $uibModal.open({
		// 	animation: $scope.animationEnabled,
		// 	ariaLabelledBy : 'modal-title',
		// 	ariaDescribedBy : 'modal-body',
		// 	templateUrl : 'termsnCondition.html',
		// 	controller : 'flightTermsCtrl',
		// 	backdrop: true,
		// 	size : size,
		// 	appendTo : parentElem,
		// 	resolve: {
		// 		items: function(){
		// 			return $scope.items
		// 		}
		// 	}
		// })
		}
});

ouaApp.controller('flightTermsCtrl', function($scope, $uibModalInstance,$rootScope){

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	/* modelDismiss function */
	$scope.modelDismiss = function(){
		$uibModalInstance.close();
		}
})

ouaApp.controller('flightFareRulesContent', function($scope, $uibModalInstance, $rootScope, stopsService){
	

	
	 

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	/* modelDismiss function */
	$scope.modelDismiss = function(){
		$uibModalInstance.close();
		}
	/* end */
	
});
