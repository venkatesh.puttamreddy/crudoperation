
ouaApp.controller('hotelLoadingCtrl', function($scope,$http,refreshService, $location,$rootScope, $timeout, ServerService){

$('html, body').scrollTop(0);
$scope.searching = "hotel"
$scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));

searchHotel = function(){
 $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
 var tokenname = $scope.tokenresult.data;
 var headersOptions = {
   'Content-Type': 'application/json',
   'Authorization': 'Bearer ' + tokenname.authToken
 }
 // sessionStorage.setItem("hotelSearchdataobj",angular.toJson($scope.hotelSearch));
 $scope.hotelSearchObj = $scope.hotelSearch;
 $scope.hotelSearchObj.city = $scope.hotelSearchObj.city.split(',')[0];

//  var City = $scope.hotelSearchObj.city.split(',')
//  $scope.hotelSearchObj = City[0];

 $http.post(ServerService.serverPath + 'hotel/availability', JSON.stringify($scope.hotelSearchObj), {

   headers: headersOptions
 }).then(function successCallback(response) {
   $scope.loading = false;
   $scope.disabled = false;
   if (response.data.status == 'success') {
       sessionStorage.setItem('hotelSearchResp',(response.data.sessionId));
       sessionStorage.setItem('hotelSearchResp1',JSON.stringify(response.data.data));
       $scope.minPrice=response.data.data.filterParams.priceBean.minPrice;
       $scope.maxPrice=response.data.data.filterParams.priceBean.maxPrice;
       sessionStorage.setItem('hotelMinPrice',($scope.minPrice));
       sessionStorage.setItem('hotelMaxPrice',(  $scope.maxPrice));
				// sessionStorage.setItem('hotelTotalData',JSON.stringify(data.data));

	   
   /*   sessionStorage.setItem('hotelSearchResp', JSON.stringify(response.data)); */
    //  sessionStorage.setItem("hotelSearchdataobj",JSON.stringify($scope.hotelSearch));
     $location.path('/hotelResult');
   } else {
     Lobibox.alert('error', {
       msg: response.data.errorMessage
     })
      $location.path('/hotel');
      $scope.loading = false;
      $scope.disabled = false;
   }

  }, function errorCallback(response) {
    if(response.status == 403){
        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
        status.then(function(greeting) {
          searchHotel();
        });
    }
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
}
searchHotel();

});

