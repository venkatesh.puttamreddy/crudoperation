ouaApp.controller('hotelResultController', function($scope, $http, $rootScope, $location, ajaxService, $uibModal, ConstantService, starService, ServerService, customSession) {
    $scope.isCollapsed = true;

    $scope.refineSearchShow = false;
    $scope.artloading = false;

   
    $scope.refineSearch = function() {
        $scope.refineSearchShow = !$scope.refineSearchShow;
    }
    $scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));
    $scope.totalPaxs = 0;
    for (var r = 0; r < $scope.hotelSearch.roomBean.length; r++) {
        var adultcount = $scope.hotelSearch.roomBean[r].adultCount;
        var childcount = $scope.hotelSearch.roomBean[r].childCount;
        $scope.totalPaxs += parseInt(adultcount) + parseInt(childcount);
    }

/* 	$scope.myInterval = 5000;

	$('.carousel').carousel()
	
	$('.carousel').carousel({
		interval: 2000
	  }) */

    $(".refine-search").click(function() {
        $(".flight-search").slideToggle();
    });
    $(".refine-search-close").click(function() {
        $(".flight-search").slideUp();
    });
    // $(window).scroll(function() {
    //     if (scroll >= 150)
    //       console.log("scroll111");
    //       $(".amenitiesfilter").addClass("active");
    //     if (scroll >= 2320)
    //     $(".amenitiesfilter").addClass("active");
    //     console.log("scroll1144441");
           
    // });
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }


    /* 	$scope.hotelSearchResp = JSON.parse(sessionStorage.getItem('hotelSearchResp')).data;
    	
    	  var priceStart = $scope.hotelSearchResp.filterParams.priceBean.minPrice;
      var priceEnd = $scope.hotelSearchResp.filterParams.priceBean.maxPrice;
                     $scope.hotelstars=$scope.hotelSearchResp.filterParams.starRatingBeans;
    	
    	 */
    var priceStart;
    var priceEnd;
    $scope.sessionvalue = sessionStorage.hotelSearchResp;
    $scope.filtermodel = {};
    $scope.filtermodel.sessionId = $scope.sessionvalue;
    $scope.filtermodel.defaultSortType = "PRICE";
    $scope.filtermodel.defaultSortOrder = "DESC";
    $scope.filtermodel.isOnlySorting = true;
    $scope.filtermodel.hotelName = "";
    $scope.filtermodel.priceBean = { minPrice: priceStart, maxPrice: priceEnd };


    $scope.filtermodel.amenitys = [];

    $scope.filtermodel.providers = [];
    $scope.filtermodel.luxurys = [];
    $scope.filtermodel.fireSafetyBeans = [];
    $scope.filtermodel.starRatings = [];


    $scope.isOnly=function(starvalue,indexvalue,filtervalue){
        $scope.hotelSearchResp = JSON.parse(sessionStorage.getItem("hotelSearchResp1"));
        var i = indexvalue;

var priceStart;
var priceEnd;
$scope.sessionvalue = sessionStorage.hotelSearchResp;
$scope.filtermodel = {};
$scope.filtermodel.sessionId = $scope.sessionvalue;
if($scope.clearFilter=='true'){
    
}
$scope.filtermodel.defaultSortType = "PRICE";
$scope.filtermodel.defaultSortOrder = "DESC";
$scope.filtermodel.isOnlySorting = true;
$scope.filtermodel.hotelName = "";
$scope.filtermodel.priceBean = { minPrice: priceStart, maxPrice: priceEnd };


$scope.filtermodel.amenitys = [];

$scope.filtermodel.providers = [];
$scope.filtermodel.luxurys = [];
$scope.filtermodel.fireSafetyBeans = [];
$scope.filtermodel.starRatings = [];
$scope.hotelSearchResp.filterParams.starRatingBeans.forEach(starBean => { //checking with actual list
    if (starBean.value === starvalue) {

        $scope.filtermodel.starRatings.push(starBean.value);
    }

});
if(filtervalue){
$scope.hotelSearchResp.filterParams.luxuryBeans.forEach(luxuryBean => { //checking with actual list
    if (luxuryBean.value === starvalue) {

        $scope.filtermodel.luxurys.push(luxuryBean.value);
        for(var s=0; s<$scope.hotelSearchResp.filterParams.luxuryBeans.length; s++){
            if(s==i){
             
                $('#checkboxlux_selected_' + s).prop('checked', true);
               
            }else{
                $('#checkboxlux_selected_' + s).prop('checked', false);
            }
                    }
    }

});
}
$scope.filtermodel.isOnlySorting = false;
if($scope.clearFilters==true){
    $scope.filtermodel.isOnlySorting = false;
    $scope.filtermodel.defaultSortType = "PRICE";
$scope.filtermodel.defaultSortOrder = "ASC";
}
$scope.artloading = true;
$http.post(ServerService.serverPath + 'hotel/filter', $scope.filtermodel, {
    headers: headersOptions
}).then(function(response) {
    $scope.artloading = false;
    if (response.data.success) {
        // $scope.priceStart=response.data.data.filterParams.priceBean.minPrice;
        // $scope.priceEnd=response.data.data.filterParams.priceBean.maxPrice;
     
        // let my_range = $(".js-range-slider").data("ionRangeSlider");
        // my_range.update({
        //     min:  $scope.priceStart,
        //     max:  $scope.priceEnd,
        //     from:  $scope.priceStart,
        //     to: $scope.priceEnd
        // });
        $scope.isSelected=false;
       
        for(var s=0; s<$scope.hotelSearchResp.filterParams.starRatingBeans.length; s++){
if(s==i){
 
    $('#checkbox_selected_' + s).prop('checked', true);
   
}else{
    $('#checkbox_selected_' + s).prop('checked', false);
}
        }
    
        $scope.hotelSearchResp = response.data.data;
     
       
      
  
        $scope.hotelSearchResp1 = $scope.hotelSearchResp.searchResults
        $scope.hotelName='';
        $scope.sortItem ="PRICE,ASC";
        // $scope.sortItem = sortItem.split(",");
        sessionStorage.setItem("hotelFilterData", JSON.stringify($scope.hotelSearchResp1))
    } else {
        Lobibox.alert('error', {
            msg: response.data.errorMessage
        });

    }
    $('html, body').scrollTop(0);
});
    }



    // Hotel Provider Bean Start

    $(document).ready(function() {
       
   
                $scope.hotelSearchResp = JSON.parse(sessionStorage.getItem("hotelSearchResp1"));
           
              



                $scope.priceStart = $scope.hotelSearchResp.filterParams.priceBean.minPrice;
                $scope.priceEnd = $scope.hotelSearchResp.filterParams.priceBean.maxPrice;
                $scope.hotelstars = $scope.hotelSearchResp.filterParams.starRatingBeans;
                $scope.hotelluxeryBeans = $scope.hotelSearchResp.filterParams.luxuryBeans



                // for (var index in $scope.hotelSearchResp.filterParams.providerBeans) {
                //     $scope.filtermodel.providers.push(

                //         $scope.hotelSearchResp.filterParams.providerBeans[index].code,


                //     );
                // }



                // Hotel starBean

                // for (var index in $scope.hotelSearchResp.filterParams.starRatingBeans) {
                //     $scope.filtermodel.starRatings.push(

                //         $scope.hotelSearchResp.filterParams.starRatingBeans[index].value,


                //     );
                // }


                // Hotel luxurys Bean Start

                // for (var index in $scope.hotelSearchResp.filterParams.luxuryBeans) {
                //     $scope.filtermodel.luxurys.push(

                //         $scope.hotelSearchResp.filterParams.luxuryBeans[index].value,


                //     );
                // }

                // Hotel amenity Bean Start

                // for (var index in $scope.hotelSearchResp.filterParams.amenityBeans) {
                //     $scope.filtermodel.amenitys.push(

                //         $scope.hotelSearchResp.filterParams.amenityBeans[index].code,


                //     );
                // }

                $scope.starRatingImg = ["../images/star/1Star.png", "../images/star/2Star.png", "../images/star/3Star.png", "../images/star/4Star.png", "../images/star/5Star.png"]
                $scope.totalHotels = $scope.hotelSearchResp.searchResults;
                $scope.totalNights = $scope.hotelSearchResp.duration;
                $scope.checkInDate = $scope.hotelSearchResp.checkInDate;
                $scope.checkOutDate = $scope.hotelSearchResp.checkOutDate;
                $scope.totalRooms = $scope.hotelSearchResp.totalRooms;
                $scope.country = $scope.hotelSearchResp.city + ',' + $scope.hotelSearchResp.country;

                $scope.passengerInfo = localStorage.getItem("passengerHotelInfo");
                $scope.filterParams = $scope.hotelSearchResp.filterParams;
                $scope.stars = $scope.filterParams.starRatingBeans;
                for (var i in $scope.filterParams.starRatingBeans) {
                    if ($scope.filterParams.starRatingBeans[i].value == "null")
                        $scope.filterParams.starRatingBeans[i].code = 1;
                    if ($scope.filterParams.starRatingBeans[i].value == "Star Rating")
                        $scope.filterParams.starRatingBeans[i].code = 2;
                    if ($scope.filterParams.starRatingBeans[i].value == "Key Rating")
                        $scope.filterParams.starRatingBeans[i].code = 3;
                    if ($scope.filterParams.starRatingBeans[i].value == "Official AA Rating")
                        $scope.filterParams.starRatingBeans[i].code = 4;
                    if ($scope.filterParams.starRatingBeans[i].value == "Official Visit England Rating")
                        $scope.filterParams.starRatingBeans[i].code = 5;
                    if ($scope.filterParams.starRatingBeans[i].value == "Official Visit Wales Rating")
                        $scope.filterParams.starRatingBeans[i].code = 6;
                }
                $scope.hotelSearchResp.filterParams.starRatingBeans.sort(function(obj1, obj2) {
                    // Ascending: first age less than the previous
                    return obj1.code - obj2.code;
                })

                $scope.starbeanRating = $scope.hotelSearchResp.filterParams.starRatingBeans;
                $scope.starss = [];
                angular.forEach($scope.starbeanRating, function(itemList) {
                    if (itemList.value != null) {
                        $scope.starss.push(itemList);
                    } else {
                        itemList.code = 1;
                        itemList.value = "No star";
                        $scope.starss.push(itemList);
                    }

                })
                $scope.currency = $scope.hotelSearchResp.currency;
                $scope.hotelSearchResp1 = $scope.hotelSearchResp.searchResults
                    // $scope.totalFlight = $scope.hotelSearchResp1;
                $scope.totalHotels = $scope.hotelSearchResp1;
                $scope.minPrice = $scope.filterParams.priceBean.minPrice;
                $scope.maxPrice = $scope.filterParams.priceBean.maxPrice;
                $scope.maximum = $scope.filterParams.priceBean.maxPrice;
                $scope.minimum = $scope.filterParams.priceBean.minPrice;
                $scope.starRatings = [];
                $scope.luxuryBeans = [];
                $scope.startsObjects = {}
                $scope.startsObjects.selectedStartList = [];
                $scope.luxuryObjects = {}
                $scope.luxuryObjects.selectedLuxuryList = [];
                $scope.amenityObjects = {}
                $scope.amenityObjects.selectedAmenityList = [];
                $scope.starLists = $scope.filterParams.starRatingBeans;
                $scope.luxaryList = $scope.filterParams.luxuryBeans;
                $scope.amenityList = $scope.filterParams.amenityBeans;



            

      
    });







    /* $scope.hotelSearchResp = JSON.parse(sessionStorage.getItem('hotelSearchResp')).data; */




    // star FILTERS
    var startCount = 0;
    $scope.starChange = function(starvalue, isSelected,indexvalue) {
        $scope.hotelSearchResp = JSON.parse(sessionStorage.getItem("hotelSearchResp1"));
           
        let inputs = document.getElementById('checkbox_selected_'+indexvalue);
        if (inputs.checked) {
            if (startCount == 0) {
                $scope.filtermodel.starRatings = [];
                startCount++;
            }
            $scope.hotelSearchResp.filterParams.starRatingBeans.forEach(starBean => { //checking with actual list
                if (starBean.value === starvalue) {

                    $scope.filtermodel.starRatings.push(starBean.value);
                }

            });
        } else {
            var i = 0;
            $scope.filtermodel.starRatings.forEach(existedAirlineCode => {
                if (existedAirlineCode === starvalue) {
                    $scope.filtermodel.starRatings.splice(i, 1);
                }
                i++;
            });
        }
        console.log($scope.filtermodel);
        $scope.filterParam();
    }


    // amenties FILTERS
    var amentyCount = 0;
    $scope.amenityChange = function(amentycode, isSelected) {
        if (isSelected) {
            if (amentyCount == 0) {	
                $scope.filtermodel.starRatings = [];
                amentyCount++;
            }
            $scope.hotelSearchResp.filterParams.amenityBeans.forEach(amentyBean => { //checking with actual list
                if (amentyBean.code === amentycode) {

                    $scope.filtermodel.amenitys.push(amentyBean.code);
                }

            });
        } else {
            var i = 0;
            $scope.filtermodel.amenitys.forEach(existedAmentyCode => {
                if (existedAmentyCode === amentycode) {
                    $scope.filtermodel.amenitys.splice(i, 1);
                }
                i++;
            });
        }
        console.log($scope.filtermodel);
        $scope.filterParam();
    }




    // luxurys FILTERS
    var luxuryyCount = 0;
    $scope.luxurysChange = function(luxuryyvalue, isSelected) {
        $scope.hotelSearchResp = JSON.parse(sessionStorage.getItem("hotelSearchResp1"));
        let inputs = document.getElementById('luxurysChange');
        if (inputs.checked) {
            if (luxuryyCount == 0) {
                $scope.filtermodel.starRatings = [];
                luxuryyCount++;
            }
            $scope.hotelSearchResp.filterParams.luxuryBeans.forEach(luxuryBean => { //checking with actual list
                if (luxuryBean.value === luxuryyvalue) {

                    $scope.filtermodel.luxurys.push(luxuryBean.value);
                }

            });
        } else {
            var i = 0;
            $scope.filtermodel.luxurys.forEach(existedAmentyCode => {
                if (existedAmentyCode === luxuryyvalue) {
                    $scope.filtermodel.luxurys.splice(i, 1);
                }
                i++;
            });
        }
        console.log($scope.filtermodel);
        $scope.filterParam();
    }




    $scope.checkAll = false;
    $scope.filter1 = [];
    $scope.sortItem = "PRICE,ASC";
    var sortList = $scope.sortItem.split(",");
    $scope.filtermodel.defaultSortType = sortList[0];
    $scope.filtermodel.defaultSortOrder = sortList[1];


    $scope.sort = function(sortItem) {
        var sortList = sortItem.split(",");
        $scope.filtermodel.defaultSortType = sortList[0];
        $scope.filtermodel.defaultSortOrder = sortList[1];
        $scope.filterParam();
    }

    $scope.hotelFilter = function() {


        if($scope.hotelName.length>=3){

            var UiValue = $scope.hotelName;
            $scope.filterParamObj.hotelName = $scope.hotelName;
            $scope.filtermodel.hotelName = $scope.hotelName;
            $scope.filterParam($scope.filterParamObj);
    
        }
        if($scope.hotelName.length==0){
            var UiValue = $scope.hotelName;
            $scope.filterParamObj.hotelName = $scope.hotelName;
            $scope.filtermodel.hotelName = $scope.hotelName;
            $scope.filterParam($scope.filterParamObj);
        }


    }


    $scope.priceStart = JSON.parse(sessionStorage.getItem('hotelMinPrice'));
    $scope.priceEnd = JSON.parse(sessionStorage.getItem('hotelMaxPrice'));
    $(document).ready(function() {
        $('.js-range-slider').ionRangeSlider({
            type: "double",
            min: $scope.priceStart,
            max: $scope.priceEnd,
            from: $scope.priceStart,
            to: $scope.priceEnd,
            grid: true,
            prefix: "AED ",
            onFinish: function(data) {
                var fromRange = data.from_pretty.replace(" ", "");
                var toRange = data.to_pretty.replace(" ", "");
                $scope.filtermodel.priceBean.minPrice = fromRange;
                $scope.filtermodel.priceBean.maxPrice = toRange;
                console.log($scope.filtermodel);
                $scope.filterParam();
            }
        });

        var start = moment('2016-10-02 00:00', 'YYYY-MM-DD HH:mm');
        var end = moment('2016-10-02 23:59', 'YYYY-MM-DD HH:mm');

        $('.departure-range-slider, .arrival-range-slider').ionRangeSlider({
            type: 'double',
            grid: true,
            min: start.format('x'),
            max: end.format('x'),
            step: 1800000,
            prettify: function(num) {
                return moment(num, 'x').format('h:mm A');
            },
        });
    });







    $scope.amentiesQuantity = 4;
    $scope.options = "More";
    $scope.moreAmenties = function() {
            if ($scope.options == "More") {
                $scope.options = "Less";
                $scope.amentiesQuantity = $scope.hotelSearchResp.filterParams.amenityBeans.length;
            } else {
                $scope.options = "More";
                $scope.amentiesQuantity = 4;
            }

        }
        // Hotel Amenties More End


    /* selectedHotel function */
    $scope.hotelResults = {};


    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }

    $scope.hotelBooking = function(selectingHotel) {
        
        $scope.artloading = true;
        // $scope.selectingHotel=$scope.hotelSearchResp.searchResults[hotelindex];
        var particularHotelDetails = {
            "sessionId": $scope.sessionvalue,
            "resultIndex": selectingHotel.index,

        }


        $http.post(ServerService.serverPath + 'hotel/details', particularHotelDetails, {

            headers: headersOptions
        }).then(function(response) {

          
            // console.log(response);
            if (response.data.success) {

                $scope.selectedHotelResp = response.data;
                sessionStorage.setItem('selectedHotelResp', JSON.stringify($scope.selectedHotelResp));
                $location.path('/hotelDetails')
               
            } else {
                $scope.artloading = false;
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }



        });





    }


    $scope.isAllStartsSelected = false;
    $scope.isAllLuxurySelected = false;
    $scope.isAllAmenitiesSelected = false;


    $scope.hotelFilter = function(hotel) {




        if($scope.hotelName.length>=3){

        var UiValue = $scope.hotelName;
        $scope.filterParamObj.hotelName = $scope.hotelName;
        $scope.filtermodel.hotelName = $scope.hotelName;
        $scope.filterParam($scope.filterParamObj);

    }
    if($scope.hotelName.length==0){
        var UiValue = $scope.hotelName;
        $scope.filterParamObj.hotelName = $scope.hotelName;
        $scope.filtermodel.hotelName = $scope.hotelName;
        $scope.filterParam($scope.filterParamObj);
    }


    }


    $scope.filterParamObj = {
        defaultSortType: "PRICE",
        defaultSortOrder: "ASC",
        isOnlySorting: false,
        hotelName: "",
        starRatings: null,
        priceBean: null,
        luxuryBeans: null,
        amenityBeans: null,
    }


    /* starToggle function */
    $scope.checkAll = false;








$scope.clearFilter=function(){
    // console.log("filter")
    // $scope.artloading = true;
    // $scope.filtermodel={};
$scope.clearFilters=true;
    $scope.isOnly();
    $scope.priceStart = JSON.parse(sessionStorage.getItem('hotelMinPrice'));
    $scope.priceEnd = JSON.parse(sessionStorage.getItem('hotelMaxPrice'));
    $(document).ready(function() {
        var instance = $(".js-range-slider").data("ionRangeSlider");

        instance.update({
            min: $scope.priceStart,
            max: $scope.priceEnd,
            from: $scope.priceStart,
            to: $scope.priceEnd,
        });
    });
   
}



    /* Price Filter Function End*/

    /*Hotel Name Filter Function*/
    $scope.hotelChange = function(searchHotelName) {
        $scope.hotelName = searchHotelName;
    }

    /**/

    /*
    Invoke Hotel Filter Service
    */

    // $scope.filtermodel.isOnlySorting = false;


    $scope.filterParam = function() {
        $scope.filtermodel.isOnlySorting = false;
        $scope.artloading = true;
        $http.post(ServerService.serverPath + 'hotel/filter', $scope.filtermodel, {
            headers: headersOptions
        }).then(function(response) {
            $scope.artloading = false;
            if (response.data.success) {

                $scope.hotelSearchResp = response.data.data;
                $scope.hotelSearchResp1 = $scope.hotelSearchResp.searchResults
                sessionStorage.setItem("hotelFilterData", JSON.stringify($scope.hotelSearchResp1))
            } else {
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });

            }
            $('html, body').scrollTop(0);
        });
    }

    /*
    	Invoke Hotel Filter Service end
    */

    $scope.hotelDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
    /* $scope.range = function(n) {
        n = Number(n);
        return new Array(n);
	}; */

    $scope.hotelLocation = function(size, parentSelector, latitude,longitude) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectlocationLatitude = latitude;
        $rootScope.selectlocationlongitude = longitude;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'hotellocation.html',
            controller: 'hotelmapControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.hotelImages = function(size, parentSelector, hoteldtls) {
        setTimeout(function () {
            angular.element(document.getElementById('#tiggerclick')).triggerHandler('click');
          }, 0);
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedhoteldtls = hoteldtls;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'hotelImage.html',
            controller: 'hoteldescDetailControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.hotelAmenties = function(size, parentSelector, hoteldtls) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedhoteldtls = hoteldtls;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'hotelAmenties.html',
            controller: 'hoteldescDetailControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.nearByAttraction = function(size, parentSelector, hoteldtls) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedhoteldtls = hoteldtls;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'nearByAttraction.html',
            controller: 'hoteldescDetailControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.nearByTransportation = function(size, parentSelector, hoteldtls) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedhoteldtls = hoteldtls;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'nearTransportaion.html',
            controller: 'hoteldescDetailControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    


    $scope.hoteldescDetails = function(size, parentSelector, hoteldtls) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedhoteldtls = hoteldtls;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'hoteldescDetail.html',
            controller: 'hoteldescriptionDetailControllers',
            windowClass: 'hotelresult-modal',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }


});
ouaApp.service('starService', function() {
    this.getStar = function(listing, starItem, luxItem) {
        var star = [];
        var hotelResp = listing;
        var hotelList = hotelResp.searchResults;
        for (var s = 0; s < hotelList.length; s++) {
            var temp = hotelList[s]
            var starValue = hotelResp.searchResults[s].starRating;
            if (starValue == starItem) {
                star.push(temp);
            }

        }
        //console.log(star);
        return star;
    }





})


ouaApp.controller('hoteldescDetailControllers', function($scope, $uibModalInstance, $rootScope, $http, ServerService,$timeout) {

    // var flightInfo=JSON.parse(sessionStorage.getItem('hotelSearchResp')).data.flightSearchResults[$rootScope.selectedBagIndex].indx

    // console.log(JSON.parse(sessionStorage.getItem('hotelSearchResp')))
    $scope.hotel_name = $rootScope.selectedhoteldtls.name;
    $scope.hotelamenities = $rootScope.selectedhoteldtls.htlAmenities
    $scope.hotelAmentiesList=[];
for(var i=0; i< $scope.hotelamenities.length; i++){

    $scope.hotelAmentiesList.push(" "+$scope.hotelamenities[i]);
}

    $scope.hotelnearestAirports = $rootScope.selectedhoteldtls.nearestAirports
    $scope.hotelnearestRailway = $rootScope.selectedhoteldtls.nearestRailway
    $scope.attractions = $rootScope.selectedhoteldtls.attractions
    $scope.galleryImgIndex = null;
    
	$scope.myInterval = 5000;
    $scope.thambnailChange = function(index){
        $scope.galleryImgIndex = index;
        $timeout(function(){
            $scope.galleryImgIndex = null;
        },1000)
    }
    $('#demo').bind('slide.bs.carousel', function () {
        alert("hi");
     
    //    var active =$(this).find('.active').index();
    //    var to = $(e.relatedTarget).index();
       
    //    if(active == 4 && to == 5) {
    //     alert("hi");
    //    }
    //    if(active == 9 && to == 0) {
    //      $('#thumbcarousel').carousel('next');
    //    }
     });
     $('.carousel-inner>.item').on('webkitAnimationEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
        alert("hi");
      })
	//  $('.carousel').carousel()
	$scope.carouselFunction=function(){
        $('.carousel').carousel({
            interval: 2000
          })
    }
	
    // $scope.hotelDescription = $rootScope.selectedhoteldtls.htlDesc

    $scope.hotelDescription = $rootScope.selectedhoteldtls.htlDesc
        // $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
        // var tokenname = $scope.tokenresult.data;

    // var headersOptions = {
    // 	'Content-Type': 'application/json',
    // 	'Authorization': 'Bearer ' + tokenname.authToken
    // }


    // 	var repriceDetails={
    // 	  "sessionId":JSON.parse(sessionStorage.getItem('flightSearchRS')).sessionId,
    // 	  "resultIndexId":flightInfo
    // 	}
    // 	// console.log($rootScope.selectedBagIndex );
    //    $http.post(ServerService.serverPath+'flight/ssr', repriceDetails,{ 

    // 	 headers: headersOptions
    // 	 }).then(function(response){
    // 	   console.log(response);
    // 	 });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

ouaApp.controller('hoteldescriptionDetailControllers', function($scope, $http, $uibModalInstance, $rootScope,  ServerService) {

    $scope.sessionvalue = sessionStorage.hotelSearchResp;
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    // $scope.selectingHotel=$scope.hotelSearchResp.searchResults[hotelindex];
    var particularHotelDetails = {
        "sessionId": $scope.sessionvalue,
        "resultIndex":  $rootScope.selectedhoteldtls.index,

    }


    $http.post(ServerService.serverPath + 'hotel/select', particularHotelDetails, {

        headers: headersOptions
    }).then(function(response) {

      
        // console.log(response);
        if (response.data.success) {

         
        
    $scope.hotelDescription = response.data.data.htlDesc
              $scope.hotel_name =  response.data.data.name;
        } else {
            $scope.artloading = false;
            Lobibox.alert('error', {
                msg: response.data.errorMessage
            });
        }



    });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

ouaApp.controller('hotelmapControllers', function($scope,$sce, $uibModalInstance, $rootScope, $http, ServerService) {

    // var flightInfo=JSON.parse(sessionStorage.getItem('hotelSearchResp')).data.flightSearchResults[$rootScope.selectedBagIndex].indx

    // console.log(JSON.parse(sessionStorage.getItem('hotelSearchResp')))

    $scope.latitude=$rootScope.selectlocationLatitude;
    $scope.longitude=$rootScope.selectlocationlongitude;
    $scope.hotellocation=$sce.trustAsResourceUrl('https://maps.google.com/maps?q='+  $scope.latitude+','+$scope.longitude+'&z=15&output=embed');
    // $scope.hotellocation='https://maps.google.com/maps?q='+  $scope.latitude+','+$scope.longitude+'&z=15&output=embed'

    // $scope.hotel_name = $rootScope.selectedhoteldtls.name;
    // $scope.hotelDescription = $rootScope.selectedhoteldtls.htlDesc
        // $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
        // var tokenname = $scope.tokenresult.data;

    // var headersOptions = {
    // 	'Content-Type': 'application/json',
    // 	'Authorization': 'Bearer ' + tokenname.authToken
    // }


    // 	var repriceDetails={
    // 	  "sessionId":JSON.parse(sessionStorage.getItem('flightSearchRS')).sessionId,
    // 	  "resultIndexId":flightInfo
    // 	}
    // 	// console.log($rootScope.selectedBagIndex );
    //    $http.post(ServerService.serverPath+'flight/ssr', repriceDetails,{ 

    // 	 headers: headersOptions
    // 	 }).then(function(response){
    // 	   console.log(response);
    // 	 });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});