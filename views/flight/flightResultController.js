ouaApp.controller('flightResultController', function($scope,refreshService, $timeout,$http, $location, $uibModal, $rootScope, ServerService) {
    // var Serverurl = "http://localhost:8090/ibp/api/v1/";
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    sessionStorage.removeItem('SelectedBagLst');
    sessionStorage.removeItem('SelectedMealLst');
    sessionStorage.removeItem('SelectedSeatLst');
    var priceStart=0;
    var priceEnd=0;


    $scope.moreOptionFun1 = function() {
        $scope.flightSearchResults1 = [];
        var result = {};
        $scope.flightSearchResults.forEach(function(val) {


            var key = val.segGrpList[0].segList[0].airlineName + ":" + val.grndTotal;

            result[key] = result[key] || { airlineName: val.segGrpList[0].segList[0].airlineName, grndTotal: val.grndTotal, airlineList: [] };
            result[key].airlineList.push(val);
        });

        $scope.showMore = [];
        for (var i = 0; i < $scope.flightSearchResults1.length; i++) {
            $scope.showMore[i] = false;

        }
        return Object.keys(result).map(function(key) {
            $scope.flightSearchResults1.push(result[key]);

        });




    }
	 
    $scope.filterFunction=function(){
        
    
            $(".js-range-slider").ionRangeSlider({
                type: "double",
                min: priceStart,
                max: priceEnd,
                from: priceStart,
                to: priceEnd,
                grid: true,
                prefix: "AED ",
                onFinish: function(data) {
                    var fromRange = data.from_pretty.replace(" ", "");
                    var toRange = data.to_pretty.replace(" ", "");
                    $scope.filtermodel.priceBean.fromRange = fromRange;
                    $scope.filtermodel.priceBean.toRange = toRange;
                    $scope.filterParam();
                }
            });
    
            var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
            var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");
    
            // DEPT FILTER
            $(".departure-range-slider").ionRangeSlider({
                type: "double",
                grid: true,
                min: start.format("x"),
                max: end.format("x"),
                step: 1800000,
                prettify: function(num) {
    
                    return moment(num, "x").format("HH:mm:ss");
                },
                onFinish: function(data) {
                    var fromtimeArr = data.from_pretty.split(':');
    
                    var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                    var totimeArr = data.to_pretty.split(':');
    
                    var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);
    
                    $scope.filtermodel.deptTimeBean.fromDept = fromtimeInMilliseconds; // data.from_pretty;
                    $scope.filtermodel.deptTimeBean.toDept = totimeInMilliseconds; //data.to_pretty;
                    $scope.filterParam();
                }
            });
            // ARRIVAL FILTER
            $(".arrival-range-slider").ionRangeSlider({
                type: "double",
                grid: true,
                min: start.format("x"),
                max: end.format("x"),
                step: 1800000,
                prettify: function(num) {
    
                    return moment(num, "x").format("HH:mm:ss");
                },
                onFinish: function(data) {
                    var fromtimeArr = data.from_pretty.split(':');
    
                    var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                    var totimeArr = data.to_pretty.split(':');
    
                    var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);
    
                    $scope.filtermodel.arrivalTimeBean.fromArrival = fromtimeInMilliseconds; //data.from_pretty;
                    $scope.filtermodel.arrivalTimeBean.toArrival = totimeInMilliseconds; //data.to_pretty;
                    $scope.filterParam();
                }
            });
    
    
            $sidebar = $("#filterSideBar");
            $window = $(window);
            var sidebarOffset = $sidebar.offset();
    
            // $("#filterSideBar").sticky({ topSpacing: 150 });
            // $("#filterSideBar").on("bottom-reached", function () {
      
            // });
    
            $(window).scroll(function() {
                var sticky = $("#filterSideBar"),
             
                    scroll = $(window).scrollTop();
                  
                var footerScroll =
                    $(".footer").offset().top -
                    $(".footer").height() -
                    $(".header").height() -
                    125;
                if (scroll >= 150 && scroll < footerScroll)
                    sticky.addClass("sticky-sidebar");
                 
                if (scroll >= 2320 && scroll < footerScroll)
                    sticky.removeClass("sticky-sidebar");
                else sticky.removeClass("sticky-sidebar");
               
            });
           
    
            $('[data-toggle="tab"]').click(function() {
                let _href = $(this).attr("data-flightType");
                if (_href === "oneWay") {
                    $("#flyingto").slideUp();
                } else {
                    $("#flyingto").slideDown();
                }
            });
            $(".add-trip-btn").click(function() {
                let self = $(this),
                    _clone = $(self.attr("data-clone")),
                    _append = $(self.attr("data-append"));
                let _groups = $(".multicity-group", _append).length;
           
                if (_groups + 1 < 5) {
                    _clone.clone().appendTo(_append);
                }
            });
            $(".remove-trip-btn").click(function() {
                let _groups = $(".multicity-group", ".multicity-groups");
                if (_groups.length >= 3) {
                    _groups.last().remove();
                }
            });
    
            $('[data-toggle="tab"]').click(function() {
                $(".boottabpane").removeClass("active");
                let _href = $(self).attr("href");
                $(_href).addClass("active");
            });
    
    
    

    
    }
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    
    getcode =  function(airport){
       return airport.split('(')[1].split(')')[0];
    }

	 $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
     if($scope.searchFlightObj.frmAirport != undefined)
         $scope.searchFlightObj.frmAirport = getcode($scope.searchFlightObj.frmAirport);
    else
        $scope.searchFlightObj.frmAirport = getcode($scope.searchFlightObj.multicityInfos[0].frmAirport);


     if($scope.searchFlightObj.frmAirport != undefined){
        if($scope.searchFlightObj.toAirport)
            $scope.searchFlightObj.toAirport = getcode($scope.searchFlightObj.toAirport);
     }else{
        $scope.searchFlightObj.toAirport = getcode($scope.searchFlightObj.multicityInfos[1].toAirport);
     }

	 $scope.artloading = true;
	        $http.post(ServerService.serverPath + 'flight/availability', JSON.stringify($scope.searchFlightObj), {
    
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.loading = false;
        $scope.disabled = false;
        if (response.data.status == 'success') {
            // sessionStorage.setItem('flightSearchRS', JSON.stringify(response.data));

               $scope.artloading = false;
    $scope.loading = false;
    $scope.disabled = false;
    // $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    $scope.result = response.data;
    sessionStorage.setItem('flightSessionId', JSON.stringify(response.data.sessionId));
    
    // $scope.flightSearchbind = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    $scope.flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));

    $scope.isAllStopsSelected = true;
    $scope.isAllAirLinesSelected = true;
    $scope.flightResultList = $scope.result.data;

    $scope.flightResultList = $scope.result.data;
    $scope.additionaldata = $scope.flightResultList.additionalProperties;

    $scope.flightResultList.filterParams.stopsBeanList.forEach(stoplst => {
        stoplst.selected = true;
    });

    $scope.flightResultList.filterParams.airlineBeanList.forEach(stoplst => {
        stoplst.selected = true;
    });

    $scope.flightSearchResults = $scope.flightResultList.flightSearchResults;
    $scope.firstFlightRes=$scope.flightSearchResults[0];
    sessionStorage.setItem('firstFlightResponse', JSON.stringify($scope.firstFlightRes.segGrpList[0]));
    var searchflights = $scope.flightResultList.flightSearchResults;
    // $scope.loopFlightHotelResults($scope.flightSearchResults);

    sessionStorage.removeItem("flightFilterData");



    /* 	var fareRequest = new Object();		
      fareRequest.provider = $scope.provider ;
      fareRequest.key = $scope.farekey;
      var fareRequestJSON = JSON.stringify(fareRequest); */
    $scope.loading = true;
    $scope.disabled = true;



    $scope.flightResults = {}
    $scope.flightResults.flightSearchResults = [];
    var priceStart = $scope.flightResultList.filterParams.priceBean.minPrice;
    var priceEnd = $scope.flightResultList.filterParams.priceBean.maxPrice;
    $scope.filtermodel = {};
    $scope.filtermodel.sessionId =  $scope.result.sessionId;

    if($scope.flightResultList.filterParams.priceBean.minPrice && $scope.flightResultList.filterParams.priceBean.maxPrice)
    $scope.ispricethr = true;
    else
    $scope.ispricethr = false;
    if($scope.flightResultList.filterParams.deptTimeBean){
    if($scope.flightResultList.filterParams.deptTimeBean.maxDeparture && $scope.flightResultList.filterParams.deptTimeBean.minDeparture)
    $scope.isdepthr = true;
    else
    $scope.isdepthr = false;

    if($scope.flightResultList.filterParams.arrivalTimeBean.maxArrival && $scope.flightResultList.filterParams.arrivalTimeBean.minArrival)
    $scope.isarrthr = true;
    else
    $scope.isarrthr = false;
}else{
    $scope.isarrthr = false;
}
    // if($scope.)
    $scope.filtermodel.selectedBound = "Outbound";
    $scope.filtermodel.isOnlySorting = false;
    $scope.filtermodel.priceBean = { fromRange: priceStart, toRange: priceEnd };

    $scope.filtermodel.fareType = { refundable: true, nonRefundable: true }
    $scope.filtermodel.baggageBean = { withBaggage: true, withOutBaggage: true }

    $scope.filtermodel.arrivalTimeBean = { "fromArrival": $scope.flightResultList.filterParams.arrivalTimeBean.minArrival, "toArrival": $scope.flightResultList.filterParams.arrivalTimeBean.maxArrival };
    $scope.filtermodel.deptTimeBean = { "fromDept": $scope.flightResultList.filterParams.deptTimeBean.minDeparture, "toDept": $scope.flightResultList.filterParams.deptTimeBean.maxDeparture };

    $scope.filtermodel.durationBean = { "fromDuration": $scope.flightResultList.filterParams.durationBean.minDuration, "toDuration": $scope.flightResultList.filterParams.durationBean.maxDuration };

    $scope.filtermodel.airlineBeanList = [];

    $scope.filtermodel.stopsBeanList = [];
    $scope.filtermodel.providerBeanList = [];
	
    $scope.checkAll = false;
    $scope.filter1 = [];
    $scope.sortItem = "PRICE,ASC";
    var sortList = $scope.sortItem.split(",");
    $scope.filtermodel.defaultSortType = sortList[0];
    $scope.filtermodel.defaultSortOrder = sortList[1];
	    var airlineCode;
    var stopIndex;
    $scope.airlineFilter;
    var filterDatas = {}


     for (var index in $scope.flightResultList.filterParams.airlineBeanList) {
         $scope.filtermodel.airlineBeanList.push(

             $scope.flightResultList.filterParams.airlineBeanList[index].airlineCode,


         );
     }
     for (var index in $scope.flightResultList.filterParams.providerBeanList) {
         $scope.filtermodel.providerBeanList.push(

           $scope.flightResultList.filterParams.providerBeanList[index].code,


        );
    }

     for (var index in $scope.flightResultList.filterParams.stopsBeanList) {
         $scope.filtermodel.stopsBeanList.push(

             $scope.flightResultList.filterParams.stopsBeanList[index].stopCode,


       );
     }
	 
	     if ($scope.result.data.deptDate !== null) {
        // $scope.getflightData = sessionStorage.getItem("flightSearchRS");
        $scope.searchresults =  $scope.result.data;
        $scope.currency = 'AED';
        var currency = $scope.currency;

        $scope.fromAirport = $scope.searchresults.frmAirprt;
        var fromAirport = $scope.fromAirport;
        var fromAirportSplt = fromAirport.split(',');
        $scope.fromAirportSplt = fromAirportSplt[0];
        $scope.toAirport = $scope.searchresults.toAirprt;
        $scope.departureDate = $scope.searchresults.deptDate;
        $scope.returnDate = $scope.searchresults.arrDate;
        var toAirport = $scope.toAirport;
        var toAirportSplt = toAirport.split(',');
        $scope.toAirportSplt = toAirportSplt[0];
        $scope.hideCity = false;
    } else {
        $scope.city = JSON.parse(sessionStorage.getItem('flightSearchRQ'))
            //    $scope.city = tmp.multicityInfos;
        $scope.hideCity = true;
    }
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: priceStart,
            max: priceEnd,
            from: priceStart,
            to: priceEnd,
            grid: true,
            prefix: "AED ",
            onFinish: function(data) {
                var fromRange = data.from_pretty.replace(" ", "");
                var toRange = data.to_pretty.replace(" ", "");
                $scope.filtermodel.priceBean.fromRange = fromRange;
                $scope.filtermodel.priceBean.toRange = toRange;
                $scope.filterParam();
            }
        });

        var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
        var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");

        // DEPT FILTER
        $(".departure-range-slider").ionRangeSlider({
            type: "double",
            grid: true,
            min: start.format("x"),
            max: end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
            onFinish: function(data) {
                var fromtimeArr = data.from_pretty.split(':');

                var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                var totimeArr = data.to_pretty.split(':');

                var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);

                $scope.filtermodel.deptTimeBean.fromDept = fromtimeInMilliseconds; // data.from_pretty;
                $scope.filtermodel.deptTimeBean.toDept = totimeInMilliseconds; //data.to_pretty;
                $scope.filterParam();
            }
        });
        // ARRIVAL FILTER
        $(".arrival-range-slider").ionRangeSlider({
            type: "double",
            grid: true,
            min: start.format("x"),
            max: end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
            onFinish: function(data) {
                var fromtimeArr = data.from_pretty.split(':');

                var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                var totimeArr = data.to_pretty.split(':');

                var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);

                $scope.filtermodel.arrivalTimeBean.fromArrival = fromtimeInMilliseconds; //data.from_pretty;
                $scope.filtermodel.arrivalTimeBean.toArrival = totimeInMilliseconds; //data.to_pretty;
                $scope.filterParam();
            }
        });


        $sidebar = $("#filterSideBar");
        $window = $(window);
        var sidebarOffset = $sidebar.offset();

        // $("#filterSideBar").sticky({ topSpacing: 150 });
        // $("#filterSideBar").on("bottom-reached", function () {
  
        // });

        $(window).scroll(function() {
            var sticky = $("#filterSideBar"),
                scroll = $(window).scrollTop();
            var footerScroll =
                $(".footer").offset().top -
                $(".footer").height() -
                $(".header").height() -
                125;
            if (scroll >= 150 && scroll < footerScroll)
                sticky.addClass("sticky-sidebar");
            if (scroll >= 2320 && scroll < footerScroll)
                sticky.removeClass("sticky-sidebar");
            else sticky.removeClass("sticky-sidebar");
        });

        $('[data-toggle="tab"]').click(function() {
            let _href = $(this).attr("data-flightType");
            if (_href === "oneWay") {
                $("#flyingto").slideUp();
            } else {
                $("#flyingto").slideDown();
            }
        });
        $(".add-trip-btn").click(function() {
            let self = $(this),
                _clone = $(self.attr("data-clone")),
                _append = $(self.attr("data-append"));
            let _groups = $(".multicity-group", _append).length;
       
            if (_groups + 1 < 5) {
                _clone.clone().appendTo(_append);
            }
        });
        $(".remove-trip-btn").click(function() {
            let _groups = $(".multicity-group", ".multicity-groups");
            if (_groups.length >= 3) {
                _groups.last().remove();
            }
        });

        $('[data-toggle="tab"]').click(function() {
            $(".boottabpane").removeClass("active");
            let _href = $(self).attr("href");
            $(_href).addClass("active");
        });




      
	                     $scope.moreOptionFun1();
                    

                    $scope.filterFunction();
                    $scope.artloading = false;
        } else {
$scope.artloading=false;
            Lobibox.alert('error', {
                msg: response.data.errorMessage
            })
            $timeout(function() {
                $location.path('/home')
            }, 1000);
        }

    }, function errorCallback(response) {
        if(response.status == 403){
           var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
           status.then(function(greeting) {
           searchflight();
           });
            // $scope.autoAirlines(value);
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });



    
    $(document).ready(function(){
        $(window).scrollTop(0);
         $(this).scrollTop(0);
         $('html, body').animate({scrollTop:0},500);
    });
    document.getElementById('amenitiesfilter').style.height = '250px';
    $(window).scroll(function(){ 
        
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 10) { 
            document.getElementById('amenitiesfilter').style.height = '200px';
        }
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 80) { 
            document.getElementById('amenitiesfilter').style.height = '280px';
        }
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 150) { 
            document.getElementById('amenitiesfilter').style.height = '300px';
        }
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 200) { 
            document.getElementById('amenitiesfilter').style.height = '370px';
        }
        //  else { 
        //     document.getElementById('amenitiesfilter').style.height = '250px';
        // } 
    }); 
    

if(sessionStorage.getItem("travellersListByRoom")!=null){
    // sessionStorage.removeItem("travellersListByRoom"); 
}

    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    sessionStorage.removeItem("selectedInsurancePlanId");

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }

    $scope.stationBind = function(plc){
       var place1 = plc.split("(")[0];
       var place2 = plc.split(",")[1];
       return place1+ ', '+place2;
    }
    // $scope.artloading = false;
    // $scope.loading = false;
    // $scope.disabled = false;
    // $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    // // $scope.flightSearchbind = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    // $scope.flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));

    // $scope.isAllStopsSelected = true;
    // $scope.isAllAirLinesSelected = true;
    // $scope.flightResultList = $scope.result.data;

    // $scope.flightResultList = $scope.result.data;
    // $scope.additionaldata = $scope.flightResultList.additionalProperties;


    // $scope.flightSearchResults = $scope.flightResultList.flightSearchResults;

    // var searchflights = $scope.flightResultList.flightSearchResults;
    // // $scope.loopFlightHotelResults($scope.flightSearchResults);

    // sessionStorage.removeItem("flightFilterData");



    /* 	var fareRequest = new Object();		
      fareRequest.provider = $scope.provider ;
      fareRequest.key = $scope.farekey;
      var fareRequestJSON = JSON.stringify(fareRequest); */
//     $scope.loading = true;
//     $scope.disabled = true;



//     $scope.flightResults = {}
//     $scope.flightResults.flightSearchResults = [];
//     var priceStart = $scope.flightResultList.filterParams.priceBean.minPrice;
//     var priceEnd = $scope.flightResultList.filterParams.priceBean.maxPrice;
//     $scope.filtermodel = {};
//     $scope.filtermodel.sessionId = JSON.parse(sessionStorage.getItem('flightSearchRS')).sessionId;

//     if($scope.flightResultList.filterParams.priceBean.minPrice && $scope.flightResultList.filterParams.priceBean.maxPrice)
//     $scope.ispricethr = true;
//     else
//     $scope.ispricethr = false;
//     if($scope.flightResultList.filterParams.deptTimeBean){
//     if($scope.flightResultList.filterParams.deptTimeBean.maxDeparture && $scope.flightResultList.filterParams.deptTimeBean.minDeparture)
//     $scope.isdepthr = true;
//     else
//     $scope.isdepthr = false;

//     if($scope.flightResultList.filterParams.arrivalTimeBean.maxArrival && $scope.flightResultList.filterParams.arrivalTimeBean.minArrival)
//     $scope.isarrthr = true;
//     else
//     $scope.isarrthr = false;
// }else{
//     $scope.isarrthr = false;
// }
//     // if($scope.)
//     $scope.filtermodel.selectedBound = "Outbound";
//     $scope.filtermodel.isOnlySorting = false;
//     $scope.filtermodel.priceBean = { fromRange: priceStart, toRange: priceEnd };

//     $scope.filtermodel.fareType = { refundable: true, nonRefundable: true }
//     $scope.filtermodel.baggageBean = { withBaggage: true, withOutBaggage: true }

//     $scope.filtermodel.arrivalTimeBean = { "fromArrival": 0 * 60000, "toArrival": 1439 * 60000 };
//     $scope.filtermodel.deptTimeBean = { "fromDept": 0 * 60000, "toDept": 1439 * 60000 };

//     $scope.filtermodel.durationBean = { "fromDuration": 0, "toDuration": 950 };

//     $scope.filtermodel.airlineBeanList = [];

//     $scope.filtermodel.stopsBeanList = [];
//     $scope.filtermodel.providerBeanList = [];

$scope.isOnly=function(stopCode,indexvalue,filtervalue){
    var i = indexvalue;
  
 


  
   
if(filtervalue=="stopvalue"){
    $scope.filtermodel.stopsBeanList = [];
//     $scope.flightResultList.filterParams.stopsBeanList.forEach(stopBean => {

//         if (stopBean.stopCode === stopCode) {
//             $scope.filtermodel.stopsBeanList.push(stopBean.stopCode);
//             for(var s=0; s< $scope.flightResultList.filterParams.stopsBeanList.length; s++){
// if(s==i){
//     $('#stop_selected_' + s).prop('checked', true);
// }else{
//     $('#stop_selected_' + s).prop('checked', false);
// }
//             }
//         }



//     });
    $scope.flightResultList.filterParams.stopsBeanList.forEach(stoplst => {
        if(stopCode == stoplst.stopCode){
            stoplst.selected = true;
            $scope.filtermodel.stopsBeanList.push(stopCode);
        }
        else
            stoplst.selected = false;
    });
}
if(filtervalue=="airlines"){
    
    $scope.filtermodel.airlineBeanList = [];
    // $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => { //checking with actual list
    //     if (airlineBean.airlineCode === stopCode) {

    //         $scope.filtermodel.airlineBeanList.push(airlineBean.airlineCode);
    //         for(var s=0; s< $scope.flightResultList.filterParams.airlineBeanList.length; s++){
    //             if(s==i){
    //                 $('#airline_selected_' + s).prop('checked', true);
    //             }else{
    //                 $('#airline_selected_' + s).prop('checked', false);
    //             }
    //                         }
    //     }

    // });
    $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => {
        if(stopCode == airlineBean.airlineCode){
            airlineBean.selected = true;
            $scope.filtermodel.airlineBeanList.push(stopCode);
        }
        else
            airlineBean.selected = false;
    });
}
if(filtervalue=="refundablevalue"){
    if(stopCode=='true'){
    $scope.filtermodel.fareType.refundable = true;
    $('#refundable_selected_' + 0).prop('checked', true);
    $scope.filtermodel.fareType.nonRefundable = false;

    $('#nonrefundable_selected_' + 1).prop('checked', false);
    }
}
if(filtervalue=="nonRefundable"){
    if(stopCode=='true'){
    $scope.filtermodel.fareType.nonRefundable = true;
    $('#nonrefundable_selected_' + 1).prop('checked', true);
    $scope.filtermodel.fareType.refundable = false;

    $('#refundable_selected_' + 0).prop('checked', false);
    }
}
if(filtervalue=="withBaggage"){
    if(stopCode=='true'){
    $scope.filtermodel.baggageBean.withBaggage = true;
    $('#withBaggage_selected_' + 0).prop('checked', true);
    $scope.filtermodel.baggageBean.withOutBaggage = false;

    $('#withoutBaggage_selected_' + 1).prop('checked', false);
    }
}
if(filtervalue=="withOutBaggage"){
    if(stopCode=='true'){
    $scope.filtermodel.baggageBean.withOutBaggage = true;
    $('#withoutBaggage_selected_' + 1).prop('checked', true);
    $scope.filtermodel.baggageBean.withBaggage = false;

    $('#withBaggage_selected_' + 0).prop('checked', false);
    }
}




    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
  
    $scope.flightSearchResults1 = [];
    $scope.flightSearchResults = [];
    $scope.artloading = true;
    $http.post(ServerService.serverPath + 'flight/filter', $scope.filtermodel, {
        headers: headersOptions
    }).then(function successCallback(response) {
        if (response.data.success) {
          
            $scope.flightSearchResults = response.data.data;
            $scope.firstFlightFilter=$scope.flightSearchResults[0];
             sessionStorage.setItem("flightFilterData", JSON.stringify($scope.firstFlightFilter));
            $scope.showErrorMesg = false;
            $(window).scrollTop(0);
            $(this).scrollTop(0);
            $('html, body').animate({scrollTop:0},500);
            $scope.moreOptionFun1();
            $scope.artloading = false;
    } else {
        $scope.showErrorMesg = true;
        $scope.artloading = false;

     
    }
}, function errorCallback(response) {
    if(response.status == 403){
        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
        status.then(function(greeting) {
            $scope.isOnly();
           });
    }
   
  });
}

    // var airlineCode;
    // var stopIndex;
    // $scope.airlineFilter;
    // var filterDatas = {}


    //  for (var index in $scope.flightResultList.filterParams.airlineBeanList) {
    //      $scope.filtermodel.airlineBeanList.push(

    //          $scope.flightResultList.filterParams.airlineBeanList[index].airlineCode,


    //      );
    //  }
    //  for (var index in $scope.flightResultList.filterParams.providerBeanList) {
    //      $scope.filtermodel.providerBeanList.push(

    //        $scope.flightResultList.filterParams.providerBeanList[index].code,


    //     );
    // }

    //  for (var index in $scope.flightResultList.filterParams.stopsBeanList) {
    //      $scope.filtermodel.stopsBeanList.push(

    //          $scope.flightResultList.filterParams.stopsBeanList[index].stopCode,


    //    );
    //  }

    function formatNumber(n) {
        return Math.round(n);
    }

    function fetchCityOnly(place) {
        var placeSplit = place.split(',');
        var parenthSplt = placeSplit[0].split("(");
        return parenthSplt[0];
    }

    function flydur(n1, n2) {
        return parseInt((parseInt(n1) + parseInt(n2)) / 60);
    }

    function flypos(n1, n2) {
        var returnvalue = parseInt((parseInt(n1) + parseInt(n2)) % 60);
        if (returnvalue > 9) {
            return returnvalue;
        } else {
            return "0" + returnvalue;
        }
    }
    // if ($scope.result.data.deptDate !== null) {
    //     $scope.getflightData = sessionStorage.getItem("flightSearchRS");
    //     $scope.searchresults = JSON.parse($scope.getflightData).data;
    //     $scope.currency = 'AED';
    //     var currency = $scope.currency;

    //     $scope.fromAirport = $scope.searchresults.frmAirprt;
    //     var fromAirport = $scope.fromAirport;
    //     var fromAirportSplt = fromAirport.split(',');
    //     $scope.fromAirportSplt = fromAirportSplt[0];
    //     $scope.toAirport = $scope.searchresults.toAirprt;
    //     $scope.departureDate = $scope.searchresults.deptDate;
    //     $scope.returnDate = $scope.searchresults.arrDate;
    //     var toAirport = $scope.toAirport;
    //     var toAirportSplt = toAirport.split(',');
    //     $scope.toAirportSplt = toAirportSplt[0];
    //     $scope.hideCity = false;
    // } else {
    //     $scope.city = JSON.parse(sessionStorage.getItem('flightSearchRQ'))
    //         //    $scope.city = tmp.multicityInfos;
    //     $scope.hideCity = true;
    // }


    $scope.flightSearchResults1 = [];


    // $scope.moreOptionFun = function() {
    //     $scope.flightSearchResults1 = [];
    //     var result = {};
    //     $scope.flightSearchResults.forEach(function(val) {

     

    //         var key = val.segGrpList[0].segList[0].airlineName + ":" + val.grndTotal;

    //         result[key] = result[key] || { airlineName: val.segGrpList[0].segList[0].airlineName, grndTotal: val.grndTotal, airlineList: [] };
    //         result[key].airlineList.push(val);
    //     });

    //     $scope.showMore = [];
    //     for (var i = 0; i < $scope.flightSearchResults1.length; i++) {
    //         $scope.showMore[i] = false;

    //     }
    //     return Object.keys(result).map(function(key) {
    //         $scope.flightSearchResults1.push(result[key]);

    //     });
     


    // }
    $scope.convertDt = function(convertDt) {
            var oneR = convertDt;
            var yr = oneR.substring(0, 4);
            var month = oneR.substring(4, 6);
            var date = oneR.substring(6, 8);
            return yr + '-' + month + '-' + date;
        }
        // $scope.showLess=true;

    $scope.showResultGroup = function(index) {
            $scope.showMore[index] = !$scope.showMore[index];
            $scope.scrollTraget = index;
            var scroll = $(window).scrollTop();
            console.log(scroll);
            $scope.scrollTraget = scroll;
        }
        $scope.showResultGroupHide = function(index) {
            $scope.showMore[index] = !$scope.showMore[index];
            $('html, body').animate({scrollTop:$scope.scrollTraget},500);
            // $('html,body').animate({
            //         scrollTop: $("#scrolltop"+index).offset().top},
            //         'slow');
        }
        // $scope.showLess=true;
    

    // $scope.flightSearchResults1.forEach(function(data) {


    // var oneR = data.airlineList[0].segGrpList[0].segList[0].fltInfoList[0].startDt;
    // var yr = oneR.substring(0, 4);
    // var month = oneR.substring(4, 6);
    // var date = oneR.substring(6, 8);
    // var oneR = yr + '-' + month + '-' + date;
    // data.airlineList[0].segGrpList[0].segList[0].fltInfoList[0].startDt = oneR;

    // var oneR1 = data.airlineList[0].segGrpList[0].segList[0].fltInfoList[data.airlineList[0].segGrpList[1].segList[0].fltInfoList.length - 1].endDt;
    // var yr = oneR1.substring(0, 4);
    // var month = oneR1.substring(4, 6);
    // var date = oneR1.substring(6, 8);
    // var oneR1 = yr + '-' + month + '-' + date;
    // data.airlineList[0].segGrpList[0].segList[0].fltInfoList[data.airlineList[0].segGrpList[1].segList[0].fltInfoList.length - 1].endDt = oneR1;


    // var twoR = data.airlineList[0].segGrpList[1].segList[0].fltInfoList[0].startDt;
    // var yr = twoR.substring(0, 4);
    // var month = twoR.substring(4, 6);
    // var date = twoR.substring(6, 8);
    // var twoR = yr + '-' + month + '-' + date;
    // data.airlineList[0].segGrpList[1].segList[0].fltInfoList[0].startDt = twoR;



    // var twoR1 = data.airlineList[0].segGrpList[1].segList[0].fltInfoList[data.airlineList[0].segGrpList[1].segList[0].fltInfoList.length - 1].endDt;
    // var yr = twoR1.substring(0, 4);
    // var month = twoR1.substring(4, 6);
    // var date = twoR1.substring(6, 8);
    // var twoR1 = yr + '-' + month + '-' + date;
    // data.airlineList[0].segGrpList[1].segList[0].fltInfoList[data.airlineList[0].segGrpList[1].segList[0].fltInfoList.length - 1].endDt = twoR1




    // var tmp = {
    //     'one_one': oneR,
    //     'one_two': oneR1,
    //     'two_one': twoR,
    //     'two_two': twoR1
    // }

    // dateArr.push(tmp);
  

    // })
    // $scope.loopFlightHotelResults = function(searchflights) {
    // if($scope.flightSearchResults) {
    // 	var ab = [];
    // 	var max = 0;
    // 	var listcls = 'price';
    // 	var margincls = 'mt5';
    // 	var margintop = 'mt_13';
    // 	var margintop20 = 'mt_20';
    // 	var lastGrndTotal = 0; 
    // 	for (var a in $scope.flightSearchResults) {
    // 		var bc = []; var rp = [];var fs = []; var sp;
    // 		for(var b in $scope.flightSearchResults[a].segGrpList) {
    // 			var cd = []; 
    // 			sp = fetchCityOnly($scope.flightSearchResults[a].segGrpList[b].segmntOrigin)+' <i class="fa fa-long-arrow-right"></i> '+fetchCityOnly($scope.flightSearchResults[a].segGrpList[b].segmntDest);
    // 			var line;
    // 			if(b==0) {
    // 				line = 'fblueline';
    // 				} else {
    // 				line = 'fgreenline';
    // 			}
    // 			var isRefund = $scope.flightSearchResults[a].refund;
    // 			var dl = ''; var gh = []; var ss; var op; 
    // 			for(var c in $scope.flightSearchResults[a].segGrpList[b].segList) {
    // 				for(var e in $scope.flightSearchResults[a].segGrpList[b].segList[c].routes) {
    // 					if(e!=0) {
    // 						gh.push(' <i class="fa fa-long-arrow-right"></i> '+$scope.flightSearchResults[a].segGrpList[b].segList[c].routes[e]);
    // 						} else {
    // 						gh.push($scope.flightSearchResults[a].segGrpList[b].segList[c].routes[e]);
    // 					}
    // 				}
    // 				gh = gh.join('');
    // 				if($scope.flightSearchResults[a].segGrpList[b].segList[c].stops==0) {
    // 					ss = 'Non';
    // 					} else {
    // 					ss = $scope.flightSearchResults[a].segGrpList[b].segList[c].stops;
    // 				}

    // 				var fd = flydur($scope.flightSearchResults[a].segGrpList[b].segList[c].totFlyDuration,$scope.flightSearchResults[a].segGrpList[b].segList[c].totTransitDuration);
    // 				var fp = flypos($scope.flightSearchResults[a].segGrpList[b].segList[c].totFlyDuration,$scope.flightSearchResults[a].segGrpList[b].segList[c].totTransitDuration);
    // 				op = ('<span>'+fd+'h&nbsp;'+fp+'m</span>');
    // 				var fi = [];
    // 				for(var d in $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList) {
    // 					var fn = formatNumber($scope.flightSearchResults[a].grndTotal);
    // 					var airlinedata = ''; var bookButton = ''; var timeIcon = ''; var detailIcon = '';
    // 					var frmAir, toAir, departureDate, td;
    // 					var flights = $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList[d];
    // 					if(d==1) {
    // 						td = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="col-xs-12 col-sm-2 col-md-1 col-lg-1" style="height:10px;"></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="border-bottom:1px solid #ccc;height:10px;"></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Layover - '+flights.dispTransitDuration+'</div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="border-bottom:1px solid #ccc;height:10px;"></div></div>';
    // 						} else if(d==2) {
    // 						td = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="col-xs-12 col-sm-2 col-md-1 col-lg-1" style="height:10px;"></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="border-bottom:1px solid #ccc;height:10px;"></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Layover - '+flights.dispTransitDuration+'</div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="border-bottom:1px solid #ccc;height:10px;"></div></div>';
    // 						} else {
    // 						td = '';
    // 					}
    // 					var clasType;
    // 					if($scope.searchresults.clasType == null){
    // 						clasType = '';
    // 						}else{
    // 						clasType = $scope.searchresults.clasType;
    // 					}

    // 					var fltClassCode;
    // 					if(flights.fltClsCode== null || flights.fltClsCode== ''){
    // 						fltClassCode = "";
    // 						}else{
    // 						fltClassCode = '-'+flights.fltClsCode;
    // 					}
    // 					var fareKey;
    // 					if($scope.flightSearchResults[a].fareslst[0].fareInfoRefKey!=null) {
    // 						fareKey = $scope.flightSearchResults[a].fareslst[0].fareInfoRefKey[0];
    // 						} else {
    // 						fareKey = '';
    // 					}
    // 					var fareProvd;
    // 					if($scope.flightSearchResults[a].provider!=null) {
    // 						fareProvd = $scope.flightSearchResults[a].provider;
    // 						} else {
    // 						fareProvd = '';
    // 					}
    // 					var aircraftType = flights.aircraftType!=null? flights.aircraftType:'';
    // 					fi.push('<div class="list-group row">'+td+'<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"  style="width: 20%;"><img style="float: left; height: 25px;" src="app/images/Airlines/sm'+flights.airline+'.png" /><span class="airline-name">'+flights.airlineName+'</span><span class="aircraft-name names">'+flights.airline+' - '+flights.fltNum+'</span><span class="aircraft-name names" style="margin-left: 25px;">'+aircraftType+'</span></div><div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 offset-0"><div class="list-group"><div><p class="fltOriginAlign"><span><span class="text-bold">'+flights.origin+'</span><br/>'+flights.dispStartDate+'<br/><span><i class="fa fa-clock-o"></i></span>'+'&nbsp;'+flights.dispStartTime+'</span></p></div></div></div><div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"><div class="list-group"><div><p><span><span class="text-bold">'+flights.dispDuration+'</span><br>Non Stop<br></span></p></div></div></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><div class="list-group"><div><p class="fltDestAlign"><span><span class="text-bold">'+flights.dest+'</span><br/>'+flights.dispEndDate+'<br/><span><i class="fa fa-clock-o"></i></span>'+'&nbsp;'+flights.dispArrTime+'</p></div></div></div>'+'<div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">'+clasType+fltClassCode+'</div>'+'</div>');
    // 					var flLen = $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList.length;
    // 					var depStDate = $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList[0].dispStartDate;
    // 					var date1 = new Date(depStDate);
    // 					var depEndDate = $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList[flLen-1].dispEndDate;
    // 					var date2 = new Date(depEndDate);
    // 					var nodays = (date2 - date1) / 1000 / 60 / 60 / 24;
    // 					var xsNodays = (date2 - date1) / 1000 / 60 / 60 / 24;
    // 					if(nodays== 0) {
    // 						nodays =  '';
    // 						xsNodays =  '';
    // 						} else if(nodays==1) {
    // 						xsNodays = '+'+nodays+'d';
    // 						nodays = '+'+nodays+'day';
    // 						} else {
    // 						xsNodays = '+'+nodays+'d';
    // 						nodays = '+'+nodays+'days';
    // 					}
    // 					if(b==0){
    // 						airlinedata = '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><div title="'+$scope.flightSearchResults[a].segGrpList[b].segList[c].airlineName+'"><img style="height:25px; margin-left:15px;" src="app/images/Airlines/sm'+$scope.flightSearchResults[a].segGrpList[b].segList[c].airline+'.png" alt="Image"/><div class="airline-name hidden-xs size11">'+$scope.flightSearchResults[a].segGrpList[b].segList[c].airlineName+'</div><div class="mobileAirlineClass visible-xs">'+$scope.flightSearchResults[a].segGrpList[b].segList[c].airlineName+'</div></div></div>';
    // 						bookButton = '<div class="col-xs-3 col-sm-2 col-md-1 col-lg-1 hidden-xs offset-0 pull-right text-center" style="width: 11%; margin-top: -2px;"><span class="size13"><b>'+currency+' '+fn+'</b></span><br/><a class="btn btn-theme customBook btn-sm" onclick="selectedFlight('+$scope.flightSearchResults[a].indx+','+fn+')">Pick</a></div>';
    // 						timeIcon = '';
    // 						frmAir = fromAirportSplt[0];
    // 						toAir = toAirportSplt[0];
    // 						departureDate = $scope.departureDate;
    // 						detailIcon = '';
    // 						margincls = 'mt5';
    // 						margintop = '';
    // 						margintop20 = '';

    // 						} else {
    // 						airlinedata = '';
    // 						bookButton = '';
    // 						timeIcon = '<span class="centerIcon"><i class="fa fa-clock-o"></i></span>';
    // 						frmAir = toAirportSplt[0];
    // 						toAir = fromAirportSplt[0];
    // 						departureDate = $scope.returnDate;
    // 						margincls = '';
    // 						margintop = 'mt_13';
    // 						margintop20 = 'mt_20';
    // 						detailIcon = '';
    // 					}
    // 					var routeData = '';
    // 					var dispStartDate = flights.dispStartDate;
    // 					var departDate = dispStartDate?dispStartDate.split(','):[];
    // 					var dispDepartDate = departDate;//[1];
    // 					var fDispTime = $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList[0];
    // 					var fArrTime = $scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList[$scope.flightSearchResults[a].segGrpList[b].segList[c].fltInfoList.length-1];

    // 				}

    // 				dl ='<div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 offset-0 hidden-xs routeOverflw hide '+margintop+'"><span class="size12 labelOverRide">'+sp+'</span></div><div class="col-xs-2 col-sm-1 col-md-2 col-lg-2 offset-0 text-center visible-xs '+margintop+'"><span class="size12 dark" style="display: inline-block; width:50px;">'+dispDepartDate+'</span></div>';
    // 				dl +='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 offset-0 text-center '+margintop+'"><span class="size12 dark">'+fDispTime.dispStartTime+'</span></div><div class="col-xs-1 col-sm-2 col-md-2 col-lg-2 offset-1 visible-xs '+margintop+'"><i style="color: #333;" class="fa fa-long-arrow-right"></i></div><div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 offset-1 text-center '+margintop+'"><span class="size12 dark">'+fArrTime.dispArrTime+'<span class="hidden-xs size11 mt3 hide" style="color: #3172aa;position: absolute;">'+'&nbsp;'+nodays+'</span>'+'<span class="visible-xs size11 pull-right mt4" style="color: #3172aa;margin-left: -14px;">'+xsNodays+'</span>'+'</span></div><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 offset-0 text-center hidden-xs '+margintop+'"><span class="size12 dark">'+op+'</span></div><div class="col-xs-4 col-sm-2 col-md-2 col-lg-2 '+margintop+' hidden-xs text-right"><span class="size12 dark">'+ss+'Stop(s)</span></div>'+bookButton;
    // 				cd.push('<div class="list-group '+margincls+'" style="margin: 0; list-style: none;">'+airlinedata+dl+'</div>');
    // 				fi = fi.join('');
    // 				var ft = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+fi+'</div>';
    // 			}
    // 			cd = cd.join('');
    // 			bc.push(cd);

    // 			fs.push('<div class="flight-detail-seg-head col-xs-12 col-sm-12 col-md-12 col-lg-12">('+frmAir+') <i class="fa fa-long-arrow-right"></i> ('+toAir+') <span class="datetext">'+departureDate+'</span></div>'+ft);
    // 		}
    // 		bc = bc.join(''); rp = rp.join(''); fs = fs.join('');
    // 		var margtop10;
    // 		if(a==0) {
    // 			margtop10 = '';
    // 			} else {
    // 			margtop10 = 'margtop10';
    // 		}
    // 		var fl = [];
    // 		for(var g in $scope.flightSearchResults[a].fareslst) {
    // 			var fareList = $scope.flightSearchResults[a].fareslst[g];
    // 			var cc = []; var ba = [];
    // 			for(var h in fareList.taxBreakups) {
    // 				var taxBreak = fareList.taxBreakups[h];
    // 				cc.push('<td>'+taxBreak.countryCode+'</td>');
    // 				ba.push('<td>'+taxBreak.breakupAmt+'</td>');
    // 			}
    // 			cc = cc.join(''); ba = ba.join('');
    // 			var p;
    // 			if(fareList.changePenalty==null && fareList.cancelPenalty==null) {
    // 				p = '';
    // 				} else if(fareList.changePenalty==null) {
    // 				p = '<a class="tooltips" style="cursor:pointer;"><i class="fa fa-info-circle"></i><div><span><b>Cancel Penalty :</b>&nbsp;'+fareList.cancelPenalty+'</span></div></a>';
    // 				} else if(fareList.cancelPenalty==null) {
    // 				p = '<a class="tooltips" style="cursor:pointer;"><i class="fa fa-info-circle"></i><div><span><b>Change Penalty :</b>'+'&nbsp;'+fareList.changePenalty+'</span></div></a>';
    // 				} else {
    // 				p = '<a class="tooltips" style="cursor:pointer;"><i class="fa fa-info-circle"></i><div><span><b>Change Penalty :</b>'+'&nbsp;'+fareList.changePenalty+'</span><span><br/><b>Cancel Penalty :</b>&nbsp;'+fareList.cancelPenalty+'</span></div></a>';
    // 			}
    // 			// fl.push('<div class="full clearfix"><div class="twenty">'+fareList.passengerType+'</div><div class="twenty">'+fareList.currencyCode+' '+fareList.baseFare+'</div><div class="twenty">'+fareList.taxFare+'</div><div class="twenty">'+fareList.totFare+'&nbsp;x&nbsp;'+fareList.passengerCount+'</div><div class="twenty">'+fareList.serviceCharge+'</div><div class="twenty">'+fareList.discount+'</div><div class="twenty twenty4">'+'&nbsp;'+fareList.totPsgrsFare+'&nbsp;</div></div>');
    // 			fl.push('<div class="full clearfix"><div class="twenty">'+fareList.passengerType+'</div><div class="twenty">'+fareList.currencyCode+' '+fareList.baseFare+'</div><div class="twenty">'+fareList.taxFare+'</div><div class="twenty">'+fareList.totFare+'&nbsp;x&nbsp;'+fareList.passengerCount+'</div><div class="twenty">'+fareList.serviceCharge+'</div><div class="twenty twenty4">'+'&nbsp;'+fareList.totPsgrsFare+'&nbsp;</div></div>');
    // 		}
    // 		fl = fl.join('');
    // 		var sc;
    // 		var serviceCharge = $scope.flightSearchResults[a].servCharge;
    // 		if(serviceCharge == 0) {
    // 			sc = '';
    // 			} else {
    // 			sc = '<div class="full clearfix" style="padding:2px;"><div class="twenty twenty4" style="float:right;">'+serviceCharge+'</div><div class="twenty" style="float:right; font-size: 13px;">Tot Serv Charge(+)</div></div>';
    // 		}
    // 		var dc;
    // 		var discount = $scope.flightSearchResults[a].discnt;
    // 		if(discount == 0) {
    // 			dc = '';
    // 			} else {
    // 			// dc = '<div class="full clearfix" style="padding:2px;"><div class="twenty twenty4" style="float:right;">'+discount+'</div><div class="twenty" style="float:right; font-size: 13px;">Tot Discount(-)</div></div>';
    // 			dc = '<div class="full clearfix" style="padding:2px;"><div class="twenty twenty4" style="float:right;">'+discount+'</div></div>';
    // 		}

    // 		var refundDesc = isRefund ? 'Refundable' : 'Non-Refundable';

    // 		var refundable = 'refundable'; var nonRefundabel = 'nonRefundabel';
    // 		if(isRefund==true){
    // 			refundable = 'refundable';
    // 			nonRefundabel = '';
    // 			}else{
    // 			refundable = '';
    // 			nonRefundabel = 'nonRefundabel';
    // 		}


    // 		var dg ='</div><div class="col-xs-4 col-sm-6 col-md-5 hidden-xs col-xs-offset-8 col-sm-offset-6 col-md-offset-7 mb5 text-right hide"><span class="size11 '+refundable+' '+nonRefundabel+'">'+refundDesc+'</span><a onclick="flightDetails(this,event)" class="flight_details" title="Flight details"><span class="link_details size11">Flight details</span></a><a onclick="fareDetails(this,event)" class="fare_details" title="Fare details"><span class="link_details size11">Fare details</span></a><a onclick="fareRules(this,event,\''+fareKey+'\',\''+fareProvd+'\');" class="fare_details fare_rules_hit hidden" title="Fare Rules"><span class="link_details size11">Fare Rules</span></a></div>'+'<div class="flight_details_span flight_details_arrow hide" style="background: #FFFFF6;">'+fs+'</div><div class="flightname fare_details_span flight_details_arrow hide" style="background: #FFFFF6;"><div class="full clearfix fare-header"><div class="twenty">Pax Type</div><div class="twenty">Base Fare</div><div class="twenty">Taxes & Surcharges</div><div class="twenty">Per Pax x Count</div><div class="twenty">Service Charge</div><div class="twenty">Discount</div><div class="twenty twenty4">Total Fare</div></div>'+fl+sc+dc+'<div class="full clearfix" style="padding:2px;"><div class="twenty twenty4" style="float:right;">'+fn+'</div><div class="twenty" style="float:right;">Grand Total</div></div></div><div class="farerule1 fare_rule_span flight_details_arrow hide" style="height: 230px; overflow-y: auto; background: #FFFFF6; margin-bottom: 17px;"><div align="center" style="margin-top: 50px;" class="fareruleloading" style="display:block;"><img src="app/images/loaders/loader4.gif" alt="" /></div><p class="farerulerror mt10" style="color:red;">Fare rules not available</p><div class="fareContent"></div></div></div>';

    // 		var tripType = $scope.searchresults.srchType==1?'One Way':'Round';
    // 		var max1 = '';
    // 		var lastRowMarg = 'mb0';
    // 		var viewMoreBorder = 'viewMoreBorder';
    // 		if(fn == $scope.maximum){
    // 			lastRowMarg = 'mb0';
    // 			}else{
    // 			lastRowMarg = '';
    // 		}
    // 		if(fn == $scope.maximum && a==$scope.flightSearchResults.length-1){
    // 			viewMoreBorder = '';
    // 			}else{
    // 			viewMoreBorder = 'viewMoreBorder';
    // 		}
    // 		if(fn==lastGrndTotal) {
    // 			listcls = 'sameprice '+lastGrndTotal;
    // 			max++;
    // 			if(a==$scope.flightSearchResults.length-1) {
    // 				max1 = '<div class="samePriceOption '+viewMoreBorder+'"><a class="viewMore col-xs-6 col-xs-offset-2 offset-0 bold size12" id="'+lastGrndTotal+'"><span><i class="fa fa-plus-square lred2"></i>&nbsp;'+max+' More</span><span class="visible-xs pull-right">flight(s)</span><span class="hidden-xs"> options available </span></a></div>';
    // 				max = 0;
    // 				//lastGrndTotal = 0;
    // 			} 
    // 			} else {
    // 			if(max>0) {
    // 				max1 = '<div class="samePriceOption '+viewMoreBorder+'"><a class="viewMore col-xs-6 col-xs-offset-2 offset-0 bold size12" id="'+lastGrndTotal+'"><span><i class="fa fa-plus-square lred2"></i>&nbsp;'+max+' More</span><span class="visible-xs pull-right">flight(s)</span><span class="hidden-xs"> options available </span></a></div>';
    // 			}
    // 			listcls = 'price';
    // 			max = 0;
    // 		}
    // 		if(a==$scope.flightSearchResults.length-1 && fn==lastGrndTotal) {
    // 			ab.push('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 '+listcls+' fontOverride" data-airline="'+$scope.flightSearchResults[a].segGrpList[0].segList[0].airline+'" data-price="'+fn+'"><div class="row list '+lastRowMarg+'" style="min-height: 125px; margin-bottom: 0;">'+bc+dg+'</div></div>'+max1);
    // 			} else {
    // 			ab.push(max1+'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 '+listcls+' fontOverride" data-airline="'+$scope.flightSearchResults[a].segGrpList[0].segList[0].airline+'" data-price="'+fn+'"><div class="row list '+lastRowMarg+'" style="min-height: 125px; margin-bottom: 0;">'+bc+dg+'</div></div>');
    // 		}
    // 		lastGrndTotal = fn;
    // 	}
    // 	$rootScope.fResultsLen = ab.length;
    // 	$rootScope.fMinimum = formatNumber($scope.flightSearchResults[0].grndTotal);
    // 	ab = ab.join('');
    // 	return $('#result-list').html(ab);
    // }
    // }

    // refreshpage();





    // $scope.checkAll = false;
    // $scope.filter1 = [];
    // $scope.sortItem = "PRICE,ASC";
    // var sortList = $scope.sortItem.split(",");
    // $scope.filtermodel.defaultSortType = sortList[0];
    // $scope.filtermodel.defaultSortOrder = sortList[1];


    $scope.sort = function(sortItem) {
            var sortList = sortItem.split(",");
            $scope.filtermodel.defaultSortType = sortList[0];
            $scope.filtermodel.defaultSortOrder = sortList[1];
            $scope.filterParam();
        }
        //STOP'S FILTER
    var stopsCount = 0;
    $scope.isStopSelected = true;
     $scope.isSelected = true;
    // $scope.option.selected=true;
$scope.clearFilter=function(){


    $scope.flightResults = {}
    $scope.flightResults.flightSearchResults = [];
    var priceStart = $scope.flightResultList.filterParams.priceBean.minPrice;
    var priceEnd = $scope.flightResultList.filterParams.priceBean.maxPrice;
    $scope.filtermodel = {};
    $scope.filtermodel.sessionId =  $scope.result.sessionId;

    var instance = $(".js-range-slider").data("ionRangeSlider");

    instance.update({
        min: priceStart,
        max: priceEnd,
        from: priceStart,
        to: priceEnd,
    });
   
 
    // if($scope.)
   
    for(var s=0; s< $scope.flightResultList.filterParams.stopsBeanList.length; s++){

        $scope.flightResultList.filterParams.stopsBeanList[s].selected = false;


$('#stop_selected_' + s).prop('checked', false);

    }
    for(var s=0; s< $scope.flightResultList.filterParams.airlineBeanList.length; s++){
       
        $scope.flightResultList.filterParams.airlineBeanList[s].selected = false;
          
       
            $('#airline_selected_' + s).prop('checked', false);
        
                    }
    $('#refundable_selected_' + 0).prop('checked', false);
    

    $('#nonrefundable_selected_' + 1).prop('checked', false);
    $('#withBaggage_selected_' + 0).prop('checked', false);


    $('#withoutBaggage_selected_' + 1).prop('checked', false);
    $scope.filtermodel.selectedBound = "Outbound";
    $scope.filtermodel.isOnlySorting = false;
    $scope.filtermodel.priceBean = { fromRange: priceStart, toRange: priceEnd };

    $scope.filtermodel.fareType = { refundable: true, nonRefundable: true }
    $scope.filtermodel.baggageBean = { withBaggage: true, withOutBaggage: true }

    // $scope.filtermodel.arrivalTimeBean = { "fromArrival": 0 * 60000, "toArrival": 1439 * 60000 };
    // $scope.filtermodel.deptTimeBean = { "fromDept": 0 * 60000, "toDept": 1439 * 60000 };

    // $scope.filtermodel.durationBean = { "fromDuration": 0, "toDuration": 950 };
    $scope.filtermodel.arrivalTimeBean = { "fromArrival": $scope.flightResultList.filterParams.arrivalTimeBean.minArrival, "toArrival": $scope.flightResultList.filterParams.arrivalTimeBean.maxArrival };
    $scope.filtermodel.deptTimeBean = { "fromDept": $scope.flightResultList.filterParams.deptTimeBean.minDeparture, "toDept": $scope.flightResultList.filterParams.deptTimeBean.maxDeparture };

    $scope.filtermodel.airlineBeanList = [];

    $scope.filtermodel.stopsBeanList = [];
    $scope.filtermodel.providerBeanList = [];
    $scope.filtermodel.defaultSortType="PRICE";
    $scope.filtermodel.defaultSortOrder="ASC";
  

        for (var index in $scope.flightResultList.filterParams.providerBeanList) {
            $scope.filtermodel.providerBeanList.push(
   
              $scope.flightResultList.filterParams.providerBeanList[index].code,
   
   
           );
       }
        $scope.filtermodel.providerBeanList = [];
        $scope.showErrorMesg = true;
        $scope.artloading = false;
        $scope.flightSearchResults =  $scope.flightResultList.flightSearchResults;
        $scope.firstFlightFilter=$scope.flightSearchResults[0];
        sessionStorage.setItem("flightFilterData", JSON.stringify($scope.firstFlightFilter));
        $scope.showErrorMesg = false;
        $scope.refundable = false;
        $scope.nonRefundable = false;
        
     $scope.withBaggage = false;
     $scope.withOutBaggage = false;
     $scope.isStopSelected = false;
     $scope.isSelected = false;
        $(window).scrollTop(0);
        $(this).scrollTop(0);
        $('html, body').animate({scrollTop:0},500);
        $scope.moreOptionFun1();
        $scope.artloading = false;
        var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
        var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");


        var depinstance = $(".departure-range-slider").data("ionRangeSlider");
    
        depinstance.update({
            min: start.format("x"),
            max: end.format("x"),
            from: start.format("x"),
        to: end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
        });
    
        var arrinstance = $(".arrival-range-slider").data("ionRangeSlider");
    
        arrinstance.update({
            min: start.format("x"),
            max: end.format("x"),
            from: start.format("x"),
        to:end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
        });
        $scope.filterParam();
        console.log($scope.filtermodel)
        $scope.withBaggage = false;
        $scope.withOutBaggage = false;
        $scope.refundable = false;
        $scope.nonRefundable = false;
}

    $scope.stopsChange = function(stopCode, isSelected) {
        $scope.filtermodel.stopsBeanList = [];
        $scope.flightResultList.filterParams.stopsBeanList.forEach(stopBean => {

            if (stopBean.selected) {
                $scope.filtermodel.stopsBeanList.push(stopBean.stopCode);
            }

        });

            // if (isSelected) {
            //     // if (stopsCount == 0) {
            //     //     $scope.filtermodel.stopsBeanList = [];
            //     //     stopsCount++;
            //     // }

            //     $scope.flightResultList.filterParams.stopsBeanList.forEach(stopBean => {

            //         if (stopBean.stopCode === stopCode) {
            //             $scope.filtermodel.stopsBeanList.push(stopBean.stopCode);
            //         }



            //     });
            // } else {
            //     var i = 0;
            //     $scope.filtermodel.stopsBeanList.forEach(existedStopCode => {
            //         if (existedStopCode === stopCode) {
            //             $scope.filtermodel.stopsBeanList.splice(i, 1);
            //         }
            //         i++;
            //     });
            // }

            // if ($scope.filtermodel.stopsBeanList.length == $scope.flightResultList.filterParams.stopsBeanList.length)
            //     $scope.isAllStopsSelected = true;
            // else
            //     $scope.isAllStopsSelected = false;
            $scope.filterParam();

        }
        // FARE TYPE FILTER

     $scope.refundable = true;
     $scope.nonRefundable = true;

    $scope.fareType = function(refundable, nonRefundable) {

            // if (!refundable)
            //     $scope.filtermodel.fareType.refundable = false;
            // else
        $scope.filtermodel.fareType.nonRefundable = nonRefundable;
        $scope.filtermodel.fareType.refundable = refundable;
            $scope.filterParam();
    }

    $scope.nonfareType = function(refundable, nonRefundable) {

        $scope.filtermodel.fareType.nonRefundable = nonRefundable;
        $scope.filtermodel.fareType.refundable = refundable;

        $scope.filterParam();
    }
        //BAGGAGE FILTERS

     $scope.withBaggage = true;
     $scope.withOutBaggage = true;

    $scope.baggage = function(withBaggage, withOutBaggage) {

        // if(!withBaggage &&)
        // if (withBaggage == null)
        //     $scope.filtermodel.baggageBean.withBaggage = false;
        // else
        $scope.filtermodel.baggageBean.withBaggage = $scope.withBaggage;
        $scope.filtermodel.baggageBean.withOutBaggage = $scope.withOutBaggage;

        $scope.filterParam();
    }
    $scope.withoutbaggagefun = function(withBaggage, withOutBaggage) {

        $scope.filtermodel.baggageBean.withOutBaggage = $scope.withOutBaggage;
        $scope.filtermodel.baggageBean.withBaggage = $scope.withBaggage;

        $scope.filterParam();
    }

    // AIRLINE FILTERS
    var airlineCount = 0;
    $scope.airlineFilter = function(airlineCode, isSelected) {
        $scope.filtermodel.airlineBeanList = [];
        $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => {

            if (airlineBean.selected) {
                $scope.filtermodel.airlineBeanList.push(airlineBean.airlineCode);
            }

        });
        // if (!isSelected) {
        //     // if (airlineCount == 0) {
        //     //     $scope.filtermodel.airlineBeanList = [];
        //     //     airlineCount++;
        //     // }
        //     $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => { //checking with actual list
        //         if (airlineBean.airlineCode === airlineCode) {

        //             $scope.filtermodel.airlineBeanList.push(airlineBean.airlineCode);
        //         }

        //     });
        // } else {
        //     var i = 0;
        //     $scope.filtermodel.airlineBeanList.forEach(existedAirlineCode => {
        //         if (existedAirlineCode === airlineCode) {
        //             $scope.filtermodel.airlineBeanList.splice(i, 1);
        //         }
        //         i++;
        //     });
        // }

        $scope.filterParam();
    }

    //PRICE FILTER

    function refreshpage() {
        // The last "domLoading" Time //
        var currentDocumentTimestamp =
            new Date(performance.timing.domLoading).getTime();
        // Current Time //
        var now = Date.now();
        // Ten Seconds //
        var tenSec = 10 * 1000;
        // Plus Ten Seconds //
        var plusTenSec = currentDocumentTimestamp + tenSec;
        if (now > plusTenSec) {
            location.reload();
        } else {}
    }

    $(".refine-search").click(function() {
        $(".flight-search").slideToggle();
    });
    $(".refine-search-close").click(function() {
        $(".flight-search").slideUp();
    });

    $(".only-link").click(function(event) {
        event.preventDefault();
        let parent = $(this).closest(".checkbox-container").parent();
        let currentCb = $(this).closest(".checkbox-label");
        $("input", parent).prop("checked", false);
        $("input", currentCb).prop("checked", true);
    });

  
       $scope.showErrorMesg = false;
        $scope.filterParam = function() {
            $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        
            $scope.flightSearchResults1 = [];
            $scope.flightSearchResults = [];
            $scope.artloading = true;
            $http.post(ServerService.serverPath + 'flight/filter', $scope.filtermodel, {
                headers: headersOptions
            }).then(function successCallback(response) {
                if (response.data.success) {
                    $scope.flightSearchResults = response.data.data;
                    $scope.firstFlightFilter=$scope.flightSearchResults[0];
                     sessionStorage.setItem("flightFilterData", JSON.stringify($scope.firstFlightFilter));
                    $scope.showErrorMesg = false;
                    $scope.moreOptionFun1();
                    $scope.artloading = false;
            } else {
                $scope.showErrorMesg = true;
                $scope.artloading = false;

                //       if(response.data.error=='001'){
                //         $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
                // /* var Serverurl = "http://localhost:8090/ibp/api/v1/"; */
                // $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
                // var tokenname = $scope.tokenresult.data;
                // var headersOptions = {
                //     'Content-Type': 'application/json',
                //     'Authorization': 'Bearer ' + tokenname.authToken
                // }

                // $http.post(ServerService.serverPath + 'flight/availability', JSON.stringify($scope.searchFlightObj), {

                //     headers: headersOptions
                // }).then(function (response) {
                //     $scope.loading = false;
                //     $scope.disabled = false;
                //     if (response.data.status == 'success') {
                //         sessionStorage.setItem('flightSearchRS', JSON.stringify(response.data));

                //       	$location.path('/flightResult');
                //     } else {

                //         Lobibox.alert('error', {
                //             msg: response.data.errorMessage
                //         })
                //         $timeout(function () {
                //          	$location.path('/flight')
                //         }, 1000);
                //     }

                // });


                //       }
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.filterParam();
                   });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }
    $scope.flightBook = function(flightInfo) {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
  
        sessionStorage.setItem('flightindex', JSON.stringify(flightInfo.indx));

        var repriceDetails = {
            "sessionId":  $scope.result.sessionId,
            "resultIndexId": flightInfo.indx
        }


        $scope.artloading = true;
        $http.post(ServerService.serverPath + 'flight/reprice', repriceDetails, {

            headers: headersOptions
        }).then(function successCallback(response) {
            if(response.data.success){

            $scope.artloading = false;
         
            $scope.flightPriceData = response.data.data
            sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
            // sessionStorage.setItem('flightPriceData', JSON.stringify($scope.flightPriceData));

            $location.path('/flightDetails');

        }else{
            $scope.artloading = false;
                     Lobibox.alert('error', {
                            msg: response.data.errorMessage
                        })
        }
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                $scope.flightBook(flightInfo);
            });
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }


    $scope.totalFare = function(size, parentSelector, index) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightFareTotal.html',
            controller: 'flightFaretotalControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }


    $scope.flightBaggageDetails = function(size, parentSelector, BagDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedBagIndex = BagDetails.indx;
        $rootScope.selectedBagDetails = BagDetails;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightBaggageInfo.html',
            controller: 'flightbaggageinfoController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.flightBaggageDetailsInclusions = function(size, parentSelector, BagDetails) {

        var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    $rootScope.selectedBagIndex = BagDetails.indx;
    $rootScope.selectedBagDetails = BagDetails;
    
    $scope.animationEnabled = true;
    var modalInstance = $uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'flightBaggageInclusions.html',
        controller: 'flightbaggageinfoController',
        backdrop: true,
        size: size,
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
    })
    
    }
    $scope.miniFareRules = function(size, parentSelector, index) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightFareinfoContent.html',
            controller: 'flightFareinfoControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.eachFlightDetails = function(size, parentSelector, FlightDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedIndex = FlightDetails.indx;
        $rootScope.selectedFlightDetails = FlightDetails;
    

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightDetailedinfoContent.html',
            controller: 'flightDetailedinformationContent',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }


});
// flightdetailcontroller....
ouaApp.controller('flightDetailedinformationContent', function($scope, $uibModalInstance, $rootScope) {

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }

    $scope.cabinClass = localStorage.getItem('cabinClass');
    var value = $rootScope.selectedIndex;
    if (!JSON.parse(sessionStorage.getItem('flightFilterData'))) {
        $scope.flightDetails = $scope.result;
        $scope.flightDetails1 = $rootScope.selectedFlightDetails;
    } else {
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
        $scope.flightDetails1 = $rootScope.selectedFlightDetails;
    }

    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }


    $scope.getTotalDuration = function(a, b) {
        $scope.totalDuration = timeConvert(parseInt(a) + parseInt(b));

    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
// flightdetailcontrollerends.....




ouaApp.controller('flightbaggageinfoController', function($scope,refreshService, $uibModalInstance, $rootScope, $http, ServerService) {

    var flightInfo = $rootScope.selectedBagIndex;

    // if ($rootScope.selectedBagDetails.additionalProperties.PTCFareBreakdownList != undefined) {
    //     $scope.baggageWeight = $rootScope.selectedBagDetails.additionalProperties.PTCFareBreakdownList[0].passengerFare.tpaextensions1.baggageInformationList.baggageInformations[0].allowanceWeight;
    // } else {
    //     $scope.baggageWeight = 0
    // }
    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }

    splitFun = function(orgin,dest){
        var orgins = orgin.split("(")[1].split(")")[0];
        var dests = dest.split("(")[1].split(")")[0];
        return orgins+"_"+dests;
    }
    $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    $scope.flightDetails1 = $scope.selectedBagDetails;

    // $scope.flightDetails1.segGrpList.forEach(element => {
    //     if(element.inclusions != null){
    //     element.ssrInclsions = (element.ssrInclsions || {});
    //     if(element.inclusions._PURCHASE_BAGGAGE){
    //         element.ssrInclsions.PurchaseBaggage = (element.ssrInclsions.PurchaseBaggage || "");
    //         element.ssrInclsions.PurchaseBaggage =  element.inclusions._PURCHASE_BAGGAGE
    //     }
    //     if(element.inclusions._CABIN_BAGGAGE){
    //         element.ssrInclsions.CabinBaggage = (element.ssrInclsions.CabinBaggage || "");
    //         element.ssrInclsions.CabinBaggage =  element.inclusions._CABIN_BAGGAGE
    //     }
    //     if(element.inclusions._CHECKED_BAGGAGE){
    //         element.ssrInclsions.CheckedBaggage = (element.ssrInclsions.CheckedBaggage || "");
    //         element.ssrInclsions.CheckedBaggage =  element.inclusions._CHECKED_BAGGAGE
    //     }
    //     if(element.inclusions._MEALS){
    //         element.ssrInclsions.Meal = (element.ssrInclsions.Meal || "");
    //         element.ssrInclsions.Meal =  element.inclusions._MEALS
    //     }
    //     if(element.inclusions._SEATS){
    //         element.ssrInclsions.Seat = (element.ssrInclsions.Seat || "");
    //         element.ssrInclsions.Seat =  element.inclusions._SEATS
    //     }
    //     if(element.inclusions._CANCELLATION){
    //         element.ssrInclsions.Cancellation = (element.ssrInclsions.Cancellation || "");
    //         element.ssrInclsions.Cancellation =  element.inclusions._CANCELLATION
    //     }
       
    // }
    // });
    baggagedtls = function(){

    // if ($scope.particularFlightData.additionalProperties.PTCFareBreakdownList != undefined) {
    //     $scope.baggageWeight = $scope.particularFlightData.additionalProperties.PTCFareBreakdownList[0].passengerFare.tpaextensions1.baggageInformationList.baggageInformations[0].allowanceWeight;
    // } else {
    //     $scope.baggageWeight = 0
    // }
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;

    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }

    var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
    var repriceDetails = {
            "sessionId": sessionId,
            "resultIndexId": flightInfo
        }
    $scope.artloading = true;
    $http.post(ServerService.serverPath + 'flight/ssr', repriceDetails, {

        headers: headersOptions
    }).then(function successCallback(response) {
        if(response.data.success){
         
        $scope.flightSsrData = response
        $scope.baggageSsr = $scope.flightSsrData.data.data;
        $totallBagaggeInfo = [];
        // for (var key in $scope.baggageSsr) {
            $scope.DEFAULT_CABIN_BAGGAGE_COMMENTS=$scope.flightSsrData.data.clientAdditionalProperties.DEFAULT_CABIN_BAGGAGE_COMMENTS;
            $scope.DEFAULT_CABIN_BAGGAGE=$scope.baggageSsr.DEFAULT_CABIN_BAGGAGE;
        for(var fl=0; fl<$scope.flightDetails1.segGrpList.length;fl++){
            for(var sl=0; sl<$scope.flightDetails1.segGrpList[fl].segList.length;sl++){
                for(var fls=0; fls<$scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList.length;fls++){
                    var fltinfo = $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls];
                    var key = splitFun(fltinfo.origin,fltinfo.dest);
                    if($scope.baggageSsr[key] != undefined ){
                    $scope.baggageinfo = $scope.baggageSsr[key].baggageSSRs;
                    var defaultBag = $scope.baggageSsr[key].defaultBaggage[0].cabin;
                    for (var b = 0; b < $scope.baggageinfo.length; b++) {
                        if($scope.baggageinfo[b].passengerKey == 'ADT'){
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt = defaultBag;
                        }
                        if($scope.baggageinfo[b].passengerKey == 'CNN'){
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt = defaultBag;
                       }
                        if($scope.baggageinfo[b].passengerKey == 'INF'){
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt = defaultBag;
                        }
                    }
                 }
                }
            }
        }
        $scope.artloading = false;

        // }
        
        sessionStorage.setItem('ssrtotalData', JSON.stringify($scope.flightSsrData));
        $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrData'));
    }else{
        $scope.artloading = false;
    }


}, function errorCallback(response) {
    if(response.status == 403){
        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                       status.then(function(greeting) {
                        baggagedtls();
                    });
        // $scope.autoAirlines(value);
    }
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
}

if(!$scope.flightDetails1.isLcc){
    baggagedtls();
}

    //  fromflight......for.
    // $scope.cabinClass = localStorage.getItem('cabinClass');
    // var value = $rootScope.selectedBagIndex;
    // if (!JSON.parse(sessionStorage.getItem('flightFilterData'))) {
    //     $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightSearchRS'))
    //     $scope.flightDetails1 = $scope.selectedBagDetails;
    // } else {
    //     $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
    //     $scope.flightDetails1 = $scope.selectedBagDetails;
    // }
    //  $scope.flightDetails2 = $scope.flightDetails
    //  $scope.filter = $rootScope.sCheckId
    //  $scope.airlineCode = $rootScope.airlineCodes;
    //  var flag = false;
    //  var filterDatas = {};
    //  if ($scope.filter != null && $scope.airlineCode != null) {

    //    filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    //    $scope.flightDetails1 = filterDatas[value];
    //  } else if (!flag) {
    //    if ($scope.filter != null || $scope.filter != undefined) {
    //      filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    //      $scope.flightDetails1 = filterDatas[value];
    //    } else if ($scope.airlineCode != null || $scope.airlineCode != undefined) {
    //      filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    //      $scope.flightDetails1 = filterDatas[value];
    //    }
    //  }
    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }


    $scope.getTotalDuration = function(a, b) {
        $scope.totalDuration = timeConvert(parseInt(a) + parseInt(b));

    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



ouaApp.controller('flightFaretotalControllers', function($scope, $http, $uibModalInstance, $rootScope) {
    $scope.currency = "AED";
    $scope.farerule = 'test';
    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }
    // $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    // $scope.flightResultList = $scope.result.data;
    // $scope.flightResultList1 = $scope.flightResultList.flightSearchResults
        // $scope.airlinenames=$scope.flightResultList1[0].segGrpList[0].segList[0].airlineName;
        // $scope.aircraft=$scope.flightResultList1[0].segGrpList[0].segList[0].fltInfoList[0].aircraftType;
    
        var index = $rootScope.selectedFareIndex.indx;
        // $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightSearchRS'));
        $scope.flightDetail1 = $rootScope.selectedFareIndex


        // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $rootScope.selectedFareIndex.fareslst;
        // $scope.provider = $scope.flightDetail.data.flightSearchResults[index].provider;
        // $scope.farekey = $scope.flightDetail.data.flightSearchResults[index].fareslst[0].fareInfoRefKey[0];
    
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    /*   $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      }; */
    // var fareRequest = new Object();		
    // fareRequest.provider = $scope.provider ;
    // fareRequest.key = $scope.farekey;
    // var fareRequestJSON = JSON.stringify(fareRequest);

});


ouaApp.controller('flightFareinfoControllers', function($scope, $http,refreshService, ServerService, $uibModalInstance, $rootScope) {

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }
    // $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    // $scope.flightResultList = $scope.result.data;
    // $scope.flightResultList1 = $scope.flightResultList.flightSearchResults
    
        var index = $rootScope.selectedFareIndex;
        // $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightSearchRS'));
        $scope.flightDetail1 = $rootScope.selectedFareIndex
        $scope.baggagedatafinding = $scope.flightDetail1.additionalProperties.SSR;
        // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $rootScope.selectedFareIndex.fareslst;
        // $scope.bagWeight = $scope.flightDetail.data.flightSearchResults[index].segGrpList[index].ssrDetails[index].baggageList[index].weight;
        // $scope.bagUnit = $scope.flightDetail.data.flightSearchResults[index].segGrpList[index].ssrDetails[index].baggageList[index].Unit;
        $scope.airlinenames = $rootScope.selectedFareIndex.segGrpList[0].segList[0].airlineName;
        $scope.aircraft = $rootScope.selectedFareIndex.segGrpList[0].segList[0].fltInfoList[0].aircraftType;
        if ($scope.aircraft == null) {
            // 	$scope.aircraft="$scope.flightDetail.data.flightSearchResults[index].segGrpList[0].segList[0].fltInfoList[0].aircraftType";
            // }else{
            $scope.aircraft = "-";
        }
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
        if ($rootScope.selectedFareIndex != null) {

            $scope.seggrplist = $rootScope.selectedFareIndex.segGrpList;
            $scope.baggageDetails = [];
            $scope.baggageDetails.segList = [];
            $scope.baggageDetails.ssrDetails = []
            for (var i = 0; i < $scope.seggrplist.length; i++) {
                for (var j = 0; j < $scope.seggrplist[i].segList.length; j++) {
                    $scope.baggageDetails.segList.push($scope.seggrplist[i].segList[j]);
                }
                if ($scope.seggrplist[i].ssrDetails != null) {
                    for (var j = 0; j < $scope.seggrplist[i].ssrDetails.length; j++) {
                        $scope.baggageDetails.ssrDetails.push($scope.seggrplist[i].ssrDetails[j]);
                    }
                } else {
                    $scope.baggagessrdetails = "nossr";
                }
            }
        } else {
            $scope.seggrplist = $scope.flightDetails[index].segGrpList
            $scope.baggageDetails = [];
            $scope.baggageDetails.segList = [];
            $scope.baggageDetails.ssrDetails = []
            for (var i = 0; i < $scope.seggrplist.length; i++) {
                for (var j = 0; j < $scope.seggrplist[i].segList.length; j++) {
                    $scope.baggageDetails.segList.push($scope.seggrplist[i].segList[j]);
                }

                for (var j = 0; j < $scope.seggrplist[i].ssrDetails.length; j++) {
                    $scope.baggageDetails.ssrDetails.push($scope.seggrplist[i].ssrDetails[j]);
                }
            }

        }
        localStorage.setItem('baggagedata', JSON.stringify($scope.baggageDetails));






        $scope.refundtypes = $rootScope.selectedFareIndex.fareslst[0].refundable;
        if ($scope.refundtypes == true) {
            $scope.refundtype = "Refundable";
        } else {
            $scope.refundtype = "Non-Refundable";
        }
        $scope.provider = $rootScope.selectedFareIndex.provider;
        if ($rootScope.selectedFareIndex.fareslst[0].fareInfoRefKey) {
            $scope.farekey = $rootScope.selectedFareIndex.fareslst[0].fareInfoRefKey[0];
        } else {
            $scope.farekey = $rootScope.selectedFareIndex.fareslst[0].fareInfoRefKey;
        }



    

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.loading = true;
    $scope.disabled = true;
    faredetils = function(){
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
    var repriceDetails = {
        "sessionId": sessionId,
        "resultIndexId": index.indx
    }

    $scope.artloading = true;

    $http.post(ServerService.serverPath + 'flight/farerules', repriceDetails, {

        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.artloading = false;
        var data = response.data.data;
        $scope.farerule = data;
        if (data != null) {
            $('.loading').hide();
            $scope.loading = false;
            $scope.disabled = false;


            $scope.farerule = data;
            // if (data != null) {

            //     // for (var f = 0; f < data.length; f++) {
            //     //     $scope.farerule = data[f].content;
            //     //     $('.fareRules').append(data[f].content)
            //     //     $('.fareHeader').append(data[f].headerText)
            //     // }
            // } else {
            //     $scope.farerule = [];//"Fare rules not available";
            //     $('.fareRules').append('Fare rules not available');
            //     $('.fareRules').css('text-align', 'center');
            //     $('.fareRules').css('color', 'red');

            // }
     

        } else {
            $scope.farerule = [];//"Fare rules not available";
            // $('.fareRules').append('Fare rules not available');
            $('.fareRules').css('text-align', 'center');
            $('.fareRules').css('color', 'red');


        }

    }, function errorCallback(response) {
        if(response.status == 403){
           var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                          status.then(function(greeting) {
                            faredetils();
                   });
            // $scope.autoAirlines(value);
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });

    }

    faredetils();

});