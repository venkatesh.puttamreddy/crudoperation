ouaApp.controller('flightItenaryController', function($scope, $http,renderHTMLFactory, refreshService, $window,ServerService, $location, customSession) {
    localStorage.removeItem("loginToken");
    sessionStorage.setItem('activeService',1);
  loadCMS();
    
        $scope.monthcnvrt =  function(val){
            return new Date(2014, val-1);
        }

        $scope.renderHTMLFactory=renderHTMLFactory;
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname =tokenresult.data;
        $scope.flightSearchRQ=JSON.parse(sessionStorage.getItem('flightSearchRQ'));
 
    // $scope.confirmJson = $scope.response.data;
    $scope.flightconfirmJson = JSON.parse(sessionStorage.getItem("bookingItneryResponse")); //Retrieve the stored data
    $scope.flightBookingReq = JSON.parse(localStorage.getItem('flight/pnr/create'));
    if( $scope.flightBookingReq!=null){
        $scope.flightBookingReq.paymentParameters.paymentReferenceNo= $scope.flightconfirmJson.referenceNo;
    }
    // if($scope.flightconfirmJson.referenceNo != null )
    // $scope.flightBookingReq.paymentParameters.paymentReferenceNo= $scope.flightconfirmJson.referenceNo;
    $scope.agnecyEmail = "art-holidays@alrostamanigroup.ae";
    $scope.agencyPhoneNo ="800278 (800-ART)";
   
    // if(!$scope.flightconfirmJson.success){
	// 	Lobibox.alert('error', {
	// 		msg: $scope.flightconfirmJson.errorMessage
	// 	});	
		
	// }

//adding SSR been
$scope.ssrdetails = false;
$scope.ancillarydetails =[];
$scope.ancillary = function(){
    var groups = {};
    var routList = [];
    // for (var ts = 0; ts < $scope.routesdata.length; ts++) {
    //     key = $scope.routesdata[ts][0] + '-' + $scope.routesdata[ts][$scope.routesdata[ts].length - 1];
    //     routList.push(key);
    //     for (var j = 0; j <= $scope.routesdata[ts].length - 2; j++) {
    //         mealKey = $scope.routesdata[ts][j] + '-' + $scope.routesdata[ts][j + 1];
    //         routList.push(mealKey);
    //     }

    // }

    $scope.confirmJson.psgrLst.forEach(function(val) {
    if(val.ssrDetailList != null){
        $scope.ssrdetails = true;
        for (var i = 0; i < val.ssrDetailList.length; i++) {
            var groupSegment =val.ssrDetailList[i].flightRoute;
            if (!groups[groupSegment]) {
              groups[groupSegment] = [];
            }
            val.flightNo = "";
            val.flightNo = val.ssrDetailList[i].flightNo;
            val.ssrDetailList[i].paxType = val.passengerType || "";
            val.ssrDetailList[i].paxName = val.title +" " + val.firstName + " " + val.lastName || "";
            groups[groupSegment].push(val.ssrDetailList[i]);
          }
        }else{
            $scope.ssrdetails = false;
        }

    });
    // console.log(groups);

    for (var groupName in groups) {
        $scope.ancillarydetails.push({segments: groupName, ssrdetails: groups[groupName]});
      }
    // console.log($scope.ancillarydetails);
}


$scope.segmentconvert = function(seg){
    return seg.replace("-", "/")
}

if($scope.flightconfirmJson.data!=null){
    $scope.confirmJson = $scope.flightconfirmJson.data;
    if($scope.confirmJson.agentInfoBean!=null)
    {
        $scope.agnecyEmail = $scope.confirmJson.agentInfoBean.agentEmail;
        $scope.agencyPhoneNo = $scope.confirmJson.agentInfoBean.agentPhoneNo;
    } 
    $scope.fareInfo = $scope.confirmJson.fareInfoLst;
    $scope.discount = $scope.confirmJson.discnt;
    $scope.servicecharge = $scope.confirmJson.servChrg;
    $scope.grandtotal = $scope.confirmJson.grandTot;
    // $scope.flightPriceData = sessionStorage.getItem("flightPriceData"); //Retrieve the stored data
    // $scope.searchPasflights = JSON.parse($scope.flightPriceData);
    // if($scope.confirmJson.prvdrCode == 'SAB'){
    // $scope.isLcc = false;
    // $scope.ancillarydetails =[];
    // }
    // else{
    // $scope.isLcc = true;

    // }
    // $scope.isLcc = true;
    $scope.ancillary ();

    $scope.passengListArray = $scope.confirmJson.psgrLst;
    for (var i = 0; i < $scope.passengListArray.length; i++) {
        $scope.splSerDataLst = $scope.passengListArray[i].ssrDetailList;
    }
    
}else{
    
}

//           for (var i = 0; i < $scope.confirmJson.length; i++) {
//             var groupSegment = $scope.flightSearchResults[i].segGrpList[0].segList[0].airlineName;
//             var groupAED = $scope.flightSearchResults[i].grndTotal;
//             if (!groups[groupSegment] && !AEDGroup[groupAED]) {
//               groups[groupSegment] = [];
//             // if(!AEDGroup[groupAED]){
//               AEDGroup[groupAED] = [];
//             // }
//             }
//             groups[groupSegment].push($scope.flightSearchResults[i]);
//           }
//         for (var groupName in groups) {
//           $scope.flightSearchResults1.push({airline: groupName, airlineList: groups[groupName]});
//         }
// $scope.flightSearchResults.forEach(function(val) {
//     $scope.ancillarydetails = $scope.confirmJson.psgrLst.ssrDetailList.flightRoute;
// });

// $scope.ancillary = function(){

//     return Object.keys(result).map(function(key) {
//         $scope.ancillarydetails.push(result[key]);

//     });
// }
// $scope.ancillary ();


$scope.rePamentSelect=function(){
    $scope.artloading = true;
    var tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname =tokenresult.data;
    var headersOptions = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + tokenname.authToken
  }
$http.post(ServerService.serverPath + 'flight/pnr/create', $scope.flightBookingReq, {
    headers: headersOptions
}).then(function successCallback(response) {
    $scope.loading = false;
    $scope.disabled = false;
    $scope.artloading = false;
    /* $scope.paymentDetail = $scope.cardDetail; */
    if (response.data.success) {
        // sessionStorage.setItem("bookingResponse", JSON.stringify(response.data.data));

        window.open(response.data.data.hostedPageLink, "_self")
    } else {

        Lobibox.alert('error', {
            msg: response.data.errorMessage
        });
        // }else{

        // 			Lobibox.alert('error',
        // 		{
        // 			msg: 'Unexpected Error Occurred, Please Try Again Later'
        // 		});
    }


});

}




    $scope.tripsDetail = function() {
        $('body').addClass("loading");
        $location.path('/user-trips');
        $timeout(function() {
            $('body').removeClass("loading");
        }, 1000);
    };

    $scope.printRequest = {};
    $scope.printTkt = function(i) {
        sessionStorage.removeItem('hotelBookJson');
        $scope.printRequest.bookingRefNo = $scope.confirmJson.pnrNo;
        $scope.printRequest.serviceType = $scope.confirmJson.serviceOrderType;
        $scope.printRequest.clientRefNo = $scope.confirmJson.clientRefNo;
        $scope.printRequest.type = "DB";
        $http({
            method: 'POST',
            data: $scope.printRequest,
            url: ServerService.serverPath + 'rest/retrieve/itinerary'
        }).then(function successCallback(response, status) {
            if (response.data.status == "SUCCESS") {
                var nav_url = window.location.hostname + window.location.pathname + "#!/terms_conditions"
                sessionStorage.setItem("navUrl", nav_url);
                response.data.data.tIndex = i;
                var jsondata = response.data.data;
                //sessionStorage.setItem("eTicketFlight",JSON.stringify(jsondata));
                //	console.log(jsondata.serviceOrderType);
                if (jsondata.serviceOrderType == 'FLT') {
                    sessionStorage.setItem("flightconfirmJson", JSON.stringify(jsondata));
                }
                if (jsondata.serviceOrderType == 'FNH') {
                    sessionStorage.setItem("flightconfirmJson", JSON.stringify(jsondata.fltBookingInfo));
                    sessionStorage.setItem("hotelBookJson", JSON.stringify(jsondata.htlBookingInfo));
                }
                window.open("print_pages/flight/printflightItenary.html", '_blank');
            } else {
                Lobibox.alert('error', {
                    msg: data.errorMessage
                });
            }

        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
               status.then(function(greeting) {
                $scope.printTkt(i);
            });                
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }


    $scope.loginCheck = false;
    $scope.logged = JSON.parse(localStorage.getItem('loginName'));
    if ($scope.logged != null) {
        $scope.loginCheck = true;
    }

    $scope.tripsDetail = function() {
        $location.path('/user');
    }

    $scope.printDiv = function(flightprintDiv) {
        // function printDiv(tourprintDiv) {
        //console.log($scope.holidayDetails);
        var printContents = document.getElementById('flightprintDiv').innerHTML;
        var popupWin = window.open('', '_blank', 'width=300,height=300');
        popupWin.document.open();
        // popupWin.document.write('<html><head><link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet"><link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" /><link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css" /><link rel="stylesheet" href="styles.css" /><link rel="stylesheet" href="assets/css/holiday.css" /><link rel="stylesheet" href="assets/css/jquery.desoslide.css" /><link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.write('<html><head><link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet"><link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" /><link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css" /><link rel="stylesheet" href="styles.css" /><link rel="stylesheet" href="assets/css/holiday.css" /><link rel="stylesheet" href="assets/css/jquery.desoslide.css" /><link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" /></head><body onload="window.print()">  <div class="row"><div class="print-logo col-12" style="float:right!important;text-align: end;" > <img src="./assets/logoart.gif" alt="Alogo"></div></div>' + printContents + '</body></html>');

        popupWin.document.close();
        $scope.printLogotime=true;
   

        // window.open(pdfUrl, '_blank');
    }

    $scope.fltDownload = function(){
        // ServerService.serverPath+'process-pdf/F/'+$scope.confirmJson.pnrNo
        window.open(ServerService.serverPath+'process-pdf1/F/'+$scope.confirmJson.docketNo);
        // window.open('http://192.168.1.99/gateway/ibp/api/v1/process-pdf1/F/65309440');
    }

    $scope.sendMailId = function() {
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname =tokenresult.data;
            var headersOptions = {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + tokenname.authToken
            }
          
        $http.get(ServerService.serverPath+'mail/send/F/'+$scope.confirmJson.docketNo+'?to='+$scope.userCreationReqst.loginid,{
            headers: headersOptions
        }).then(function(response) {
            if(response.data.success){
                $scope.termsresp = response.data;
                Lobibox.alert('success', {
                    msg:response.data.message
                    }); 
            }else{
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                })
            }
            
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
               status.then(function(greeting) {
                $scope.sendMailId();
            });                  
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    }
});