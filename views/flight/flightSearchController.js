ouaApp.controller('flightSearchCtrl', function($scope, $http,refreshService, $location, $timeout, ServerService, $filter) {
    // $(".refine-search").click(function () {
    // 	$(".flight-search").slideToggle();
    //   });
    $('.show-calendar ').hide();
    $(".refine-search-close").click(function() {
        $(".flight-search").slideUp();
    });
    $("#load").show();
    setTimeout(function () {
       $('#load').hide();
    }, 1000);
    // window.addEventListener('keydown', function(e){
    //     if((e.key=='Escape'||e.key=='Esc')){
    //         e.preventDefault();
    //         $('.daterangepicker').hide();
    //     }
    // }, true);
    // daterangepicker.appendTo('#element');
    // document.onkeyup = function (e) {
    // if (e.altKey && e.keyCode === 84 /* t */) {
    //     daterangepicker.element.focus();
    // }
    // $scope.clickdate = function(){
    //     $('.show-calendar ').show();

    // }
    // $(window).scroll(function(){ 
        
    //     if ($(this).scrollTop() > 200) { 
    //         if(document.getElementById("amenitiesfilter")!=null)
    //         document.getElementById("amenitiesfilter").style.height = "600px";
    //     } else { 
    //         if(document.getElementById("amenitiesfilter")!=null)
    //         document.getElementById("amenitiesfilter").style.height = "250px";
    //     } 
    // }); 
    // $scope.searchType="multiCity";
    $scope.searchTrip = "roundtrip"
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    
    var minmultiDate = new Date();
    var today = new Date();
    today.setDate(today.getDate() + 321 - 1);


    $scope.imagepath = ServerService.listPath + "file/download/";
    $scope.mclassType = "Economy";
    $scope.createMultiCityInfo = function() {

        $scope.multiCityFlightSearchRQ.multicityInfos.push({
            frmAirport: "",
            toAirport: "",
            strDeptDate: "",
            srchType:3,
            classType: $scope.mclassType,
            prefferedAirlineList: [],
            prefNonStop: false,

        });

    }


    $scope.multicityWhendata = function(val, val1, index) {
        var totalTravllersCount = parseInt($scope.multiCityFlightSearchRQ.adtCnt) + parseInt($scope.multiCityFlightSearchRQ.chldCnt) +
            parseInt($scope.multiCityFlightSearchRQ.infCnt);
            var minDate=new Date();
            if(index!=0)
            minDate= $scope.multiCityFlightSearchRQ.multicityInfos[index-1].strDeptDate
            $('#multi_flying_departure_'+index).daterangepicker({
                "singleDatePicker": true,
                "autoUpdateInput": true,
                "locale": {
                    "format": "DD/MM/YYYY"
                },
                "autoApply": true,
                "startDate": $scope.multiCityFlightSearchRQ.multicityInfos[index].strDeptDate,
                "minDate":minDate
              
            }, function(start, end, label) {
                $scope.multiCityFlightSearchRQ.multicityInfos[index].strDeptDate= start.format('DD/MM/YYYY') 
                if($scope.multiCityFlightSearchRQ.multicityInfos.length>index+1)
                $scope.multiCityFlightSearchRQ.multicityInfos[index+1].strDeptDate= start.format('DD/MM/YYYY') 
            });

       

    }
    
    $scope.initMultiCityTrip = function() {
        $("#oneWay").removeClass("active");
        $("#round").removeClass("active");
        $("#multiCity").addClass("active");
        $("#roundTrip").removeClass("active");
        $("#multiple").addClass("active");
        $("#oneWay a").removeClass("active");
        $("#round a").removeClass("active");
        var multiCityFlightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
        $scope.searchType = "multiCity";
        $scope.flightop = {
            opens: "center",
            singleDatePicker: true,
            autoApply: true,
            autoUpdateInput: true,
            minDate: minmultiDate,
            maxDate: today,

            eventHandlers: {
                'show.daterangepicker': function(ev, picker) {
                    // $scope.flightop = {
                    //     singleDatePicker: true //$scope.checkboxModel.value
                    // }
                }
            }

        }
        $scope.showOnewayDatepicker = true;
        // $scope.multiCityFlightSearchRQ.multicityInfos = [] 
        if (multiCityFlightSearchRQ != null && multiCityFlightSearchRQ.multicityInfos != undefined) {
            for (var i = 0; i < multiCityFlightSearchRQ.multicityInfos.length; i++) {
                $('#multi_flying_departure_'+i).val(multiCityFlightSearchRQ.multicityInfos[i].strDeptDate);
                $scope.mclassType=multiCityFlightSearchRQ.multicityInfos[i].classType;
            }

        } else {
            $scope.multiCityFlightSearchRQ.multicityInfos = [];
            for (var i = 0; i < 2; i++) {
                $scope.createMultiCityInfo();
                $scope.totalSelectedSize++;
            }

            if($scope.flightSearchRQ.frmAirport != null && $scope.flightSearchRQ.toAirport != null){
                $scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport = $scope.flightSearchRQ.frmAirport;
            
                $scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport = $scope.flightSearchRQ.toAirport;
                $scope.multiCityFlightSearchRQ.multicityInfos[1].frmAirport = $scope.flightSearchRQ.toAirport;
             
            }
            if($scope.flightSearchRQ.strDeptDate != null && $scope.flightSearchRQ.strRetDate != null){
                $('#multi_flying_departure_0').val($scope.flightSearchRQ.strDeptDate);
                $('#multi_flying_departure_1').val($scope.flightSearchRQ.strRetDate);
                $scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate = $scope.flightSearchRQ.strDeptDate;
                $scope.multiCityFlightSearchRQ.multicityInfos[1].strDeptDate = $scope.flightSearchRQ.strRetDate

            }
        }
        // $scope.intiMultiCityFromAirPortAutoComplete();
        // $scope.intiMultiCityToAirPortAutoComplete()
        // $scope.bindingFun();

    }

    $scope.flightop = {
        opens: "center",
        singleDatePicker: false,
        autoApply: true,
        autoUpdateInput: true,
        minDate: new Date(),
        maxDate: today,
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                // $scope.flightop = {
                //         singleDatePicker: false 
                //     }
            },
            'hide.daterangepicker':function(ev, picker) {
                $('select[name=naming]').focus();

            }
        }

    }

    $("#round").addClass("active");
    $scope.bindingFun = function() {
        if (sessionStorage.getItem('flightSearchRQ') != undefined) {
            // && $scope.urlPath=='/flight'
            // if (searchType === 'roundTrip') {
            $scope.flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
            // }else{

            // }
            if ($scope.flightSearchRQ.frmAirport != undefined) {
                $('#from_place').val($scope.flightSearchRQ.frmAirport);
                $('#to_place').val($scope.flightSearchRQ.toAirport);
                $scope.totalDays = $scope.flightSearchRQ.totDays;
                var combine = $scope.flightSearchRQ.strDeptDate + " - " + $scope.flightSearchRQ.strRetDate
            }
            $scope.searchTrip = 'roundtrip';
   
            if($scope.flightSearchRQ.srchType==1){
                $("#oneWay").addClass("active");
                $("#round").removeClass("active");
                $("#multiCity").removeClass("active");
                $("#roundTrip").addClass("active");
                $("#multiple").removeClass("active");
                $("#round a").removeClass("active");
                $("#multiple a").removeClass("active");
                $scope.searchTrip = "oneway";
                        
            }else if($scope.flightSearchRQ.srchType==2){
                $("#oneWay").removeClass("active");
                $("#multiCity").removeClass("active");
                $("#roundTrip").addClass("active");
                $("#multiple").removeClass("active");
                $("#oneWay a").removeClass("active");
                $("#multiple a").removeClass("active");
                $("#round").addClass("active");
                $scope.searchTrip = 'roundtrip';
   
            // $scope.searchTrip = "ro";
            }else{
                $("#multiple").addClass("active");
                $("#round").removeClass("active");
                $("#oneWay").removeClass("active");
                $("#multiCity").addClass("active");
                $("#roundTrip").removeClass("active");
                $("#multiple").addClass("active");
                $("#oneWay a").removeClass("active");
                $("#round a").removeClass("active");
                $scope.multiCityFlightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
                $scope.initMultiCityTrip();
            }
            var infArr = [];
            for (var k = 0; k <= $scope.flightSearchRQ.adtCnt; k++) {
                infArr.push(k);
            }
            $scope.fInfantCount = infArr;
            if ($scope.flightSearchRQ.multicityInfos != undefined) {
                $scope.flightSearchRQ = {

                    frmAirport: "",
                    toAirport: "",
                    adtCnt: "1",
                    chldCnt: "0",
                    infCnt: "0",
                    strDeptDate: "",
                    strRetDate: "",
                    prefNonStop: false,
                    totDays: "",
                    classType: "Economy",
                    prefAirlineList: [],
                    promoCode: ""

                }
                $scope.multiCityFlightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
                for (var i = 0; i < $scope.multiCityFlightSearchRQ.multicityInfos.length; i++) {
                    $('#multi_flying_departure_'+i).val($scope.multiCityFlightSearchRQ.multicityInfos[i].strDeptDate);

                    var minDate=new Date();
                    if(i!=0)
                    minDate= $scope.multiCityFlightSearchRQ.multicityInfos[i-1].strDeptDate
                    $('#multi_flying_departure_'+i).daterangepicker({
                        "singleDatePicker": true,
                        "autoUpdateInput": true,
                        "locale": {
                            "format": "DD/MM/YYYY"
                        },
                        "autoApply": true,
                        "startDate": $scope.multiCityFlightSearchRQ.multicityInfos[i].strDeptDate,
                        "minDate":minDate
                      
                    }, function(start, end, label) {
                        $scope.multiCityFlightSearchRQ.multicityInfos[i].strDeptDate= start.format('DD/MM/YYYY') 
                        if($scope.multiCityFlightSearchRQ.multicityInfos.length>i+1)
                        $scope.multiCityFlightSearchRQ.multicityInfos[i+1].strDeptDate= start.format('DD/MM/YYYY') 
                    });
        
                }
            } else if ($scope.flightSearchRQ.strRetDate === null) {
                $('#daterange1').val($scope.flightSearchRQ.strDeptDate);
                $scope.flightop = {
                    opens: "center",
                    singleDatePicker: true,
                    autoApply: true,
                    autoClose: true,
                    autoHide: true,
                    autoUpdateInput: true,
                    minDate: new Date(),
                


                    eventHandlers: {
                        'show.daterangepicker': function(ev, picker) {
                            // $scope.flightop = {
                            //     singleDatePicker: true //$scope.checkboxModel.value
                            // }
                        }
                    }
                }
                $scope.totalDays = '0';
                $scope.showOnewayDatepicker = true;
                $scope.multiCityFlightSearchRQ = {
                    /*  frmAirport: "",
            toAirport: "", */
                    adtCnt: "1",
                    chldCnt: "0",
                    infCnt: "0",
                    promoCode: "",
                    classType:"Economy",
                    srchType:3

                    /*  prefAirlineList: [],
                     prefNonStop: false,
                     multicityInfos: [ $scope.initMultiCityTrip()], */


                }
            } else {
                $('#daterange1').val(combine);
                $scope.flightop = {
                    opens: "center",
                    autoClose: true,
                 autoHide: true,

                    singleDatePicker: false,
                    autoApply: true,
                    autoUpdateInput: true,
                    minDate: new Date(),

                    eventHandlers: {
                        'show.daterangepicker': function(ev, picker) {
                            // $scope.flightop = {
                            //     singleDatePicker: false //$scope.checkboxModel.value
                            // }
                        }
                    }
                }

                $scope.showOnewayDatepicker = false;
                $scope.multiCityFlightSearchRQ = {
                    /*  frmAirport: "",
            toAirport: "", */
                    adtCnt: "1",
                    chldCnt: "0",
                    infCnt: "0",
                    promoCode: "",
                    classType:"Economy",
                    srchType:3

                    /*  prefAirlineList: [],
                     prefNonStop: false,
                     multicityInfos: [ $scope.initMultiCityTrip()], */
                }
            }
        } else {

            $scope.totalDays = '0';

            $scope.flightSearchRQ = {
                frmAirport: "",
                toAirport: "",
                adtCnt: "1",
                chldCnt: "0",
                infCnt: "0",
                strDeptDate: "",
                strRetDate: "",
                prefNonStop: false,
                totDays: "",
                classType: "Economy",
                prefAirlineList: [],
                promoCode: ""

            }
            $scope.multiCityFlightSearchRQ = {
                /*  frmAirport: "",
        toAirport: "", */
                adtCnt: "1",
                chldCnt: "0",
                infCnt: "0",
                promoCode: "",
                classType: "Economy",
                srchType:3
                /*  prefAirlineList: [],
                 prefNonStop: false,
                 multicityInfos: [ $scope.initMultiCityTrip()], */
            }

            if ($scope.multiCityFlightSearchRQ.multicityInfos != undefined) {
                for (var i = 0; i < $scope.multiCityFlightSearchRQ.multicityInfos.length; i++) {
                    $('#multi_flying_departure_'+i).val($scope.multiCityFlightSearchRQ.multicityInfos[i].strDeptDate);
                }

            } else {
                $scope.multiCityFlightSearchRQ.multicityInfos = [];
                for (var i = 0; i < 2; i++) {
                    $scope.createMultiCityInfo();
                    // $scope.totalSelectedSize++;
                }
            }


        }
    }
    $scope.bindingFun();
    //  var maxDate = new Date(today.getFullYear(), today.getMonth(), today.setDate(today.getDate()) + 10);

    $scope.prefAirlineList = []
   

    $scope.myFunction = function(x, y) {
           // if(x !=undefined && y!=undefined){
        //     $('select[name=naming]').focus();
        // }
    //       window.addEventListener('keydown', function(e){
    //      $(document).on('keypress', function(e) {
    //     if((e.key=='Tab'||e.key=='Tab')){
    //         e.preventDefault();
    //         e.which !== 9 || $('[tabIndex=' + (+this.tabIndex + 1) + ']')[0].focus();
    //     }            
    // });
    // }, true);

        // var a = $scope.flightSearchRQ.strDeptDate ;
        // var b =  $scope.flightSearchRQ.strRetDate ;
        var a = x;
        var b = y;
        //  var totDays = $('#daterange1').val();
        // var totD = []
        // totD = totDays.split(' ');
        //totD[0].toString();
        //totD[2].toString();
        // var x = new Date(totD[0]);
        //  var y = new Date(totD[2]);
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const firstDate = a;
        const secondDate = b;

        const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
        if (diffDays > 0) {
            $scope.totalDays = diffDays;
        }else{
            $scope.totalDays = '0'
        }
    }

    $scope.dateConvert = function(str) {
        var date = new Date(str),
          mnth = ("0" + (date.getMonth() + 1)).slice(-2),
          day = ("0" + date.getDate()).slice(-2);
        return [day, mnth,  date.getFullYear()].join("/");
      }

    $scope.oneWayAndRoundTrip = function(searchType, checkboxModelvalue) {

        if (searchType == 1) {
            $("#oneWay").addClass("active");
            $("#round").removeClass("active");
                $("#multiple").removeClass("active");
                $("#multiCity").removeClass("active");
            $("#roundTrip").addClass("active");
            $("#multiple a").removeClass("active");
            $("#round a").removeClass("active");

            $scope.searchType = searchType
            $scope.searchTrip = "oneway";
            $scope.onewayduration = true;
            // $('.daterangepicker.dropdown-menu').css('max-width','fit-content !important');
            // var flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));

            $scope.flightSearchRQ.strDeptDate =  $scope.flightSearchRQ.strDeptDate;
            $scope.flightSearchRQ.strRetDate = null;
            $scope.flightSearchRQ.chldCnt = "0";
            $scope.flightSearchRQ.infCnt = "0";
            $scope.flightSearchRQ.adtCnt = "1";
            $scope.flightop = {
                opens: "center",
                 autoclose: true,
                singleDatePicker: true,
                autoApply: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,

                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    },
                    'hide.daterangepicker':function(ev, picker) {
                        $('select[name=naming]').focus();
                      
        
                    }
                }
            }
            $scope.totalDays = 0;
            if($("#daterange1").val().split('-')){
                $("#daterange1").val($("#daterange1").val().split('-')[0]);
                $scope.totalDays = 0;
            }else{
                // $("#daterange1").val('');
          
            }
            // if(flightSearchRQ.strDeptDate != null && flightSearchRQ.strRetDate == null){
            //     $("#daterange1").val(flightSearchRQ.strDeptDate);
            //     }else{
            //     }

            if($scope.multiCityFlightSearchRQ != null && $scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport != ""
            && $scope.multiCityFlightSearchRQ.multicityInfos[1].frmAirport != ""){
                $scope.flightSearchRQ.frmAirport = $scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport
                $scope.flightSearchRQ.toAirport = $scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport
                $('#from_place').val($scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport);
                $('#to_place').val($scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport);
                if($scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport==$scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport){
                    $scope.flightSearchRQ.toAirport = null;
                    $('#to_place').val('');
                }
                if($scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate != "" && $scope.multiCityFlightSearchRQ.multicityInfos[1].strDeptDate != ""){
                    $("#daterange1").val($scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate);
                    $scope.flightSearchRQ.strDeptDate =  $scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate;
                    $scope.flightSearchRQ.strRetDate = null;
                }
            }        
            $scope.showOnewayDatepicker = true;
            // $scope.totalDays = '0';
        } else if (searchType == 2) {
            $("#oneWay").removeClass("active");
            $("#round").addClass("active");
                $("#multiple").removeClass("active");
                $("#multiCity").removeClass("active");
            $("#roundTrip").addClass("active");
            $("#multiple a").removeClass("active");
            $("#oneWay a").removeClass("active");

            // $scope.myFunction($scope.flightSearchRQ.strDeptDate,$scope.flightSearchRQ.strRetDate)
            // var flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
            // if(flightSearchRQ.strRetDate != null){
            // var combine = flightSearchRQ.strDeptDate + " - " + flightSearchRQ.strRetDate;
            // $scope.flightSearchRQ.strDeptDate = flightSearchRQ.strDeptDate;
            // $scope.flightSearchRQ.strRetDate = flightSearchRQ.strRetDate;
            // }else{
            //     $scope.flightSearchRQ.strDeptDate = "";
            //     $scope.flightSearchRQ.strRetDate = "";
            // }
            $scope.searchType = searchType
            $scope.searchTrip = "roundtrip"
            $scope.onewayduration = false;
                // $('.daterangepicker.dropdown-menu').css('max-width','493px !important');
            
                // $scope.flightSearchRQ.strDeptDate = "";// $scope.flightSearchRQ.strDeptDate;
                // $scope.flightSearchRQ.strRetDate = "";
                // $scope.flightSearchRQ.totDays = 0;
            $scope.flightSearchRQ.chldCnt = "0";
            $scope.flightSearchRQ.infCnt = "0";
            $scope.flightSearchRQ.adtCnt = "1";
            $scope.flightop = {
                opens: "center",
                autoclose: true,
                singleDatePicker: false,
                autoApply: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,
                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: false //$scope.checkboxModel.value
                        // }
                    },
                    'hide.daterangepicker':function(ev, picker) {
                        // $('select[name=naming]').focus();
                    
        
                    }
                }
            }
            // if($("#daterange1").val().split('-')){
            //     // $("#daterange1").val().split('-')[0];
            // }else{
            //     $("#daterange1").val();
            // }
            if($scope.strDeptDate != undefined && $scope.strDeptDate.startDate != "" && $scope.strDeptDate.endDate){
                var strdt = $scope.dateConvert(new Date($scope.strDeptDate.startDate))
                var enddt = $scope.dateConvert(new Date($scope.strDeptDate.endDate))

                $("#daterange1").val(strdt + ' - ' + enddt);
                $scope.flightSearchRQ.strDeptDate = strdt;
                $scope.flightSearchRQ.strRetDate = enddt;
            }else{
                $scope.totalDays = '0';
                $scope.strDeptDate = "";
                $scope.flightSearchRQ.strDeptDate =  "";
                $scope.flightSearchRQ.strRetDate = "";
                $("#daterange1").val('');

            }  
            if($scope.multiCityFlightSearchRQ != null && $scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport != ""
            && $scope.multiCityFlightSearchRQ.multicityInfos[1].frmAirport != ""){
                $scope.flightSearchRQ.frmAirport = $scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport
                $scope.flightSearchRQ.toAirport = $scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport
                $('#from_place').val($scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport);
                $('#to_place').val($scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport);
                if($scope.multiCityFlightSearchRQ.multicityInfos[0].frmAirport==$scope.multiCityFlightSearchRQ.multicityInfos[0].toAirport){
                    $scope.flightSearchRQ.toAirport = null;
                    $('#to_place').val('');
                }
                if($scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate != "" && $scope.multiCityFlightSearchRQ.multicityInfos[1].strDeptDate != ""){
                    $("#daterange1").val($scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate + " - " + $scope.multiCityFlightSearchRQ.multicityInfos[1].strDeptDate);
                    $scope.flightSearchRQ.strDeptDate =  $scope.multiCityFlightSearchRQ.multicityInfos[0].strDeptDate;
                    $scope.flightSearchRQ.strRetDate = $scope.multiCityFlightSearchRQ.multicityInfos[1].strDeptDate;
                }
            }     
            $scope.showOnewayDatepicker = false;
        }


        var date = new Date();
        var currentDate = date.getDate() + 1;

        if (checkboxModelvalue == true) {

            var dateelement = angular.element('#daterange1');
            var dateval = dateelement.val();
            var dateclass = dateelement.attr("class");
            var datevalsingle = dateval.split(" - ");

            localStorage.setItem("isOneWay", true)
            $("#daterange1").parent().addClass('has-error');
            // $scope.flightSearchRQ.strDeptDate = angular.element('#daterange1').val();
            $scope.flightSearchRQ.strDeptDate=$scope.flightSearchRQ.strDeptDate;

        } else if (checkboxModelvalue == false) {
            var dateelement = angular.element('#daterange1');
            dateelement.val('');


            $('input[name=daterange]').focus();
            $("#daterange1").parent().addClass('has-error');
            localStorage.setItem("isOneWay", false)


        }
        // $scope.bindingFun();
                $scope.totalDays = '0';

    }



    $scope.multicityWhen = function(val, val1, index) {
        var totalTravllersCount = parseInt($scope.multiCityFlightSearchRQ.adtCnt) + parseInt($scope.multiCityFlightSearchRQ.chldCnt) +
            parseInt($scope.multiCityFlightSearchRQ.infCnt);

        if (index == 0) {
            $scope.flightop = {
                opens: "left",
                singleDatePicker: true,
                autoApply: true,
                autoclose: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,

                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    }
                }

            }
            $scope.multiCityFlightSearchRQ.multicityInfos[index].strDeptDate = $('#multi_flying_departure_0').val();

        } else if (index === 1) {
            minmultiDate = $('#multi_flying_departure_0').val();
            $scope.flightop = {
                opens: "left",
                autoclose: true,
                singleDatePicker: true,
                autoApply: true,
                autoUpdateInput: true,
                minDate: minmultiDate,
                maxDate: today,

                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    }
                }

            }

            $scope.multiCityFlightSearchRQ.multicityInfos[index].strDeptDate = $('#multi_flying_departure_1').val();
        } else if (index === 2) {
            minmultiDate = $('#multi_flying_departure_1').val();
            $scope.flightop = {
                opens: "left",
                autoclose: true,
                singleDatePicker: true,
                autoApply: true,
                autoUpdateInput: true,
                minDate: minmultiDate,
                maxDate: today,

                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    }
                }

            }
            $scope.multiCityFlightSearchRQ.multicityInfos[index].strDeptDate = $('#multi_flying_departure_2').val();
        } else if (index === 3) {
            minmultiDate = $('#multi_flying_departure_2').val();
            $scope.flightop = {
                opens: "left",
                autoclose: true,
                singleDatePicker: true,
                autoApply: true,
                autoUpdateInput: true,
                minDate: minmultiDate,
                maxDate: today,

                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    }
                }

            }
            $scope.multiCityFlightSearchRQ.multicityInfos[index].strDeptDate = $('#multi_flying_departure_3').val();
        }
        // if (val1 !== undefined) {


        //     var selectedDate = val1;
        //     //  tmp = angular.element('#departure');
        //     // var d1 = tmp.val();
        //     var d2 = new Date(selectedDate);
        //     d2.setDate(d2.getDate() + 1);
        //     // minmultiDate = d2;
        //     // var date2 = d1.split('/').reverse().join("-");
        //     // minmultiDate = date2;

        // }

    }
    $scope.addMultiCityTrip = function() {
        if ($scope.multiCityFlightSearchRQ.multicityInfos.length < 4)
            $scope.createMultiCityInfo();
        //$scope.intiMultiCityAutoComplete();
    }
    $scope.removeMultiCityTrip = function() {
        if ($scope.multiCityFlightSearchRQ.multicityInfos.length > 2)
            $scope.multiCityFlightSearchRQ.multicityInfos.pop(1);
    }






    // if($scope.searchFlightObjJson.prefNonStop){
    //     $scope.prefNonStop = true;
    //     }

    var reminder2 = "";
    jQuery('#contenteditables1').keyup(function(e) {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        if (jQuery(this).val().length == 2) {
            $http.get(ServerService.serverPath + 'flight/airlines/' + jQuery(this).val().toUpperCase(), {
                headers: headersOptions
            }).then(function successCallback(result, status) {
                var availableTags = [];
                var data = result.data.data;
                for (var i = 0; i < data.length; i++) {
                    var prefcount = 0;
                    var contentcount = 0;
                    var checkCity4 = data[i].shortName;
                    var checkCity1 = checkCity4.toLowerCase();
                    var checkCity2 = data[i].displayname + ' (' + data[i].shortName + ')';
                    if (jQuery('#contenteditables1').val().toLowerCase() == data[i].shortName.toLowerCase()) {

                        jQuery('.contenttags').find('.destination').each(function() {

                            if (data[i].shortName.toLowerCase() == jQuery(this).text().toLowerCase()) {
                                contentcount = contentcount + 1;
                                prefcount = prefcount + 1;
                            }
                            if (prefcount == 0) {
                                if (jQuery('#contenteditables1').val().toLowerCase() != data[i].shortName.toLowerCase()) {
                                    availableTags.push(data[i].displayname + ' (' + data[i].shortName + ')');
                                }
                            }
                        });
                        if (contentcount == 0) {
                            availableTags.push(data[i].displayname + ' (' + data[i].shortName + ')');
                        }
                    }
                }
                for (var i = 0; i < data.length; i++) {
                    var prefcount = 0;
                    jQuery('.contenttags').find('.destination').each(function() {
                        if (data[i].shortName.toLowerCase() == jQuery(this).text().toLowerCase()) {
                            prefcount = prefcount + 1;
                        }
                    });
                    if (prefcount == 0) {
                        if (jQuery('#contenteditables1').val().toLowerCase() != data[i].shortName.toLowerCase()) {
                            availableTags.push(data[i].displayname + ' (' + data[i].shortName + ')');
                        }
                    }
                }
                jQuery("#contenteditables1").autocomplete({
                    source: availableTags,
                    select: function(event, ui) {
                        reminder2 = ui.item.value;
                        contentfunction();
                        $("#contenteditables1").val("");
                        return false;
                    }

                });
                var e = jQuery.Event("keydown");
                e.keyCode = 50;
                $("#contenteditables1").trigger(e);
            }, function errorCallback(response) {
                if(response.status == 403){
                    refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    // $scope.autoAirlines(value);
                }
                // called asynchronously if an error occurs
                // or server returns response with an error status.
              });
        }
    });

    function contentfunction() {
        var prefercount = 0;
        var reminders2 = reminder2.split('(');
        var reminders1 = reminders2[1].replace(')', '');
        jQuery('.contenttags').find('.destination').each(function() {
            if (reminders1 == jQuery(this).text()) {
                prefercount = prefercount + 1;
            }
        });
        if (prefercount == 0) {
            jQuery("#contentdiv").after('<span class="contenttags"><span class="destination" style="float:left;" data-toggle="tooltip" title="'+ reminders2[0] +'">' + reminders1 + '</span><span class="xclass">&times;</span><span>');
        }
    }

    jQuery('body').on('click', '.xclass', function() {
        jQuery(this).parent('span').remove();
    });
    jQuery('body').on('mouseover', '.destination', function() {
        jQuery(this).prev('.airlinenames').show();
    });
    jQuery('body').on('mouseout', '.destination', function() {
        jQuery(this).prev('.airlinenames').hide();
    });
    $scope.data = '';


    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
        });

    });
    $scope.hoverIn = function() {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
        });
    }

    $scope.removeprefAirlineList = function(airline) {
        $scope.flightSearchRQ.prefAirlineList.pop(airline);
        $scope.multiCityFlightSearchRQ.prefAirlineList.pop(airline);
    };

    $scope.flightSearch = function(searchType, flightobj) {
        var totalTravllersCount = parseInt($scope.flightSearchRQ.adtCnt) + parseInt($scope.flightSearchRQ.chldCnt);
            var totalTravllersCountmulticity = parseInt($scope.multiCityFlightSearchRQ.adtCnt) + parseInt($scope.multiCityFlightSearchRQ.chldCnt);

        if(parseInt($scope.flightSearchRQ.infCnt) > parseInt($scope.flightSearchRQ.adtCnt)){
            Lobibox.alert('info', {
                msg: "Infant count should not be greater than Adult count"
            });
        } else if (totalTravllersCount > 9) {

            Lobibox.alert('info', {
                msg: "You can book upto 9 passengers per booking, including adults and children"
            });
            return;
        }else {
            var arrayvalue = [];
            jQuery('.contenttags').each(function() {
                arrayvalue.push(jQuery(this).children('.destination').text());
                $scope.flightSearchRQ.prefAirlineList = arrayvalue;
                $scope.multiCityFlightSearchRQ.prefAirlineList = arrayvalue;
            });
            // if ($scope.searchTrip == "oneway") {
            //     $("#oneway").addClass("active");
            //     $("#round").removeClass("active");
            // } else {
            //     $("#round").addClass("active");
            //     $("#oneway").removeClass("active");
            // }
            var date = $('#daterange1').val();
            var date1 = []
            date1 = date.split(' ');
            if (searchType === 'roundTrip') {
                
                if ($("#from_place").val() != "" && $("#to_place").val() && $('#daterange1').val() != "") {
                    if ($scope.searchTrip == 'roundtrip') {
                        $scope.flightSearchRQ.srchType = 2;
                   
                        $("#round").addClass("active");


                        $scope.flightSearchRQ.strDeptDate = date1[0]
                        $scope.flightSearchRQ.strRetDate = date1[2];
                        $scope.flightSearchRQ.totDays = $scope.totalDays;
                        sessionStorage.setItem("flightSearchRQ", JSON.stringify($scope.flightSearchRQ));
                    } else {
                        $scope.flightSearchRQ.srchType = 1;
                        $("#oneway").addClass("active");
                        var d = new Date($scope.flightSearchRQ.strDeptDate);
                        var day = d.getDate();
                        var monthIndex = d.getMonth() + 1;
                        var year = d.getFullYear();

                        $scope.checkin = day + '/' + monthIndex + '/' + year;
                        $scope.flightSearchRQ.strDeptDate = date1[0];
                        sessionStorage.setItem("flightSearchRQ", JSON.stringify($scope.flightSearchRQ));
                        // sessionStorage.removeItem("")
                    }
                } else {
                    $scope.emptyError = true;
                    $timeout(function() { $('.emptyError').hide(); }, 1000);
                    return;
                }

            } else if (searchType === 'multiCity') {
                $scope.multiCityFlightSearchRQ.srchType = 3;
                $scope.multiCityFlightSearchRQ.classType=   $scope.multiCityFlightSearchRQ.classType;
                if (totalTravllersCountmulticity > 9) {
    
                    Lobibox.alert('info', {
                        msg: "You can book upto 9 passengers per booking, including adults and children"
                    });
                    return;
                } 
                else {
                $("#multiple").addClass("active");
                var tot = $scope.multiCityFlightSearchRQ.multicityInfos.length;
                for (var i = 0; i < tot; i++) {
                    if ($("#multi_flying_from_" + i).val() != ""  &&
                        $("#multi_flying_to_" + i).val() != "" &&
                        $('#multi_flying_departure_' + i).val() != "") {

                        $scope.multiCityFlightSearchRQ.multicityInfos[i].strDeptDate = $('#multi_flying_departure_' + i).val();
                    } else {
                        $scope.emptyError = true;
                        $timeout(function() { $('.emptyError').hide(); }, 1000);
                        return;
                    }
                    $scope.multiCityFlightSearchRQ.multicityInfos[i].classType=$scope.multiCityFlightSearchRQ.classType;
                }
                sessionStorage.setItem("flightSearchRQ", JSON.stringify($scope.multiCityFlightSearchRQ));
                var totalTravllersCount = parseInt($scope.multiCityFlightSearchRQ.adtCnt) + parseInt($scope.multiCityFlightSearchRQ.chldCnt) +
                    parseInt($scope.multiCityFlightSearchRQ.infCnt);
            }
        }


            $location.path('/searchingFlight');
        }

    }


    $(function() {

        'use strict';
        var f = 0;
        var flag = false;
        // FROM ARIPORT AUTOCOMPELTE
        $('#from_place').autocomplete({
            source: function(request, response) {
                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                var tokenname = $scope.tokenresult.data;
                var searchreqq = {"searchStr":request.term};
                //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokenname.authToken
                }
                $http.post(ServerService.listPath + 'list/custom/data/airports', searchreqq, {
                    headers: headersOptions
                }).then(function successCallback(data, status) {
                    var success = data.data.success
                    var data = data.data.data;
                    if (success) {
                        f = 0;
                        response(data);
                    } else {
                        $("#from_place").val('');
                        $("#from_place").focus(0);
                        $('.ui-autocomplete').hide();
                    }
                    }, function errorCallback(response) {
                        if(response.status == 403){
                            refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                            $("#from_place").val('');
                        }
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                });
            },
            // focus: function(event, ui) {
            //     // $("#from_place").val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
            //     // return false;
            // },
            focus: function( event, ui ) { event.preventDefault(); },
            change: function(event, ui) {
                var uiCity = JSON.stringify(ui.item); 
                if(uiCity == 'null'){
                    $("#from_place").val('');
                }
                /*   if (("#from_place").val()) {
                    $("#from_place").val($scope.flightSearchRQ.frmAirports); */
                    if ($("#to_place").val() == $("#from_place").val()) {
                        $("#from_place").val("");
                        $scope.flightSearchRQ.frmAirports ="";
                        return false;
                    }
                
                /*if(flag==false) {
                $('#from_place').val('');
                $('#from_place').focus(0);
                return false;
                }*/
            },
            autoFocus: true,
            minLength: 3,
            select: function(event, ui) {
                if ($("#from_place").val() == $("#to_place").val()) {
                    $("#from_place").val('');
                    $('.destcheck').show().fadeOut(5000);
                } else {
                    $("#from_place").val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
                    // $scope.flightSearchRQ.frmAirport = ui.item.code;
                    $scope.flightSearchRQ.frmAirport = ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")";
                    $scope.$apply();
                    $("#from_place").parent().removeClass('has-error');
                    $scope.errorDisp = false;
                    $('#to_place').focus(0);
                    flag = true;
                }
                return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function(event, ui) {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (f == 0) {
                f++;
                //$scope.flightSearchRQ.frmAirport = item.city + ", " + item.countryCode +  " (" + item.code + ")";
                return $("<li>").append("<a class='ui-state-focus' id='fltfrmctry'>" + item.city + "," + item.countryCode +  "(" + item.code + ")</a>").appendTo(ul);
                // "-" + item.name +
            } else {
                return $("<li>").append("<a>" + item.city + "," + item.countryCode +  "(" + item.code + ")</a>").appendTo(ul);
                // "-" + item.name +
            }
        };

    });
    //TO AIRPORT AUTOCOMPLETE

    $('#to_place').autocomplete({
        source: function(request, response) {
            $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            var searchreqq = {"searchStr":request.term};
            //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
            $http.post(ServerService.listPath + 'list/custom/data/airports', searchreqq, {
                headers: headersOptions
            }).then(function successCallback(data, status) {
                var success = data.data.success
                var data = data.data.data;
                if (success) {
                    f = 0;
                    response(data);
                } else {
                    $("#to_place").val('');
                    $("#to_place").focus(0);
                    $('.ui-autocomplete').hide();
                }
            }, function errorCallback(response) {
                if(response.status == 403){
                    refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    $("#to_place").val('');
                }
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        // focus: function(event, ui) {
        //     $("#to_place").val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
        //     return false;
        // },
        change: function(event, ui) {
            var uiCity = JSON.stringify(ui.item); 
            if(uiCity == 'null'){
                $("#to_place").val('');
            }

            if ($("#to_place").val()) {
                $("#to_place").val($scope.flightSearchRQ.toAirport);
                if ($("#to_place").val() == $("#from_place").val()) {
                    $("#to_place").val("");
                    $scope.flightSearchRQ.toAirport ="";
                    return false;
                }
            }
            /*if(flag==false) {
            $('#from_place').val('');
            $('#from_place').focus(0);
            return false;
            }*/
        },
        autoFocus: true,
        minLength: 3,
        select: function(event, ui) {
            if ($("#to_place").val() == $("#from_place").val()) {
                $("#to_place").val('');
                $('.destcheck').show().fadeOut(5000);
            } else {
                $("#to_place").val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
                // $scope.flightSearchRQ.toAirport = ui.item.code;
                $scope.flightSearchRQ.toAirport = ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")";
                $scope.$apply();
                $("#to_place").parent().removeClass('has-error');
                $scope.errorDisp = false;
                $('#daterange1').focus(0);
                flag = true;
            }
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function(event, ui) {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        if (f == 0) {
            f++;
            //$scope.flightSearchRQ.toAirport = item.city + ", " + item.countryCode +  " (" + item.code + ")";
            return $("<li>").append("<a class='ui-corner-all' id='flttoctry'>" + item.city + "," + item.countryCode +  "(" + item.code + ")</a>").appendTo(ul);
        } else {
            return $("<li>").append("<a>" + item.city + "," + item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
        }
    };


    $scope.intiMultiCityFromAirPortAutoComplete = function(indexId) {

        var count = $scope.multiCityFlightSearchRQ.multicityInfos.length;
        // for (var i = 0; i < count; i++) {
        var i = indexId;
        
        'use strict';
        var f = 0;
        var flag = false;
        $('#multi_flying_from_' + i).autocomplete({
            source: function(request, response) {
                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                var tokenname = $scope.tokenresult.data;
                var searchreqq = {"searchStr":request.term};
                //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokenname.authToken
                }
                $http.post(ServerService.listPath + 'list/custom/data/airports', searchreqq, {
                    headers: headersOptions
                }).then(function successCallback(data, status) {
                    var success = data.data.success
                    var data = data.data.data;
                    if (success) {
                        f = 0;
                        response(data);
                    } else {
                        $("#multi_flying_from_" + i).val('');
                        $("#multi_flying_from_" + i).focus(0);
                        $('.ui-autocomplete').hide();
                    }
                }, function errorCallback(response) {
                    if(response.status == 403){
                        refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                        $scope.intiMultiCityFromAirPortAutoComplete(indexId);
                    }
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            },
            // focus: function(event, ui) {
            //     $("#multi_flying_from_" + i).val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
            //     return false;
            // },
            change: function(event, ui) {
                var uiCity = JSON.stringify(ui.item); 
                if(uiCity == 'null'){
                    $("#multi_flying_from_" + i).val('');
                }

                if ($("#multi_flying_from_" + i).val()) {
                    $("#multi_flying_from_" + i).val($scope.multiCityFlightSearchRQ.multicityInfos[indexId].frmAirport);
                    if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to_" +i).val()) {
                        $("#multi_flying_from_" + i).val("");
                        $scope.multiCityFlightSearchRQ.multicityInfos[indexId].frmAirport = ""
                        return false;
                    }
                }

                /*if(flag==false) {
                $('#from_place').val('');
                $('#from_place').focus(0);
                return false;
                }*/
            },
            autoFocus: true,
            minLength: 3,
            select: function(event, ui) {
                if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to_" +i).val()) {
                    $("#multi_flying_from_" + i).val('');
                    $('.destcheck').show().fadeOut(5000);
                } else {
                    $("#multi_flying_from_" + i).val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
                    $scope.multiCityFlightSearchRQ.multicityInfos[indexId].frmAirport = ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")";
                    $scope.$apply();
                    $("#multi_flying_from_" + i).parent().removeClass('has-error');
                    $scope.errorDisp = false;
                    $('#multi_flying_to_' + i).focus(0);
                    flag = true;
                }
                return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function(event, ui) {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (f == 0) {
                f++;
                // $scope.multiCityFlightSearchRQ.multicityInfos[indexId].frmAirport = item.city + ", " + item.countryCode +" (" + item.code + ")";
                return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
            } else {
                return $("<li>").append("<a>" + item.city + "," +  item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
            }
        };
        // }
    }

    $scope.intiMultiCityToAirPortAutoComplete = function(indexId) {
        var count = $scope.multiCityFlightSearchRQ.multicityInfos.length;
        // for (var i = 0; i < count; i++) {
            'use strict';
            var f = 0;
            var flag = false;
        var i = indexId;
        var incofi = i+1;
        $('#multi_flying_to_' + i).autocomplete({
            source: function(request, response) {
                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                var tokenname = $scope.tokenresult.data;
                var searchreqq = {"searchStr":request.term};
                //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokenname.authToken
                }
                $http.post(ServerService.listPath + 'list/custom/data/airports', searchreqq, {
                    headers: headersOptions
                }).then(function successCallback(data, status) {
                    var success = data.data.success
                    var data = data.data.data;
                    if (success) {
                        f = 0;
                        response(data);
                    } else {
                        $("#multi_flying_to_" + i).val('');
                        $("#multi_flying_from_" + incofi).val('');
                        $("#multi_flying_to_" + i).focus(0);
                        $('.ui-autocomplete').hide();
                    }
                }, function errorCallback(response) {
                    if(response.status == 403){
                        refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                        // $scope.autoAirlines(value);
                        $scope.intiMultiCityToAirPortAutoComplete(indexId);
                    }
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            },
            // focus: function(event, ui) {
            //     $("#multi_flying_to_" + i).val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
            //     $("#multi_flying_from_" + incofi).val(ui.item.city + ", " + ui.item.countryCode +" (" + ui.item.code + ")");
            //     return false;
            // },
            change: function(event, ui) {
                var uiCity = JSON.stringify(ui.item); 
                if(uiCity == 'null'){
                    $("#multi_flying_to_" + i).val('');
                }
                if ($("#multi_flying_to_" + i).val()) {
                    $("#multi_flying_to_" + i).val($scope.multiCityFlightSearchRQ.multicityInfos[indexId].toAirport);
                    $("#multi_flying_from_" + incofi).val($scope.multiCityFlightSearchRQ.multicityInfos[indexId].toAirport);
                    if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to_" + i).val()) {
                        $("#multi_flying_to_" + i).val("");
                        $("#multi_flying_from_" + incofi).val('');
                        $scope.multiCityFlightSearchRQ.multicityInfos[indexId].toAirport = ""
                        return false;
                    }
                }
                /*if(flag==false) {
                $('#from_place').val('');
                $('#from_place').focus(0);
                return false;
                }*/
            },
            autoFocus: true,
            minLength: 3,
            select: function(event, ui) {
                if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to_" + i).val()) {
                    $("#multi_flying_to_" + i).val('');
                    $("#multi_flying_from_" + incofi).val('');

                    $('.destcheck').show().fadeOut(5000);
                } else {
                    $("#multi_flying_to_" + i).val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
                    $("#multi_flying_from_" + incofi).val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
                    $scope.multiCityFlightSearchRQ.multicityInfos[indexId].toAirport = ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")";
                    if($scope.multiCityFlightSearchRQ.multicityInfos[indexId+1] != undefined){
                    $scope.multiCityFlightSearchRQ.multicityInfos[indexId+1].frmAirport = ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")";
                    if( $scope.multiCityFlightSearchRQ.multicityInfos[indexId+1].frmAirport== $scope.multiCityFlightSearchRQ.multicityInfos[indexId+1].toAirport){
                    $("#multi_flying_to_" + incofi).val('');
                    }
                    }
                    // $scope.$apply();
                    $("#multi_flying_to_" + i).parent().removeClass('has-error');
                    $scope.errorDisp = false;
                    $('#multi_flying_departure_' + i).focus(0);
                    flag = true;
                }
                return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function(event, ui) {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (f == 0) {
                f++;
                // $scope.multiCityFlightSearchRQ.multicityInfos[indexId].frmAirport = item.city + ", " + item.countryCode +" (" + item.code + ")";
                return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
            } else {
                return $("<li>").append("<a>" + item.city + "," +  item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
            }
        };
        // }
        // })




    }

});