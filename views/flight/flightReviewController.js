ouaApp.controller('flightReviewController', function($scope, $compile,refreshService, $interval, $http, NationService, $window, $rootScope, Month, Year, $uibModal, baseUrlService, ServerService, $location, $timeout, httpService, $state, customSession, $uibModal, ConstantService, renderHTMLFactory) {
    $scope.countryList = {};
    NationService.nationGet();
    $("#modal").removeClass("show");
    $scope.mealsselectedlist = [];
    $("html,body").scrollTop(10);
    $scope.backtodetail = false;
    $scope.backtotraveler = false;
    //$scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    // $scope.loggedvalue = false;
    $(function () {
		$('[data-toggle="tooltip"]').tooltip()
      })

    $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));
    $scope.flightBook = $scope.flightBookSession.data;
      
    $scope.promocode = "";
    $scope.discount = 0;
    $scope.getpromocode = function(promocode)
    {
        var token = JSON.parse(localStorage.authentication);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.data.authToken
        }

        var sessionId=JSON.parse(sessionStorage.getItem('flightPriceData'));

        $scope.artloading = true;

        $http.post(ServerService.serverPath + 'promoCode/apply-promo/FLT/'+promocode, sessionId.sessionId, 
        {
            headers: headersOptions
        }).then(function successCallback(response) 
        {
            $scope.artloading = false;
            if(response.data.success)
            {
                $scope.discount = response.data.data;
            }
            else{
                Lobibox.alert('error',
                {
                    msg: response.data.errorMessage
                });
            }
        });


    }
 
    // $scope.loggedIn = false;

    // if (sessionStorage.loginToken != null) {
    //     $scope.loggedvalue = true;
    // }
    $scope.loopTot = 0;
    $scope.trvellerSeat = 0;
    $scope.artloading = true;
    $scope.paysubmitted = false;
    $scope.selectedInsurancePlantotalFare = 0;
    $('html, body').scrollTop(0);

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
    }

     // fordateofbirth..dropdown...author nandhiniD.
     $scope.months = 12;
     $scope.getminexpirymonth = 12;
     var dobmonth = [];
     var cdobmonth = [];
     // var idobmonth = [];
     for (var i = 0; i < $scope.months; i += 1) {
         dobmonth.push(i + 1);
         cdobmonth.push(i + 1);
         // idobmonth.push(i+1);
 
     }
     $scope.dobmonths = dobmonth;
     $scope.cdobmonths = cdobmonth;


     $scope.bdates=31;
     var adate= [];
     var cdate = [];
     var idate = [];
     for (var i = 0; i < $scope.bdates; i += 1) {
        adate.push(i + 1);
        cdate.push(i + 1);
        idate.push(i + 1);
     }
     $scope.dobdates = adate;
     $scope.cdobdates = cdate;
     $scope.idobdates = idate;

     // $scope.idobmonths = idobmonth;
 
     // forchild......
     var cyear = new Date().getFullYear() - 2;
     var crange = [];
     crange.push(cyear);
     for (var c = 1; c < 16; c++) {
         crange.push(cyear - c);
     }
     $scope.cyears = crange;
 
     // foradult........
     var ayear = new Date().getFullYear() - 18;
     var arange = [];
     arange.push(ayear);
     for (var i = 1; i < 55; i++) {
         arange.push(ayear - i);
     }
     $scope.ayears = arange;
     // forinfantYear......fordob..
     $scope.imonths = 12;
     var idobmonth = [];
     for (var i = 0; i < $scope.imonths; i += 1) {
         idobmonth.push(i + 1);
     }
     $scope.idobmonths = idobmonth;
     var iyear = new Date().getFullYear();
     var irange = [];
         irange.push(iyear);
         for (var i = 1; i < 2; i++) {
             irange.push(iyear - i);
         }
         $scope.iyears = irange;
     $scope.changeinfantDate = function(selectedYear) {
        //  console.log("selectedYear", selectedYear)
         var iyear = new Date().getFullYear();
         var infantToday = new Date();
         var getInfanntmonth = infantToday.getMonth();
 
         var selectedYear = selectedYear.infant.personaldata.birthyear || 0;
         if (selectedYear == iyear) {
             $scope.imonths = getInfanntmonth + 1;
         } else {
             $scope.imonths = 12;
         }
 
         var idobmonth = [];
         for (var i = 0; i < $scope.imonths; i += 1) {
             idobmonth.push(i + 1);
         }
         $scope.idobmonths = idobmonth;
 
         var irange = [];
         irange.push(iyear);
         for (var i = 1; i < 2; i++) {
             irange.push(iyear - i);
         }
         $scope.iyears = irange;
     }
        // forinfantYear......for..passportexpiry..
        $scope.ipemonths = 12;
        var ipemonth = [];
        for (var i = 0; i < $scope.imonths; i += 1) {
         ipemonth.push(i + 1);
        }
        $scope.ipemonths = ipemonth;
        var iexpiryyear = new Date().getFullYear();
        var aexpiryyear = new Date().getFullYear();
        var cexpiryyear = new Date().getFullYear();
        var iperange = [];
        iperange.push(iexpiryyear);
        for (var i = 1; i < 50; i++) {
         iperange.push(iexpiryyear - i);
        }
        $scope.iexpiryyears = iperange;
        $scope.changeinfantpeYear = function(selectedYear) {
            var iexpiryyear = new Date().getFullYear();
            var infantToday = new Date();
            var getInfanntmonth = infantToday.getMonth();
    
            var selectedYear = selectedYear.infant.personaldata.expiryyear || 0;
            if (selectedYear == iexpiryyear) {
                $scope.currentmonth = getInfanntmonth;
            } else {
                $scope.currentmonth = 0;
                $scope.imonths = 12;
            }
    
            var ipemonth = [];
            for (var i = $scope.currentmonth; i < $scope.imonths; i += 1) {
             ipemonth.push(i + 1);
            }
            $scope.ipemonths = ipemonth;
    
            var iperange = [];
            iperange.push(iexpiryyear);
            for (var i = 1; i < 50; i++) {
             iperange.push(iexpiryyear - i);
            }
            $scope.iexpiryyears = iperange;
        }
 
 
     
 //passport expiry

//  var apemonth = [];
//  var cpemonth = [];
//  for (var i = 0; i < $scope.getminexpirymonth; i += 1) {
//      apemonth.push(i + 1);
//      cpemonth.push(i + 1);
//  }
//  $scope.apemonths = apemonth;
//  $scope.cpemonths = cpemonth;

 $scope.pdates=31;
 var apdate= [];
 var cpdate = [];
 for (var i = 0; i < $scope.pdates; i += 1) {
    apdate.push(i + 1);
     cpdate.push(i + 1);
 }
 $scope.pedates = apdate;
 $scope.cpedates = cpdate;


 
     // adultmonthchange...
     $scope.adays = 31;

     $scope.changeadultMonth = function(selectedMonth) {
             var selectedMonth = selectedMonth.adult.personaldata.birthmonth || 0;
             if (selectedMonth == 2) {
                 $scope.adays = 28;
                 //console.log("$scope.dobdates", $scope.dobdates)
             } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                 $scope.adays = 30;
                 //console.log("$scope.dobdates", $scope.dobdates)
             } else {
                 $scope.adays = 31;
             }
             var dobdate = [];
             for (var i = 0; i < $scope.adays; i += 1) {
                 dobdate.push(i + 1);
             }
             $scope.dobdates = dobdate;
             //console.log("$scope.dobdates", $scope.dobdates)
 
         }
         
           // adultYearchange..for..passportexpiry.
           var apemonth = [];
           $scope.amonths = 12;
           for (var i = 0; i < $scope.amonths; i += 1) {
               apemonth.push(i + 1);
           }
           $scope.apemonths = apemonth;
         
           var pefullyear = new Date().getFullYear();
           var arange = [];
           arange.push(pefullyear);
           for (var i = 1; i < 50; i++) {
               arange.push(pefullyear + i);
           }
           $scope.apeyears = arange
         $scope.changeadultpeYear = function(selectedYear) {
         

               var aexpiryyear = new Date().getFullYear();
               var adultToday = new Date();
               var getAdultmonth = adultToday.getMonth();
       
               var selectedYear = selectedYear.adult.personaldata.expiryyear || 0;
               if (selectedYear == pefullyear) {
                   $scope.currentmonth = getAdultmonth;
               } else {
                   $scope.currentmonth = 0;
                   $scope.amonths = 12;
               }
       
               var apemonth = [];
               for (var i = $scope.currentmonth; i < $scope.amonths; i += 1) {
                apemonth.push(i + 1);
               }
               $scope.apemonths = apemonth;
       
               var aperange = [];
               aperange.push(aexpiryyear);
               for (var i = 1; i < 50; i++) {
                aperange.push(aexpiryyear - i);
               }
               $scope.aexpiryyears = aperange;
         }


          // adultmonthchange..for..passportexpiry.
     $scope.apedays = 31;
     $scope.changeadultpeMonth = function(selectedMonth) {
             var selectedPeMonth = selectedMonth.adult.personaldata.expirymonth || 0;
             if (selectedPeMonth == 2) {
                 $scope.apedays = 28;
             } else if (selectedPeMonth == 4 || selectedPeMonth == 6 || selectedPeMonth == 9 || selectedPeMonth == 11) {
                 $scope.apedays = 30;
             } else {
                 $scope.apedays = 31;
             }
            var currentday = new Date();
            var today = new Date().getDate();
            var aexpirymonth = currentday.getMonth()+1;
            var selectedYear = selectedMonth.adult.personaldata.expiryyear;
            if (selectedPeMonth == aexpirymonth && selectedYear ==pefullyear ) {
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var pedate = [];
            for (var i = $scope.currentDate; i < $scope.apedays; i += 1) {
                pedate.push(i + 1);
            }
            $scope.pedates = pedate;
         }
         // childmonthchange....
     $scope.cdays = 31;
     $scope.changechildMonth = function(selectedMonth) {
             var selectedMonth = selectedMonth.child.personaldata.birthmonth || 0;
             if (selectedMonth == 2) {
                 $scope.cdays = 28;
             } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                 $scope.cdays = 30;
             } else {
                 $scope.cdays = 31;
             }
             var cdobdate = [];
             for (var i = 0; i < $scope.cdays; i += 1) {
                 cdobdate.push(i + 1);
             }
             $scope.cdobdates = cdobdate;
         }
   
             // childYearchange..for..passportexpiry.
             var cpemonth = [];
             $scope.cmonths = 12;
             for (var i = 0; i < $scope.cmonths; i += 1) {
                 cpemonth.push(i + 1);
             }
             $scope.cpemonths = cpemonth;
           
             var cyear = new Date().getFullYear();
             var crange = [];
             crange.push(cyear);
             for (var i = 1; i < 50; i++) {
                 crange.push(cexpiryyear + i);
             }
             $scope.cexpiryyears = crange
           $scope.changechildpeYear = function(selectedYear) {
           
  
                 var cexpiryyear = new Date().getFullYear();
                 var childToday = new Date();
                 var getChildmonth = childToday.getMonth();
         
                 var selectedYear = selectedYear.child.personaldata.expiryyear || 0;
                 if (selectedYear == cyear) {
                     $scope.currentmonth = getChildmonth;
                 } else {
                     $scope.currentmonth = 0;
                     $scope.cmonths = 12;
                 }
         
                 var cpemonth = [];
                 for (var i = $scope.currentmonth; i < $scope.cmonths; i += 1) {
                  cpemonth.push(i + 1);
                 }
                 $scope.cpemonths = cpemonth;
         
                //  var cperange = [];
                //  cperange.push(cexpiryyear);
                //  for (var i = 1; i < 50; i++) {
                //   cperange.push(cexpiryyear - i);
                //  }
                //  $scope.cexpiryyears = cperange;
           }
          // childmonthchange..for..passport..expiry..
     $scope.cpedays = 31;
     $scope.changechildpeMonth = function(selectedMonth) {
             var selectedpeMonth = selectedMonth.child.personaldata.expirymonth || 0;
             if (selectedpeMonth == 2) {
                 $scope.cpedays = 28;
             } else if (selectedpeMonth == 4 || selectedpeMonth == 6 || selectedpeMonth == 9 || selectedpeMonth == 11) {
                 $scope.cpedays = 30;
             } else {
                 $scope.cpedays = 31;
             }
            var currentday = new Date();
            var today = new Date().getDate();
            var cexpirymonth = currentday.getMonth()+1;
            // if (selectedpeMonth == cexpirymonth) {
                var selectedYear = selectedMonth.child.personaldata.expiryyear;
            if (selectedpeMonth == cexpirymonth && selectedYear ==pefullyear ) {
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var cpedate = [];
            for (var i = $scope.currentDate; i < $scope.cpedays; i += 1) {
                cpedate.push(i + 1);
            }
            $scope.cpedates = cpedate;
         }
         // infantmonthchange..
     $scope.idays = 31;
     $scope.changeinfantMonth = function(selectedMonth) {
         
        var infantYear = new Date().getFullYear();
        var adultToday = new Date();
        var infantDate=  adultToday.getDate();
        var infantMonth =  adultToday.getMonth()+1;
         var selectedMonth = selectedMonth.infant.personaldata.birthmonth || 0;
         if (selectedMonth == 2) {
             $scope.idays = 28;
         } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
           if(infantMonth==selectedMonth){
            $scope.idobdates = infantDate;
         $scope.idays = $scope.idobdates;
            var idobdate = [];
            for (var i = 0; i < $scope.idays; i += 1) {
                idobdate.push(i + 1);
            }
            $scope.idobdates = idobdate;
            return false;
           }else{
            $scope.idays = 30;
           }
           
         } else {
             $scope.idays = 31;
         }
         var idobdate = [];
         for (var i = 0; i < $scope.idays; i += 1) {
             idobdate.push(i + 1);
         }
         $scope.idobdates = idobdate;
     }
        // infantmonthchange.for.passport...expirydate.
        $scope.ipedays = 31;
        var ipedate = [];
        for (var i = 0; i < $scope.ipedays; i += 1) {
            ipedate.push(i + 1);
        }
        $scope.ipedates = ipedate;

        $scope.changeinfantpeMonth = function(selectMonth) {
            //console.log("selectedMonth",selectedMonth)
            var selectedMonth = selectMonth.infant.personaldata.expirymonth || 0;
            if (selectedMonth == 2) {
                $scope.ipedays = 28;
            } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                $scope.ipedays = 30;
            } else {
                $scope.ipedays = 31;
            }

            var currentday = new Date();
            var today = new Date().getDate();
            var iexpirymonth = currentday.getMonth()+1;
            var selectedYear = selectMonth.infant.personaldata.expiryyear;
            if (selectedMonth == iexpirymonth && selectedYear== pefullyear ) {
                // $scope.currentDate = iexpirymonth;
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var ipedate = [];
            for (var i = $scope.currentDate; i < $scope.ipedays; i += 1) {
                ipedate.push(i + 1);
            }
            $scope.ipedates = ipedate;
        }
 
    // ends.....dob and pe......
    $scope.handler = function(event) {
        //  //console.log("min date", $scope.min);
        var date = new Date($scope.min);

        // var month = date.getMonth() + 7; //months from 1-12
        // var day = date.getDate();
        // var year = date.getFullYear();
        date.setMonth(date.getMonth() + 6);
        // var newDate = year + '-' + month + '-' + day;
        //   console.log("date", date);
        var selectedDate = event.target.value;
        //   console.log('selected date', event.target.value);
        var date1 = new Date(selectedDate);
        if (date1 <= date) {
            //	Lobibox.notify('info', { size: 'mini', delay: 1500, msg: "Loreum ipsum asdfa asdfklj asdlkfj; ljadsf" });
            Lobibox.alert('info', //AVAILABLE TYPES: "error", "info", "success", "warning"
                {
                    msg: "Please note, if you travel to any country  other than your passport issued country, then as a general rule passports should have at least six months of validity.Please contact us for more information."
                });
        }
    }
       /*backloader   */
       $scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
           $('#load').hide();
        }, 1000);
    }
        $scope.backloader=function() {
            $scope.timeload();
            if(localStorage.islogged == 'true'){
                      $scope.$parent.bookingHide = true;
                $scope.$parent.bookingHide1 = true;
            }else{
                    $scope.$parent.bookingHide = false;
                $scope.$parent.bookingHide1 = false;
            }
                        document.getElementById("load").style.display="block";
            setTimeout("hide()", 500); 
            $location.path('/flightDetails');
    }
        $scope.hide=function() {
            document.getElementById("load").style.display="none";
           
            // $scope.setTimeout("",5000);
        }

    $scope.searchReq = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    var multi = $scope.searchReq.multicityInfos;

    // sessionStorage.setItem('multiCity', JSON.stringify($scope.searchFlightObj.multicityInfos));
    if (multi === undefined) {
        $scope.hideCity = true;
    } else {
        $scope.hideCity = false;
    }
    //   $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));

    // var tokenname = $scope.tokenresult.data;
    // var headersOptions = {
    // 'Content-Type': 'application/json',
    // 'Authorization': 'Bearer ' + tokenname.authToken
    // }

    $scope.flightTermsDetails = function(info,size, parentSelector, index) {
       
        var parentElem = parentSelector ?
        
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightTermsandConditions.html',
            controller: 'flightTermsController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.totMeals = 0;
    $scope.totseat = 0;

    getTotMeals = function(){
        $scope.totMeals = 0;
            for(var mls=0;mls<$scope.mealsData.length;mls++){
                for(var ml=0;ml<$scope.mealsData[mls].travelerDetails.length;ml++){
                    $scope.totMeals = Number($scope.mealsData[mls].travelerDetails[ml].price) + $scope.totMeals;
                }
            }
    }

    $scope.selectmealDetails = function(size, parentSelector, mealsDetails, parentIndex, index,mealsdtls) {
        //console.log($scope.mealsData);

        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            sessionStorage.setItem("bindmealsdetails1", JSON.stringify(mealsDetails))

        // $rootScope.bindmealsdetails1 = mealsDetails;
        $rootScope.parentIndex = parentIndex;
        $rootScope.key = $scope.mealsData[parentIndex].key;
        $rootScope.index = index;
        $rootScope.multiMealsEnabled = $scope.mealsData[0].mealmultiple;
        $rootScope.bundledFareId = mealsdtls.prf;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'mealDetailedinfoContent.html',
            controller: 'mealinfoContent',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })

        modalInstance.result.then(function(bindmealsdetails, mealsselectedlist) {
            var totprice = 0;
            var count = 0;
            $scope.fngrandtotal = 0;
            // console.log($scope.mealsData[parentIndex].travelerDetails[index]);
            // $scope.mealsselectedlist.push(JSON.parse(sessionStorage.getItem("mealsselectedlist")));
            bindmealsdetails.forEach(element => {
                if (!isNaN(element.totprice))
                    totprice = Number(element.totprice) + totprice;
                if (!isNaN(element.count))
                    count = Number(element.count) + count;
            });
            // console.log(selectedItem);
            // console.log($scope.mealsData[parentIndex].travelerDetails[index]);
            $scope.mealsData[parentIndex].travelerDetails[index].quantity = Number(count);
            $scope.mealsData[parentIndex].travelerDetails[index].price = Number(totprice);
            $scope.mealsData[parentIndex].travelerDetails[index].mealslist = bindmealsdetails;

            //Get totmeals
            getTotMeals();

        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    SEGsplit = function(or,dt){
        return or.split('(')[1].split(')')[0]+"⇌"+dt.split('(')[1].split(')')[0];
    }

    $scope.loggedvalue = JSON.parse(localStorage.getItem('islogged'));

    $scope.isLcc = $scope.flightBook.selectedResult.flightSearchResults[0].isLcc;

    // $scope.providerName="JJR";
    if (!$scope.isLcc) {

        $scope.serviceshide = "true";
    } else {
        $scope.serviceshide = "false";
        $scope.validateservicedata = "false";
    }
    if ($scope.searchReq.multicityInfos) {
        $scope.min = ($scope.searchReq.multicityInfos[0].strDeptDate).split('/')[2] + '-' + ($scope.searchReq.multicityInfos[0].strDeptDate).split('/')[1] + '-' + ($scope.searchReq.multicityInfos[0].strDeptDate).split('/')[0]

        var arrivalDateTime = $scope.searchReq.multicityInfos[0].strDeptDate;
        var depDateSplt = arrivalDateTime.split('/');
        $scope.adultmaxDate = parseInt(depDateSplt[2]) - 12;
        $scope.adultminDate = parseInt(depDateSplt[2]) - 76;
        $scope.adultmaxyear = $scope.adultmaxDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.adultminyear = $scope.adultminDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.childmaxyear = parseInt(depDateSplt[2]) - 2 + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
        $scope.childtminyear = (parseInt(depDateSplt[2]) - 12) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infminyear = (parseInt(depDateSplt[2]) - 2) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    } else if ($scope.searchReq.strRetDate) {
        $scope.min = ($scope.searchReq.strRetDate).split('/')[2] + '-' + ($scope.searchReq.strRetDate).split('/')[1] + '-' + ($scope.searchReq.strRetDate).split('/')[0]
        var arrivalDateTime = $scope.searchReq.strRetDate;
        var depDateSplt = arrivalDateTime.split('/');
        $scope.adultmaxDate = parseInt(depDateSplt[2]) - 12;
        $scope.adultminDate = parseInt(depDateSplt[2]) - 76;
        $scope.adultmaxyear = $scope.adultmaxDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.adultminyear = $scope.adultminDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.childmaxyear = parseInt(depDateSplt[2]) - 2 + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
        $scope.childtminyear = (parseInt(depDateSplt[2]) - 12) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infminyear = (parseInt(depDateSplt[2]) - 2) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    } else {
        // $scope.min=($scope.searchReq.onwardDateTime)
        $scope.min = ($scope.searchReq.strDeptDate).split('/')[2] + '-' + ($scope.searchReq.strDeptDate).split('/')[1] + '-' + ($scope.searchReq.strDeptDate).split('/')[0]

        var arrivalDateTime = $scope.searchReq.strDeptDate;
        var depDateSplt = arrivalDateTime.split('/');
        $scope.adultmaxDate = parseInt(depDateSplt[2]) - 12;
        $scope.adultminDate = parseInt(depDateSplt[2]) - 76;
        $scope.adultmaxyear = $scope.adultmaxDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.adultminyear = $scope.adultminDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.childmaxyear = parseInt(depDateSplt[2]) - 2 + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
        $scope.childtminyear = (parseInt(depDateSplt[2]) - 12) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infminyear = (parseInt(depDateSplt[2]) - 2) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    }

    $(document).ready(function() {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function() {

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({ opacity: 0 }, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({ 'opacity': opacity });
                },
                duration: 600
            });
        });

        $(".previous").click(function() {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({ opacity: 0 }, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({ 'opacity': opacity });
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function() {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function() {
            return false;
        })

    });

    function clearFFFieldValues() {
        $('#profile_flyerNo').val("");
        $('#profileFlyerNo').val("");
        $('#profile_airline').val("");
        $('#profileAirlineName').val("");
        $('#profileAirlineCode').val("");
        $('#profile_AirlineCode').val("");
        $('#addFrequentFlyer').show();
    }
    $scope.removeFrequentFlyer = function(trvltype, frequentflyer, indexvalue) {
        $scope.freqflyerlist.pop(frequentflyer);
        if(trvltype == 'ADT')
        $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls.splice(frequentflyer,1);
        if(trvltype == 'CNN')
        $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls.splice(frequentflyer,1);
        if(trvltype == 'INF')
        $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls.splice(frequentflyer,1);

    };

    $scope.freqflyerlist = [];
    $scope.ffChildObjEdit;
    $scope.addFrequentFlyer = function(indexvalue, trvltype) {
        // console.log("hi");
        var isValid = true;
        var frequentflyerObj = null;
        var mode = 'insert';
        if ($scope.ffChildObjEdit == null) {
            frequentflyerObj = new Object();
        } else {
            frequentflyerObj = $scope.ffChildObjEdit;
            mode = 'update';
        }
        $scope.airlineName = $('#profile_airline_'+trvltype+'_'+indexvalue).val();
			
        $scope.airCodes = $('#profile_flyerNo_'+trvltype+'_'+indexvalue).val();


        if ($scope.airlineName == "" || $scope.airlineName == null) {
            Lobibox.notify('error', { size: 'mini', delay: 5000,   position: 'right top', msg: "Please Choose Airline" });
            isValid = false;
        } else if ($scope.airCodes == "" || $scope.airCodes == null || $scope.airCodes == undefined) {
            Lobibox.notify('error', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Flyer No" });
            isValid = false;
        } else {
            var ffairlinename = $scope.airlineName;
            var ffnumber = $scope.airCodes;
            $scope.profile_AirlineCode = ffairlinename;
            var ffairline = $scope.profile_AirlineCode;
            if (mode == 'insert') {
                if (!checkFFDtlDuplicate(ffairline)) {
                    isValid = false;
                    Lobibox.notify('warning', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Flyer No" });
                }
            }
            if (isValid == true) {
                frequentflyerObj.airline = ffairlinename;
                /* frequentflyerObj.ffairline = ffairline; */
                frequentflyerObj.ffNo = ffnumber;
                clearFFFieldValues();
                if (mode == 'insert') {
                    $scope.freqflyerlist.push(frequentflyerObj);
                    if(trvltype == 'ADT'){
                    $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls=($scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls || []);
                    $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls.push(frequentflyerObj);
                    }
                    if(trvltype == 'CNN'){
                    $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls=($scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls || []);
                    $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls.push(frequentflyerObj);
                    }
                    if(trvltype == 'INF'){
                    $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls=($scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls || []);

                    $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls.push(frequentflyerObj);
                    }
                    //   console.log("array of flyerlist", $scope.freqflyerlist);
                    Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "Airline Added Successfully" });
                } else {
                    Lobibox.notify('success', { size: 'mini', delay: 5000,   position: 'right top',msg: "Airline Updated Successfully" });
                    $scope.ffChildObjEdit = null;
                }
            }
        }

        $scope.airlineName = $('#profile_airline_'+trvltype+'_'+indexvalue).val("");
			
        $scope.airCodes = $('#profile_flyerNo_'+trvltype+'_'+indexvalue).val("");
    };


    function checkFFDtlDuplicate(currValue) {
        var isValid = true;
        if ($scope.freqflyerlist.length != 0) {
            for (var i = 0, size = $scope.freqflyerlist.length; i < size; i++) {
                var value = $scope.freqflyerlist[i].ffairline;
                if (currValue == value) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }




    
    $scope.autoAirlines = function(value) {
            var token = JSON.parse(localStorage.authentication);
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.data.authToken
            }
        if (value != null && value.length == 1) {
            $http.get(ServerService.serverPath + 'flight/airlines/' + value.toUpperCase(), {
                headers: headersOptions
            }).then(function successCallback(response, status) {
                $scope.airlines = [];
                $scope.airlineCode = [];
                var airlist = response.data.data
                if (response.data.data != 'null' && response.data.data.length > 0) {
                    for (var i = 0; i < response.data.data.length; i++) {
                        var airfrom = {
                            Title: response.data.data[i].displayname + ' - ' + '(' + response.data.data[i].shortName + ')'

                        }
                        var aircode = {
                            Code: response.data.data[i].shortName
                        }
                        $scope.airlines.push(airfrom);
                        $scope.airlineCode.push(aircode);
                        //console.log($scope.airlines);

                    }
                    $scope.airlines.sort();
                    $scope.airlineCode.sort();
                }


            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
                    status.then(function(greeting) {
                    $scope.autoAirlines(value);
                    });
                }
                // called asynchronously if an error occurs
                // or server returns response with an error status.
              });
        }


    }


    $scope.airCodeChange = function(value) {
        $scope.autoAirlines(value)
    }




    /**
    @author Raghava Muramreddy
    to showing payment error message
    */
    // $scope.isActive = false;


    $scope.payment = false;

    $scope.receiveInfo = {
        value: 'yes'
    }
    $scope.currencies = ConstantService.currency;
    if (sessionStorage.getItem("paymentError")) {
        var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
        Lobibox.alert('error', {
            msg: paymentError.errorMessage,
            callback: function(lobibox, type) {
                if (type === 'ok') {
                    sessionStorage.removeItem("paymentError");
                }
            }
        });

    }
    $scope.seatTotal = 0;
    $scope.mealTotal = 0;
    // $scope.seatSelect= function (s){$scope.selectedButton =s }
    $scope.secondMeal = true;

    $(document).on('change keyup', '.required', function(e) {
        let Disabled = true;
        $(".required").each(function() {
            let value = this.value
            if ((value) && (value.trim() != '')) {
                Disabled = false
            } else {
                Disabled = true
                return false
            }
        });

        if (Disabled) {
            $('.toggle-disabled').prop("disabled", true);
        } else {
            $('.toggle-disabled').prop("disabled", false);
        }
    })

    $scope.travelerselectdata = '';

    $scope.pamentbutton = true;

    $scope.showMealQuantity = false;

    $scope.goBack = function(path) {
        $location.path(path);
    }

    var loginUser = "";
    $scope.user = {};

    $scope.showSignIn = false;

    var loginCredential;
    $scope.isValidUser = false;
    var booking = {};

    $("#contact").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    //for (var a=0; a<5; a++){


    $("#acontact_0").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    //}

    var allSegments=[];
    $scope.flightResults=JSON.parse(sessionStorage.getItem('flightSearchRS'));
    $scope.flightDeatailsList=JSON.parse(sessionStorage.getItem('firstFlightResponse'));
    // $scope.flightDeatailsList=$scope.flightResults.data.flightSearchResults;
    var Outbound=$scope.flightDeatailsList;

    for(var d=0; d<Outbound.segList[0].fltInfoList.length; d++){
        var origin=Outbound.segList[0].fltInfoList[d].origin.split(',')[1].trim();
        var destination=Outbound.segList[0].fltInfoList[d].dest.split(',')[1].trim();
        if(!allSegments.includes(origin))
        allSegments.push(origin);
        if(!allSegments.includes (destination))
        allSegments.push(destination);

    }



    $scope.normalnationalityIds=true;
    $scope.nationalityChange=function(nationalty,indexvalue){
       for(var a=0;a<$scope.travellersListByRoom[0].adultTravellersList.length; a++){
        $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.passIssueGovt=nationalty;
       }
        if(allSegments.length==1){
            $scope.countryResult.forEach(nlt=>{
                if(nlt.code==nationalty){
                    $scope.saudinationalityId= allSegments[0].toUpperCase().includes (nlt.name.toUpperCase());
                    // if(  $scope.saudinationalityId=='true'){
                    //     $scope.saudinationalityIds=true;
                    //     $scope.normalnationalityIds=false;
                    // }
                    //console.log( $scope.nationalityId)
                    return;
                }
            })
      
     
        }  
       else{
        $scope.nationalityId  = false;
       }
    }

    $scope.nationalityChangeChaild=function(nationalty,indexvalue){
        for(var a=0;a<$scope.travellersListByRoom[0].childTravellersList.length; a++){
            $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.passIssueGovt=nationalty;
           }
        if(allSegments.length==1){
            $scope.countryResult.forEach(nlt=>{
                if(nlt.code==nationalty){
                    $scope.saudinationalityIdchild= allSegments[0].toUpperCase().includes (nlt.name.toUpperCase());
                    // if(  $scope.saudinationalityId=='true'){
                    //     $scope.saudinationalityIds=true;
                    //     $scope.normalnationalityIds=false;
                    // }
                    //console.log( $scope.nationalityId)
                    return;
                }
            })
      
     
        }  
       else{
        $scope.nationalityId  = false;
       }
    }
    
    $scope.nationalityChangeInfant=function(nationalty,indexvalue){
        for(var a=0;a<$scope.travellersListByRoom[0].infantTravellersList.length; a++){
            $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.passIssueGovt=nationalty;
           }
        if(allSegments.length==1){
            $scope.countryResult.forEach(nlt=>{
                if(nlt.code==nationalty){
                    $scope.saudinationalityIdInfant= allSegments[0].toUpperCase().includes (nlt.name.toUpperCase());
                    // if(  $scope.saudinationalityId=='true'){
                    //     $scope.saudinationalityIds=true;
                    //     $scope.normalnationalityIds=false;
                    // }
                    //console.log( $scope.nationalityId)
                    return;
                }
            })
      
     
        }  
       else{
        $scope.nationalityId  = false;
       }
    }

    $scope.month = Month;
    $scope.year = Year;


    $scope.seatDetailss = null;
    $scope.mealDetails = null;

    $scope.totalSSR = 0;


    $scope.searchtotalldata = $scope.flightBook.selectedResult;
    $scope.myJson = $scope.flightBook.selectedResult.flightSearchResults;
    $scope.outsegpref = $scope.flightBook.selectedResult.flightSearchResults[0].segGrpList[0];
    if($scope.flightBook.selectedResult.flightSearchResults[0].segGrpList.length>1)
    $scope.insegpref = $scope.flightBook.selectedResult.flightSearchResults[0].segGrpList[1];

    /* $scope.bagageinfo=$scope.flightBook.selectedResult.flightSearchResults[0].baggageDtls[0].baggageInfo; */
    $scope.baggageDetails = $scope.flightBook.selectedResult.flightSearchResults[0].baggageDtls;

    $scope.travelerscount = [];
    $scope.baggagedata = false



    $scope.flightBooking = $scope.flightBook.selectedResult;
    //console.log("$scope.flightBooking",$scope.flightBooking);
    $scope.fromairport = $scope.flightBooking.frmAirprt;
    $scope.toairport = $scope.flightBooking.toAirprt;
    $scope.flightresults = $scope.flightBooking.flightSearchResults;
    

    // $scope.bagdetails=$scope.flightresults[0].baggageDtls;
    // $scope.bagageinfo=$scope.bagdetails[0].baggageInfo;
    $scope.totalFare = $scope.flightBooking.flightSearchResults[0].totFare;

    $scope.fetchCity = function(place) {
        var placeSplit = place.split(',');
        return placeSplit[0];
    }

    seatPef = function(){
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var token;
        token = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.authToken
        }
        
        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=SEAT_PREF',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {


                $rootScope.seatList = data.data.data;
                mealPef();
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
             var status= refreshService.refreshToken(token.refreshToken,token.authToken);
             status.then(function(greeting) {
                seatPef();
            });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    }


    
    mealPef = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var token;
            token = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.authToken
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=MEAL_PREF',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {


                $rootScope.totallMealsList = data.data.data;
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                mealPef();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    nationalityget = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var token;
            token = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.authToken
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/nationality',

            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $scope.nationalityResult = data;
                $scope.allNationalitys = $scope.nationalityResult.data.data;
                $scope.nationality1 = $scope.allNationalitys;
                $scope.nationality = JSON.stringify($scope.nationality1);
                countrycode();
            } else {
                alert('error');
            }

        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                nationalityget();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }


    countryget = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var token;
            token = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.authToken
            }
        $http({
            method: 'GET',
            url:  ServerService.listPath + 'list/country',

            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $scope.countryResult = data.data.data;
            } else {
                alert('error');
            }

        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                countryget();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    countryget();
    gettingssr = function() {
        $scope.index = JSON.parse(sessionStorage.getItem('flightindex'));
        var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
        var repriceDetails = {
            "sessionId": sessionId,
            "resultIndexId": $scope.index
        }


        // $scope.ssrDetailsBind();


        // if (localStorage.loginToken != null) {
        //     $scope.loggedvalue = true;
        // }
            var token = JSON.parse(localStorage.authentication);
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.data.authToken
            }
        $http.post(ServerService.serverPath + 'flight/ssr', repriceDetails, {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.artloading = false;

            if (!response.data.success) {
                $scope.noSsr = false;
                $scope.noSsrerror = response.data.errorMessage;
                sessionStorage.removeItem('ssrtotalData');

            } else {
                //console.log(response);

                $scope.flightSsrData = response
                sessionStorage.setItem('ssrtotalData', JSON.stringify($scope.flightSsrData));

                $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrtotalData'));
                $scope.noSsr = true;
                // $scope.ssrDetailsBind();
            }

            // $scope.outboundsegemtData= $scope.ssrTotalData.data['DEL-SHJ'];
            // console.log($scope.ssrTotalData.data['DEL-SHJ'],"$scope.ssrTotalData.data['DEL-SHJ']");


        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                    gettingssr();
                   });                
            }
        });
    }
    countrycode = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var token;
            token = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.authToken
            }
        $http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;

                for(var a=0; a<  $scope.countryList.length;a++ ){
                    if( $scope.countryList[a].name==='971'){
                        $scope.guestuser.ccode=$scope.countryList[1];
                        $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
                    }
                }

                // var index=$scope.countryList.splice($scope.countryList.findIndex(a => a.name ===971) , 1)
	  
                // $scope.guestuser.ccode=$scope.countryList[1];
                
            
                if($scope.isLcc){
                    gettingssr();
                    $scope.selectedBagLst = JSON.parse(sessionStorage.getItem('SelectedBagLst'));
                    if($scope.selectedBagLst != undefined){
                        $scope.loopTot =$scope.selectedBagLst.totPrice;
                        $scope.fngrandtotal = $scope.selectedBagLst.totgrand;
                    }
                    $scope.selectedMealLst = JSON.parse(sessionStorage.getItem('SelectedMealLst'));
                    if($scope.selectedMealLst != undefined){
                        $scope.totMeals =$scope.selectedMealLst.totPrice;
                        $scope.fngrandtotal = $scope.selectedMealLst.totgrand;
                    }
                    $scope.SelectedSeatLst = JSON.parse(sessionStorage.getItem('SelectedSeatLst'));
                    if($scope.SelectedSeatLst != undefined){
                        $scope.totseat =$scope.SelectedSeatLst.totPrice;
                        $scope.fngrandtotal = $scope.SelectedSeatLst.totgrand;
                    }
                }else{
                    $scope.artloading = false;
                }
                // $scope.allcountrys = response.data.data;
                // $scope.countryList = $scope.allcountrys.sort((a, b) => a.name.localeCompare(b.name));
                //     } else {
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                countrycode();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }
    nationalityget();

    dropdownfun = function(){
        seatPef();
        // countrycode();
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var token;
            token = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.authToken
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/airline',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {


                $rootScope.freqFlyerList = data.data.data;
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                mealPef();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    };

    dropdownfun();
    $scope.travellerSelect = function(b, f, t) {
        $scope.sbound = b;
        $scope.sflight = f;
        $scope.straveller = t;
        $scope.seatRows = $scope.seatDetails[b].flightInfoDtls[f].seatRows;
        $scope.seatIterate(b, f, t, 'travel');
    };

    $scope.seatRemove = function(b, f, t) {
        $('#seatMapTraveller_' + b + '_' + f + '_' + t).find('.seatNo').empty();
        $('#seatMapTraveller_' + b + '_' + f + '_' + t).find('.seatPrice').empty();
        $('#seatMapTraveller_' + b + '_' + f + '_' + t).find('.seatremove').hide();
        $scope.seatIterate(b, f, t, 'seat');
    };

    $scope.sbound = 0;
    $scope.sflight = 0;
    $scope.straveller = 0;
    // $scope.seatRows = $scope.seatDetails[$scope.sbound].flightInfoDtls[$scope.sflight].seatRows;
    /* $timeout(function(){
        document.getElementById('traveller_0_0_0').checked = true;
    },10);
    */

    $scope.baggagedataList = [];


    $scope.bagChange = function(bagData,selectbag,index) {
        // console.log(bagData);
        $scope.baggagedataList = [];
        $scope.loopTot = [];
        $scope.showBag = true;
        $scope.grandtotal = $scope.myJson[0].grndTotal;
        $scope.loopTot = 0;
        var zero = '0';
        $scope.fngrandtotal = 0;
        for (var b = 0; b < $scope.baggageData.length; b++) {
            if ($scope.baggageData[b].passengerType != 'Infant') {
                $scope.baggagedataList = ($scope.baggagedataList || []);
                for(var tevlbg = 0; tevlbg < $scope.baggageData[b].travellerBaggages.length; tevlbg++){
                    if($scope.baggageData[b].travellerBaggages[tevlbg].selectedBaggage != undefined){
                        // $scope.baggageData[index].travellerBaggages[tevlbg].selectedBaggage.ssrdetails.paxIndex = index;
                        // if ($scope.baggageData[index].passengerType == 'ADT') 
                        // {
                        //     $scope.baggageData[index].travellerBaggages[tevlbg].selectedBaggage.ssrdetails.passengerKey = 'A'+(Number(index)+1);
                        // }
                        // if ($scope.baggageData[index].passengerType == 'CNN')
                        // {
                        //     $scope.baggageData[index].travellerBaggages[tevlbg].selectedBaggage.ssrdetails.passengerKey = 'C'+(Number(index)+1);
                        // }
                        
                        $scope.baggagedataList.push($scope.baggageData[b].travellerBaggages[tevlbg].selectedBaggage);
                        $scope.loopTot += $scope.baggageData[b].travellerBaggages[tevlbg].selectedBaggage.price;
                        }
                    }
            }
        }
        // $scope.totalSSR+=$scope.loopTot;

        return $scope.loopTot;

    }

    // $scope.nationality1 = JSON.parse(localStorage.getItem('nationality')).data;
    $scope.nationality = JSON.stringify($scope.nationality1);
    $scope.salutations = ["Mr", "Mrs", "Miss"];
    $scope.childsalutations = ["Miss", "Mstr"];
    $scope.infantsalutations = ["Miss", "Mstr"];
    var personaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.code="";
        // this.charge="";
        this.trvtype = "ADT";
        this.age = 32;
        this.city = "Chennai";
        this.custPrefTypeInbound = null;
        this.custPrefTypeOutbound = null;
        this.seatPrefTypeOutbound = null;
        this.seatPrefTypeInbound = null;
        this.customerFfDtls = []
        this.country = "IN";
        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
        this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
    }
    var childPersonaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.code="";
        // this.charge="";
        this.trvtype = "CNN";
        this.age = 5;
        this.city = "Chennai";
        this.custPrefTypeInbound = null;
        this.custPrefTypeOutbound = null;
        this.seatPrefTypeOutbound = null;
        this.seatPrefTypeInbound = null;
        this.customerFfDtls = []
        this.country = "IN";
        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
        this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
    }
    var infantPersonaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.code="";
        // this.charge="";
        this.trvtype = "INF";
        this.age = 1;
        this.city = "Chennai";
        this.custPrefTypeInbound = null;
        this.custPrefTypeOutbound = null;
        this.seatPrefTypeOutbound = null;
        this.seatPrefTypeInbound = null;
        this.customerFfDtls = []
        this.country = "IN";
        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
        this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
    }

    var miscdata = function(){
        this.mealpref=[
            {
                "code":null
            }
        ];
        this.seatpref =[
            {
                "code":null
            }
        ];
    }
    var traveller = function() {
        this.personaldata = new personaldata();
        this.miscdata = new miscdata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }
    var childTraveller = function() {
        this.personaldata = new childPersonaldata();
        this.miscdata = new miscdata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }
    var infantTraveller = function() {
        this.personaldata = new infantPersonaldata();
        this.miscdata = new miscdata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }

    var travellerByRoom = function() {
        this.adultTravellersList = [];
        this.childTravellersList = [];
        this.infantTravellersList = [];
    }

    $scope.traveller = function() {
        this.personaldata = new personaldata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }

    /**We have to maintain same Order below until "update Traveller Details end" */
    var travellerByRoomObj = new travellerByRoom();
    $scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
    if ($scope.travellersListByRoom != null) {

        $scope.travelerscount = [];
        $scope.travelerscount.push($scope.travellersListByRoom[0].adultTravellersList);
        $scope.travelerscount.push($scope.travellersListByRoom[0].childTravellersList);
        $scope.travelerscount.push($scope.travellersListByRoom[0].infantTravellersList);
    }

    $scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))
    $scope.guestLoginName = JSON.parse(sessionStorage.getItem("guestLoginDatails"))
    if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
        $scope.loggedvalue = true;
        $scope.$parent.bookingHide = true;
        $scope.$parent.guestLogin = true;
        $scope.$parent.bookingHide1 = true;
        $rootScope.dispName = $scope.guestLoginName;
    }
    if ($scope.userDetails != null) {
        $scope.user = $scope.userDetails;
    }
    /* check user Already login*/
    if (localStorage.getItem('loginName') && localStorage.getItem('loginName') != "null") {
        $scope.isValidUser = true;
        $scope.loggedvalue = true;
        $rootScope.loginsignIn=true;
        loginUser = JSON.parse(localStorage.getItem('loginName'));
        $scope.user.email = loginUser.email;
        $scope.user.contactNumber = parseInt(loginUser.contactNumber);
        $scope.user.mobcntrycode = loginUser.mobcntrycode;
        /*check user Already login end*/
        /**update Traveller Details  */

        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];
            // var travellerByRoomObj=new travellerByRoom();
            for (i = 0; i < $scope.flightBooking.adtCnt; i++) {
                var travellerObj = new traveller();
                travellerByRoomObj.adultTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                // travellerObj.personaldata.fname = $scope.user.fname;
                travellerObj.personaldata.mobile = parseInt($scope.user.contactNumber);
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            for (j = 0; j < $scope.flightBooking.chldCnt; j++) {
                var travellerObj = new childTraveller();
                travellerByRoomObj.childTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                // travellerObj.personaldata.fname = $scope.user.fname;
                travellerObj.personaldata.mobile = parseInt($scope.user.contactNumber);
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            for (j = 0; j < $scope.flightBooking.infntCnt; j++) {
                var travellerObj = new infantTraveller();
                travellerByRoomObj.infantTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                // travellerObj.personaldata.fname = $scope.user.fname;
                travellerObj.personaldata.mobile = parseInt($scope.user.contactNumber);
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            $scope.travellersListByRoom.push(travellerByRoomObj);

        }

    } else {

        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];
            var travellerByRoomObj = new travellerByRoom();
            for (i = 0; i < $scope.flightBooking.adtCnt; i++) {
                var travellerObj = new traveller();
                travellerByRoomObj.adultTravellersList.push(travellerObj);
            }
            for (j = 0; j < $scope.flightBooking.chldCnt; j++) {
                var travellerObj = new childTraveller();
                travellerByRoomObj.childTravellersList.push(travellerObj);
            }
            for (j = 0; j < $scope.flightBooking.infntCnt; j++) {
                var travellerObj = new infantTraveller();
                travellerByRoomObj.infantTravellersList.push(travellerObj);
            }
            $scope.travellersListByRoom.push(travellerByRoomObj);

        }

    }
    
    document.querySelector(".guestmobilenumm").addEventListener("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });

    
    $scope.selectpaxChecked = function(i,pax,paxAge) {
        document.getElementById('checkedvalue'+i).checked = true;
        $scope.addTravel('true',pax,paxAge,i);
    }

    /*assign guestUserDetails to Travellers */
    $scope.updateGustUserDetailsToTravller = function() {
            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = jQuery('#email').val();
            if ($scope.user.emailId) {
                $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = $scope.user.emailId;
            }
            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;

            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;

        }
        /*assign guestUserDetails to Travellers end*/


    $scope.mealCalculate = function() {
        $scope.mealTotal = 0;
        if ($scope.showBag == false) {
            $scope.baggageTot = $scope.totBagCal();
        } else {
            $scope.baggageTot = $scope.loopBagFun();
        }
        $scope.grandtotal = $scope.myJson[0].grndTotal + $scope.loopTot + $scope.seatTotal + $scope.insureAmt;
        $('.mealCharge').each(function() {
            if ($(this).text() != '') {
                $scope.mealTotal += parseInt($(this).text());
            }
        });
        $scope.grandtotal += $scope.mealTotal;
    };

    $scope.noMeal = function(likeMeal) {
        $scope.likeMeal = likeMeal;
        if (likeMeal == 0) {
            $scope.totMeals = 0;
            sessionStorage.removeItem('SelectedMealLst');
            $scope.mealsData.forEach(meals => {
                meals.travelerDetails.forEach(trvldets => {
                    trvldets.price = 0
                    trvldets.quantity = 0
                    trvldets.mealslist.forEach(mlst => {
                        if(mlst.count > 0){
                            mlst.count = 0;
                        }
                    })
                    
                });
            });
    $scope.fngrandtotal = 0;
            $scope.grandtotal = $scope.myJson[0].grndTotal + $scope.loopTot + $scope.seatTotal + $scope.insureAmt;
        } else {
            // $scope.mealCalculate();
        }
    }
    
    $scope.noseatshow = true;
    $scope.noSeat = function(likeseat) {
        $scope.likeSeat = likeseat;
        if (likeseat == 1) {
            $scope.noseatshow = false;
            $scope.totseat = 0;
            sessionStorage.removeItem('SelectedSeatLst');
            $scope.seatData.forEach(seat => {
                seat.travelerDetails.forEach(trvldets => {
                    trvldets.price = 0
                    trvldets.seatNumber = '-'
                    // trvldets.seatsslist = [];                    
                });
            });
            
    $scope.fngrandtotal = 0;
    $scope.seats = [];  
            $scope.grandtotal = $scope.myJson[0].grndTotal + $scope.loopTot + $scope.seatTotal + $scope.insureAmt;
        } else {
            $scope.noseatshow = true;
            // $scope.mealCalculate();
        }
    }


    /**
         //@author Raghava Murmareddy
        get Selected Insurance Details which we select at Review Time
    	
    */
    $scope.selectedInsurancePlanId = sessionStorage.getItem('selectedInsurancePlanId');
    $scope.travelInsurancee = JSON.parse(sessionStorage.getItem('travelInsurancee'));

    if ($scope.travelInsurancee != undefined && $scope.selectedInsurancePlanId != null) {
        $scope.withInsurance = true;
        $scope.selectedInsurancePlan = $scope.travelInsurancee.availablePlans[$scope.selectedInsurancePlanId];
        $scope.selectedInsurancePlantotalFare = $scope.selectedInsurancePlan.totalFare;
    } else {
        $scope.selectedInsurancePlantotalFare = 0;
        
    }

    $scope.fngrandtotal = 0;
    repriceCall= function(selectedSSRBean){
        var token = JSON.parse(localStorage.authentication);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.data.authToken
        }
        var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
    var repriceDetail = {
            "sessionId": sessionId,
            "resultIndexId": JSON.parse(sessionStorage.getItem('flightindex')),
            "selectedSSRBean": selectedSSRBean
        }
    sessionStorage.setItem("ssrRQ",JSON.stringify(selectedSSRBean));
        $scope.artloading = true;
    $http.post(ServerService.serverPath + 'flight/reprice', repriceDetail, {
        headers: headersOptions
    }).then(function successCallback(response) {
        if(response.data.success){
            if(response.data.statusCode == "S003"){
                Lobibox.alert('success', {
                    msg:  response.data.data.msg,
                    // callback: function (lobibox, type) {
                    //   if (type === 'ok') {
                       
                    //   }
                    // }
                  });
                  $scope.artloading = false;
                  $scope.loading = false;
                  $scope.disabled = false;
                  $scope.validateservicedatapament = true;
                  $scope.validateservicedata = false;
                  $scope.validatedata = false;
                  $scope.validateguestdata = true;
                  $scope.fngrandtotal = response.data.data.selectedResult.flightSearchResults[0].grndTotal;
                  sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
            }else{
                $scope.artloading = false;
                $scope.loading = false;
                $scope.disabled = false;
                $scope.validateservicedatapament = true;
                $scope.validateservicedata = false;
                $scope.validatedata = false;
                $scope.validateguestdata = true;
                sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
            }

        }else{
            Lobibox.alert('error',
            				{
            					msg: response.data.errorMessage
            			});
        }
        $scope.artloading = false;

    }, function errorCallback(response) {
        if(response.status == 403){
           var status =  refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
           status.then(function(greeting) {
            repriceCall(selectedSSRBean);
        });
        }
    });
    }

    $scope.seatfunctionality = function() {
        $("#serbill").addClass("active")
        $scope.loading = false;
        $scope.billing=true
        // for(var rp=0; rp<this.mealsData.)
        $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrtotalData'));
        // var keylist = Object.keys($scope.ssrTotalData.data.data);
        var selectedSSRBean = {};
       selectedSSRBean.selectedSSR = {};
        // Object.keys($scope.ssrTotalData.data.data).forEach(k => delete $scope.ssrTotalData.data.data[k])
        // $scope.seats.key = {}
        // var set = []
        // var selectedSSRBean ={};
        // var ssrchildNodes ={"routeInfo":{}, "mealSSRs":[], "baggageSSRs":[], "selectedSeat":[]};
        if($scope.ssrTotalData){
            
        for (const [key, value] of Object.entries($scope.ssrTotalData.data.data)) {

            // selectedSSRBean[`${key}`] = ssrchildNodes;
            // $scope.selectedSSRBean[key];
            // $scope.selectedSSRBean[key].mealSSRs =($scope.selectedSSRBean[key].mealSSRs || []);
            // $scope.selectedSSRBean[key].seats =($scope.selectedSSRBean[key].seats || []);
            // $scope.selectedSSRBean[key].baggageSSRs =($scope.selectedSSRBean[key].baggageSSRs || []);
            var mealSSRs = [];
            var seats = [];
            var baggageSSRs = [];

            for (var ml = 0; ml < $scope.mealsselectedlist.length; ml++) {
                for (var mls = 0; mls < $scope.mealsselectedlist[ml].length; mls++) {
                    if (key == $scope.mealsselectedlist[ml][mls].key) {
                        mealSSRs.push($scope.mealsselectedlist[ml][mls].list); 
                        sessionStorage.setItem("SelectedMealLst",JSON.stringify({"mealRQ":$scope.mealsselectedlist,"mealSelectedLst":$scope.mealsData,"totPrice": $scope.totMeals,"totgrand":$scope.fngrandtotal}));
                    }
                }
            }

            $scope.mealsData.forEach(meals => {
                if (key == meals.key) {
                meals.travelerDetails.forEach(trvldets => {
                    trvldets.mealslist.forEach(mlst => {
                        if(mlst.count > 0){
                            mealSSRs.push(mlst); 
                            sessionStorage.setItem("SelectedMealLst",JSON.stringify({"mealRQ":$scope.mealsselectedlist,"mealSelectedLst":$scope.mealsData,"totPrice": $scope.totMeals,"totgrand":$scope.fngrandtotal}));
                        }
                    })
                    
                });
            }
            });

            for (var sl = 0; sl < $scope.seats.length; sl++) {
                if (key == $scope.seats[sl].key) {
                    seats.push($scope.seats[sl].seat);
                    sessionStorage.setItem("SelectedSeatLst",JSON.stringify({"seatRQ":$scope.seats,"seatSelectedLst":$scope.seatData,"totPrice": $scope.totseat,"totgrand":$scope.fngrandtotal}));
                }
            }

            for (var bl = 0; bl < $scope.baggagedataList.length; bl++) {
                if (key == $scope.baggagedataList[bl].key) {
                    baggageSSRs.push($scope.baggagedataList[bl].ssrdetails);
                    sessionStorage.setItem("SelectedBagLst",JSON.stringify({"bagRQ":$scope.baggagedataList,"bagSelectedLst":$scope.baggageData,"totPrice": $scope.loopTot,"totgrand":$scope.fngrandtotal}));
                }
            }

            // if(mealSSRs.length==0 && baggageSSRs.length==0 && seats.length==0 ){
            //     selectedSSRBean = null;
            // }else{
 
            selectedSSRBean.selectedSSR[`${key}`] = { "routeInfo": {}, "mealSSRs": mealSSRs, "baggageSSRs": baggageSSRs, "seats": seats };

        // if(selectedSSRBean){
        }

        if(selectedSSRBean.selectedSSR != null){
            repriceCall(selectedSSRBean);
        }else{
            $scope.validateservicedatapament = true;
            $scope.validateservicedata = false;
            $scope.validatedata = false;
            $scope.validateguestdata = true;
        }
            // console.log(JSON.stringify($scope.selectedSSRBean));
            /* $scope.paymentDetail = $scope.cardDetail; */


            // }else  {
            // 			Lobibox.alert('error',
            // 				{
            // 					msg: response.data.errorMessage
            // 			});
            // 		}
            // called asynchronously if an error occurs
            // or server returns response with an error status.
    // }
    }else{
        $scope.validateservicedatapament = true;
        $scope.validateservicedata = false;
        $scope.validatedata = false;
        $scope.validateguestdata = true;
    }
    $window.scrollTo(0, 0);
    }


    // gettingssr();

    $scope.guestuser = {};
    var displayName = {};
    var loginResp = {};
    $scope.userBooking = {}
    $scope.gsubmitted = false;
    $scope.proceedGuestLoginDetails = function() {

        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        $scope.gsubmitted = true;
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+tokenname.authToken
            }
            // if(valid ){

        if (document.getElementById('guestLogin').style.display == 'block') {
            if ($scope.guestuser.email != null && $scope.guestuser.contactNumber != null && $scope.guestuser.ccode != null) {
                // $("#step2tab").addClass("active");
                // $("#step2").removeClass("disabled");
                // $("#step2").addClass("active");
                $("#email").parent().removeClass('has-error');
                $("#contact").parent().removeClass('has-error');
                $("#ccode1").parent().removeClass('has-error');

                $scope.artloading = true;
                var guestLoginData = '{ "username	" : "' + $scope.guestuser.email + '", "mobileNo" : "' + $scope.guestuser.contactNumber + '"}'

                $http.post(ServerService.authPath + 'guest-login/user', '{ "username" : "' + $scope.guestuser.email + '", "mobileNo" : "' + $scope.guestuser.contactNumber + '"}', {
                    headers: headersOptions
                }).then(function successCallback(response) {
                    $scope.artloading = false;
                    // $scope.backtodetail = true;
                    if (response.data.authToken!=null){
                        localStorage.setItem('authentication', JSON.stringify(response));
                    if (response.data.alreadyRegistered==false) {
                       
                        $scope.validatedata = true;
                        $scope.validateguestdata = true;
                        $scope.loggedValue = true;
                        $rootScope.bookingHide = true;
                        $rootScope.guestLogin = true;
                        $rootScope.bookingHide1 = true;
                        // $scope.userBooking.bookingHide = true;
                        // $scope.userBooking.bookingHide1 = true;
                        // var userBookingdetails = $scope.userBooking;
                        // sessionStorage.setItem('userBookingdetails', JSON.stringify(userBookingdetails));

                    
                        localStorage.setItem('authentication', JSON.stringify(response));
                        $rootScope.dispName = $scope.guestuser.email;
                        $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email =  $scope.guestuser.email ;
                        $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;
 
                        $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
                        sessionStorage.setItem('guestLoginDatails', JSON.stringify($scope.guestuser.email));
                    sessionStorage.setItem('particulatUserData', JSON.stringify(response));
                    $scope.loggedValue = true;
                    // Lobibox.alert('success', {
                    //     msg: "Login Successfully"
                    // });
                    $scope.validatedata = true;
                    $scope.validateguestdata = true;
                    $window.scrollTo(0, 0);

                    $("#trvl").addClass("active");
                }else{
                    Lobibox.confirm({
			
                        msg:'You have already registered, please enter your password to login or select No to Continue as Guest  ?!',
                        callback: function ($this, type, ev) {
                            if(type == 'yes') {
                                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                                var tokenname =$scope.tokenresult.data;
                                var headersOptions = {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + tokenname.authToken
                                }
                                $http.get(ServerService.listPath + 'crud/list/language',{
                                    headers: headersOptions
                                }).then(function(result){
                                    if(response.data.alreadyRegistered==true){
                                        $scope.user.emailId=$scope.guestuser.email;
                                    
                            document.getElementById('loginContinue').style.display='block'
                
                            document.getElementById('guestLogin').style.display='none';
                                    }
                                })
                
                            }else{
                                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                                var tokenname =$scope.tokenresult.data;
                                var headersOptions = {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + tokenname.authToken
                                }
                                $http.get(ServerService.listPath + 'crud/list/language',{
                                    headers: headersOptions
                                }).then(function(result){
                                    $scope.backtodetail = true;
                                localStorage.isguestlogged = true;
                                $scope.validatedata=true;
                                $scope.validateguestdata=true;
                                $scope.loggedValue=true;
                                $rootScope.bookingHide = true;
                                $rootScope.guestLogin = true;
                                           $rootScope.bookingHide1 = true; 
                                         
                                    
                                           $rootScope.dispName = $scope.guestuser.email ;
                                           $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email =  $scope.guestuser.email ;
                                           $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;
                    
                                           $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
                                           sessionStorage.setItem('guestLoginDatails', JSON.stringify( $scope.guestuser.email));
                                           $("#trvl").addClass("active");	
                
                                })
        
        
                            
                                
                    }
                }
            });
            }
        }else{
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }

                }, function errorCallback(response) {
                    if(response.status == 403){
                        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                    $scope.proceedGuestLoginDetails();
                    }); 
                    }
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                  });
            } else {
                // $("#email").parent().addClass('has-error');
                // $("#contact").parent().addClass('has-error');
                // $("#ccode1").parent().addClass('has-error');

                return false;
                // Lobibox.alert('error', {
                //     msg: response.data.errorMessage
                // })

            }
        } else {
            document.getElementById('guestLogin').style.display = 'block';
            
            document.getElementById('loginContinue').style.display = 'none';

        }
        // }else{
        // 	return false;
        // }
       

    }



    $scope.logindataReview=function(){
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            "g-recaptcha-response" :$scope.token,
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.post(ServerService.authPath + 'authenticate', '{ "username" : "' + $scope.user.emailId + '", "password" : "' + $scope.user.password + '"}', { headers: headersOptions })
        .then(function successCallback(response) {
                if(response != undefined){

            $scope.backtodetail = true;
            $scope.artloading = false;

            /* $http.post(ServerService.authPath + url, datas).then(function(response) { */
            $scope.loadingsignIn = false;
            $scope.loggedvalue = true;
             localStorage.setItem('islogged', true);

            localStorage.setItem('authentication', JSON.stringify(response));
            if (response.data.authToken) {
                var tokennames = response.data;
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokennames.authToken
                }
         
                $http.get(ServerService.authPath + 'user-details', {
                    headers: headersOptions
                }).then(function successCallback(response) {
                    $scope.userId = response.data.userId;
                    // $scope.artloading = false;

                    sessionStorage.setItem('particulatUserData', JSON.stringify(response));
                    $scope.loggedValue = true;
                    $scope.loginsignIn = true;
                    $scope.$parent.bookingHide = true;
                    $scope.$parent.bookingHide1 = true;
                    $scope.userBooking.bookingHide = true;
                    $scope.userBooking.bookingHide1 = true;
                    var userBookingdetails = $scope.userBooking;
                    $scope.updateGustUserDetailsToTravller();
                    localStorage.setItem('userBookingdetails', JSON.stringify(userBookingdetails));
                    displayName.fname = $scope.user.emailId;
                    $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email =  $scope.user.emailId
                    //displayName.email = response.data.data.profileData.personaldata.email;
                    //displayName.contactNumber = response.data.data.profileData.personaldata.mobile;
                    //displayName.mobcntrycode = response.data.data.profileData.personaldata.mobcntrycode;
                    // sessionStorage.setItem('authentication', JSON.stringify(loginResp));
                    localStorage.setItem('loginName', JSON.stringify(displayName))
                    $rootScope.dispName = response.data.sub;

                    // window.location.reload();
                    /* 	Lobibox.alert('success', {
                        msg: "Login Successfully"
                    }); */
                    $scope.validatedata = true;
                    $scope.validateguestdata = true;

                }, function errorCallback(response) {
                    if(response.status == 403){
                        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                        status.then(function(greeting) {
                            $scope.login();
                           }); 
                    }
                  });
            } else {
                Lobibox.alert('error', {
                    msg: "Invalid Login Credential"
                })
                $scope.showSignIn = true;
            }
                }else{
                    Lobibox.alert('error', {
                        msg: "Invalid Login Credential"
                    })
                    $scope.artloading = false;
                }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.login();
                   });
            }
            if(response.status == 401){
                Lobibox.alert('error', {
                    msg: "Invalid Login Credential"
                })
                $scope.artloading = false;
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    }

    $scope.login = function() {
        $scope.recaptchaId = JSON.parse(localStorage.getItem('configKeys'));
        if (document.getElementById('loginContinue').style.display == 'block') {

            if ($scope.user.emailId != null && $scope.user.password != null) {
                $scope.loadingsignIn = true;
                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

                var url = 'authenticate';
                var datas = '{ "username	" : "' + $scope.user.emailId + '", "password" : "' + $scope.user.password + '"}'
                $scope.artloading = true;
                grecaptcha.ready(function() {
                    grecaptcha.execute($scope.recaptchaId.google_recaptcha_public_key, {})
                            .then(function(token) {
                                // sessionStorage.setItem("captcha",token);
                                $scope.token=token;
                                $scope.logindataReview();
                            });
                });

    }else {
        if ($("#password").val() != "") {
            $("#password").parent().removeClass('has-error');
            $("#emailId").parent().removeClass('has-error');
        } else {
            $("#password").parent().addClass('has-error');
            $("#emailId").parent().addClass('has-error');
        }
    }
} else {
    document.getElementById('loginContinue').style.display = 'block';
    
        document.getElementById('guestLogin').style.display='none';

}
    }


    $scope.allTravellerData = JSON.parse(localStorage.getItem('loginTravellerData'));
    var adultCount = $scope.travellersListByRoom[0].adultTravellersList.length;
    var chaildCount = $scope.travellersListByRoom[0].childTravellersList.length;
    var infantCount = $scope.travellersListByRoom[0].infantTravellersList.length;
    $scope.addTravel = function(checked, singlePsgr, age, indexvalue) {

        $scope.particularindexvalue=indexvalue;
        $scope.checked=checked
        //  $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
        $scope.usersObj = {};
        $scope.usersObj.salutation = singlePsgr.salutation;
        $scope.usersObj.fname = singlePsgr.firstName
        $scope.usersObj.lname = singlePsgr.lastName
        $scope.usersObj.customerFfDtls=singlePsgr.customerFfDtls;
        $scope.usersObj.customerPreferences=singlePsgr.customerPreferences
      
        var unwantedCharacter = "0";
        var dob=singlePsgr.dateOfBirth.split("/");
        var birthdate=dob[0];
        if( birthdate.charAt( 0 ) === unwantedCharacter ){
            birthdate = birthdate.slice( 1 );

        }
        var birthmonth=dob[1];
        
       
        if( birthmonth.charAt( 0 ) === unwantedCharacter ){
            birthmonth = birthmonth.slice( 1 );

        }

        $scope.usersObj.birthdate=birthdate;

        $scope.usersObj.birthmonth=birthmonth;

        $scope.usersObj.birthyear=dob[2];
        
        var  mobcntrycodeObj=[];
        if(singlePsgr.countryCode === '' || singlePsgr.countryCode === null || singlePsgr.countryCode === undefined){
            $scope.usersObj.mobcntrycodetrobj = singlePsgr.countryCode;
        }else{
        for(var c=0; c<$scope.countryList.length; c++){
            if($scope.countryList[c].name==singlePsgr.countryCode){
                 mobcntrycodeObj.push($scope.countryList[c]);
            }
        }
        $scope.usersObj.mobcntrycodetrobj = mobcntrycodeObj[0];
    }
       
        // $scope.usersObj.mobcntrycode = mobcntrycodeObj[0];
        $scope.usersObj.mobile = singlePsgr.phoneNumber
        $scope.usersObj.email = singlePsgr.emailId
        $scope.usersObj.gender = singlePsgr.gender
        $scope.usersObj.nationalitycode = singlePsgr.nationality
        $scope.usersObj.nationalityname = singlePsgr.nationality
        $scope.usersObj.passportNo = singlePsgr.passportNo
        $scope.usersObj.customerId = singlePsgr.customerId
        // $scope.usersObj.city = singlePsgr.passportNo
        // $scope.usersObj.country = singlePsgr.passportNo

        if(singlePsgr.passportExpiryDate!=null){
        var passExp=singlePsgr.passportExpiryDate.split("/");
        var expirydate=passExp[0];
        if( expirydate.charAt( 0 ) === unwantedCharacter ){
            expirydate = expirydate.slice( 1 );

        }
        var expMonth = passExp[1];
       
        if( expMonth.charAt( 0 ) === unwantedCharacter ){
            expMonth = expMonth.slice( 1 );

        }
        $scope.usersObj.expirydate=expirydate
        $scope.usersObj.expirymonth=expMonth;
        $scope.usersObj.expiryyear=passExp[2];
    }
        $scope.usersObj.nationalitycode =singlePsgr.saudiNationalityId
        $scope.usersObj.passIssueGovt =singlePsgr.nationality
            //  var adultCount=$scope.travellersListByRoom[0].adultTravellersList.length;
        if (age >= 12) {
            // $scope.usersObj.clsname = 'adultage';
            $scope.usersObj.trvtype = 'ADT';
            if ( $scope.checked) {
                if (adultCount != 0) {
                    $scope.usersObj=$scope.usersObj;
                   
                } else {
                  
                }








            } else {
                if (adultCount != $scope.travellersListByRoom[0].adultTravellersList.length)
                    adultCount++
                    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = "";

            }
        } else if (age >= 2 && age < 12) {
            $scope.usersObj.trvtype = 'CNN';
            if ( $scope.checked) {
                if (chaildCount != 0) {
                    /* chaildCount-- */
                    $scope.childusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.usersObj


                  
                } else {
                   
                }








            } else {
                if (chaildCount != $scope.travellersListByRoom[0].childTravellersList.length)
                    chaildCount++
                    $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = "";

            }


        } else {
            $scope.usersObj.trvtype = 'INF';
            if ( $scope.checked) {
                if (infantCount != 0) {
                    /* chaildCount-- */
                    $scope.infantusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.usersObj


                    // Lobibox.notify('success', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
                    //     delay: 1500
                    // });
                } else {
                    // Lobibox.notify('error', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: "Infant limit Reached!",
                    //     delay: 1500
                    // });
                }








            } else {
                if (infantCount != $scope.travellersListByRoom[0].infantTravellersList.length)
                infantCount++
                    $scope.travellersListByRoom[0].infantTravellersList[$scope.infantIndex].personaldata = "";

            }
        }

    }

    $scope.ConfirmTravellerData=function(){
        if($scope.TravelType=='adult'){
if($scope.TravelType=='adult' && $scope.usersObj!=undefined &&  $scope.checked=='true'){
    Lobibox.notify('success', {
        size: 'mini',
        position: 'top right',
        msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
        delay: 1500
    });
    $scope.modalInstance.dismiss();
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = $scope.usersObj
    if($scope.usersObj.customerPreferences.length > 0){
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].miscdata.mealpref[0].code = $scope.usersObj.customerPreferences[0].custPrefType;
    
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].miscdata.seatpref[0].code = $scope.usersObj.customerPreferences[1].custPrefType;

    }

}else{
    Lobibox.notify('error', {
        size: 'mini',
        position: 'top right',
        msg: "Please select at least one traveller!",
        delay: 1500
    });
}
        } else if($scope.TravelType=='child'){
            if($scope.TravelType=='child' && $scope.childusersObj!=undefined &&  $scope.checked=='true'){
                $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.childusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.childusersObj.fname + " " + $scope.childusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg: "Please select at least one traveller!",
                    delay: 1500
                });
            }
        }else{
            if($scope.TravelType=='infant' && $scope.infantusersObj!=undefined &&  $scope.checked=='true'){
                $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.infantusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.infantusersObj.fname + " " + $scope.infantusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg: "Please select at least one traveller!",
                    delay: 1500
                });
            }
        }

    }

            
    $scope.infantsearchTrvlr = function(infantindexvalue,travelType) {
        $scope.modalInstance=$uibModal.open({
            templateUrl: 'myTestModal.tmpl.html',
            backdrop: 'static',
			keyboard: true,
            scope:$scope
        });
        $scope.infantusersObj=undefined;
    
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
         
            $scope.allTravellerData=[];
            $scope.infantIndex=infantindexvalue;
            $scope.TravelType=travelType;
                     $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                     for(var t=0;  t<$scope.allTravellerDataa.length; t++){
    if( $scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age < 2){
        $scope.allTravellerData.push($scope.allTravellerDataa[t])
    }
                     }
                     if($scope.allTravellerData.length==0) {
                        $scope.childText=" Traveller Data matching the age criteria given is not available."
                    }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.infantsearchTrvlr();
                });
            }
          });
    
                };

                $scope.adultsearchTrvlr=function(adultindexvalue,travelType){
                    $scope.modalInstance=$uibModal.open({
                        templateUrl: 'myTestModal.tmpl.html',
                        backdrop: 'static',
                        keyboard: true,
                        scope:$scope
                    });
                    $scope.usersObj=undefined;
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.get(ServerService.authPath + 'user/retrieve', {
                        headers: headersOptions
                    }).then(function successCallback(response) {
                        $scope.trvSearchdata = response.data.data;
            
                        $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
                        // sessionStorage.setItem('authentication', JSON.stringify(response));
                        // $scope.trvSearchRes.push()
                        sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
                        $scope.allTravellerData=[];
                        $scope.TravelType=travelType;
                        $scope.adultIndex=adultindexvalue;
                             $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                                 for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                if( $scope.allTravellerDataa[t].age>= 12){
                    $scope.allTravellerData.push($scope.allTravellerDataa[t])
                }
                                 }
                                 if($scope.allTravellerData.length==0) {
                                    $scope.childText=" Traveller Data matching the age criteria given is not available."
                                }
                    }, function errorCallback(response) {
                        if(response.status == 403){
                            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                            status.then(function(greeting) {
                                $scope.adultsearchTrvlr();
                            });
                        }
                      });
                
                }

                $scope.childsearchTrvlr = function(childindexvalue,travelType) {
                    $scope.modalInstance=$uibModal.open({
                        templateUrl: 'myTestModal.tmpl.html',
                        backdrop: 'static',
                        keyboard: true,
                        scope:$scope
                    });
                    $scope.childusersObj=undefined;
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.get(ServerService.authPath + 'user/retrieve', {
                        headers: headersOptions
                    }).then(function successCallback(response) {
                        $scope.trvSearchdata = response.data.data;
            
                        $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
                        // sessionStorage.setItem('authentication', JSON.stringify(response));
                        // $scope.trvSearchRes.push()
                        sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
                        $scope.allTravellerData=[];
                    $scope.childIndex=childindexvalue;
                    $scope.TravelType=travelType;
                             $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                             for(var t=0;  t<$scope.allTravellerDataa.length; t++){
             if($scope.allTravellerDataa[t].age >= 2 && $scope.allTravellerDataa[t].age < 12){
                $scope.allTravellerData.push($scope.allTravellerDataa[t])
            }
                             }
                            if($scope.allTravellerData.length==0) {
                                $scope.childText="Traveller Data matching the age criteria given is not available."
                            }
                    }, function errorCallback(response) {
                        if(response.status == 403){
                            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                            status.then(function(greeting) {
                                $scope.childsearchTrvlr();
                            });
                        }
                      });
                  
                }
            
                
                $scope.close=function(){
                    $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
                };


    /* AgencyTermsOfBusiness End*/
    /* signIn function */
    $scope.showSignInText = "Sign in to book faster";
    $scope.showSignIn = false;
    $scope.signIn = function() {
            $scope.showSignIn = $scope.showSignIn ? false : true;

            var buttonText = "Sign in to book faster";
            if ($scope.showSignIn == true) {
                var buttonText = "OR Continue As Guest";
                $("input#password").attr("required", "true");
                $("input#contact").removeAttr("required");
            } else {
                var buttonText = "Sign in to book faster";
                $("input#password").removeAttr("required");
                $("input#contact").attr("required", "true");
            }

            $scope.showSignInText = buttonText;

        }
      
    $scope.paymentDetail = {

        firstName: "",
        lastName: "",
        address: "",
        emailAddress: "",
        countryCode: "",


        city: ""
    }

    /* proceedCustomDetails function */
    $scope.loading = false;
    $scope.submitted = false;
    $scope.disabled = false;
    /* $scope.paymentDetail = {} */
    $scope.selectDoc = {}
    $scope.bookingReq = {}
    $scope.specialServiceRequest = {}
    $scope.selectDoc.dockNo = null;
    var expireDate = {}
    var traveller = {};
    var childTraveller = {};
    var infantTraveller = {};

    $scope.travelerscount = [];
    $scope.baggagedata = false

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.backdetail=function() {
        $scope.loggedvalue = true;
         $("#trvl").removeClass("active");
        $scope.backtodetail = false;
        $scope.backtotraveler = false;
        $scope.validateguestdata = false;
        // $scope.validateservicedatapament = true;
        $scope.validatedata = false;
        // $scope.validateservicedata = true;
        // $("#trvl").addClass("active")
        // $("#bill").removeClass("active");
        // $window.scrollTo(0, 0);

    }
    $scope.backtraveler=function() {
        // $scope.timeload();
        
        $("#bill").removeClass("active");
        $("#serv").removeClass("active");
        $("#serbill").removeClass("active");
        
        
        $scope.backtodetail = true;
        $scope.backtotraveler = false;
        $scope.validateservicedatapament = false;
       
        $scope.validatedata = true;
        $scope.validateservicedata=false
        
    }
    $scope.backttoService=function() {
        // $scope.timeload();
        
        $("#bill").removeClass("active");
      
        $("#serbill").removeClass("active");
        $("#serv").addClass("active")
        $scope.billing=false
       
        $scope.backtotraveler = true;
        $scope.validateservicedatapament = false;
       
        $scope.validatedata = false;
        $scope.validateservicedata=true
        
    }
    $scope.pamentToTraveller = function() {
        $scope.timeload();
        $scope.backtodetail = true;
        $scope.validateguestdata = true;
        $scope.validateservicedatapament = false;
        $scope.validatedata = true;
        $scope.validateservicedata = false;
        $("#trvl").addClass("active")
        $("#bill").removeClass("active");
        $("#serbill").removeClass("active");

        $("#serv").removeClass("active");
        
        $window.scrollTo(0, 0);
    }

    $scope.travellerToLoginPage = function() {
        $scope.timeload();
        $scope.backtodetail = false;
        $scope.backtotraveler = false;
        
        $scope.validatedata = false;
        $("#trvl").removeClass("active");
        $scope.validateguestdata = false;
        $scope.validateservicedatapament = false;
        $scope.validateservicedata = false;
        $window.scrollTo(0, 0);
    }
    $scope.loginAndContinue = function() {
        $scope.timeload();
        $("#trvl").addClass("active");
        $scope.validatedata = true;
        $scope.validateguestdata = true;
        $window.scrollTo(0, 0);
    }

    $scope.seatToTraveller = function() {
        $("#trvl").addClass("active");
        $("#serv").removeClass("active");
        $scope.validatedata = true;
        $scope.validateservicedata = false;
        $scope.validateservicedatapament = false;
        $window.scrollTo(0, 0);
        $scope.validateguestdata = true;
$scope.backtotraveler=true;


    }

    getTotSeat = function(){
    $scope.totseat = 0;
    for(var mls=0;mls<$scope.seatData.length;mls++){
        for(var ml=0;ml<$scope.seatData[mls].travelerDetails.length;ml++){
           $scope.totseat = Number($scope.seatData[mls].travelerDetails[ml].price) + $scope.totseat;
        }
    }
}

    $scope.ssrDetailsBind = function() {
        var inboundkey;
        var outboundkey;
        $scope.ssrFunction=false;
        $scope.seatFunction=true;
        $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrtotalData'));
        /* baggage details */
        $scope.flightRepriceData = $scope.flightBook;
        $scope.twosegments = $scope.flightRepriceData.selectedResult.flightSearchResults[0].segGrpList;
        $scope.routesdata = [];
        for (var s = 0; s < $scope.twosegments.length; s++) {
            $scope.routesdata.push($scope.twosegments[s].segList[0].routes);
        }
        $scope.outboundbaggagedata = [];
        $scope.inboundbaggagedata = [];
        $scope.totalboundmealsdata = [];
        $scope.totalboundseatdata = [];
        $scope.inboundmealsdata = [];
        $scope.mealDetails = [];

        // End Static data
        $scope.mealsData = [];
        $scope.mealsDetails = [];
        $scope.baggageDetails = [];
        $scope.seatDetails = [];

        $scope.outboundroutesdata = [];
        
        for (var ts = 0; ts < $scope.routesdata.length; ts++) {
            key = $scope.routesdata[ts][0] + '-' + $scope.routesdata[ts][$scope.routesdata[ts].length - 1];
            $scope.outbounddata = 0;
            if ($scope.outbounddata == ts) {
                if ($scope.ssrTotalData.data.data[key] != undefined) {
                    $scope.outboundsegemtData = $scope.ssrTotalData.data.data[key];
                } 
                     if ($scope.outboundsegemtData !=undefined && $scope.outboundsegemtData.baggageSSRs.length != 0) {
                        outboundkey = key;
                        // for (var out = 0; out < $scope.outboundsegemtData.baggageSSRs.length; out++) {
                        //     $scope.outboundsegemtData.baggageSSRs[out].baggageList = ($scope.outboundsegemtData.baggageSSRs[out].baggageList || '');
                        //     $scope.outboundsegemtData.baggageSSRs[out].baggageList = $scope.outboundsegemtData.ssrMap[$scope.outboundsegemtData.baggageSSRs[out].ssrCode];
                        // }
                        $scope.outboundbaggagedata.push({key : key,outboundbaggage:$scope.outboundsegemtData.baggageSSRs});

                    }
                } else {
                    //   $scope.inboundsegemtData= $scope.ssrTotalData.data.data[key];
                    if ($scope.ssrTotalData.data.data[key] != undefined) {
                        $scope.inboundsegemtData = $scope.ssrTotalData.data.data[key];
                    } 
                    if ($scope.inboundsegemtData != undefined &&  $scope.inboundsegemtData.baggageSSRs.length != 0) {
                        inboundkey = key;
                        // for (var out = 0; out < $scope.inboundsegemtData.baggageSSRs.length; out++) {
                        //     $scope.inboundsegemtData.baggageSSRs[out].baggageList = ($scope.inboundsegemtData.baggageSSRs[out].baggageList || '');
                        //     $scope.inboundsegemtData.baggageSSRs[out].baggageList = $scope.inboundsegemtData.ssrMap[$scope.inboundsegemtData.baggageSSRs[out].ssrCode];
                        // }
                        $scope.inboundbaggagedata.push({key : key,outboundbaggage:$scope.inboundsegemtData.baggageSSRs});

                    }
                }
            for (var j = 0; j <= $scope.routesdata[ts].length - 2; j++) {
                mealKey = $scope.routesdata[ts][j] + '-' + $scope.routesdata[ts][j + 1];
                $scope.outbounddata = 0;
                if ($scope.outbounddata == ts) {
                        if ($scope.ssrTotalData.data.data[mealKey] != undefined) {
                            var outboundmeals = $scope.ssrTotalData.data.data[mealKey];
                            var outboundseat = $scope.ssrTotalData.data.data[mealKey];
                        } else {
                            outboundmeals = $scope.ssrTotalData.data.data[$scope.routesdata[ts][j] + '-' + $scope.routesdata[ts][j + 1]];
                            outboundseat = $scope.ssrTotalData.data.data[$scope.routesdata[ts][j] + '-' + $scope.routesdata[ts][j + 1]];
                        }
                        if (outboundmeals != undefined ){
                        for (var out = 0; out < outboundmeals.mealSSRs.length; out++) {
                            // outboundmeals.mealSSRs[out].totqnty = 0;
                            outboundmeals.mealSSRs[out].totprice = 0;
                            outboundmeals.mealSSRs[out].ssrData = [];
                            outboundmeals.mealSSRs[out].ssrData = outboundmeals.ssrMap[outboundmeals.mealSSRs[out].ssrCode];
                            outboundmeals.mealSSRs[out].ssrcodeList = outboundmeals.ssrMap[outboundmeals.mealSSRs[out].ssrCode];

                        }
                        if (outboundmeals != undefined && outboundmeals.mealSSRs.length != 0) {
                            $scope.totalboundmealsdata.push({ key: mealKey, boundlist: outboundmeals.mealSSRs,mealmultiple: outboundmeals.multiMealsEnabled ,boundtype: "outbound" });
                        }
                        }

                        if (outboundseat != undefined && outboundseat.seatRows.length != 0) {
                            $scope.totalboundseatdata.push({ key: mealKey,passengerkey:outboundseat.seats, boundlist: outboundseat.seatRows, boundtype: "outbound" });
                        }
                    // } else {
                    // }
                } else {
                    if ($scope.ssrTotalData.data.data[mealKey] != undefined) {
                        var inboundmeals = $scope.ssrTotalData.data.data[mealKey];
                        var inboundseat = $scope.ssrTotalData.data.data[mealKey];
                    } else {
                        inboundmeals = $scope.ssrTotalData.data.data[$scope.routesdata[ts][j] + '-' + $scope.routesdata[ts][j + 1]];
                        inboundseat = $scope.ssrTotalData.data.data[$scope.routesdata[ts][j] + '-' + $scope.routesdata[ts][j + 1]];
                    }
                    if (inboundmeals != undefined ){
                    for (var out = 0; out < inboundmeals.mealSSRs.length; out++) {
                        // inboundmeals.mealSSRs[out].totqnty = 0;
                        inboundmeals.mealSSRs[out].totprice = 0;
                        inboundmeals.mealSSRs[out].ssrData = [];
                        inboundmeals.mealSSRs[out].ssrData = inboundmeals.ssrMap[inboundmeals.mealSSRs[out].ssrCode];
                        inboundmeals.mealSSRs[out].ssrcodeList = inboundmeals.ssrMap[inboundmeals.mealSSRs[out].ssrCode];
                    }
                    if (inboundmeals.mealSSRs.length != 0) {
                        $scope.totalboundmealsdata.push({ key: mealKey, boundlist: inboundmeals.mealSSRs,mealmultiple: inboundmeals.multiMealsEnabled , boundtype: "inbound" });
                    }
                }

                    if (inboundseat != undefined && inboundseat.seatRows.length != 0) {

                        $scope.totalboundseatdata.push({ key: mealKey,passengerkey:inboundseat.seats, boundlist: inboundseat.seatRows, boundtype: "inbound" });
                    }

                }
            }
        }

        $scope.adultdataSplit = {};
        $scope.totalBaggage = [];
        adultOutBaggageTypeList = [];
        $scope.adultOutBaggageFareList = [];

        adultInBaggageTypeList=[];
        childInBaggageTypeList=[]
        childOutBaggageTypeList = [];
        $scope.childOutBaggageFareList = [];
        infantBaggageTypeList = [];
        $scope.infantBaggageFareList = [];
        $scope.adultInBaggageFareList=[];
        $scope.childInBaggageFareList=[];
        var count = 0;
        var childprice = 0;
        $scope.outboundbaggagedata.forEach(prebaggageDetails => {
            prebaggageDetails.outboundbaggage.forEach(baggageDetails => {

            //Adult pax  bagage  details
            if (baggageDetails.paxType === 'ADT' || baggageDetails.paxType == null) {

                // if (!adultOutBaggageTypeList.includes(baggageDetails.ssrCode)) {
                    if (count == 0) {
                        $scope.adultSelectedSsrCode = baggageDetails.ssrCode;
                        $scope.adultSelectedSsrAmount = baggageDetails.price;
                    }

                    adultOutBaggageTypeList.push(baggageDetails.ssrCode);
                    $scope.adultOutBaggageFareList.push({
                        ssrCode: baggageDetails.ssrCode,
                        price: baggageDetails.price,
                        key:prebaggageDetails.key,
                        ssrdetails:baggageDetails
                    });
                // }
            }
            //Child pax  bagage  details
            else if (baggageDetails.paxType === 'CNN') {

                // if (!childOutBaggageTypeList.includes(baggageDetails.ssrCode)) {
                    if (childprice == 0) {
                        $scope.childSelectedSsrCode = baggageDetails.ssrCode;
                        $scope.childSelectedSsrAmount = baggageDetails.price;
                    }

                    childOutBaggageTypeList.push(baggageDetails.ssrCode);
                    $scope.childOutBaggageFareList.push({
                        ssrCode: baggageDetails.ssrCode,
                        price: baggageDetails.price,
                        key:prebaggageDetails.key,
                        ssrdetails:baggageDetails
                    });
                // }
            }
        });
    });
    $scope.inboundbaggagedata.forEach(prebaggageDetails => {
        prebaggageDetails.outboundbaggage.forEach(baggageDetails => {
            //Adult pax  bagage  details
            if (baggageDetails.paxType === 'ADT' || baggageDetails.paxType == null) {

                // if (!adultInBaggageTypeList.includes(baggageDetails.ssrCode)) {
                    if (count == 0) {
                        $scope.adultSelectedSsrCode = baggageDetails.ssrCode;
                        $scope.adultSelectedSsrAmount = baggageDetails.price;
                    }

                    adultInBaggageTypeList.push(baggageDetails.ssrCode);
                    $scope.adultInBaggageFareList.push({
                        ssrCode: baggageDetails.ssrCode,
                        price: baggageDetails.price,
                        key:prebaggageDetails.key,
                        ssrdetails:baggageDetails
                    });
                // }

            }

            //Child pax  bagage  details
            else if (baggageDetails.paxType === 'CNN') {

                // if (!childInBaggageTypeList.includes(baggageDetails.ssrCode)) {
                    if (childprice == 0) {
                        $scope.childSelectedSsrCode = baggageDetails.ssrCode;
                        $scope.childSelectedSsrAmount = baggageDetails.price;
                    }

                    childInBaggageTypeList.push(baggageDetails.ssrCode);
                    $scope.childInBaggageFareList.push({
                        ssrCode: baggageDetails.ssrCode,
                        price: baggageDetails.price,
                        key:prebaggageDetails.key,
                        ssrdetails:baggageDetails
                    });
                // }

            }
            //infant pax  bagage  details
            else if (baggageDetails.paxType === 'INF') {
                // $scope.infantBaggageFareList.push({
                //     ssrCode: { ssrDetails: 'No Bag' },
                // if (!infantBaggageTypeList.includes(baggageDetails.ssrCode)) {
                    infantBaggageTypeList.push(baggageDetails.ssrCode);
                    $scope.infantBaggageFareList.push({
                        ssrCode:baggageDetails.ssrCode,
                        price: baggageDetails.price,
                        key:prebaggageDetails.key,
                        ssrdetails:baggageDetails
                    });
                // }
            }
            count++;
        });
    });
    var pasangerkeylist = $scope.flightRepriceData.selectedResult.flightSearchResults[0].fareslst;
    var adtKeyList = []
    var cnnKeyList = [];
    var psKeyList = [];
    for (var pskl = 0; pskl < pasangerkeylist.length; pskl++) {
        for (var keyl = 0; keyl < pasangerkeylist[pskl].travellerRefNo.length; keyl++) {
            if(pasangerkeylist[pskl].passengerType == 'ADT'){
                psKeyList.push(pasangerkeylist[pskl].travellerRefNo[keyl]);
            }
            if(pasangerkeylist[pskl].passengerType == 'CNN'){
                psKeyList.push(pasangerkeylist[pskl].travellerRefNo[keyl]);
            }
        }
    }
    for (var t = 0; t < $scope.travelerstotallcount.length; t++) {
    if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') {
        // for (var keyl = 0; keyl < psKeyList.length; keyl++) {
                $scope.travelerstotallcount[t].travellerRefRPH = ($scope.travelerstotallcount[t].travellerRefRPH || '');
                $scope.travelerstotallcount[t].travellerRefRPH = psKeyList[t].rph;
            // } 
        // }
        // if ($scope.travelerstotallcount[t].personaldata.trvtype == 'CNN') {
        // for (var keyl = 0; keyl < psKeyList.length; keyl++) {
        //         $scope.travelerstotallcount[t].travellerRefRPH = ($scope.travelerstotallcount[t].travellerRefRPH || '');
        //         $scope.travelerstotallcount[t].travellerRefRPH = cnnKeyList.travellerRefNo[keyl].rph;
        //     } 
        }
    }
    $scope.loopTot = 0
    $scope.selectedBagLst = JSON.parse(sessionStorage.getItem('SelectedBagLst'));
    if($scope.selectedBagLst != undefined){
        $scope.baggageData = JSON.parse(angular.toJson($scope.selectedBagLst.bagSelectedLst));
        $scope.baggagedataList = $scope.selectedBagLst.bagRQ;
        $scope.loopTot =$scope.selectedBagLst.totPrice;
        $scope.fngrandtotal = $scope.selectedBagLst.totgrand;

    }else{
        $scope.baggageData = [];
        var totTravel = 0;
        if ($scope.adultOutBaggageFareList.length > 0 || $scope.adultInBaggageFareList.length > 0) {
            for (var t = 0; t < $scope.travelerstotallcount.length; t++) {
                if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') {
                    $scope.baggageData[t] = {};
                    $scope.baggageData[t].travellerBaggages = [];
                    if ($scope.travelerstotallcount[t].personaldata.trvtype == 'ADT' || $scope.travelerstotallcount[t].personaldata.trvtype == 'CNN') {
                        totTravel += 1;
                        for (var r = 1; r < totTravel + 1; r++) {
                            if ($scope.travelerstotallcount[t].personaldata.trvtype == 'ADT') {
                                $scope.baggageData[t].travellerRefRPH = 'A' + r;
                                // $scope.travelerstotallcount[t].travellerRefRPH = ($scope.travelerstotallcount[t].travellerRefRPH || '');
                                // $scope.travelerstotallcount[t].travellerRefRPH = 'A' + r;
                            } else {
                                $scope.baggageData[t].travellerRefRPH = 'C' + r;
                                // $scope.travelerstotallcount[t].travellerRefRPH = ($scope.travelerstotallcount[t].travellerRefRPH || '');
                                // $scope.travelerstotallcount[t].travellerRefRPH = 'C' + r;
                            }
                        }
                    }
                    for (var s = 0; s < $scope.searchFlightObj.srchType; s++) {

                        if (s == 0) {
                            $scope.baggageData[t].profileid = $scope.travelerstotallcount[t].profileid;
                            $scope.baggageData[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.baggageData[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.baggageData[t].passengerType = $scope.travelerstotallcount[t].personaldata.trvtype;
                            $scope.baggageData[t].travellerBaggages[s] = {};
                            $scope.baggageData[t].travellerBaggages[s].travelType = 'outbound';
                            for(var sbl=0;sbl<$scope.adultOutBaggageFareList.length;sbl++){
                            if($scope.travelerstotallcount[t].personaldata.trvtype == 'ADT' && 
                            $scope.travelerstotallcount[t].travellerRefRPH == $scope.adultOutBaggageFareList[sbl].ssrdetails.passengerKey){
                                $scope.baggageData[t].travellerBaggages[s].baggagelist = ($scope.baggageData[t].travellerBaggages[s].baggagelist || []);
                                $scope.baggageData[t].travellerBaggages[s].baggagelist.push($scope.adultOutBaggageFareList[sbl]);
                               if($scope.adultOutBaggageFareList[sbl].price == '0'){
                                    $scope.baggageData[t].travellerBaggages[s].selectedBaggage = $scope.adultOutBaggageFareList[sbl];
                                }
                                }
                            }
                                for(var sbl=0;sbl<$scope.childOutBaggageFareList.length;sbl++){
                            if($scope.travelerstotallcount[t].personaldata.trvtype == 'CNN' && 
                            $scope.travelerstotallcount[t].travellerRefRPH == $scope.childOutBaggageFareList[sbl].ssrdetails.passengerKey){
                                $scope.baggageData[t].travellerBaggages[s].baggagelist = ($scope.baggageData[t].travellerBaggages[s].baggagelist || []);
                                $scope.baggageData[t].travellerBaggages[s].baggagelist.push($scope.childOutBaggageFareList[sbl]);
                                if($scope.childOutBaggageFareList[sbl].price == '0'){
                                        $scope.baggageData[t].travellerBaggages[s].selectedBaggage = $scope.childOutBaggageFareList[sbl];
                                    }
                                    }
                                }
                            
                         } else {
                            $scope.baggageData[t].profileid = $scope.travelerstotallcount[t].profileid;
                            // $scope.baggageData[t].key = inboundkey;
                            $scope.baggageData[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.baggageData[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.baggageData[t].passengerType = $scope.travelerstotallcount[t].personaldata.trvtype;
                            $scope.baggageData[t].travellerBaggages[s] = {};
                            $scope.baggageData[t].travellerBaggages[s].travelType = 'inbound';
                            
                                for(var sbl=0;sbl<$scope.adultInBaggageFareList.length;sbl++){
                            if($scope.travelerstotallcount[t].personaldata.trvtype == 'ADT' && 
                            $scope.travelerstotallcount[t].travellerRefRPH == $scope.adultInBaggageFareList[sbl].ssrdetails.passengerKey){
                                $scope.baggageData[t].travellerBaggages[s].baggagelist = ($scope.baggageData[t].travellerBaggages[s].baggagelist || []);
                                $scope.baggageData[t].travellerBaggages[s].baggagelist.push($scope.adultInBaggageFareList[sbl]);
                                if($scope.adultInBaggageFareList[sbl].price == '0'){
                                        $scope.baggageData[t].travellerBaggages[s].selectedBaggage = $scope.adultInBaggageFareList[sbl];
                                        $scope.bagChange()
                                    }
                                    }
                                }
                                    for(var sbl=0;sbl<$scope.childInBaggageFareList.length;sbl++){
                                if($scope.travelerstotallcount[t].personaldata.trvtype == 'CNN' && 
                                $scope.travelerstotallcount[t].travellerRefRPH == $scope.childInBaggageFareList[sbl].ssrdetails.passengerKey){
                                    $scope.baggageData[t].travellerBaggages[s].baggagelist = ($scope.baggageData[t].travellerBaggages[s].baggagelist || []);
                                    $scope.baggageData[t].travellerBaggages[s].baggagelist.push($scope.childInBaggageFareList[sbl]);
                                    if($scope.childInBaggageFareList[sbl].price == '0'){
                                            $scope.baggageData[t].travellerBaggages[s].selectedBaggage = $scope.childInBaggageFareList[sbl];
                                            $scope.bagChange()
                                        }
                                        }
                                    }
                        }


                    }
                }
            }

        }
    }
        $scope.selectedMealLst = JSON.parse(sessionStorage.getItem('SelectedMealLst'));

        if($scope.selectedMealLst != undefined){
            $scope.mealsData = $scope.selectedMealLst.mealSelectedLst;
            $scope.mealsselectedlist = $scope.selectedMealLst.mealRQ;
            getTotMeals();
            $scope.totMeals =$scope.selectedMealLst.totPrice;
            $scope.fngrandtotal = $scope.selectedMealLst.totgrand;

        }else{
        if ($scope.totalboundmealsdata.length > 0) {
            $scope.mealsData = [];
            for (var b = 0; b < $scope.totalboundmealsdata.length; b++) {
                $scope.mealsData[b] = {};
                $scope.mealsData[b].travelerDetails = [];
                for (var t = 0; t < $scope.travelerstotallcount.length; t++) {
                    if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') {

                    $scope.mealsData[b].travelerDetails[t] = {};

                    if ($scope.totalboundmealsdata[b].boundtype == "outbound") {
                        $scope.mealsData[b].travelType = "outbound";
                        $scope.mealsData[b].key = $scope.totalboundmealsdata[b].key;
                        $scope.mealsData[b].mealmultiple = $scope.totalboundmealsdata[b].mealmultiple;
                        $scope.mealsData[b].travelerDetails[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                        $scope.mealsData[b].travelerDetails[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                        $scope.mealsData[b].travelerDetails[t].price = 0;
                        $scope.mealsData[b].travelerDetails[t].quantity = 0;
                        $scope.mealsData[b].travelerDetails[t].prf = $scope.outsegpref;
                        for (var bt = 0; bt < $scope.totalboundmealsdata[b].boundlist.length; bt++) {
                            if ($scope.travelerstotallcount[t].travellerRefRPH == $scope.totalboundmealsdata[b].boundlist[bt].passengerKey) {
                                // $scope.totalboundmealsdata[b].boundlist[bt].paxIndex = t;
                                $scope.mealsData[b].travelerDetails[t].mealslist = ($scope.mealsData[b].travelerDetails[t].mealslist || []);
                                $scope.mealsData[b].travelerDetails[t].mealslist.push($scope.totalboundmealsdata[b].boundlist[bt]);
                            }
                        }
                    } else {
                        $scope.mealsData[b].key = $scope.totalboundmealsdata[b].key;
                        $scope.mealsData[b].travelType = "inbound";
                        $scope.mealsData[b].mealmultiple = $scope.totalboundmealsdata[b].mealmultiple;
                        $scope.mealsData[b].travelerDetails[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                        $scope.mealsData[b].travelerDetails[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                        $scope.mealsData[b].travelerDetails[t].price = 0;
                        $scope.mealsData[b].travelerDetails[t].prf = $scope.insegpref;
                        $scope.mealsData[b].travelerDetails[t].quantity = 0;
                        for (var bt = 0; bt < $scope.totalboundmealsdata[b].boundlist.length; bt++) {
                            if ($scope.travelerstotallcount[t].travellerRefRPH == $scope.totalboundmealsdata[b].boundlist[bt].passengerKey) {
                                // $scope.totalboundmealsdata[b].boundlist[bt].paxIndex = t;
                                $scope.mealsData[b].travelerDetails[t].mealslist = ($scope.mealsData[b].travelerDetails[t].mealslist || []);
                                $scope.mealsData[b].travelerDetails[t].mealslist.push($scope.totalboundmealsdata[b].boundlist[bt]);
                            }
                        }
                    }
                }
            }
            }

        }
    }
    
    $scope.SelectedSeatLst = JSON.parse(sessionStorage.getItem('SelectedSeatLst'));

    if($scope.SelectedSeatLst != undefined){
        $scope.seatData = $scope.SelectedSeatLst.seatSelectedLst;
        $scope.seats = $scope.SelectedSeatLst.seatRQ;
        $scope.seatRowDetails = [];
        $scope.seatRowDetails = $scope.seatData[0];
        getTotSeat();
        $scope.totseat =$scope.SelectedSeatLst.totPrice;
        $scope.fngrandtotal = $scope.SelectedSeatLst.totgrand;
        $scope.onseatchange($scope.seatData[0].travelerDetails[0].seatNumber, 0,0);
    }else{
        if ($scope.totalboundseatdata.length > 0) {
            // $('#'+Seat.seatNumber).prop("checked", false);
            // $timeout(function(){
            //     $('#seatrdio_0_0').prop("checked", true);
            // },10)
            // 
            $scope.seatData = [];
            for (var b = 0; b < $scope.totalboundseatdata.length; b++) {
                $scope.seatData[b] = {};
                $scope.seatData[b].travelerDetails = [];
                for (var t = 0; t < $scope.travelerstotallcount.length; t++) {
                    if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') {

                        $scope.seatData[b].travelerDetails[t] = {};

                        if ($scope.totalboundseatdata[b].boundtype == "outbound") {
                            $scope.seatData[b].travelType = "outbound";
                            $scope.seatData[b].key = $scope.totalboundseatdata[b].key;
                            $scope.seatData[b].travelerDetails[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.seatData[b].travelerDetails[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.seatData[b].travelerDetails[t].price = 0;
                            $scope.seatData[b].travelerDetails[t].selectedseat1 = 0;
                            for (var bt = 0; bt < $scope.totalboundseatdata[b].passengerkey.length; bt++) {
                                if ($scope.travelerstotallcount[t].travellerRefRPH == $scope.totalboundseatdata[b].passengerkey[bt].passengerKey) {
                                    $scope.seatData[b].travelerDetails[t].seatsslist = ($scope.seatData[b].travelerDetails[t].seatsslist || []);
                                    $scope.seatData[b].travelerDetails[t].seatsslist.push($scope.totalboundseatdata[b].passengerkey[bt]);
                                }
                            }
                            $scope.seatData[b].seatlist = $scope.totalboundseatdata[b].boundlist;
                            
                        } else {
                            $scope.seatData[b].key = $scope.totalboundseatdata[b].key;
                            $scope.seatData[b].travelType = "inbound";
                            $scope.seatData[b].travelerDetails[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.seatData[b].travelerDetails[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.seatData[b].travelerDetails[t].price = 0;
                            $scope.seatData[b].travelerDetails[t].selectedseat1 = 0;
                            for (var bt = 0; bt < $scope.totalboundseatdata[b].passengerkey.length; bt++) {
                                if ($scope.travelerstotallcount[t].travellerRefRPH == $scope.totalboundseatdata[b].passengerkey[bt].passengerKey) {
                                    $scope.seatData[b].travelerDetails[t].seatsslist = ($scope.seatData[b].travelerDetails[t].seatsslist || []);
                                    $scope.seatData[b].travelerDetails[t].seatsslist.push($scope.totalboundseatdata[b].passengerkey[bt]);
                                }
                            }                            
                            $scope.seatData[b].seatlist = $scope.totalboundseatdata[b].boundlist;

                        }
                    }
                }
            }
            $scope.seatRowDetails = [];
            $scope.seatRowDetails = $scope.seatData[0];
            $scope.travellerPassengerkey = $scope.seatData[0].travelerDetails[0].seatsslist[0].passengerKey;
        }
    }
    }

    $scope.seatsegment = "";
    $scope.trlseatlist = {};

    seatcheckfun = function(checkseat){
        // var ele = $(this).closest('.seat').find(':checkbox');
        // if ($(':checked').length) {
        //     ele.prop('checked', false);
        //     $(this).removeClass('active');
        // } else {
        //     ele.prop('checked', true);
        //     $(this).addClass('active');
        // }
        $scope.seatRowDetails.seatlist.forEach(singleSeat => {
            singleSeat.seating[0].seatColumn.forEach(Seat => {
                $('#'+Seat.seatNumber).prop("checked", false);
                $(this).removeClass('active');
            });
            singleSeat.seating[1].seatColumn.forEach(Seat => {
                $('#'+Seat.seatNumber).prop("checked", false);
                $(this).addClass('active');
            });
        });
        $timeout(function(){
            $('#'+checkseat).prop("checked", true);
        },10)
       
    }

    $scope.onseatchange = function(seat,seatNumber, index,trvlindx) {
        $scope.trlseatlist = seat.seatsslist[0];
        $scope.travellerPassengerkey = seat.seatsslist[0].passengerKey;
        $scope.seatRowDetails = $scope.seatData[index];
        $scope.seatsegment = $scope.seatData[index].key;
        $scope.seatTrvlrIndx = trvlindx;
        $scope.parentIndx = index;
        seatcheckfun(seatNumber);

    }
    $scope.selectedSSRBean = [];
    $scope.seats = [];
 
    $scope.onhoverSeat = function(seatData, trldtls,event) {
// console.log(seatData);
        $scope.reprice= $scope.flightBookSession;
         var provider=$scope.reprice.data.selectedResult.flightSearchResults[0].provider;
         if(provider=='JZR'){
            var passengerKey=$scope.travellerPassengerkey  
           
for(var i=0; i<trldtls.travelerDetails.length; i++){
    var seatParticularPassengerKey=trldtls.travelerDetails[i].seatsslist[0].passengerKey;
            if(passengerKey==seatParticularPassengerKey){
                $scope.seatToltip=trldtls.travelerDetails[i].seatsslist[0].groupWisePrice[seatData.groupId].priceAmount;
            }

        }
         }else{
            $scope.seatToltip = seatData.seatPrice;
         }
        }

    $scope.onselectSeat = function(seatData, trldtls,event) {
        $scope.fngrandtotal = 0;
        $scope.reprice= $scope.flightBookSession;
         var provider=$scope.reprice.data.selectedResult.flightSearchResults[0].provider;
         if(provider=='JZR'){
            var passengerKey=$scope.travellerPassengerkey  
           
for(var i=0; i<trldtls.travelerDetails.length; i++){
    var seatParticularPassengerKey=trldtls.travelerDetails[i].seatsslist[0].passengerKey;
            if(passengerKey==seatParticularPassengerKey){
                var seatpricJjr=trldtls.travelerDetails[i].seatsslist[0].groupWisePrice[seatData.groupId].priceAmount;
                seatData.seatPrice=seatpricJjr;
            }

        }
         }
        seatcheckfun(seatData.seatNumber);
        // angular.element(event.currentTarget).addClass('ng-not-empty');
        // var sampleseatdetls = $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatsslist[0];
        // event.currentTarget
        $scope.trlseatlist.selectedSeat = seatData;
        var data = {
                key: $scope.seatsegment,
                paxIndex: $scope.seatTrvlrIndx,
                seat: $scope.trlseatlist,
                // {
                //     count: sampleseatdetls.count,
                //     currencyCode: sampleseatdetls.currencyCode,
                //     passengerKey: sampleseatdetls.passengerKey,
                //     paxType: sampleseatdetls.paxType,
                //     price: sampleseatdetls.price,
                //     serviceCode: sampleseatdetls.serviceCode,
                //     ssrCode: sampleseatdetls.ssrCode,
                //     ssrData: sampleseatdetls.ssrData,
                //     ssrKey: sampleseatdetls.ssrKey,
                //     paxIndex: $scope.seatTrvlrIndx,
                //     selectedSeat: seatData
                // },
            }
            // var groups = {};
            // var seatgroup = seatData.seatNumber;

        //set price for all traveler
        $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].price = seatData.seatPrice;
        $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatNumber = ($scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatNumber || 0)
        $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatNumber = seatData.seatNumber;

        var keepGoing = true;
        if ($scope.seats.length > 0) {
            $scope.seats.forEach(function(seatelement,idx,obj) {
                if (seatelement.paxIndex == $scope.seatTrvlrIndx && $scope.seatsegment == seatelement.key) {
                        obj.splice(idx, 1);
                    }
            });

            $scope.seats.push(data);
        } else {
            $scope.seats.push(data);
        }
        if ($scope.seats.length > 0) {
            for(var stele=0; stele<$scope.seats.length;stele++)
            // $scope.seats.forEach(function(stelement,idx,obj) 
            {
                if(keepGoing){
                 if($scope.seats[stele].seat.selectedSeat.seatNumber == seatData.seatNumber && $scope.seatsegment == $scope.seats[stele].key && $scope.seats[stele].paxIndex != $scope.seatTrvlrIndx){
                        // element.seat.selectedSeat.seatNumber = '-';
                        Lobibox.alert('error',
                        {
                            msg: "Seat already selected, Please try another"
                        });
                        // console.log($scope.seats);
                        $scope.seats.pop();
                        // console.log($scope.seats);
                        $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].price = 0;
                        $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatNumber = ($scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatNumber || 0)
                        $scope.seatData[$scope.parentIndx].travelerDetails[$scope.seatTrvlrIndx].seatNumber = '-';            
                        keepGoing = false;
                        $timeout(function(){
                            $('#'+seatData.seatNumber).prop("checked", false);
                        },20)
                        
                        $(this).addClass('active');
                    }
                }
            }
            // );
        }
        keepGoing = true; 

        //get total seat price
        getTotSeat();
    }

    $("#one-tab").removeClass("blinking-inact");
    $("#two-tab").addClass("blinking-inact");
    $("#three-tab").addClass("blinking-inact");

    $scope.ffCall = function(val,index,prodct){
        if(val == 'one'){
            $("."+index + '_' +prodct +'_'+ val).addClass('active');
            $("."+index + '_'+prodct +'_' +val).addClass('show');
            $("."+index +'_'+prodct + "_two").removeClass('active');
            $("."+index +'_'+prodct + "_two").removeClass('show');
            $("."+index +'_'+prodct + "_three").removeClass('active');
            $("."+index +'_'+prodct + "_three").removeClass('show');
            $("#one-tab").removeClass("blinking-inact");
            $("#two-tab").addClass("blinking-inact");
            $("#three-tab").addClass("blinking-inact");
        }else if(val == 'two'){
            $("."+index + '_'+prodct +'_' +val).addClass('active');
            $("."+index + '_'+prodct +'_' +val).addClass('show');
            $("."+index +'_'+prodct + "_one").removeClass('active');
            $("."+index +'_'+prodct + "_one").removeClass('show');
            $("."+index +'_'+prodct + "_three").removeClass('active');
            $("."+index +'_'+prodct + "_three").removeClass('show');
            $("#two-tab").removeClass("blinking-inact");
            $("#one-tab").addClass("blinking-inact");
            $("#three-tab").addClass("blinking-inact");
        }else{
            $("."+index + '_'+prodct +'_' +val).addClass('active');
            $("."+index + '_'+prodct +'_' +val).addClass('show');
            $("."+index +'_'+prodct + "_two").removeClass('active');
            $("."+index +'_'+prodct + "_two").removeClass('show');
            $("."+index  +'_'+prodct + "_one").removeClass('active');
            $("."+index  +'_'+prodct + "_one").removeClass('show');
            $("#three-tab").removeClass("blinking-inact");
            $("#two-tab").addClass("blinking-inact");
            $("#two-tab").addClass("blinking-inact");
        }
    }
    // $scope.seatFunction=false;
    $scope.mealFunction=false;

    $scope.ssrtab =  function(val){
        if(val == 1){
         $scope.mealFunction=false;
         $scope.seatFunction = true
         $scope.ssrFunction=false;
         $("#mealTabActivate").removeClass('active');
         $("#seatTabActivate").removeClass('active');
         $("#baggageTabActivate").addClass('active');
            
            $("#baggage").addClass('active');
            $("#baggage").addClass('show');
            $("#seat").removeClass('active');
            $("#seat").removeClass('show');
            $("#meal").removeClass('active');
            $("#meal").removeClass('show');
        }else if(val == 2){
            $scope.seatFunction=false;
            $scope.ssrFunction=false;
            $scope.mealFunction=true;
            $("#mealTabActivate").removeClass('active');
            $("#seatTabActivate").addClass('active');

            $("#baggageTabActivate").removeClass('active');
            
            $("#seat").addClass('active');
            $("#seat").addClass('show');
            $("#baggage").removeClass('active');
            $("#baggage").removeClass('show');
            $("#meal").removeClass('active');
            $("#meal").removeClass('show');
            $("html,body").scrollTop(10);
        }else{
            $scope.ssrFunction=true;
            $scope.seatFunction=false;
            $scope.mealFunction=false;
            $("#seatTabActivate").removeClass('active');
            $("#mealTabActivate").addClass('active');
            $("#meal").addClass('active');
            $("#meal").addClass('show');
            $("#baggage").removeClass('active');
            $("#baggage").removeClass('show');
            $("#seat").removeClass('active');
            $("#seat").removeClass('show');
            $("html,body").scrollTop(10);
        }
    }

    $scope.totseat = 0;
    $scope.totMeals = 0;

    $scope.servicesDetails = function(valid) {
        // if ($("#trvl").addClass("active")) {
            $scope.backtodetail=false;
            $scope.backtotraveler=true;
        $scope.seatFunction=true;
            $("#trvl").addClass("active");
        $scope.submitted = true;
        var passengerform=true;
        if (valid) {
            if(passengerform==true){
                $scope.totaladultdata=[];
                $scope.totalchailddata=[];
                $scope.totalInfantdata=[];
                $scope.travelList=$scope.travellersListByRoom;
                for(var k=0; k<$scope.travelList.length; k++){
                for(var m=0; m<$scope.travelList[k].adultTravellersList.length; m++){		
                $scope.totaladultdata.push($scope.travelList[k].adultTravellersList[m]);
                }
                for(var n=0; n<$scope.travelList[k].childTravellersList.length; n++){
                $scope.totalchailddata.push($scope.travelList[k].childTravellersList[n]);
                }
                for(var i=0; i<$scope.travelList[k].infantTravellersList.length; i++){
                    $scope.totalInfantdata.push($scope.travelList[k].infantTravellersList[i]);
                    }
                }
                /* $scope.totaladultdata=$scope.travelList[0].adultTravellersList;
                $scope.totalchailddata=$scope.travelList[0].childTravellersList; */
                $scope.totalpassengers = $scope.totaladultdata.concat($scope.totalchailddata).concat($scope.totalInfantdata);
                // $scope.totalpassengers=$scope.totalpassenger.concat($scope.totalInfantdata);
        //@author Raghava start...
        for(var i=0;i<$scope.totalpassengers.length; i++){
            var count=0;
            var outerFirstName=$scope.totalpassengers[i].personaldata.fname;
        var outerPassport=$scope.totalpassengers[i].personaldata.passportNo;
            for(var j=0;j<$scope.totalpassengers.length; j++){
                var innerFirstName=$scope.totalpassengers[j].personaldata.fname;
                var innerPassport=$scope.totalpassengers[j].personaldata.passportNo;
        if(innerFirstName===outerFirstName||innerPassport===outerPassport){
        count++;
        if(count>1){
            Lobibox.alert('error',
            {
                msg: "Guest details are duplicated."
            });
        
            // alert("duplicate Traveller Found.")
        
            return;
        }
        }
        
        
            }
        }
            }
            if ($('#term_con').is(":checked")) {
                $scope.errorMesg = null;
                $("#serv").addClass("active");
                // $scope.paymentForm = {
                // 	// firstName:"",
                // 	// lastName:"",
                // 	// address:"",
                // 	// emailAddress:"",
                // 	// countryCode:"",
                // 	// city:""
                // };
                $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
                $scope.validateservicedata = true;
                $scope.validatedata = false;

                $scope.travelerstotallcount = [];

                $scope.travelerscount = $scope.travellersListByRoom[0].adultTravellersList;

                $scope.travelerscount1 = $scope.travellersListByRoom[0].childTravellersList;
                $scope.travelerscount2 = $scope.travellersListByRoom[0].infantTravellersList;


                $scope.travelerscount.forEach(function(selectedItem) {
                    /* 		
                    var d = new Date($scope.flightSearchRQ.strDeptDate);
                                var day = d.getDate();
                                var monthIndex = d.getMonth() + 1;
                                var year = d.getFullYear();

                                $scope.checkin = day + '/' + monthIndex + '/' + year; */
                    $scope.travelerstotallcount.push(selectedItem);
                });
                $scope.travelerscount1.forEach(function(selectedItem) {
                    $scope.travelerstotallcount.push(selectedItem);
                });
                $scope.travelerscount2.forEach(function(selectedItem) {
                    $scope.travelerstotallcount.push(selectedItem);
                });

                $scope.outboundroutemapdata = [];
                $scope.inboundroutemapdata = [];
                $scope.likeSeat = 1;
                $scope.likeMeal = 1;
                $scope.adultSelectedSsrCode = "";
                $scope.adultSelectedSsrAmount = 0;

                $scope.childSelectedSsrCode = "";
                $scope.childSelectedSsrAmount = 0;


                $scope.noSsr = true;
                $scope.paymentDel.firstName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.fname;
                $scope.paymentDel.lastName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.lname;
                $scope.paymentDel.emailAddress = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email;
                $scope.paymentDel.countryCode = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname;
                $scope.paymentDel.city = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.city;
           
                $scope.ssrDetailsBind();

               

            } else {
                $scope.errorMesg = "Please agree the terms & condition"
            }
        } else {
            $('html,body').scrollTop(240);
            $scope.errorMesg = "Enter All * (Mandatory) Fields";
            return false;
        }
        $window.scrollTo(0, 0);

        // $scope.seatFunction=false;

        // }
    }

    $scope.paymentForm = {
        firstName: "",
        lastName: "",
        address: "",
        emailAddress: "",
        countryCode: "",
        city: ""
    };

    $scope.paymentDel = {
        firstName: "",
        lastName: "",
        address: "",
        emailAddress: "",
        countryCode: "",
        city: ""
    };
        $scope.billing = function() {
        if ($("#bill").hasClass("active") || $("#serbill").hasClass("active")) {
            $scope.validateservicedata = false;
            $scope.validateservicedatapament = true;
            $scope.validatedata = false;
            $scope.validateguestdata = true;
        }
    }
    $scope.serbilling = function() {
        if ($("#serv").hasClass("active")) {
            
            $("#serbill").removeClass('active');
            $scope.validateguestdata = true;
            $scope.validateservicedata = true;
            $scope.validateservicedatapament = false;
            $scope.validatedata = false;

        }
    }

    $scope.errorMesg = null;
    $scope.proceedTravellerDetails = function(valid) {
        // if ($("#trvl").hasClass("active")) {
            $scope.backtodetail = false;
            $scope.backtotraveler = true;
        $scope.submitted = true;
        var passengerform=true;
        if (valid) {
            $window.scrollTo(0, 0);
           
            if(passengerform==true){
                $scope.totaladultdata=[];
                $scope.totalchailddata=[];
                $scope.totalInfantdata=[];
                $scope.travelList=$scope.travellersListByRoom;
                for(var k=0; k<$scope.travelList.length; k++){
                for(var m=0; m<$scope.travelList[k].adultTravellersList.length; m++){		
                $scope.totaladultdata.push($scope.travelList[k].adultTravellersList[m]);
                }
                for(var n=0; n<$scope.travelList[k].childTravellersList.length; n++){
                $scope.totalchailddata.push($scope.travelList[k].childTravellersList[n]);
                }
                for(var i=0; i<$scope.travelList[k].infantTravellersList.length; i++){
                    $scope.totalInfantdata.push($scope.travelList[k].infantTravellersList[i]);
                    }
                }
                /* $scope.totaladultdata=$scope.travelList[0].adultTravellersList;
                $scope.totalchailddata=$scope.travelList[0].childTravellersList; */
                $scope.totalpassengers = $scope.totaladultdata.concat($scope.totalchailddata).concat($scope.totalInfantdata);
                // $scope.totalpassengers=$scope.totalpassenger.concat($scope.totalInfantdata);
        //@author Raghava start...
        for(var i=0;i<$scope.totalpassengers.length; i++){
            var count=0;
            var outerFirstName=$scope.totalpassengers[i].personaldata.fname;
        var outerPassport=$scope.totalpassengers[i].personaldata.passportNo;
            for(var j=0;j<$scope.totalpassengers.length; j++){
                var innerFirstName=$scope.totalpassengers[j].personaldata.fname;
                var innerPassport=$scope.totalpassengers[j].personaldata.passportNo;
        if(innerFirstName===outerFirstName||innerPassport===outerPassport){
        count++;
        if(count>1){
            Lobibox.alert('error',
            {
                msg: "Guest details are duplicated."
            });
        
            // alert("duplicate Traveller Found.")
        
            return;
        }
        }
        
        
            }
        }
            }
            if ($('#term_con').is(":checked")) {
                $("#bill").addClass("active");
                $scope.validateguestdata = true;
                $scope.validateservicedata = false;
                $scope.validateservicedatapament = true;
                $scope.validatedata = false;
                $scope.errorMesg = null;
                $scope.paymentDel.firstName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.fname;
                $scope.paymentDel.lastName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.lname;
                $scope.paymentDel.emailAddress = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email;
                $scope.paymentDel.countryCode = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname;
                // $scope.paymentDel.city = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.city;
           
                      } else {
                $scope.errorMesg = "Please agree the terms & condition"
            }
        } else {
            $('html,body').scrollTop(240);
            $scope.errorMesg = "Enter All * (Mandatory) Fields"
            return false;
        }
        // }
    }
    pnrcreate = function(booking){
            var token = JSON.parse(localStorage.authentication);
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.data.authToken
            }
        $http.post(ServerService.serverPath + 'flight/pnr/create', booking, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            $scope.artloading = false;
            /* $scope.paymentDetail = $scope.cardDetail; */
            if (response.data.success) {
                // sessionStorage.setItem("bookingResponse", JSON.stringify(response.data.data));

                window.open(response.data.data.hostedPageLink, "_self")
            } else {

                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });

            }


        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
                status.then(function(greeting) {
                pnrcreate(booking);
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }


    $scope.proceedCustomDetails = function(bookingWay, sType, valid, validtravel) {
        $scope.paysubmitted = true;
         if (valid) {
            $scope.baggagedata = false
            $scope.payment = true;
            $scope.pamentbutton = false;
            $scope.likeMeal = 1;
            $scope.travelerstotallcount = [];

            for (var adt = 0; adt < $scope.travellersListByRoom[0].adultTravellersList.length; adt++) {
                var adttrl = $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata;
                $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
                $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.dob =
                    adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                    $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.passExp =
                    adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
            }
            for (var adt = 0; adt < $scope.travellersListByRoom[0].childTravellersList.length; adt++) {
                var adttrl = $scope.travellersListByRoom[0].childTravellersList[adt].personaldata;
                if( $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==undefined ||$scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==''|| $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==null){
                    $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode=null; 
                }else{
                   
                $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
                }

                $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.dob =
                    adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                    $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.passExp =
                    adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
            }
            for (var adt = 0; adt < $scope.travellersListByRoom[0].infantTravellersList.length; adt++) {
                var adttrl = $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata;
                if( $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode==undefined || $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode=='' || $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode==null){
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode=null; 
                }else{
                   
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
                }

               
                $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.dob =
                    adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.passExp =
                    adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;

            }
            $scope.travelerscount = $scope.travellersListByRoom[0].adultTravellersList;

            $scope.travelerscount1 = $scope.travellersListByRoom[0].childTravellersList;
            $scope.travelerscount2 = $scope.travellersListByRoom[0].infantTravellersList;
            $scope.artloading = true;

            $scope.travelerscount.forEach(function(selectedItem) {
                $scope.travelerstotallcount.push(selectedItem);
            });
            $scope.travelerscount1.forEach(function(selectedItem) {
                $scope.travelerstotallcount.push(selectedItem);
            });
            $scope.travelerscount2.forEach(function(selectedItem) {
                $scope.travelerstotallcount.push(selectedItem);
            });
            $scope.travelerstotallcount.forEach(travellerObj => {

                if (Object.prototype.toString.call(travellerObj.personaldata.passExp) === "[object Date]") {
                    var d = new Date(travellerObj.personaldata.passExp);
                    var day = d.getDate();
                    var monthIndex = d.getMonth() + 1;
                    var year = d.getFullYear();

                    travellerObj.personaldata.passExp = day + '/' + monthIndex + '/' + year;
                }


            });
            sessionStorage.setItem("fltTravellerdatalist", JSON.stringify($scope.travelerstotallcount));
            sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom))

            $scope.submitted = true;
            $scope.validateservicedata = false;
            $scope.validatedata = false;
            $scope.validateguestdata = true;
            $scope.validateservicedatapament = true; //false
            $scope.travelerstotallcount = [];
            $scope.travelerstotallcount = JSON.parse(sessionStorage.getItem("fltTravellerdatalist"));
            $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));

            $scope.sessionid = $scope.flightBookSession.sessionId;

            $scope.providerName = $scope.flightBook.selectedResult.flightSearchResults[0].provider;

            $scope.searchtotalldata = $scope.flightBook.selectedResult;
            $scope.bookingReq = new Object();

            $scope.bookingReq.sessionId = $scope.flightBookSession.sessionId;
            if($scope.selectedInsurancePlantotalFare){
            $scope.bookingReq.selectedInsurancePlanId = $scope.selectedInsurancePlan.sessionId;
            }else{
                $scope.bookingReq.selectedInsurancePlan = null;
            }
            $scope.bookingReq.paymentMode = "PAYMENT";
            $scope.bookingReq.paymentParameters = $scope.paymentDel;
            $scope.bookingReq.paymentParameters.successUrl = "paymentRedirector.html";
                $scope.bookingReq.paymentParameters.failureUrl = "paymentRedirector.html";
                $scope.bookingReq.paymentParameters.cancelUrl = "#!/flightReview";
                
                // var tempgrandtot = 0;
                // if($scope.fngrandtotal >0)
                // {
                //     tempgrandtot = $scope.fngrandtotal + $scope.selectedInsurancePlan.totalFare - $scope.discount;
                // }
                // else
                // {
                //     tempgrandtot = $scope.flightBooking.flightSearchResults[0].grndTotal + $scope.selectedInsurancePlan.totalFare + $scope.totalSSR + $scope.loopTot + $scope.totMeals + $scope.totseat - $scope.discount;
                // }

                $scope.travelerstotallcount.forEach(ele => {
                    if(ele.miscdata.mealpref[0].code == null)
                    {
                        ele.miscdata.mealpref = [];
                    }
                    if(ele.miscdata.seatpref[0].code == null)
                    {
                        ele.miscdata.seatpref = [];
                    }
                });
                
                $scope.bookingReq.travellerdatalist = $scope.travelerstotallcount;

                // $scope.bookingReq.paymentParameters.promoCodeDiscount = $scope.discount;
                // $scope.bookingReq.paymentParameters.grandTotal = tempgrandtot;
                // $scope.bookingReq.paymentParameters.promoCode = $scope.promocode;

            $scope.bookingReq = $scope.bookingReq;

            localStorage.setItem('flight/pnr/create', JSON.stringify($scope.bookingReq))
      
            pnrcreate($scope.bookingReq);       
         } else {
            return false;
            $('html,body').scrollTop(440);
            $scope.errMsg = "Enter All * (Mandatory) Fields";
        }
    }

        /* end */
        // console.log($scope.flightSearchResults);
    $scope.getNumber = function(num) {
            return new Array(num);
        }
        /* } */

});


function between(x, min, max) {
    return x >= min && x <= max;
}

    ouaApp.controller('mealinfoContent', function($scope, $uibModalInstance, $rootScope) {
        var bindmealsdetailssub = [];
        bindmealsdetailssub = JSON.parse(sessionStorage.getItem("bindmealsdetails1"));
        $scope.multiMealsEnabled = $rootScope.multiMealsEnabled;
        $scope.bundledFareId = $rootScope.bundledFareId ;
        $scope.bindmealsdetailssub = bindmealsdetailssub;

        if($scope.bundledFareId.bundledFareId != null){
            $scope.multiMealsEnabled =  'false';
        }

        var price = 0;
        var index = 0;
        var mealsselectedlist = []
            // for(var cnt=0; cnt<$scope.bindmealsdetailssub.length; cnt++){
            // 	// document.getElementById("myNumber_"+index).value = 0;
            // 	$scope.bindmealsdetailssub[index].totprice = 0;
            // 	$scope.bindmealsdetailssub[index].totqnty = 0;
            // }
        $scope.up = function(max, meal, index) {
            price = 0;
            index = index;
            var totcount = 0;
            $scope.bindmealsdetailssub.forEach(element => {
                totcount += element.count;
            });
            // if($scope.bundledFareId.bundledFareId != null && totcount > 1){
            //     Lobibox.alert('error',
            //     {
            //         msg: "Only one Meal selection will be Allowed!"
            //     });
            // }
            // else{
                document.getElementById("myNumber_" + index).value = parseInt(document.getElementById("myNumber_" + index).value) + 1;
                if (document.getElementById("myNumber_" + index).value >= parseInt(max)) {
                    document.getElementById("myNumber_" + index).value = max;
                }
                price = Number(meal.price) * Number(document.getElementById("myNumber_" + index).value)
                $scope.bindmealsdetailssub[index].totprice = price;
                $scope.bindmealsdetailssub[index].count = document.getElementById("myNumber_" + index).value;
    
            // }

            // $scope.bindmealsdetailssub[index].selectedMeal = true;
            // if (mealsselectedlist.length > 0) {
            //     mealsselectedlist.forEach(element => {
            //         if (element.index != index) {
            //             mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
            //         }
            //     });
            // } else {
            //     mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
            // }
        }
        $scope.down = function(min, meal, index) {
                price = 0;
                index = index;
                document.getElementById("myNumber_" + index).value = parseInt(document.getElementById("myNumber_" + index).value) - 1;
                if (document.getElementById("myNumber_" + index).value <= parseInt(min)) {
                    document.getElementById("myNumber_" + index).value = min;
                }
                price = Number(meal.price) * Number(document.getElementById("myNumber_" + index).value)
                $scope.bindmealsdetailssub[index].totprice = price;
                $scope.bindmealsdetailssub[index].count = document.getElementById("myNumber_" + index).value;
                // if (Number(document.getElementById("myNumber_" + index).value) > 0) {
                //     // $scope.bindmealsdetailssub[index].selectedMeal = true;
                //     if (mealsselectedlist.length > 0) {
                //         mealsselectedlist.forEach(element => {
                //             if (element.index != index) {
                //                 mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
                //             }
                //         });
                //     } else {
                //         mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
                //     }
                // }
            }
            // $scope.mealsData = MealsData;
            // $scope.mealsData[$rootScope.parentIndex].travelerDetails[$rootScope.index].quantity = 

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
            sessionStorage.setItem("mealsselectedlist", JSON.stringify(mealsselectedlist))
            $uibModalInstance.close($scope.bindmealsdetailssub, mealsselectedlist);
        };
    }).controller('flightTermsController', function($http,ServerService,renderHTMLFactory, $scope, $uibModalInstance, $rootScope) {

        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
             var tokenname = tokenresult.data;
             var headersOptions = {
                    'Content-Type': 'application/json',
                     'Authorization': 'Bearer ' + tokenname.authToken
                 }
            
             $http.get(ServerService.profilePath + 'company/latest/tandc/1', {
                 headers: headersOptions
             }).then(function successCallback(response) {
                 $scope.termsresp = response.data.data.content;
             
             }, function errorCallback(response) {
                 if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                        $scope.flightTermsDetails();
                       });
                 }
             });

        $scope.ok = function() {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    })
    ouaApp.controller('loginTravelController', function($scope, $uibModalInstance, $rootScope) {
        //console.log("fghfg");
            var travelIndexvalue=$rootScope.travelindex
            var whichpassenger=$rootScope.whichTraveller
            if(whichpassenger=='adult'){
                        $scope.allTravellerData=[];
                        $scope.adultIndex=travelIndexvalue;
                                 $scope.allTravellerDataa = JSON.parse(localStorage.getItem('loginTravellerData'));
                                  for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                 if( $scope.allTravellerDataa[t].age>= 12){
                     $scope.allTravellerData.push($scope.allTravellerDataa[t])
                 }
                                 }
                                  if($scope.allTravellerData.length==0) {
                                     $scope.childText=" Traveller Data Not Available"
                                }
                            }
        
                            if(whichpassenger=='child'){
        
        
                                $scope.allTravellerData=[];
                                $scope.childIndex=travelIndexvalue;
                                         $scope.allTravellerDataa = JSON.parse(localStorage.getItem('loginTravellerData'));
                                         for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                         if($scope.allTravellerDataa[t].age >= 2 && $scope.allTravellerDataa[t].age < 12){
                            $scope.allTravellerData.push($scope.allTravellerDataa[t])
                        }
                                         }
                                        if($scope.allTravellerData.length==0) {
                                            $scope.childText="Traveller Data Not Available"
                                        }
                            }
                            if(whichpassenger=='Infant'){
                                $scope.allTravellerData=[];
                                $scope.infantIndex=travelIndexvalue;
                                         $scope.allTravellerDataa = JSON.parse(localStorage.getItem('loginTravellerData'));
                                         for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                        if( $scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age < 2){
                            $scope.allTravellerData.push($scope.allTravellerDataa[t])
                        }
                                         }
                                         if($scope.allTravellerData.length==0) {
                                            $scope.childText=" Traveller Data Not Available"
                                        }
        
                            }
        
                
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        
            /* modelDismiss function */
            $scope.modelDismiss = function() {
                $uibModalInstance.close();
            }
        })