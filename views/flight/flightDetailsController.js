ouaApp.controller('flightDetailsController', function($scope, $http,refreshService, $uibModal,renderHTMLFactory, ConstantService, $location, $rootScope, $uibModal, $log, $document, urlService, customSession) {
        $('html, body').scrollTop(0);
        // sessionStorage.removeItem('selectedInsurancePlanId')
        $('[data-toggle=tooltip]').tooltip();
        $scope.renderHTMLFactory = renderHTMLFactory;
        sessionStorage.removeItem('SelectedBagLst');
        sessionStorage.removeItem('SelectedMealLst');
        sessionStorage.removeItem('SelectedSeatLst');
        /* $rootScope.preventNavigation = true;*/
        $scope.flightResultList = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
        $scope.InsuranceDetails = JSON.parse(sessionStorage.getItem('travelInsurancee'));
        //  console.log('travel insss', JSON.parse(sessionStorage.getItem('travelInsurancee')));

        var multi = $scope.flightResultList.multicityInfos;
        
        $scope.multiCity = $scope.flightResultList.multicityInfos;
        $scope.timeload = function(){
            $("#load").show();
            setTimeout(function () {
            //    test(); 
               $('#load').hide();
            }, 500);
        }
        $scope.backloader=function() {
            $scope.timeload();
            // document.getElementById("load").style.display="block";
            // setTimeout("hide()", 500); 
            $location.path('/flightResult');
    }
    // $scope.hide=function() {
    //     document.getElementById("load").style.display="none";
    // }
        // sessionStorage.setItem('multiCity', JSON.stringify($scope.searchFlightObj.multicityInfos));
        if (multi === undefined) {
            $scope.fromCountry = $scope.flightResultList.frmAirport;
            // .split('(')[1].split(')')[0];
            $scope.toCountry = $scope.flightResultList.toAirport;
            // .split('(')[1].split(')')[0];
            $scope.hideCity = true;
        } else {
            $scope.hideCity = false;
        }
        // $scope.getTransition = JSON.parse(sessionStorage.getItem('repricesession')).data.selectedResult.flightSearchResults[0];

        $scope.monthcnvrt =  function(val){
            return new Date(2014, val-1);
        }


        var flightPrceDatasession = JSON.parse(sessionStorage.getItem('flightPriceData'));
        $scope.flightPricingData = flightPrceDatasession.data
        $scope.pricingData = $scope.flightPricingData.selectedResult;
        $scope.flightDetails1 = $scope.flightPricingData.selectedResult.flightSearchResults[0];

        $scope.totalFare = $scope.pricingData.flightSearchResults[0].grndTotal;
        $scope.totBaseFare = $scope.pricingData.flightSearchResults[0].totBaseFare;
        $scope.totTaxFare = $scope.pricingData.flightSearchResults[0].totTaxFare;

        // $scope.flightdetails = $scope.pricingData.flightSearchResults[0].segGrpList[0].segList[0].fltInfoList;

        // for (var i = 0; i < $scope.pricingData.flightSearchResults[0].segGrpList.length; i++) {
        // 	$scope.flightdetails = $scope.pricingData.flightSearchResults[0].segGrpList[i].segList[0].fltInfoList;
        // 	console.log("$scope.flightdetails",$scope.flightdetails);
        // }
        //  $scope.transition = $scope.pricingData.flightSearchResults[0].segGrpList[0].segList[0].fltInfoList[0].duration;
        //   var num = $scope.transition;
        //  var hours = (num / 60);
        //  $scope.hours = Math.floor(hours);
        //   $scope.minutes = (hours - $scope.hours) * 60;


        if ($scope.pricingData.srchType == 2) {
            $scope.flightdetails1 = $scope.pricingData.flightSearchResults[0].segGrpList[1].segList[0];
        }


        //set default insurance plan
        // $scope.InsuranceDetails = $scope.flightPricingData.insurancePlans;
        //     if($scope.InsuranceDetails){
        //     $scope.InsuranceDetails.availablePlans.sort(function (a, b) {
        //         return a.totalFare - b.totalFare;
        //     });
        // }

            // set default 

            $scope.stationBind = function(plc){
                var place1 = plc.split("(")[0];
                var place2 = plc.split(",")[1];
                return place1+ ', '+place2;
             }

            $scope.extras = 0;
            $scope.palanContent = 0;

            $scope.getSelectedPlanId = function(selectedPlanId) {
                if (selectedPlanId != null) {
                    sessionStorage.setItem('selectedInsurancePlanId', selectedPlanId);
                    // sessionStorage.setItem('selectedInsurancePlanId', {"title":title,"selectedPlanId":selectedPlanId,"extras":$scope.extras});
                    $rootScope.value = selectedPlanId;
                    $scope.palanContent = $scope.InsuranceDetails.availablePlans[selectedPlanId]
                    $scope.extras = $scope.InsuranceDetails.availablePlans[selectedPlanId].totalFare

                } else {
                    $scope.extras = 0;
                    sessionStorage.removeItem("selectedInsurancePlanId");
                    $scope.palanContent = 0;
                }
            }

            var selectedInsurancePlanId = sessionStorage.getItem("selectedInsurancePlanId");
            if(selectedInsurancePlanId != undefined){
                $scope.getSelectedPlanId(selectedInsurancePlanId);
            }else{
                if($scope.InsuranceDetails != null){
                    for(var ins=0;ins<$scope.InsuranceDetails.availablePlans.length;ins++){
                        if($scope.InsuranceDetails.availablePlans[ins].isSelected){
                            $scope.palanContent = $scope.InsuranceDetails.availablePlans[ins];
                            $scope.extras = $scope.InsuranceDetails.availablePlans[ins].totalFare;
                            sessionStorage.setItem('selectedInsurancePlanId', ins);
        
                        }
                    }
                }
            }


        $rootScope.selectedIndex = JSON.parse(sessionStorage.getItem('flightIndex'));


        /* end */
        $scope.goBack = function(path) {
            $location.path(path);

        }

        $scope.continueBooking = function(){
    $scope.artloading = true;
            $location.path("/flightReview");
        }   

	if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
		$scope.loggedvalue = true;
		$rootScope.bookingHide = false;
		$rootScope.guestLogin = false;
		$rootScope.bookingHide1 = true;
		$rootScope.dispName = "";
		sessionStorage.removeItem("guestLoginDatails");
		localStorage.removeItem("loginToken");
		
	}

        // openbaggage function
        $scope.flightBaggageDetails = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedBagIndex = index.indx;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightBaggageInfo.html',
                controller: 'flightbaggageinfoControllers',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }
        $scope.flightBaggageDetailsInclusions = function(size, parentSelector, BagDetails) {

            var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedBagIndex = BagDetails.indx;
        $rootScope.selectedBagDetails = BagDetails;
        
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightBaggageInclusions.html',
            controller: 'flightbaggageinfoController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
        
        }
        /* openFlight function */

        $scope.openFlight = function(size, parentSelector) {

            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightDetailedinfoContent.html',
                controller: 'flightDetailsinfoContent',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }


        /* end */

        /* open FareInfo */

        $scope.selectPlan = function(values, size, parentSelector) {
            //	console.log("Values " + values);
            $rootScope.value = values;
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            //$rootScope.selectedIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'aboutInsurance.html',
                controller: 'insuranceCtrl',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })

        }

        $scope.openFareInfo = function(size, parentSelector) {

            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            //$rootScope.selectedIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightFareinfoContent.html',
                controller: 'flightFareinfoController',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        //return $scope.items
                    }
                }
            })

        }

        $scope.flighttotalFare = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightFareTotal.html',
                controller: 'flightFaretotalController',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }

        $scope.miniFareRules = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightFareinfoContent.html',
                controller: 'flightFaretotalController',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }


        $scope.travelInsurence = function(info,size, parentSelector, index) {
            $rootScope.insuranceInfodata = info.planContent.toString();
            $rootScope.insurancetitle = info.planTitle;    
            var parentElem = parentSelector ?
            
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightTravelInsurence.html',
                controller: 'flightTravelInsurenceController',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }

        // $scope.showModal = function(info){
        //     $scope.insuranceInfo = info.planContent.toString();
        //     $scope.insurancetitle = info.planTitle;         

      
        //     $("#exampleModal").modal({show:true});
       
        // }

    }).controller('insuranceCtrl', function($http, $scope, $uibModalInstance, $rootScope) {



        var flightPrceDatasession = JSON.parse(sessionStorage.getItem('flightPriceData'));
        $scope.isureFlight = flightPrceDatasession.data;
        var index = $rootScope.value
        $scope.avaliablePlan = $scope.isureFlight.insurancePlans.availablePlans[index];





        $scope.ok = function() {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }).controller('flightTravelInsurenceController', function($http,renderHTMLFactory, $scope, $uibModalInstance, $rootScope) {

        $scope.renderHTMLFactory = renderHTMLFactory;
        $scope.insuranceInfo=$rootScope.insuranceInfodata;

      

        $scope.ok = function() {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }).controller('flightFaretotalController', function($scope,$http,refreshService,ServerService,$uibModalInstance, $rootScope) {

        var index = $rootScope.selectedFareIndex
        var flightPrceDatasession = JSON.parse(sessionStorage.getItem('flightPriceData'));
        $scope.flightDetail = flightPrceDatasession.data;
        $scope.flightDetail1 = $scope.flightDetail.selectedResult.flightSearchResults[0]
        $scope.airlinenames = $scope.flightDetail1.segGrpList[0].segList[0].airlineName;
        $scope.refundtypes = $scope.flightDetail1.fareslst[0].refundable;
        if ($scope.refundtypes == true) {
            $scope.refundtype = "Refundable";
        } else {
            $scope.refundtype = "Non-Refundable";
        }
        $scope.flightDetail2 = $scope.flightDetail1.fareslst
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


        


    fareFunc = function(){
    $scope.loading = true;
    $scope.disabled = true;
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));

    var repriceDetails = {
        "sessionId": sessionId,
        "resultIndexId": index.indx
    }

    $scope.artloading = true;

    $http.post(ServerService.serverPath + 'flight/farerules', repriceDetails, {

        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.artloading = false;
        var data = response.data.data;
        if (data != null) {
            $('.loading').hide();
            $scope.loading = false;
            $scope.disabled = false;
         $scope.farerule = data;
        //   if (data != null) {

        //         for (var f = 0; f < data.length; f++) {
        //             $scope.farerule = data[f].content;
        //             $('.fareRules').append(data[f].content)
        //             $('.fareHeader').append(data[f].headerText)
        //         }
        //     } else {
        //         $scope.farerule = "Fare rules not available";
        //         $('.fareRules').append('Fare rules not available');
        //         $('.fareRules').css('text-align', 'center');
        //         $('.fareRules').css('color', 'red');

        //     }
            // console.log($scope.farerule);

        } else {
            $scope.farerule =[];// "Fare rules not available";
            // $('.fareRules').append('Fare rules not available');
            $('.fareRules').css('text-align', 'center');
            $('.fareRules').css('color', 'red');


        }
    

    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                fareFunc();
            });     
        }
    });
    }
    fareFunc();

    })
    .controller('flightDetailsinfoContent', function($scope, $uibModalInstance, $rootScope) {

        
        $scope.monthcnvrt =  function(val){
            return new Date(2014, val-1);
        }
        var flightPrceDatasession = JSON.parse(sessionStorage.getItem('flightPriceData'));
        $scope.flightPricingData = flightPrceDatasession.data;
        $scope.pricingData = $scope.flightPricingData.selectedResult;
        $scope.flightDetails1 = $scope.flightPricingData.selectedResult.flightSearchResults[0];
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('flightbaggageinfoControllers', function($scope, $uibModalInstance,refreshService, $rootScope, $http, ServerService) {
        var flightInfo = $rootScope.selectedBagIndex;
        splitFun = function(orgin,dest){
            var orgins = orgin.split("(")[1].split(")")[0];
            var dests = dest.split("(")[1].split(")")[0];
            return orgins+"_"+dests;
        }

        
        $scope.monthcnvrt =  function(val){
            return new Date(2014, val-1);
        }
        var flightPrceDatasession = JSON.parse(sessionStorage.getItem('flightPriceData'));
        $scope.flightPricingData = flightPrceDatasession.data;
        $scope.pricingData = $scope.flightPricingData.selectedResult;
        $scope.flightDetails1 = $scope.flightPricingData.selectedResult.flightSearchResults[0];

        // $scope.flightDetails1.segGrpList.forEach(element => {
        //     if(element.inclusions != null){
        //     element.ssrInclsions = (element.ssrInclsions || {});
        //     if(element.inclusions._PURCHASE_BAGGAGE){
        //         element.ssrInclsions.PurchaseBaggage = (element.ssrInclsions.PurchaseBaggage || "");
        //         element.ssrInclsions.PurchaseBaggage =  element.inclusions._PURCHASE_BAGGAGE
        //     }
        //     if(element.inclusions._CABIN_BAGGAGE){
        //         element.ssrInclsions.CabinBaggage = (element.ssrInclsions.CabinBaggage || "");
        //         element.ssrInclsions.CabinBaggage =  element.inclusions._CABIN_BAGGAGE
        //     }
        //     if(element.inclusions._CHECKED_BAGGAGE){
        //         element.ssrInclsions.CheckedBaggage = (element.ssrInclsions.CheckedBaggage || "");
        //         element.ssrInclsions.CheckedBaggage =  element.inclusions._CHECKED_BAGGAGE
        //     }
        //     if(element.inclusions._MEALS){
        //         element.ssrInclsions.Meal = (element.ssrInclsions.Meal || "");
        //         element.ssrInclsions.Meal =  element.inclusions._MEALS
        //     }
        //     if(element.inclusions._SEATS){
        //         element.ssrInclsions.Seat = (element.ssrInclsions.Seat || "");
        //         element.ssrInclsions.Seat =  element.inclusions._SEATS
        //     }
        //     if(element.inclusions._CANCELLATION){
        //         element.ssrInclsions.Cancellation = (element.ssrInclsions.Cancellation || "");
        //         element.ssrInclsions.Cancellation =  element.inclusions._CANCELLATION
        //     }
           
        // }
        // });

        baggagedtls = function(){

        // if ($scope.flightPricingData.selectedResult.flightSearchResults[0].additionalProperties.PTCFareBreakdownList != undefined) {
        //     // console.log("ptc",$scope.flightPricingData.selectedResult.flightSearchResults[0].additionalProperties.PTCFareBreakdownList)
        //     $scope.baggageWeight = $scope.flightPricingData.selectedResult.flightSearchResults[0].additionalProperties.PTCFareBreakdownList[0].passengerFare.tpaextensions1.baggageInformationList.baggageInformations[0].allowanceWeight;
        // } else {
        //     $scope.baggageWeight = 0
        // }

            // $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
            // $scope.particularFlightData = $scope.result.data.flightSearchResults[flightInfo];
            $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
        
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        
            var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
            var repriceDetails = {
                    "sessionId":sessionId,
                    "resultIndexId": flightInfo
                }
                // console.log($rootScope.selectedBagIndex );
            $scope.artloading = true;
            $http.post(ServerService.serverPath + 'flight/ssr', repriceDetails, {
        
                headers: headersOptions
            }).then(function successCallback(response) {
                // $scope.artloading = fasle;
                //   console.log(response);  
            if(response.data.success){
                $scope.flightSsrData = response
                $scope.baggageSsr = $scope.flightSsrData.data.data;
                $totallBagaggeInfo = [];
                $scope.DEFAULT_CABIN_BAGGAGE_COMMENTS=$scope.flightSsrData.data.clientAdditionalProperties.DEFAULT_CABIN_BAGGAGE_COMMENTS;
                $scope.DEFAULT_CABIN_BAGGAGE=$scope.baggageSsr.DEFAULT_CABIN_BAGGAGE;
                for(var fl=0; fl<$scope.flightDetails1.segGrpList.length;fl++){
                    for(var sl=0; sl<$scope.flightDetails1.segGrpList[fl].segList.length;sl++){
                        for(var fls=0; fls<$scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList.length;fls++){
                            var fltinfo = $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls];
                            var key = splitFun(fltinfo.origin,fltinfo.dest);
                            if($scope.baggageSsr[key] != undefined ){
                            $scope.baggageinfo = $scope.baggageSsr[key].baggageSSRs;
                            var defaultBag = $scope.baggageSsr[key].defaultBaggage[0].cabin;
                            for (var b = 0; b < $scope.baggageinfo.length; b++) {
                                if($scope.baggageinfo[b].passengerKey == 'ADT'){
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt || "");
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt || "");
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt = defaultBag;
                                }
                                if($scope.baggageinfo[b].passengerKey == 'CNN'){
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt || "");
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt || "");
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt = defaultBag;
                                }
                                if($scope.baggageinfo[b].passengerKey == 'INF'){
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt || "");
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt || "");
                                    $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt = defaultBag;
                                }
                            }
                        }
                        }
                    }
                }
                $scope.artloading = false;
            }else{
                $scope.artloading = false;
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                        baggagedtls();
                   });                
            }
            });
        }

        if(!$scope.flightDetails1.isLcc){
            baggagedtls();
        }
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    })