ouaApp.controller('homeController', function ($scope, $http, $location, $uibModal, ConstantService, ServerService, $rootScope) {
	$scope.currency = ConstantService.currency
	var currentURL=$location.absUrl();
	$scope.desimg='views/images/fileplaceholder.png';
	$scope.mobimg='views/images/fileplaceholder.png';
	$scope.relativePath = currentURL.substring(0,currentURL.lastIndexOf("/") +1);
	$scope.carouselTime = $scope.carouselTime;
	$scope.description = $scope.description;

	// $scope.inputs = [];
	// $scope.addfield=function(){
	//   $scope.inputs.push({})
	// }

	$scope.logged = JSON.parse(localStorage.getItem('loginName'));
	$(document).on('focus',".daterangepic", function(){
		$(this).daterangepicker({
			drops: 'up'
		});
	});
		// $('.daterangepic').daterangepicker({
		// 	opens: 'left'
		//   }, function(start, end, label) {
		// 	console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		//   });
	// owl.trigger('play.owl.autoplay',[200]);
	if($scope.logged!= undefined){
		$scope.role = JSON.parse(localStorage.getItem('authentication'));
		$scope.roleId = $scope.role.userId;
		}
		$scope.slideOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 10000,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 2,
            },
            1170: {
                items: 3,
            },
        }
    };
	$scope.selectArray = [{
		duration:null,
		orderNo:null,
		desimg:$scope.desimg,
		mobimg:$scope.mobimg
	  }];

	$scope.privewSelectArray = Object.create($scope.selectArray);
	//   $scope.privewSelectArray =[{
	// 	orderNo:null,
	// 	desimg:$scope.desimg
	//   }]

	//   $scope.desimg=$scope.desimg;
	$scope.sportImages = [$scope.desimg,$scope.desimg,$scope.desimg];
	console.log($scope.sportImages);

	$scope.addMultipleDialog= function(index){
		console.log($scope.selectArray);
		if($scope.selectArray.length <= 4)
		{
			
			// $scope.selectArray.push({desimg:$scope.desimg});
			var newItem = {
				duration:null,
				orderNo:null,
				desimg:'views/images/fileplaceholder.png',
				mobimg:'views/images/fileplaceholder.png'
				};
			  	$scope.selectArray.push(newItem);
			
				//   var privNewItem = {
				// 	orderNo:null,
				// 	desimg:$scope.desimg
				//   };	
			  	// $scope.privewSelectArray.push(privNewItem);
				
			//   angular.forEach($scope.selectArray,function(selectArrayVal,key){
			// 	angular.forEach($scope.privewSelectArray,function(privewSelectArrayVal,key){
			
			// 		privewSelectArrayVal.orderNo = selectArrayVal.orderNo;
			// 		privewSelectArrayVal.desimg = selectArrayVal.desimg;

			//  });
			// });

			//   angular.forEach($scope.selectArray,function(val,key){
			// 	console.log(key+":"+val.orderNo); 

			//  });
		}
		if($scope.selectArray.length > 4){
			$scope.addDisable = true;
		}


		// $scope.myText = "My name is: <h1>John Doe</h1>";
		// var myEl = angular.element( document.querySelector( '#divID' ) );
		// myEl.append('Hi<br/>'); 
		// var dialogHtml = '   <div ng-controller="homeController">		<div class="modal-header">				<h4 class="modal-title">Edit Content </h4>		  <button type="button" class="close" data-dismiss="modal">&times;</button>	</div><div class="modal-body"> <div class="container  mt-4">		<div class="row">				<div class="col-sm-12 col-lg-4 col-md-4 col-xs-12">						<div id="holder" id="holder" class="holder_default"><input type="file" ng-model-instant style="opacity:0;  z-index: 1003; width: 230px;height: 95px;						position: relative;		top: -22px;" class="upload" multiple="multiple"  name="browsefile" onchange="angular.element(this).scope().imageUpload(event)" />'
		// ,el = document.getElementById('myID');
		// $scope.value='mk';
		// angular.element(el).append( $compile(dialogHtml)($scope) )

		// $scope.selectedValue = function (value) {
		// 	$scope.val = value;
		// 	console.log($scope.val)
		// };			
	}
	// $scope.selectArray.status.sort(function(a,b) {
	// 	console.log("sort");
	// 	return a.orderNo - b.orderNo;
	// });
	$scope.removeMultipleDialog = function(index){
		// console.log(index);
		$scope.selectArray.splice(index,1);
		$scope.privewSelectArray.splice(index,1);

		if($scope.selectArray.length <= 4){
			$scope.addDisable = false;
		}
	   }

	$scope.changeImageOrder = function(event){
		console.log(event.target.value);
		var sorted = $scope.privewSelectArray.sort(function(a, b) {
			return a.orderNo - b.orderNo;
		  });
		  
		  console.log(sorted);
		  console.log($scope.selectArray);
		  console.log($scope.privewSelectArray);

	}

	$scope.addCarouselTime = function()
	{
		angular.forEach($scope.selectArray,function(val,key){
			console.log(key+":"+val.orderNo); 
		 }) 
		// owl.trigger('play.owl.autoplay',[1000]);
		// owl.owlCarousel({
		// 	items:1,
		// 	loop:true,
		// 	margin:10,
		// 	autoplay:true,
		// 	autoplayTimeout:$scope.carouselTime,
		// 	autoplayHoverPause:true,
		// 	dots:true
		// 	});
	}

	$scope.imageUpload = function (event) {
        var files = event.target.files;

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(file);
        }
    }

    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.img = e.target.result;            
        });
	}
	
	$scope.desImageUpload = function (event) {
		var files = event.target.files;
	
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			var reader = new FileReader();
			reader.onload = $scope.desImageIsLoaded;
			reader.readAsDataURL(file);
		}
	}
	$scope.desImageIsLoaded = function (e) {
		$scope.$apply(function () {
			$scope.desimg = e.target.result; 
			var temp ={desimg:$scope.desimg};  
			$scope.selectArray[$scope.selectArray.length-1].desimg=($scope.desimg);
			         
		});
	}
	
	$scope.mobImageUpload = function (event) {
		var files = event.target.files;
	
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			var reader = new FileReader();
			reader.onload = $scope.mobImageIsLoaded;
			reader.readAsDataURL(file);
		}
	}
	
	
	
	$scope.mobImageIsLoaded = function (e) {
		$scope.$apply(function () {
			$scope.mobimg = e.target.result;  
			var temp ={mobimg:$scope.mobimg};  
			$scope.selectArray[$scope.selectArray.length-1].mobimg=($scope.mobimg);          
		});
	}

    $scope.bannerOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 10000,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 1,
            },
            1170: {
                items: 1,
            },
        }
    };
    
    $scope.tabs = [
        {
            title: '<i class="icon-flights"></i> Flight + <i class="icon-hotels"></i> Hotel',
            content: 'Dynamic content 1'
        },
        {
            title: 'Dynamic Title 2',
            content: 'Dynamic content 2',
            disabled: true
        }
	];
	
  $scope.states = [];
  $scope.flightfromAutoComplet=function(value) {
	if(value!=null){
  if(value.length >= 2){
  var place = {'queryString': value}
  $http.post(ServerService.serverPath+'rest/repo/airport','{"queryString" : "'+value+'"}', 
  {
   headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
   }
   }).then(function(response){
  
   if(response.data!='null'&&response.data.length>0){
   $scope.states = []
   for(var i=0; i<response.data.length; i++){
     var airfrom= {
                      Code: response.data[i].code,
       Title: response.data[i].city+","+response.data[i].country+"-"+response.data[i].name+"("+response.data[i].code+")",
                      
     }
     $scope.states.push(airfrom);
     
    }
    $scope.states.sort();
  }
  })
 }
 }
};


  
    var intitialDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
    
    $scope.date = intitialDate;
    
    $scope.hotelDate = intitialDate;
	
	/* insuranceSearch function */
	$scope.travelInsurances = {};
	$scope.insurancedate = { startDate: null,
    endDate: null
	}
	$scope.travelInsurances.adultCount = 1
	$scope.travelInsurances.childCount = 0
	$scope.travelInsurances.infantCount = 0
	
	$scope.loading = false;
	$scope.disabled = false;
	$scope.insuranceSearch = function(){
	$scope.loading = true;
					$scope.disabled = true;
		var insurance_from = $('#insurancefrom_place').val();
		var insurance_to = $('#insuranceto_place').val();
		var insurance_date = $('#insurance_date').val();
		var insurance_adult = $('#insuranceAdult').val();
		var insurance_child = $('#insuranceChild').val();
		
		var startDate = insurance_date.split('-')[0];
		var endDate = insurance_date.split('-')[1];
		
		if(insurance_adult == "" || insurance_adult == undefined){
			$scope.travelInsurances.adultCount = 0
		} /* else {
			$scope.travelInsurance.adultCount = insurance_adult
			} */
		if(insurance_child == "" || insurance_child == undefined){
			$scope.travelInsurances.childCount = 0
		} /* else {
			$scope.travelInsurance.childCount = insurance_child
			} */
			
			$scope.searchInsure = {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
				}
			
			$scope.Insurance = {
			'travelInsurance' : {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
			}
				}
				sessionStorage.setItem('insuranceSearchObj', JSON.stringify($scope.searchInsure))
			sessionStorage.setItem('InsuranceSearchRequest',JSON.stringify($scope.Insurance));
			$location.path('/searchingInsurance');
			
		
		
		}
		$scope.submitted = false;
		$scope.insuranceSearchForm = function(valid){
			console.log("flightSearchForm*************"+valid);
		$scope.submitted = true;
		if(valid){
			var insurance_from = $('#insurancefrom_place').val();
		var insurance_to = $('#insuranceto_place').val();
		var insurance_date = $('#insurance_date').val();
		var insurance_adult = $('#insuranceAdult').val();
		var insurance_child = $('#insuranceChild').val();
		
		var startDate = insurance_date.split('-')[0];
		var endDate = insurance_date.split('-')[1];
		
		if(insurance_adult == "" || insurance_adult == undefined){
			$scope.travelInsurances.adultCount = 0
		} 
		if(insurance_child == "" || insurance_child == undefined){
			$scope.travelInsurances.childCount = 0
		} 
			
			$scope.searchInsure = {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
				}
			
			$scope.Insurance = {
			'travelInsurance' : {
				'adultCount' : $scope.travelInsurances.adultCount,
				'childCount' : $scope.travelInsurances.childCount,
				'departDateTime' : startDate,
				'arrivalDateTime' : endDate,
				'currency' : 'BHD',
				'channel' : 'IBE_DNABH',
				'cultureCode' :'EN',
				'countryCode':'BH',
				'orgin' : insurance_from,
				'destination' : insurance_to,
				'infantCount' : $scope.travelInsurances.infantCount,
				'srchType': 2
			}
				}
				sessionStorage.setItem('insuranceSearchObj', JSON.stringify($scope.searchInsure))
			sessionStorage.setItem('InsuranceSearchRequest',JSON.stringify($scope.Insurance));
			$location.path('/searchingInsurance');
			}
			}
	/* end */
	
	/* flight plus hotel  */
	
	/* end */
	
	$scope.CabinClass = "Economy";
	$scope.updateCabinClass = function(value) {
		$scope.CabinClass = value;
	}
	$scope.AirlineValue = "All Airline";
	$scope.updateAirline = function(value) {
		$scope.AirlineValue = value;
	}
	
	$scope.opts = {
		opens: 'left',
		eventHandlers: {
			'show.daterangepicker': function(ev, picker) { 
				//console.log('arguments', arguments);
			}
		}
	}
	$scope.checkboxModel = {
       value : false
    };
	
	$scope.flightOpts = {
		singleDatePicker: $scope.checkboxModel.value,
		minDate:new Date(),
		eventHandlers: {
			'show.daterangepicker': function(ev, picker) { 
				$scope.flightOpts = {
					singleDatePicker: $scope.checkboxModel.value
				}
			}
		}
	}
	
	$scope.oneWayChange = function() {
		$scope.date = ($scope.checkboxModel.value) ? $scope.date.startDate : $scope.date;
	}

//	$rootScope.tabmenuactive = 4;
	/* $rootScope.tabmenuactive = 1;
	$scope.activateTabMenu = function (value) {
		$rootScope.tabmenuactive = Number(value);
		$("html, body").stop().animate({
			scrollTop: $('#about-us').offset().top - 10
		}, '500', 'linear');
	} */
	
	
	var booking =JSON.parse(localStorage.getItem('userBookingdetails'))
	var display = JSON.parse(localStorage.getItem('loginName'));
	localStorage.getItem('authentication');
		if(booking == '' || booking == null){
			$scope.bookingHide1 =false;
		} else {
			$scope.bookingHide1 = booking.bookingHide1;
			$scope.bookingHide = booking.bookingHide;
			if(display != null){
				$scope.dispName = display.fname;
				}
			}

}).controller('passengerCtrl', function ($http, $scope, $uibModalInstance, $uibModal) {
    		$scope.paasCount = {}


    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
	console.log("cancel");
       // $uibModalInstance.dismiss('cancel');
	  $uibModalInstance.close();
    };
	;
	
	$scope.save = function(){
		sessionStorage.setItem('pasengerList', JSON.stringify($scope.paasCount));
		$uibModalInstance.dismiss('cancel');
		
		
		}
});