ouaApp.controller('uaetourDetailsController', function ($scope, $http, $stateParams, $uibModal, $log, $document, $location, urlService, renderHTMLFactory, ServerService, customSession, ConstantService, $uibModal) {
    $('html, body').scrollTop(0);
    $scope.artloading = false;
    var packID = $stateParams.id;
    holdeparture.min = new Date().toISOString().split("T")[0];

    $scope.countryList = {};
    $scope.dropdownList = function () {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;
            }
        }, function errorCallback(response) {
            if (response.status == 403) {
                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                status.then(function (greeting) {
                    $scope.dropdownList();
                });
            }
        });
    }
    getAtrHdr = function () {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }

        $http.get(ServerService.listPath + 'crud/list/attr-header', { headers: headersOptions })
            .then(function successCallback(response) {
                for (var j = 0; j < response.data.data.length; j++) {
                    if (response.data.data[j].ahId == packID) {
                        $scope.packageDetailView = response.data.data[j];
                        $http.get(ServerService.packagePath + 'attractions/details/' + packID, { headers: headersOptions }).then(function (response) {
                            $scope.artloading = false;
                            $scope.packageDetails = response.data.data;
                            $scope.isThumbnail = $scope.packageDetails[0].thumbnailImages.length;
                            if($scope.isThumbnail > 0){
                                $scope.uaefullimage = $scope.packageDetails[0].thumbnailImages[0];
                            }
                            if($scope.isThumbnail == 0){
                                $scope.uaefullimage = $scope.packageDetailView.images;
                            }
                            $scope.dropdownList();
                        })
                    }
                }
            }, function errorCallback(response) {
                if (response.status == 403) {
                    var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                    status.then(function (greeting) {
                        getAtrHdr();
                    });
                }
            });
    }

    getAtrHdr();

    $scope.brouchuredownload = function () {
        $scope.brouchureId = $scope.packageDetails[0].brochureUploadId;
        if ($scope.brouchureId == null) {
            Lobibox.alert('error', {
                msg: "No brouchure Uploaded For this Package."
            })
        } else {
            window.open("http://api.convergentechnologies.com/rest/api/v1/file/download/" + $scope.brouchureId);
        }
    }
    $scope.goPackage = function () {
        $scope.artloading = true;
        $location.path('uaeAttraction');
        $scope.artloading = false;
    }

    $scope.imagepath = ServerService.listPath + "file/download/";
    $scope.user = {};
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data.authToken;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    var tokenname = $scope.tokenresult.data.authToken;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname
    }
    $scope.modelDismiss = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.user.totalDays = 0;

    $scope.user.adultNo = 1;
    $scope.user.childNo = 0;
    $scope.user.infantNo = 0;

    $scope.holidaydep = function () {
        $scope.minmultiDate = $('#holdeparture').val();
        var depdat1 = document.getElementById('holdeparture').value;
        depdate1 = new Date(depdat1)

        today = new Date()
        currentYear = depdate1.getFullYear();
        currentMonth = depdate1.getMonth();
        currentDay = depdate1.getDate();
        $scope.minreturnDate = new Date(currentYear, currentMonth, currentDay + 2);

        holdreturn.min = new Date($scope.minreturnDate).toISOString().split("T")[0];
        $scope.user.totalDays = 1;
    }

    $scope.onThambnailChange = function(imgfile){
        $scope.uaefullimage = imgfile;
    }

    $scope.attsubmitFormEnquiryForm = function () {
        //document.getElementById('enqurysbt').disabled = true;
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
     
        var headersOptions = {
            "g-recaptcha-response" :   $scope.token,
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        d = new Date($('#holdeparture').val()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        var HoldDate = [day, month, year].join('-');
        $scope.user.depDate = HoldDate;
        $scope.user.returnDate = HoldDate;
        $scope.adultminDate;
        $scope.user.contactNumber = "+"+ $scope.user.countryCode +" "+ $scope.user.contactNumber;

        if ($scope.isvalid === true) {
            $scope.user = {
                "enquiryServiceType": "Attraction",
                "packageName": $scope.packageDetailView.attractionName,
                "heSalutation": document.getElementById('salutation').value,
                "heName": document.getElementById('name').value,
                "email": document.getElementById('emailid').value,
                "countryCode": document.getElementById('countryCode').value,
                "contactNumber": "+"+document.getElementById('countryCode').value +" "+document.getElementById('mobileNo').value,
                "adultNo": document.getElementById('adult').value,
                "childNo": document.getElementById('child').value,
                "infantNo": document.getElementById('infant').value,
                "depDate": HoldDate,
                "returnDate": HoldDate,
                "remarks": document.getElementById('message').value
            }

            $scope.artloading = true;
            $http.post(ServerService.packagePath + 'holiday/enquiry', JSON.stringify($scope.user), {
                headers: headersOptions
            }).then(function successCallback(response) {
                $scope.loading = false;
                $scope.disabled = false;
                if (response.data.success) {
                    document.getElementById('enqurysbt').disabled = false;
                    Lobibox.alert('success', {
                        msg: response.data.message
                    })
                    $(".modal-backdrop").remove();
                    $scope.artloading = false;
                    $('#myModal').click();
                    FormVal.reset();
                } else {
                    document.getElementById('enqurysbt').disabled = false;
                    Lobibox.alert('error', {
                        msg: response.data.errorMessage
                    })
                }

            }, function errorCallback(response) {
                document.getElementById('enqurysbt').disabled = false;
                if (response.status == 403) {
                    var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                    status.then(function (greeting) {
                        $scope.attsubmitForm(isvalid);
                    });
                }
            });
        }else{
            document.getElementById('enqurysbt').disabled = false;
        }
    }



    
    $scope.attsubmitForm = function(isvalid) {
        document.getElementById('enqurysbt').disabled = true;
        $scope.recaptchaId = JSON.parse(localStorage.getItem('configKeys'));
        $scope.isvalid=isvalid;
            grecaptcha.ready(function() {
                grecaptcha.execute($scope.recaptchaId.google_recaptcha_public_key, {})
                        .then(function(token) {
                            // sessionStorage.setItem("captcha",token);
                            $scope.token=token;
                            $scope.attsubmitFormEnquiryForm();
                        });
            });
        

       

    }

    $scope.printDiv = function (tourprintDiv) {
        var printContents = document.getElementById('tourprintDiv').innerHTML;
        var popupWin = window.open('', '_blank', 'width=300,height=300');
        popupWin.document.open();
        popupWin.document.write('<html><head><link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet"><link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" /><link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css" /><link rel="stylesheet" href="styles.css" /><link rel="stylesheet" href="assets/css/holiday.css" /><link rel="stylesheet" href="assets/css/jquery.desoslide.css" /><link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }

});