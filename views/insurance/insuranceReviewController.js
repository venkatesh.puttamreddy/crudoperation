ouaApp.controller('insuranceReviewController', function($scope,refreshService, $http, renderHTMLFactory, $rootScope, $uibModal, $window, $location, $parse, renderHTMLFactory, NationService, baseUrlService, ServerService, $timeout, httpService, $state, customSession, CountryService) {
    $scope.renderHTMLFactory = renderHTMLFactory;
    $scope.backtodetail = false;
    $scope.backtotraveler = false;
    NationService.nationGet();
    CountryService.countryGet();
    $('html, body').scrollTop(0);
    var text = "Highlight";
    $scope.renderHTMLFactory = renderHTMLFactory;
     // fordateofbirth..dropdown...author nandhiniD.
        $scope.months = 12;
        $scope.getminexpirymonth = 12;
        var dobmonth = [];
        var cdobmonth = [];
        // var idobmonth = [];
        for (var i = 0; i < $scope.months; i += 1) {
            dobmonth.push(i + 1);
            cdobmonth.push(i + 1);
            // idobmonth.push(i+1);
    
        }
        $scope.dobmonths = dobmonth;
        $scope.cdobmonths = cdobmonth;
   
   
        $scope.bdates=31;
        var adate= [];
        var cdate = [];
        var idate = [];
        for (var i = 0; i < $scope.bdates; i += 1) {
           adate.push(i + 1);
           cdate.push(i + 1);
           idate.push(i + 1);
        }
        $scope.dobdates = adate;
        $scope.cdobdates = cdate;
        $scope.idobdates = idate;
   
        // $scope.idobmonths = idobmonth;
    
        // forchild......
        var cyear = new Date().getFullYear() - 2;
        var crange = [];
        crange.push(cyear);
        for (var c = 1; c < 16; c++) {
            crange.push(cyear - c);
        }
        $scope.cyears = crange;
    
        // foradult........
        var ayear = new Date().getFullYear() - 18;
        var arange = [];
        arange.push(ayear);
        for (var i = 1; i < 55; i++) {
            arange.push(ayear - i);
        }
        $scope.ayears = arange;
        // forinfantYear......fordob..
        $scope.imonths = 12;
        var idobmonth = [];
        for (var i = 0; i < $scope.imonths; i += 1) {
            idobmonth.push(i + 1);
        }
        $scope.idobmonths = idobmonth;
        var iyear = new Date().getFullYear();
        var irange = [];
            irange.push(iyear);
            for (var i = 1; i < 2; i++) {
                irange.push(iyear - i);
            }
            $scope.iyears = irange;
        $scope.changeinfantDate = function(selectedYear) {
            console.log("selectedYear", selectedYear)
            var iyear = new Date().getFullYear();
            var infantToday = new Date();
            var getInfanntmonth = infantToday.getMonth();
    
            var selectedYear = selectedYear.infant.personaldata.birthyear || 0;
            if (selectedYear == iyear) {
                $scope.imonths = getInfanntmonth + 1;
            } else {
                $scope.imonths = 12;
            }
    
            var idobmonth = [];
            for (var i = 0; i < $scope.imonths; i += 1) {
                idobmonth.push(i + 1);
            }
            $scope.idobmonths = idobmonth;
    
            var irange = [];
            irange.push(iyear);
            for (var i = 1; i < 2; i++) {
                irange.push(iyear - i);
            }
            $scope.iyears = irange;
        }
           // forinfantYear......for..passportexpiry..
           $scope.ipemonths = 12;
           var ipemonth = [];
           for (var i = 0; i < $scope.imonths; i += 1) {
            ipemonth.push(i + 1);
           }
           $scope.ipemonths = ipemonth;
           var iexpiryyear = new Date().getFullYear();
           var aexpiryyear = new Date().getFullYear();
           var cexpiryyear = new Date().getFullYear();
           var iperange = [];
           iperange.push(iexpiryyear);
           for (var i = 1; i < 50; i++) {
            iperange.push(iexpiryyear - i);
           }
           $scope.iexpiryyears = iperange;
           $scope.changeinfantpeYear = function(selectedYear) {
               var iexpiryyear = new Date().getFullYear();
               var infantToday = new Date();
               var getInfanntmonth = infantToday.getMonth();
       
               var selectedYear = selectedYear.infant.personaldata.expiryyear || 0;
               if (selectedYear == iexpiryyear) {
                   $scope.currentmonth = getInfanntmonth;
               } else {
                   $scope.currentmonth = 0;
                   $scope.imonths = 12;
               }
       
               var ipemonth = [];
               for (var i = $scope.currentmonth; i < $scope.imonths; i += 1) {
                ipemonth.push(i + 1);
               }
               $scope.ipemonths = ipemonth;
       
               var iperange = [];
               iperange.push(iexpiryyear);
               for (var i = 1; i < 50; i++) {
                iperange.push(iexpiryyear - i);
               }
               $scope.iexpiryyears = iperange;
           }
    
    
        
    //passport expiry
   
   //  var apemonth = [];
   //  var cpemonth = [];
   //  for (var i = 0; i < $scope.getminexpirymonth; i += 1) {
   //      apemonth.push(i + 1);
   //      cpemonth.push(i + 1);
   //  }
   //  $scope.apemonths = apemonth;
   //  $scope.cpemonths = cpemonth;
   
    $scope.pdates=31;
    var apdate= [];
    var cpdate = [];
    for (var i = 0; i < $scope.pdates; i += 1) {
       apdate.push(i + 1);
        cpdate.push(i + 1);
    }
    $scope.pedates = apdate;
    $scope.cpedates = cpdate;
   
   
    
        // adultmonthchange...
        $scope.adays = 31;
   
        $scope.changeadultMonth = function(selectedMonth) {
                var selectedMonth = selectedMonth.adult.personaldata.birthmonth || 0;
                if (selectedMonth == 2) {
                    $scope.adays = 28;
                    console.log("$scope.dobdates", $scope.dobdates)
                } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                    $scope.adays = 30;
                    console.log("$scope.dobdates", $scope.dobdates)
                } else {
                    $scope.adays = 31;
                }
                var dobdate = [];
                for (var i = 0; i < $scope.adays; i += 1) {
                    dobdate.push(i + 1);
                }
                $scope.dobdates = dobdate;
                console.log("$scope.dobdates", $scope.dobdates)
    
            }
            
              // adultYearchange..for..passportexpiry.
              var apemonth = [];
              $scope.amonths = 12;
              for (var i = 0; i < $scope.amonths; i += 1) {
                  apemonth.push(i + 1);
              }
              $scope.apemonths = apemonth;
            
              var pefullyear = new Date().getFullYear();
              var arange = [];
              arange.push(pefullyear);
              for (var i = 1; i < 50; i++) {
                  arange.push(pefullyear + i);
              }
              $scope.apeyears = arange
            $scope.changeadultpeYear = function(selectedYear) {
            
   
                  var aexpiryyear = new Date().getFullYear();
                  var adultToday = new Date();
                  var getAdultmonth = adultToday.getMonth();
          
                  var selectedYear = selectedYear.adult.personaldata.expiryyear || 0;
                  if (selectedYear == pefullyear) {
                      $scope.currentmonth = getAdultmonth;
                  } else {
                      $scope.currentmonth = 0;
                      $scope.amonths = 12;
                  }
          
                  var apemonth = [];
                  for (var i = $scope.currentmonth; i < $scope.amonths; i += 1) {
                   apemonth.push(i + 1);
                  }
                  $scope.apemonths = apemonth;
          
                  var aperange = [];
                  aperange.push(aexpiryyear);
                  for (var i = 1; i < 50; i++) {
                   aperange.push(aexpiryyear - i);
                  }
                  $scope.aexpiryyears = aperange;
            }
   
   
             // adultmonthchange..for..passportexpiry.
        $scope.apedays = 31;
        $scope.changeadultpeMonth = function(selectedMonth) {
                var selectedPeMonth = selectedMonth.adult.personaldata.expirymonth || 0;
                if (selectedPeMonth == 2) {
                    $scope.apedays = 28;
                } else if (selectedPeMonth == 4 || selectedPeMonth == 6 || selectedPeMonth == 9 || selectedPeMonth == 11) {
                    $scope.apedays = 30;
                } else {
                    $scope.apedays = 31;
                }
               var currentday = new Date();
               var today = new Date().getDate();
               var aexpirymonth = currentday.getMonth()+1;
               var selectedYear = selectedMonth.adult.personaldata.expiryyear;
               if (selectedPeMonth == aexpirymonth && selectedYear ==pefullyear ) {
                   $scope.currentDate = today;
               } else {
                   $scope.currentDate = 0;
               }
               var pedate = [];
               for (var i = $scope.currentDate; i < $scope.apedays; i += 1) {
                   pedate.push(i + 1);
               }
               $scope.pedates = pedate;
            }
            // childmonthchange....
        $scope.cdays = 31;
        $scope.changechildMonth = function(selectedMonth) {
                var selectedMonth = selectedMonth.child.personaldata.birthmonth || 0;
                if (selectedMonth == 2) {
                    $scope.cdays = 28;
                } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                    $scope.cdays = 30;
                } else {
                    $scope.cdays = 31;
                }
                var cdobdate = [];
                for (var i = 0; i < $scope.cdays; i += 1) {
                    cdobdate.push(i + 1);
                }
                $scope.cdobdates = cdobdate;
            }
      
                // childYearchange..for..passportexpiry.
                var cpemonth = [];
                $scope.cmonths = 12;
                for (var i = 0; i < $scope.cmonths; i += 1) {
                    cpemonth.push(i + 1);
                }
                $scope.cpemonths = cpemonth;
              
                var cyear = new Date().getFullYear();
                var crange = [];
                crange.push(cyear);
                for (var i = 1; i < 50; i++) {
                    crange.push(cexpiryyear + i);
                }
                $scope.cexpiryyears = crange
              $scope.changechildpeYear = function(selectedYear) {
              
     
                    var cexpiryyear = new Date().getFullYear();
                    var childToday = new Date();
                    var getChildmonth = childToday.getMonth();
            
                    var selectedYear = selectedYear.child.personaldata.expiryyear || 0;
                    if (selectedYear == cyear) {
                        $scope.currentmonth = getChildmonth;
                    } else {
                        $scope.currentmonth = 0;
                        $scope.cmonths = 12;
                    }
            
                    var cpemonth = [];
                    for (var i = $scope.currentmonth; i < $scope.cmonths; i += 1) {
                     cpemonth.push(i + 1);
                    }
                    $scope.cpemonths = cpemonth;
            
                   //  var cperange = [];
                   //  cperange.push(cexpiryyear);
                   //  for (var i = 1; i < 50; i++) {
                   //   cperange.push(cexpiryyear - i);
                   //  }
                   //  $scope.cexpiryyears = cperange;
              }
             // childmonthchange..for..passport..expiry..
        $scope.cpedays = 31;
        $scope.changechildpeMonth = function(selectedMonth) {
                var selectedpeMonth = selectedMonth.child.personaldata.expirymonth || 0;
                if (selectedpeMonth == 2) {
                    $scope.cpedays = 28;
                } else if (selectedpeMonth == 4 || selectedpeMonth == 6 || selectedpeMonth == 9 || selectedpeMonth == 11) {
                    $scope.cpedays = 30;
                } else {
                    $scope.cpedays = 31;
                }
               var currentday = new Date();
               var today = new Date().getDate();
               var cexpirymonth = currentday.getMonth()+1;
               // if (selectedpeMonth == cexpirymonth) {
                   var selectedYear = selectedMonth.child.personaldata.expiryyear;
               if (selectedpeMonth == cexpirymonth && selectedYear ==pefullyear ) {
                   $scope.currentDate = today;
               } else {
                   $scope.currentDate = 0;
               }
               var cpedate = [];
               for (var i = $scope.currentDate; i < $scope.cpedays; i += 1) {
                   cpedate.push(i + 1);
               }
               $scope.cpedates = cpedate;
            }
            // infantmonthchange..
        $scope.idays = 31;
        $scope.changeinfantMonth = function(selectedMonth) {
            var selectedMonth = selectedMonth.infant.personaldata.birthmonth || 0;
            if (selectedMonth == 2) {
                $scope.idays = 28;
            } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                $scope.idays = 30;
            } else {
                $scope.idays = 31;
            }
            var idobdate = [];
            for (var i = 0; i < $scope.idays; i += 1) {
                idobdate.push(i + 1);
            }
            $scope.idobdates = idobdate;
        }
           // infantmonthchange.for.passport...expirydate.
           $scope.ipedays = 31;
           var ipedate = [];
           for (var i = 0; i < $scope.ipedays; i += 1) {
               ipedate.push(i + 1);
           }
           $scope.ipedates = ipedate;
           $scope.changeinfantpeMonth = function(selectMonth) {
               console.log("selectedMonth",selectedMonth)
               var selectedMonth = selectMonth.infant.personaldata.expirymonth || 0;
               if (selectedMonth == 2) {
                   $scope.ipedays = 28;
               } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                   $scope.ipedays = 30;
               } else {
                   $scope.ipedays = 31;
               }
   
               var currentday = new Date();
               var today = new Date().getDate();
               var iexpirymonth = currentday.getMonth()+1;
               var selectedYear = selectMonth.infant.personaldata.expiryyear;
               if (selectedMonth == iexpirymonth && selectedYear== pefullyear ) {
                   // $scope.currentDate = iexpirymonth;
                   $scope.currentDate = today;
               } else {
                   $scope.currentDate = 0;
               }
               var ipedate = [];
               for (var i = $scope.currentDate; i < $scope.ipedays; i += 1) {
                   ipedate.push(i + 1);
               }
               $scope.ipedates = ipedate;
           }




    // ends....dob..and..pe..ends.
        function getphncountrycode(){
            $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
            $scope.searchReq = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));
    
            $http.get(ServerService.listPath + 'list/phone-country', {

                headers: headersOptions
            }).then(function successCallback(response) {
                $scope.loading = false;
                $scope.disabled = false;
                if (response.data.success) {
                    $scope.countryList = response.data.data;
                    for(var a=0; a<  $scope.countryList.length;a++ ){
                        if( $scope.countryList[a].name==='971'){
                            $scope.guestuser.ccode=$scope.countryList[1];
                        }
                    }
                    // $scope.allcountrys = response.data.data;
                    // $scope.countryList = $scope.allcountrys.sort((a, b) => a.name.localeCompare(b.name));
                    //     } else {
                }
            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                        getphncountrycode();
                    });
                }
              });
        }

    function getsearchDest(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $scope.searchReq = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));

        $http.get(ServerService.profilePath + 'segment/validate/' + $scope.searchReq.destination, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.saudiDest = response.data;
            getphncountrycode();
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getsearchDest();
                });
            }
          });
    }
    function getsearchOrgin(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
    $scope.searchReq = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));


    $http.get(ServerService.profilePath + 'segment/validate/' + $scope.searchReq.origin, {
        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.saudiOrigin = response.data;
        getsearchDest();
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                getsearchOrgin();
            });
        }
      });
    }

    function retrievePM(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        $http.get(ServerService.profilePath + 'segment/retrieve/PM', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.passMandate = response.data.data;
            getsearchOrgin();
            //  //console.log(" $scope.passMandate", $scope.passMandate)
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    retrievePM();
                });
            }
          });
    }
    $scope.saudination = [];
    $scope.passMandate = [];
    function retrieveSN(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
    
        $http.get(ServerService.profilePath + 'segment/retrieve/SN', {
            headers: headersOptions
        }).then(function successCallback(response) {
    
            $scope.saudination = response.data.data;
            retrievePM();
            //  //console.log(" $scope.saudination", $scope.saudination)
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    retrieveSN();
                });
            }
          });
    }
   
    retrieveSN();
   


    $scope.nationalityId = false;
    $scope.nationalityselection = function() {
        for (var i = 0; i < $scope.saudination.length; i++) {
            //  //console.log(" $scope.saudination.length", $scope.saudination.length)

            for (var i = 0; i < $scope.saudination.length; i++) {

                if ($scope.saudiOrigin && $scope.saudiDest) {
                    for (var i = 0; i < $scope.saudiOrigin.length; i++) {
                        for (var i = 0; i < $scope.saudiDest.length; i++) {

                            if ($scope.saudiOrigin == "SN" && $scope.saudiDest == "SN") {
                                if (nationality == 'SN') {
                                    $scope.nationalityId = true;

                                } else {
                                    for (var i = 0; i < $scope.passMandate.length; i++) {
                                        if (nationality == "PM") {
                                            $('#apassport_').prop("required", false)

                                        }
                                    }
                                }
                            } else {
                                for (var i = 0; i < $scope.passMandate.length; i++) {
                                    if (nationality == "PM") {
                                        $('#apassport_').prop("required", false)

                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

    }
    /*backloader   */
    $scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
           $('#load').hide();
        }, 1000);
    }
$scope.backloader=function() {
    $scope.timeload();
    if(localStorage.islogged == 'true'){
              $scope.$parent.bookingHide = true;
        $scope.$parent.bookingHide1 = true;
    }else{
        
            $rootScope.bookingHide = false;
            $rootScope.bookingHide1 = false;
    }
    $location.path('/insuranceResult');
} 
    /* flightTermsDetails*/
    $scope.flightTermsDetails = function(size, parentSelector) {
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        
        $http.get(ServerService.profilePath + 'company/latest/tandc/1', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.termsresp = response.data.data.content;
            // $scope.termsresp = "response.data.data.content;";
    
            //$window.location.href = '/terms_conditions';
            // $location.path('/terms_conditions');
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.flightTermsDetails();
                });
            }
          });
        $("#termsandConditions").modal({show:true});
        // var parentElem = parentSelector ?
        //     angular.element($document[0].querySelector('.modal-demo' + parentSelector)) : undefined;

        // $scope.animationEnabled = true;
        // var modalInstance = $uibModal.open({
        //     animation: $scope.animationEnabled,
        //     ariaLabelledBy: 'modal-title',
        //     ariaDescribedBy: 'modal-body',
        //     templateUrl: 'termsnCondition.html',
        //     controller: 'flightTermsCtrl',
        //     backdrop: true,
        //     size: size,
        //     appendTo: parentElem,
        //     resolve: {
        //         items: function() {
        //             return $scope.items
        //         }
        //     }
        // })
    }
    $scope.isCollapsed = true;
    $scope.selectedRidersplan = JSON.parse(sessionStorage.getItem('ridersplan'));
    if( $scope.selectedRidersplan!=null){
        for (var a = 0; a < $scope.selectedRidersplan.length; a++) {
            riderFare = $scope.selectedRidersplan[a].totalFare
            $scope.totalFare =  parseFloat(riderFare);
        }
    }
    $scope.selectedAvilplan = JSON.parse(sessionStorage.getItem('avilplan'));
    if($scope.selectedAvilplan!=null){
        $scope.travelplaneName = $scope.selectedAvilplan.planContent.replace(/(<([^>]+)>)/ig, '');
        $scope.riderFare = 0;
        $scope.totalFare = parseFloat($scope.selectedAvilplan.totalFare);
        if ($scope.selectedRidersplan != null || $scope.selectedRidersplan != undefined) {
    
            for (var a = 0; a < $scope.selectedRidersplan.length; a++) {
                riderFare = $scope.selectedRidersplan[a].totalFare
                $scope.totalFare = $scope.totalFare + parseFloat(riderFare);
            }
    
        }
        $scope.totalFare = $scope.totalFare.toFixed(3)
    }
  
    $scope.searchReq = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));
    $scope.searchRequest = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));
    $scope.searchReq = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));
    $scope.loggedvalue = JSON.parse(localStorage.getItem('islogged'));
    var departureDate = $scope.searchRequest.onwardDateTime;
    // var depadultDateSplt = departureDate.split('/');
    // $scope.minadultDate = parseInt(minadultdepDate.getFullYear()) - 18;
    // $scope.maxAdultDate = parseInt(minadultdepDate.getFullYear()) - 76;
    var depDateSplt = departureDate.split('/');
    $scope.minadultDate = parseInt(depDateSplt[2]) - 76;
    $scope.maxAdultDate = parseInt(depDateSplt[2]) - 18;
    $scope.adultminyear = $scope.minadultDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
    $scope.adultmaxyear = $scope.maxAdultDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
    $scope.childmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    $scope.childtminyear = (parseInt(depDateSplt[2]) - 18) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])




    if ($scope.searchReq.returnDateTime) {
		$scope.trip = 'Round trip';
        $scope.min = ($scope.searchReq.returnDateTime).split('/')[2] + '-' + ($scope.searchReq.returnDateTime).split('/')[1] + '-' + ($scope.searchReq.returnDateTime).split('/')[0]
    } else {
		$scope.trip = 'One Way';
        $scope.min = ($scope.searchReq.onwardDateTime).split('/')[2] + '-' + ($scope.searchReq.onwardDateTime).split('/')[1] + '-' + ($scope.searchReq.onwardDateTime).split('/')[0]

    }
    var d = new Date();
    //  //console.log(d.setMonth(d.getMonth() + 6));

if($scope.selectedAvilplan!=null){


    var title = $scope.selectedAvilplan.planTitle;
    var spltTitle = title.search('Tune Protect');
    if (spltTitle !== -1) {
        var temp = title.split('Tune Protect');
        $scope.setTitleVal = temp[1];


        var splfamily = $scope.setTitleVal.indexOf('(Family)');
        if (splfamily !== -1) {
            var tempfamily = $scope.setTitleVal.split('(Family)');
            var setFamilyVal = tempfamily[0].split('-');

            $scope.setTitleVal = setFamilyVal[0] + " Family -" + setFamilyVal[1];
            //   //console.log($scope.setTitleVal);
        } else {

        }

    } else {
        $scope.setTitleVal = title

    }
}
    $scope.guestuser = {};
    if ($scope.searchReq.returnDateTime) {
        var arrivalDateTime = $scope.searchRequest.returnDateTime;
    } else {
        var arrivalDateTime = $scope.searchRequest.onwardDateTime;
    }
    var depDateSplt = arrivalDateTime.split('/');
    var minDepDate = new Date(depDateSplt[1] + '/' + depDateSplt[0] + '/' + depDateSplt[2]);
    var dt = daysBetween(new Date(), minDepDate) - 1;
    var chldMinDate = daysBetween(new Date(), minDepDate) + 2;

    // $timeout(function () {
    // $('.adultTrvlr').each(function (index) {
    // 	jQuery(this).attr('id', 'adultTrvlr_' + (index + 1));
    // });
    // $('.childTrvlr').each(function (index) {
    // 	jQuery(this).attr('id', 'childTrvlr_' + (index + 1));
    // });
    // $('.infantTrvlr').each(function (index) {
    // 	jQuery(this).attr('id', 'infantTrvlr_' + (index + 1));
    // });
    // $('.adateOfBirth').each(function (index) {
    // 	jQuery(this).attr('id', 'adateOfBirth' + (index + 1));
    // });
    // $('.cdatepicker').each(function (index) {
    // 	jQuery(this).attr('id', 'cdatepicker' + (index + 1));
    // });
    // $('.idatepicker').each(function (index) {
    // 	jQuery(this).attr('id', 'idatepicker' + (index + 1));
    // });
    var minAdultYear = parseInt(minDepDate.getFullYear()) - 18;

    var maxAdultEndYear = parseInt(minDepDate.getFullYear()) - 76;
    // $('[id*=adateOfBirth]').datepicker({
    // 		dateFormat:'dd-mm-yy',
    // 	singleDatePicker: true,
    // 	showDropdowns: true,
    // 		changeMonth: true,
    // 		changeYear: true, 
    // 		yearRange: '1905:'+(new Date).getFullYear(), 

    // 	startDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),
    // 	minDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate()),
    // 	maxDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),
    // 	endDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate())
    // 	/*  minYear: 1901,
    // 	 maxYear: parseInt(moment().format('YYYY'),10) */
    // }, function (start, end, label) {
    // 	var years = moment().diff(start, 'years');
    // 	$scope.age = years;
    // 	jQuery(this).attr('data-age', years);
    // });
    $scope.adultDOB = {
        opens: "left",
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        autoUpdateInput: true,
        minDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate()),
        maxDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),

        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                $scope.flightop = {
                    singleDatePicker: true //$scope.checkboxModel.value
                }
            }
        }
    }
    var childMaxYear = parseInt(minDepDate.getFullYear());
    var childMinYear = parseInt(minDepDate.getFullYear()) - 18;
    // $('[id*=cdateOfBirth]').datepicker({
    // 		dateFormat:'dd-mm-yy',
    // 	singleDatePicker: true,
    // 	showDropdowns: true,
    // 	changeMonth: true,
    // 		changeYear: true, 
    // 		yearRange: '1905:'+(new Date).getFullYear(), 
    // 	startDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),
    // 	minDate: new Date(childMinYear, minDepDate.getMonth(), minDepDate.getDate()+1),
    // 	maxDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),
    // 	/* minYear: 1901, */
    // 	maxYear: parseInt(moment().format('YYYY'), 10)
    // }, function (start, end, label) {
    // 	var years = moment().diff(start, 'years');

    // 	jQuery(this).attr('data-age', years);
    // });
    $scope.childDOB = {
            opens: "left",
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
            autoUpdateInput: true,
            minDate: new Date(childMinYear, minDepDate.getMonth(), minDepDate.getDate() + 1),
            maxDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),

            eventHandlers: {
                'show.daterangepicker': function(ev, picker) {
                    $scope.flightop = {
                        singleDatePicker: true //$scope.checkboxModel.value
                    }
                }
            }
        }
        // 	$('[id*=idateOfBirth]').daterangepicker({

    // 		singleDatePicker: true,
    // 		showDropdowns: true,
    // 		minYear: 1901,
    // 		maxYear: parseInt(moment().format('YYYY'), 10)
    // 	}, function (start, end, label) {
    // 		var years = moment().diff(start, 'years');

    // 		jQuery(this).attr('data-age', years);
    // 	});

    // }, 2000);

    $scope.infantDOB = {
        opens: "left",
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        autoUpdateInput: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),

        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                $scope.flightop = {
                    singleDatePicker: true //$scope.checkboxModel.value
                }
            }
        }
    }

    $scope.ondatePickerChange = function(evt) {
        // //console.log(evt.adult.personaldata.passExp);
        // date=evt.adult.personaldata.passExp//"27-10-2020"
        // var d = new Date(date);
        // // Get the current month number
        // var m = d.getMonth();
        // // Subtract 6 months
        // d.setMonth(d.getMonth() + 6);
        // // If the new month number isn't m - 6, set to last day of previous month
        // // Allow for cases where m < 6
        // var diff = (m + 12 - d.getMonth()) % 12;
        // if (diff < 6){
        //     alert('within 6 months');
        // }else{
        //     alert('after 6 months');
        // }

        // return d;
    }
    var displayName = {};
    var loginResp = {};
    $scope.userBooking = {}
    $scope.loginAndContinue = function() {
        $scope.backtodetail = true;
        $scope.timeload();
            $("#trvl").addClass("active");
            $scope.validatedata = true;
            $scope.validateguestdata = true;
            $window.scrollTo(0, 0);
        }
        // $scope.proceedGuestLoginDetails = function () {
        //     if (document.getElementById('guestLogin').style.display == 'block') {

    //         if ($scope.guestuser.email != null && $scope.guestuser.contactNumber != null) {
    //             $("#email").parent().removeClass('has-error');
    //             $("#contact").parent().removeClass('has-error');


    //             $scope.validatedata = true;
    //             $scope.validateguestdata = true;
    //         } else {
    //             $("#email").parent().addClass('has-error');
    //             $("#contact").parent().addClass('has-error');

    //         }

    //     } else {
    //         document.getElementById('guestLogin').style.display = 'block';
    //     }

    // }
    $scope.proceedGuestLoginDetails = function (){
		if(document.getElementById('guestLogin').style.display=='block'){
		if($scope.guestuser.email != null && $scope.guestuser.contactNumber != null){
	
	$( "#email" ).parent().removeClass('has-error');	
	$( "#contact" ).parent().removeClass('has-error');	
	$("#ccode1").parent().removeClass('has-error');
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname = $scope.tokenresult.data;
	var headersOptions = {
	  'Content-Type': 'application/json',
	  'Authorization': 'Bearer ' + tokenname.authToken
	}
	$scope.artloading = true;
	var guestLoginData = '{ "username	" : "' + $scope.guestuser.email + '", "mobileNo" : "' +$scope.guestuser.contactNumber+ '"}'

	$http.post(ServerService.authPath+'guest-login/user', '{ "username" : "' + $scope.guestuser.email + '", "mobileNo" : "' +$scope.guestuser.contactNumber+ '"}',{
		headers: headersOptions
	}).then(function(response){
		$scope.artloading = false;
        if (response.data.authToken!=null){
            localStorage.setItem('authentication', JSON.stringify(response));
		if (response.data.alreadyRegistered==false) {
            
			$scope.backtodetail = true;
			$scope.validatedata=true;
			$scope.validateguestdata=true;
			$scope.loggedValue=true;
			$rootScope.bookingHide = true;
			$rootScope.guestLogin = true;
					   $rootScope.bookingHide1 = true; 
				
					localStorage.setItem('loginToken', JSON.stringify(response.data));
					   localStorage.setItem('authentication', JSON.stringify(response));
					   $("#trvl").addClass("active");
					   $rootScope.dispName = $scope.guestuser.email ;
					   sessionStorage.setItem('guestLoginDatails', JSON.stringify( $scope.guestuser.email));
					   
		}else{
			Lobibox.confirm({
			
				msg:'You have already registered, please enter your password to login or select No to Continue as Guest  ?!',
				callback: function ($this, type, ev) {
					if(type == 'yes') {
						$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
						var tokenname =$scope.tokenresult.data;
						var headersOptions = {
							'Content-Type': 'application/json',
							'Authorization': 'Bearer ' + tokenname.authToken
						}
						$http.get(ServerService.listPath + 'crud/list/language',{
							headers: headersOptions
						}).then(function(result){
							if(response.data.alreadyRegistered==true){
								$scope.user.emailId=$scope.guestuser.email;
			
					document.getElementById('loginContinue').style.display='block'
		
					document.getElementById('guestLogin').style.display='none';
							}
						})
			
					}else{


                        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
						var tokenname =$scope.tokenresult.data;
						var headersOptions = {
							'Content-Type': 'application/json',
							'Authorization': 'Bearer ' + tokenname.authToken
						}
						$http.get(ServerService.listPath + 'crud/list/language',{
							headers: headersOptions
						}).then(function(result){
							$scope.backtodetail = true;
						localStorage.isguestlogged = true;
						$scope.validatedata=true;
						$scope.validateguestdata=true;
						$scope.loggedValue=true;
						$rootScope.bookingHide = true;
						$rootScope.guestLogin = true;
								   $rootScope.bookingHide1 = true; 
								 
							
								   $rootScope.dispName = $scope.guestuser.email ;
								   sessionStorage.setItem('guestLoginDatails', JSON.stringify( $scope.guestuser.email));
								   $("#trvl").addClass("active");	
		
						})


					
						
			}
		}
	});
	
}
        }else{
            Lobibox.alert('error', {
                msg: response.data.errorMessage
            });
        }
	
		
	})
	
}else{
	// $( "#email" ).parent().addClass('has-error');	
	// $( "#contact" ).parent().addClass('has-error');	
	// $("#ccode1").parent().addClass('has-error');
}
		}else{
				document.getElementById('guestLogin').style.display='block';
				
				document.getElementById('loginContinue').style.display = 'none';
		
		}
		

}


    /*     $(document).on('change keyup', '.required', function(e) {
            let Disabled = true;
            $(".required").each(function() {
                let value = this.value
                if ((value) && (value.trim() != '')) {
                    Disabled = false
                } else {
                    Disabled = true
                    return false
                }
            });
    
            if (Disabled) {
                $('.toggle-disabled').prop("disabled", true);
            } else {
                $('.toggle-disabled').prop("disabled", false);
            }
        }) */

    $scope.searchRequest = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));

   
    $scope.onCredit = function() {

        // $scope.cardType='CREDIT'
        $scope.msg = "Please click on the below PAY NOW button to continue"
            //  $scope.paynw=""
    }
    $scope.onDebit = function() {

        //$scope.cardType='DEBIT'
        $scope.msg = "Please click on the below PAY NOW button to continue"
    }


    /* contact not start with zero start */
    $("#contact").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    $("#acontact_0").on("keyup", function(e) {
            if ($(this).val() === '0') {
                $(this).val('');
            }
        })
        /* contact not start with zero end */

    /* $scope.totalFare = totalFare; */
    ////console.log(JSON.stringify($scope.searchRequest));
    /**
    @author Raghava Muramreddy
    to showing payment error message
    */
    $scope.goBack = function(path) {
        $location.path(path);
    }
    if (sessionStorage.getItem("paymentError")) {
        var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
        Lobibox.alert('error', {
            msg: paymentError.errorMessage,
            callback: function(lobibox, type) {
                if (type === 'ok') {
                    sessionStorage.removeItem("paymentError");
                }
            }
        });

    }
    /* check user Already login*/
    var loginUser = "";
    $scope.user = {};
    $scope.showSignIn = false;

    var loginCredential;
    $scope.isValidUser = false;
    var booking = {};


    $scope.logindataReview=function(){
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            "g-recaptcha-response" :$scope.token,
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $scope.artloading = true;
        $http.post(ServerService.authPath + 'authenticate', '{ "username" : "' + $scope.user.emailId + '", "password" : "' + $scope.user.password + '"}', { headers: headersOptions }).then(function(response) {
            $scope.loadingsignIn = false;
            $scope.artloading = false;
            // $scope.loggedvalue = true;
            localStorage.islogged = true;
            localStorage.setItem('loginToken', JSON.stringify(response.data));
            localStorage.setItem('authentication', JSON.stringify(response));

            if (response != undefined) {
                var tokennames = response.data;
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokennames.authToken
                }
       

                $http.get(ServerService.authPath + 'user-details', {
                    headers: headersOptions
                }).then(function successCallback(response) {
                    $scope.userId = response.data.userId;

                    sessionStorage.setItem('particulatUserData', JSON.stringify(response));
                    $scope.loggedValue = true;
                    $scope.loginsignIn = true;
                    $scope.$parent.bookingHide = true;
                    $scope.$parent.bookingHide1 = true;
                    $scope.$parent.guestLogin = false;
                    $scope.userBooking.bookingHide = true;
                    $scope.userBooking.bookingHide1 = true;
                    var userBookingdetails = $scope.userBooking;
                    localStorage.setItem('userBookingdetails', JSON.stringify(userBookingdetails));
                    displayName.fname = $scope.user.emailId;
                    //displayName.email = response.data.data.profileData.personaldata.email;
                    //displayName.contactNumber = response.data.data.profileData.personaldata.mobile;
                    //displayName.mobcntrycode = response.data.data.profileData.personaldata.mobcntrycode;
                    localStorage.setItem('loginName', JSON.stringify(displayName))
                    $rootScope.dispName = response.data.sub;

                    // window.location.reload();
                    /* Lobibox.alert('success', {
                    msg: "Login Successfully"
                    }); */
                    $scope.validatedata = true;
                    $scope.validateguestdata = true;
                    $("#trvl").addClass("active");

                }, function errorCallback(response) {
                    if(response.status == 403){
                        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                        status.then(function(greeting) {
                            $scope.logindataReview();
                        });
                    }
                  });
            } else {
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                })
                $scope.showSignIn = true;
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.logindataReview();
                });
            }
            if(response.status == 401){
                Lobibox.alert('error', {
                    msg: "Invalid Login Credential"
                })
                $scope.artloading = false;
            }
          });
    }

    $scope.login = function() {
        $scope.recaptchaId = JSON.parse(localStorage.getItem('configKeys'));
        // $window.scrollTo(0, 0);
        $scope.backtodetail = true;
        if (document.getElementById('loginContinue').style.display == 'block') {

            if ($scope.user.emailId != null && $scope.user.password != null) {
                $scope.loadingsignIn = true;

                var url = 'authenticate';
                var datas = '{ "username " : "' + $scope.user.emailId + '", "password" : "' + $scope.user.password + '"}'
			grecaptcha.ready(function() {
                grecaptcha.execute($scope.recaptchaId.google_recaptcha_public_key, {})
                        .then(function(token) {
                            // sessionStorage.setItem("captcha",token);
                            $scope.token=token;
                            $scope.logindataReview();
                        });
            });
		
        } else {
            if ($("#password").val() != "") {
                $("#password").parent().removeClass('has-error');
                $("#emailId").parent().removeClass('has-error');
            } else {
                $("#password").parent().addClass('has-error');
                $("#emailId").parent().addClass('has-error');
            }
        }

    }else{
        document.getElementById('loginContinue').style.display='block'

		document.getElementById('guestLogin').style.display='none';
	}
}



    function getNationalitydropdwn(){

            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/nationality',

            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $scope.nationalityResult = data;
                $scope.allNationalitys = $scope.nationalityResult.data.data;
                $scope.nationality1 = $scope.allNationalitys;
                $scope.nationality = JSON.stringify($scope.nationality1);
            } else {
                alert('error');
            }

        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.getNationalitydropdwn();
                });
            }
          });
    }
    $scope.allTravellerData = JSON.parse(localStorage.getItem('loginTravellerData'));
    $(document).ready(function() {
        getNationalitydropdwn();
    });

    

              
                         


    // $scope.nationality1 = JSON.parse(localStorage.getItem('nationality')).data;
    // $scope.nationality = JSON.stringify($scope.nationality1);
    // $scope.countryList = JSON.parse(localStorage.getItem('country')).data;
    $scope.bookingRequest = {}
        /**
        //@author Raghava Muramreddy
        Framing Traveller Details ...
        */


    $scope.salutations = ["Mr", "Mrs", "Miss"];
    $scope.childsalutations = ["Miss", "Mstr"];
    $scope.infantsalutations = ["Miss", "Mstr"];
    var personaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.passExp="";
        // this.passExpmonth = "";
        // this.passExpdate = "";
        // this.passExpyear = "";
         this.birthmonth = "";
        this.birthdate = "";
         this.birthyear = "";
    }
    var childPersonaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.passExp="";
        // this.passExpmonth = "";
        // this.passExpdate = "";
        // this.passExpyear = "";
         this.birthmonth = "";
         this.birthdate = "";
         this.birthyear = "";
    }
    var infantPersonaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.passExp="";
        // this.passExpmonth = "";
        // this.passExpdate = "";
        // this.passExpyear = "";

        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
    }

    var traveller = function() {
        this.personaldata = new personaldata();
        /*   this.agntid = 0;
          this.profileid = 0;
          this.travellerRefRPH = 'A1'; */
    }
    var childTraveller = function() {
        this.personaldata = new childPersonaldata();
        /*   this.agntid = 0;
          this.profileid = 0;
          this.travellerRefRPH = 'A1'; */
    }
    var infantTraveller = function() {
        this.personaldata = new infantPersonaldata();
        /*   this.agntid = 0;
          this.profileid = 0;
          this.travellerRefRPH = 'A1'; */
    }
    var travellerByRoom = function() {
        this.adultTravellersList = [];
        this.childTravellersList = [];
        this.infantTravellersList = [];
    }
    var travellerByRoomObj = new travellerByRoom();
    $scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
    $scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))
    $scope.guestLoginName = JSON.parse(sessionStorage.getItem("guestLoginDatails"))
    if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
        $scope.loggedvalue = true;
        $rootScope.bookingHide = true;
        $rootScope.guestLogin = true;
        $rootScope.bookingHide1 = true;
        $rootScope.dispName = $scope.guestLoginName;
    }
    if ($scope.userDetails != null) {
        $scope.user = $scope.userDetails;
    }
    if (localStorage.getItem('loginToken') && localStorage.getItem('loginToken') != "null") {
        $scope.loggedvalue = true;
    }

    if (localStorage.getItem('loginName') && localStorage.getItem('loginName') != "null") {
        $scope.isValidUser = true;
        $scope.loggedvalue = true;
        $rootScope.loginsignIn=true;
        loginUser = JSON.parse(localStorage.getItem('loginName'));
        $scope.user.email = loginUser.email;
        $scope.user.contactNumber = parseInt(loginUser.contactNumber);
        $scope.user.mobcntrycode = loginUser.mobcntrycode;
        $scope.userDetails = JSON.parse(sessionStorage.getItem('userDetails'));
        $scope.isUserCheck = JSON.parse(sessionStorage.getItem('isUserCheck'));


        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];

            for (i = 0; i < $scope.searchRequest.adultCount; i++) {
                var travellerObj = new traveller();
                if (i == 0 && $scope.isUserCheck == true) {
                    travellerByRoomObj.adultTravellersList.push(travellerObj);
                    travellerObj.personaldata = $scope.userDetails;
                } else {
                    travellerByRoomObj.adultTravellersList.push(travellerObj);
                    travellerObj.personaldata.email = $scope.user.email;
                    travellerObj.personaldata.mobile = $scope.user.contactNumber;
                    travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
                }
            }
            for (j = 0; j < $scope.searchRequest.childCount; j++) {
                var travellerObj = new childTraveller();
                travellerByRoomObj.childTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                travellerObj.personaldata.mobile = $scope.user.contactNumber;
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            for (j = 0; j < $scope.searchRequest.infantCount; j++) {
                var travellerObj = new infantTraveller();
                travellerByRoomObj.infantTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                travellerObj.personaldata.mobile = $scope.user.contactNumber;
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            $scope.travellersListByRoom.push(travellerByRoomObj);
        }
    } else {

        /*check user Already login end*/
        //$scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];
            var travellerByRoomObj = new travellerByRoom();
            for (i = 0; i < $scope.searchRequest.adultCount; i++) {
                var travellerObj = new traveller();
                travellerByRoomObj.adultTravellersList.push(travellerObj);
            }
            for (j = 0; j < $scope.searchRequest.childCount; j++) {
                var travellerObj = new childTraveller();
                travellerByRoomObj.childTravellersList.push(travellerObj);
            }
            for (j = 0; j < $scope.searchRequest.infantCount; j++) {
                var travellerObj = new infantTraveller();
                travellerByRoomObj.infantTravellersList.push(travellerObj);
            }
            $scope.travellersListByRoom.push(travellerByRoomObj);
        }
    }
    /**
        Framing Traveller Details end.
    */


    $scope.handler = function(event) {
        //   //console.log("min date", $scope.min);
        var date = new Date($scope.min);

        // var month = date.getMonth() + 7; //months from 1-12
        // var day = date.getDate();
        // var year = date.getFullYear();
        date.setMonth(date.getMonth() + 6);
        // var newDate = year + '-' + month + '-' + day;
        //   //console.log("date", date);
        var selectedDate = event.target.value;
        //  //console.log('selected date', event.target.value);
        var date1 = new Date(selectedDate);
        if (date1 <= date) {
            //	Lobibox.notify('info', { size: 'mini', delay: 1500, msg: "Loreum ipsum asdfa asdfklj asdlkfj; ljadsf" });
            Lobibox.alert('info', //AVAILABLE TYPES: "error", "info", "success", "warning"
                {
                    msg: "Please note, if you travel to any country  other than your passport issued country, then as a general rule passports should have at least six months of validity.Please contact us for more information."
                });
        }
    }


    $scope.selectpaxChecked = function(i,pax,paxAge) {
		document.getElementById('checkedvalue'+i).checked = true;
		$scope.addTravel('true',pax,paxAge,i);
	}
		
	document.querySelector(".guestmobilenumm").addEventListener("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });

    
    /* userChange function */
    $scope.travelby = {}
    var adultCount = $scope.travellersListByRoom[0].adultTravellersList.length;
    var chaildCount = $scope.travellersListByRoom[0].childTravellersList.length;
    var infantCount = $scope.travellersListByRoom[0].infantTravellersList.length;

    $scope.addTravel = function(checked, singlePsgr, age, indexvalue) {
        let inputs = document.getElementById('checkedvalue'+indexvalue);
        $scope.particularindexvalue=indexvalue;
        $scope.checked=inputs.checked
        //  $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
        $scope.usersObj = {};
        $scope.usersObj.salutation = singlePsgr.salutation;
        $scope.usersObj.fname = singlePsgr.firstName
        $scope.usersObj.lname = singlePsgr.lastName
        $scope.usersObj.customerId = singlePsgr.customerId
        
        var unwantedCharacter = "0";
        var dob=singlePsgr.dateOfBirth.split("/");
        var birthdate=dob[0];
        if( birthdate.charAt( 0 ) === unwantedCharacter ){
            birthdate = birthdate.slice( 1 );

        }
        var birthmonth=dob[1];
        
       
        if( birthmonth.charAt( 0 ) === unwantedCharacter ){
            birthmonth = birthmonth.slice( 1 );

        }
        $scope.usersObj.birthdate=birthdate;

        $scope.usersObj.birthmonth=birthmonth;

        $scope.usersObj.birthyear=dob[2];
       var  mobcntrycodeObj=[];
       if(singlePsgr.countryCode === '' || singlePsgr.countryCode === null || singlePsgr.countryCode === undefined){
        $scope.usersObj.mobcntrycodetrobj = singlePsgr.countryCode;
    }else{
        for(var c=0; c<$scope.countryList.length; c++){
            if($scope.countryList[c].name==singlePsgr.countryCode){
                 mobcntrycodeObj.push($scope.countryList[c]);
            }
        }
        $scope.usersObj.mobcntrycodetrobj = mobcntrycodeObj[0];
    }
        //  var mobcntrycodeObj=($scope.countryList.findIndex(a => a.name ===singlePsgr.countryCode))
 
      
         
        $scope.usersObj.mobile = singlePsgr.phoneNumber
        $scope.usersObj.email = singlePsgr.emailId

        $scope.usersObj.nationalityname = singlePsgr.nationality
        $scope.usersObj.passportNo = singlePsgr.passportNo
        if(singlePsgr.passportExpiryDate!=null){
        var passExp=singlePsgr.passportExpiryDate.split("/");
        var expirydate=passExp[0];
        if( expirydate.charAt( 0 ) === unwantedCharacter ){
            expirydate = expirydate.slice( 1 );

        }
        var expMonth = passExp[1];
       
        if( expMonth.charAt( 0 ) === unwantedCharacter ){
            expMonth = expMonth.slice( 1 );

        }
        $scope.usersObj.expirydate=expirydate
        $scope.usersObj.expirymonth=expMonth;
        $scope.usersObj.expiryyear=passExp[2];
    }
        $scope.usersObj.nationalitycode =singlePsgr.saudiNationalityId
        $scope.usersObj.passIssueGovt =singlePsgr.passportIssueGovt
            //  var adultCount=$scope.travellersListByRoom[0].adultTravellersList.length;
        if (age >= 12) {
            $scope.usersObj.clsname = 'adultage';
            $scope.usersObj.trvtype = 'ADT';
            if ( $scope.checked) {
                if (adultCount != 0) {
                    $scope.usersObj=$scope.usersObj;
                   
                } else {
                  
                }








            } else {
                if (adultCount != $scope.travellersListByRoom[0].adultTravellersList.length)
                    adultCount++
                    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = "";

            }
        } else if (age >= 2 && age < 12) {
            $scope.usersObj.trvtype = 'CNN';
            if ( $scope.checked) {
                if (chaildCount != 0) {
                    /* chaildCount-- */
                    $scope.childusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.usersObj


                  
                } else {
                   
                }








            } else {
                if (chaildCount != $scope.travellersListByRoom[0].childTravellersList.length)
                    chaildCount++
                    $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = "";

            }


        } else {
            $scope.usersObj.trvtype = 'INF';
            if ( $scope.checked) {
                if (infantCount != 0) {
                    /* chaildCount-- */
                    $scope.infantusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.usersObj


                    // Lobibox.notify('success', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
                    //     delay: 1500
                    // });
                } else {
                    // Lobibox.notify('error', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: "Infant limit Reached!",
                    //     delay: 1500
                    // });
                }








            } else {
                if (infantCount != $scope.travellersListByRoom[0].infantTravellersList.length)
                infantCount++
                    $scope.travellersListByRoom[0].infantTravellersList[$scope.infantIndex].personaldata = "";

            }
        }

    }

    $scope.ConfirmTravellerData=function(){
        if($scope.TravelType=='adult'){
if($scope.TravelType=='adult' && $scope.usersObj!=undefined &&  $scope.checked==true){
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = $scope.usersObj


    Lobibox.notify('success', {
        size: 'mini',
        position: 'top right',
        msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
        delay: 1500
    });
    $scope.modalInstance.dismiss();
}else{
    Lobibox.notify('error', {
        size: 'mini',
        position: 'top right',
        msg: "Please select at least one traveller!",
        delay: 1500
    });
}
        } else if($scope.TravelType=='child'){
            if($scope.TravelType=='child' && $scope.childusersObj!=undefined &&  $scope.checked==true){
                $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.childusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.childusersObj.fname + " " + $scope.childusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg: "Please select at least one traveller!",
                    delay: 1500
                });
            }
        }else{
            if($scope.TravelType=='infant' && $scope.infantusersObj!=undefined &&  $scope.checked==true){
                $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.infantusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.infantusersObj.fname + " " + $scope.infantusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg: "Please select at least one traveller!",
                    delay: 1500
                });
            }
        }

    }



                   
    $scope.infantsearchTrvlr = function(infantindexvalue,travelType) {
        $scope.modalInstance=$uibModal.open({
            templateUrl: 'myTestModal.tmpl.html',
            backdrop: 'static',
			keyboard: true,
            scope:$scope
        });
        $scope.infantusersObj=undefined;
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
         
            $scope.allTravellerData=[];
            $scope.infantIndex=infantindexvalue;
            $scope.TravelType=travelType;
                     $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                     for(var t=0;  t<$scope.allTravellerDataa.length; t++){
    if( $scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age < 2){
        $scope.allTravellerData.push($scope.allTravellerDataa[t])
    }
                     }
                     if($scope.allTravellerData.length==0) {
                        $scope.childText=" Traveller Data matching the age criteria given is not available."
                    }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.infantsearchTrvlr();
                });
            }
          });
    
                };
    $scope.adultsearchTrvlr=function(adultindexvalue,travelType){
        $scope.modalInstance=$uibModal.open({
            templateUrl: 'myTestModal.tmpl.html',
            backdrop: 'static',
			keyboard: true,
            scope:$scope
        });
        $scope.usersObj=undefined;
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
            $scope.allTravellerData=[];
            $scope.TravelType=travelType;
            $scope.adultIndex=adultindexvalue;
                 $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                     for(var t=0;  t<$scope.allTravellerDataa.length; t++){
    if( $scope.allTravellerDataa[t].age>= 12){
        $scope.allTravellerData.push($scope.allTravellerDataa[t])
    }
                     }
                     if($scope.allTravellerData.length==0) {
                        $scope.childText=" Traveller Data matching the age criteria given is not available."
                    }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.adultsearchTrvlr();
                });
            }
          });
    
    }




    $scope.childsearchTrvlr = function(childindexvalue,travelType) {
        $scope.modalInstance=$uibModal.open({
            templateUrl: 'myTestModal.tmpl.html',
            backdrop: 'static',
			keyboard: true,
            scope:$scope
        });
        $scope.childusersObj=undefined ;
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
            $scope.allTravellerData=[];
        $scope.childIndex=childindexvalue;
        $scope.TravelType=travelType;
                 $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                 for(var t=0;  t<$scope.allTravellerDataa.length; t++){
 if($scope.allTravellerDataa[t].age >= 2 && $scope.allTravellerDataa[t].age < 12){
    $scope.allTravellerData.push($scope.allTravellerDataa[t])
}
                 }
                if($scope.allTravellerData.length==0) {
                    $scope.childText="Traveller Data matching the age criteria given is not available."
                }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.childsearchTrvlr();
                });
            }
          });
      
    }

    
    $scope.close=function(){
        $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
    };
    /* if(sessionStorage.getItem('userDetails') != null){
    $scope.isUserCheck = JSON.parse(sessionStorage.getItem('isUserCheck'));
if($scope.isUserCheck == true){
    $scope.travellersListByRoom = []
    var travellerByRoomObj=new travellerByRoom();
    $scope.userDetails = JSON.parse(sessionStorage.getItem('userDetails'));
    var travellerObj = new traveller() ;
                    	
                for(var a=0; a<$scope.travellersListByRoom.length; a++){
                        if(a==0){
                            travellerByRoomObj.adultTravellersList[a] = travellerObj;
                    travellerObj.personaldata = $scope.userDetails
                    $scope.travellersListByRoom[a].push(travellerByRoomObj);
                        } else {
                            $scope.travellersListByRoom.push(travellerByRoomObj);
                        }
                    }
                	
                	
        }
	
}  */


    /* end */


    /*assign guestUserDetails to Travellers */
    $scope.updateGustUserDetailsToTravller = function() {
       // $scope.travellersListByRoom.forEach(function(travellersList) {
            //     var adultTraveller = travellersList.adultTravellersList[0];
            //     adultTraveller.personaldata.email = jQuery('#email').val(); //$scope.user.email ;
            //     adultTraveller.personaldata.mobile = $scope.guestuser.contactNumber;
            //     adultTraveller.personaldata.mobcntrycode = $scope.guestuser.ccode;

            //     // adultTraveller.personaldata.mobcntrycode = jQuery('#userCountryCode').val();

            // })
            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = jQuery('#email').val();
            if ($scope.user.emailId) {

                $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = $scope.user.emailId;

            }
            // $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.fname=jQuery('#fname').val();
            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;
            //  adultTraveller.personaldata.mobcntrycode = $scope.guestuser.ccode;

            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
            // $scope.travellersListByRoom.forEach(function(travellersList) {
            //     travellersList.adultTravellersList.forEach(function(adultTraveller) {
            //         adultTraveller.personaldata.email = jQuery('#email').val(); //$scope.user.email ;
            //         adultTraveller.personaldata.mobile = $scope.guestuser.contactNumber;
            //         adultTraveller.personaldata.mobcntrycode = jQuery('#userCountryCode').val();
            //     });
            // })
            // $scope.travellersListByRoom.forEach(function(travellersList) {
            //     travellersList.childTravellersList.forEach(function(childTravellers) {
            //         childTravellers.personaldata.email = jQuery('#email').val(); //$scope.user.email ;
            //         childTravellers.personaldata.mobile = $scope.guestuser.contactNumber;
            //         childTravellers.personaldata.mobcntrycode = jQuery('#userCountryCode').val();
            //     });
            // })
            // $scope.travellersListByRoom.forEach(function(travellersList) {
            //     travellersList.infantTravellersList.forEach(function(infantTravellers) {
            //         infantTravellers.personaldata.email = jQuery('#email').val(); //$scope.user.email ;
            //         infantTravellers.personaldata.mobile = $scope.guestuser.contactNumber;
            //         infantTravellers.personaldata.mobcntrycode = jQuery('#userCountryCode').val();
            //     });
            // })
        }
        /*assign guestUserDetails to Travellers end*/





    /* signIn function */
    $scope.showSignInText = "Sign in to book faster";
    $scope.showSignIn = false;
    $scope.signIn = function() {
        $scope.showSignIn = $scope.showSignIn ? false : true;
        var buttonText = "Sign in to book faster";
        if ($scope.showSignIn == true) { var buttonText = "For Continue As Guest"; } else { var buttonText = "Sign in to book faster"; }

        $scope.showSignInText = buttonText;
    }

    /* end */

    /* AgencyTermsOfBusiness*/
    $scope.openAgencyTermsOfBusiness = function(size, parentSelector) {

        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'agencyTermsOfBusiness.html',
            controller: 'flightFareRulesContent',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    /* AgencyTermsOfBusiness End*/

    /* multiple date */
    /* 	var arrivalDateTime = $scope.searchRequest.travelInsurance.arrivalDateTime;
        var depDateSplt = arrivalDateTime.split('/');
        var minDepDate = new Date(depDateSplt[1] + '/' + depDateSplt[0] + '/' + depDateSplt[2]);
        var dt = daysBetween(new Date(), minDepDate) - 1;
        var chldMinDate = daysBetween(new Date(), minDepDate) + 2;

        $timeout(function () {
            $('.adultTrvlr').each(function (index) {
                jQuery(this).attr('id', 'adultTrvlr_' + (index + 1));
            });
            $('.childTrvlr').each(function (index) {
                jQuery(this).attr('id', 'childTrvlr_' + (index + 1));
            });
            $('.infantTrvlr').each(function (index) {
                jQuery(this).attr('id', 'infantTrvlr_' + (index + 1));
            });
            $('.adateOfBirth').each(function (index) {
                jQuery(this).attr('id', 'adateOfBirth' + (index + 1));
            });
            $('.cdatepicker').each(function (index) {
                jQuery(this).attr('id', 'cdatepicker' + (index + 1));
            });
            $('.idatepicker').each(function (index) {
                jQuery(this).attr('id', 'idatepicker' + (index + 1));
            });
            var minAdultYear = parseInt(minDepDate.getFullYear()) - 24;

            var maxAdultEndYear = parseInt(minDepDate.getFullYear()) - 76;
            $('[id*=adateOfBirth]').datepicker({
                    dateFormat:'dd-mm-yy',
                singleDatePicker: true,
                showDropdowns: true,
                    changeMonth: true,
                    changeYear: true, 
                    yearRange: '1905:'+(new Date).getFullYear(), 
                	
                startDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),
                minDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate()),
                maxDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),
                endDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate())
        	
            }, function (start, end, label) {
                var years = moment().diff(start, 'years');
                $scope.age = years;
                jQuery(this).attr('data-age', years);
            });
            var childMaxYear = parseInt(minDepDate.getFullYear());
            var childMinYear = parseInt(minDepDate.getFullYear()) - 24;
            $('[id*=cdateOfBirth]').datepicker({
                    dateFormat:'dd-mm-yy',
                singleDatePicker: true,
                showDropdowns: true,
                changeMonth: true,
                    changeYear: true, 
                    yearRange: '1905:'+(new Date).getFullYear(), 
                startDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),
                minDate: new Date(childMinYear, minDepDate.getMonth(), minDepDate.getDate()+1),
                maxDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),
    	
                maxYear: parseInt(moment().format('YYYY'), 10)
            }, function (start, end, label) {
                var years = moment().diff(start, 'years');

                jQuery(this).attr('data-age', years);
            });

            $('[id*=idateOfBirth]').daterangepicker({

                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'), 10)
            }, function (start, end, label) {
                var years = moment().diff(start, 'years');

                jQuery(this).attr('data-age', years);
            });

        }, 2000);
     

    /* end */


    $scope.status = {
        isFirstOpen: true
    };

    $scope.getNumber = function(num) {
        return new Array(num);
    };

    $scope.traveller = {}
        /*     $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            $scope.format = 'dd-MMMM-yyyy';
            
            $scope.dates = [{
                date: '01-05-2001'
            }, {
                date: '05-05-2014'
            }]; */


    $scope.open = function($event, dt) {
        $event.preventDefault();
        $event.stopPropagation();
        dt.opened = true;
    };

    /* passengerHits function */
    $scope.paymentDetail = {}
    var expireDate = {}


    $scope.validateCardDetails = function() {
        var flage = true;
        if ($scope.paymentDetail.cardNo == null) {
            $('#cardNo').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#cardNo').css({ 'background-color': '' });
        }
        if ($scope.paymentDetail.expireMonth == null) {
            $('#expiremonth').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#expiremonth').css({ 'background-color': '' });
        }
        if ($scope.paymentDetail.expireYear == null) {
            $('#expireYrs').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#expireYrs').css({ 'background-color': '' });
        }
        if ($scope.paymentDetail.cvv == null) {
            $('#cardName').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#cardName').css({ 'background-color': '' });
        }

        if ($scope.paymentDetail.cardName == null) {
            $('#cardNo').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#cardNo').css({ 'background-color': '' });
        }
        return flage;
    }

    $scope.travellerToLoginPage = function() {
        // $scope.timeload();
$scope.backtodetail=false;
        $("#trvl").removeClass("active");
        $scope.validatedata = false;
        $scope.validateguestdata = false;
        $scope.loggedvalue = true;
        $("#serv").removeClass("active");
        $scope.validateservicedatapament=true;
    }

    $scope.billingToTravellerDetails = function() {
        // $scope.timeload();

        $("#trvl").addClass("active");
        $scope.validatedata = true;
        $scope.validateguestdata = true;
        $scope.loggedvalue = true;
        $("#serv").removeClass("active");
        $scope.validateservicedatapament=true;
    }

    $scope.backdetail=function() {
        $scope.timeload();
        $("#trvl").removeClass("active");
        $scope.backtodetail = false;
        $scope.backtotraveler = false;
        $scope.validateguestdata = false;
        // $scope.validateservicedatapament = true;
        $scope.validatedata = false;
        $scope.loggedvalue = true;

    }
    $scope.backtraveler=function() {
        $scope.timeload();
        $("#serv").removeClass("active");
        $scope.backtodetail = true;
        $scope.backtotraveler = false;
        $scope.validateservicedatapament = true;
        $scope.validatedata = true;


    }

    $scope.pamentToTraveller = function() {
        // $scope.timeload();
        $("#serv").removeClass("active");
        $("#trvl").addClass("active");

        $scope.validateservicedata = false;
        $scope.validatedata = true;
        $scope.validateguestdata = true;
        $scope.validateservicedatapament = true;

    }


    function insurancePurchaseRQ(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));

            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }

        $scope.artloading = true;
        localStorage.setItem('insurance/purchase', JSON.stringify($scope.bookingRequest))
        $http.post(ServerService.serverPath + 'insurance/purchase', $scope.bookingRequest, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.artloading = false;
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.status == 'success') {
                /*	window.location.href = response.data.returnUrl; */

                $scope.artloading = false;
                //    //console.log("$scope.bookingRequest,", $scope.bookingRequest)



                // sessionStorage.setItem("bookingResponse", JSON.stringify(response.data));


                var data = response.data;
                var expiry_date = $scope.paymentDetail.expireYear + $scope.paymentDetail.expireMonth
                    // $('#hdn_access_code').attr('name', 'access_code');
                    // $('#hdn_access_code').attr('value', data.data.access_code);
                    // $('#hdn_language').attr('name', 'language');
                    // $('#hdn_language').attr('value', data.data.language);
                    // $('#hdn_merchant_id').attr('name', 'merchant_identifier');
                    // $('#hdn_merchant_id').attr('value', data.data.merchant_identifier);
                    // $('#hdn_merchant_ref').attr('name', 'merchant_reference');
                    // $('#hdn_merchant_ref').attr('value', data.data.merchant_reference);
                    // $('#hdn_merchant_ref').attr('value', data.data.merchant_reference);
                    // $('#hdn_return_url').attr('name', 'return_url');
                    // $('#hdn_return_url').attr('value', data.data.return_url);
                    // $('#hdn_serv_command').attr('name', 'service_command');
                    // $('#hdn_serv_command').attr('value', data.data.service_command);
                    // $('#hdn_signature').attr('name', 'signature');
                    // $('#hdn_signature').attr('value', data.data.signature);
                    // $('#hdn_tok_name').attr('name', 'token_name');
                    // $('#hdn_tok_name').attr('value', data.data.token_name);

                // $('#hdn_card_security_code').attr('name', 'card_security_code');
                // $('#hdn_card_security_code').attr('value', $scope.paymentDetail.cvv);
                // $('#hdn_card_holder_name').attr('name', 'card_holder_name');
                // $('#hdn_card_holder_name').attr('value', $scope.paymentDetail.cardName);
                // $('#hdn_card_number').attr('name', 'card_number');
                // $('#hdn_card_number').attr('value', $scope.paymentDetail.cardNo);
                // $('#hdn_expiry_date').attr('name', 'expiry_date');
                // $('#hdn_expiry_date').attr('value', expiry_date);
                // $('#paymentPage').attr('action', data.data.hostedPageLink);
                // $('#paymentPage').submit();
                window.open(data.data.hostedPageLink, "_self")

            } else {
                $scope.artloading = false;


                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
                // } else {

                //     Lobibox.alert('error', {
                //         msg: 'Unexpected Error Occurred, Please Try Again Later'
                //     });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.getNationalitydropdwn();
                });
            }
          });
    }
    $scope.insurancePayment = function(bookingWay, valid, insurevalue) {



            // if($scope.cardType==='CREDIT'){
            // 	valid=$scope.validateCardDetails();

            // }
            
            var isChecked = $('#termsCheckbox').prop('checked');
            $scope.submitted = true;
            var passengerform=true;
            if (valid) {
                $scope.timeload();
                // $scope.artloading = true;
                $scope.errMsg = "";
                $window.scrollTo(0, 0);
                $scope.backtodetail = false;
                $scope.backtotraveler = true;
                $scope.paymentDetail.firstName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.fname;
                $scope.paymentDetail.lastName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.lname;
                $scope.paymentDetail.emailAddress = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email;
                $scope.paymentDetail.countryCode = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname;
                
                if (isChecked == false) {

                    $scope.termsError = true;

                    return false;
                }else if(passengerform==true){
                    $scope.totaladultdata=[];
                    $scope.totalchailddata=[];
                    $scope.totalInfantdata=[];
                    $scope.travelList=$scope.travellersListByRoom;
                    for(var k=0; k<$scope.travelList.length; k++){
                    for(var m=0; m<$scope.travelList[k].adultTravellersList.length; m++){		
                    $scope.totaladultdata.push($scope.travelList[k].adultTravellersList[m]);
                    }
                    for(var n=0; n<$scope.travelList[k].childTravellersList.length; n++){
                    $scope.totalchailddata.push($scope.travelList[k].childTravellersList[n]);
                    }
                    for(var i=0; i<$scope.travelList[k].infantTravellersList.length; i++){
                        $scope.totalInfantdata.push($scope.travelList[k].infantTravellersList[i]);
                        }
                    }
                    /* $scope.totaladultdata=$scope.travelList[0].adultTravellersList;
                    $scope.totalchailddata=$scope.travelList[0].childTravellersList; */
                    $scope.totalpassengers = $scope.totaladultdata.concat($scope.totalchailddata).concat($scope.totalInfantdata);
                    // $scope.totalpassengers=$scope.totalpassenger.concat($scope.totalInfantdata);
            //@author Raghava start...
            for(var i=0;i<$scope.totalpassengers.length; i++){
                var count=0;
                var outerFirstName=$scope.totalpassengers[i].personaldata.fname;
            var outerPassport=$scope.totalpassengers[i].personaldata.passportNo;
                for(var j=0;j<$scope.totalpassengers.length; j++){
                    var innerFirstName=$scope.totalpassengers[j].personaldata.fname;
                    var innerPassport=$scope.totalpassengers[j].personaldata.passportNo;
            if(innerFirstName===outerFirstName||innerPassport===outerPassport){
            count++;
            if(count>1){
                Lobibox.alert('error',
                {
                    msg: "Guest Details are duplicated.Please check your Passport Number."
                });
            
                // alert("duplicate Traveller Found.")
            
                return;
            }
            }
            
            
                }
            }
                }
                if (insurevalue == false) {
                    $("#serv").addClass("active");

                    $scope.validateservicedata = true;
                    $scope.validatedata = false;
                    $scope.validateguestdata = true;
                    $scope.validateservicedatapament = false;

                    /* $scope.submitted = true; */
                    return false;
                }
                $scope.loading = true;
                $scope.disabled = true;
                $scope.errMsg = "";

                var cardId = $('#cardNo').val();

                $scope.travellerdatalist = [];
                expireDate = $scope.paymentDetail.expireYear + $scope.paymentDetail.expireMonth
                sessionStorage.setItem("contactDetails", JSON.stringify($scope.user))
                    /* passengerList */
                $('.adultTrvlr').each(function(index) {
                    jQuery(this).attr('id', 'adultTrvlr_' + (index + 1));
                });

                $('.childTrvlr').each(function(index) {
                    jQuery(this).attr('id', 'childTrvlr_' + (index + 1));
                });
                $('.infantTrvlr').each(function(index) {
                    jQuery(this).attr('id', 'infantTrvlr_' + (index + 1));
                });

                $('.travellers').each(function(ind) {
                    var id = $(this).attr('id');
                    //   //console.log(id);
                    var passId = id.split('_')
                    if (passId[0] == 'adultTrvlr') {
                        var salutationVal = $(this).find('#salutation_' + ind).val();
                        var fname = $(this).find('#afname_' + ind).val();
                        var lnameVal = $(this).find('#alname_' + ind).val();
                        var datepickerVal = new Date($(this).find('#adateOfBirth_' + ind).val()).toLocaleDateString();;

                        var c_codeVal = $(this).find('#acontact_' + ind).val();
                        var c_noVal = $(this).find('#aphone_' + ind).val();
                        var emailid = $(this).find('#amail_' + ind).val();
                        var nationalityVal = $(this).find('#anationalityVal_' + ind).val();
                        var passport = $(this).find('#apassport_' + ind).val();

                    } else if (passId[0] == 'childTrvlr') {
                        var salutationVal = $(this).find('.csalutation').val();
                        //var salutation = $(this).find('.salutation').val();
                        var fname = $(this).find('.cfname').val();
                        var lnameVal = $(this).find('.clname').val();
                        var datepickerVal = new Date($(this).find('.cdateOfBirth').val()).toLocaleDateString();;
                        var c_codeVal = $(this).find('.ccontact').val();
                        var c_noVal = $(this).find('.cphone').val();
                        var emailid = $(this).find('.cmail').val();
                        var nationalityVal = $(this).find('.cnation').val();
                        var passport = $(this).find('.cpassport').val();
                    } else if (passId[0] == 'infantTrvlr') {
                        var salutationVal = $(this).find('.isalutation').val();
                        //var salutation = $(this).find('.salutation').val();
                        var fname = $(this).find('.ifname').val();
                        var lnameVal = $(this).find('.ilname').val();
                        var datepickerVal = new Date($(this).find('.idateOfBirth').val()).toLocaleDateString();;
                        var c_codeVal = $(this).find('.icontact').val();
                        var c_noVal = $(this).find('.imail').val();
                        var emailid = $(this).find('.imail').val();
                        var nationalityVal = $(this).find('.ination').val();
                        var passport = $(this).find('.ipassport').val();
                    }
                    var personalObj = new Object();
                    var travellerdataObj = new Object();
                    if (salutationVal.toLowerCase() == 'mr' || salutationVal.toLowerCase()=='mstr') {
                        personalObj.gender = 'M'
                    } else {
                        personalObj.gender = 'F'
                    }
                    personalObj.salutation = salutationVal;
                    personalObj.fname = fname;
                    personalObj.lname = lnameVal;
                    personalObj.dob = datepickerVal;
                    personalObj.mobcntrycode = c_codeVal;
                    personalObj.mobile = c_noVal;
                    personalObj.trvtype = 'ADT';
                    personalObj.email = emailid;
                    personalObj.nationalitycode = nationalityVal;
                    personalObj.passportNo = passport;

                    travellerdataObj.personaldata = personalObj;

                    $scope.travellerdatalist.push(travellerdataObj)
                })



                for (var a = 0; a < $scope.travellersListByRoom[0].adultTravellersList.length; a++) {
                    if ($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.nationalitycode == undefined) {
                        $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.nationalityname
                            // $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.trvtype= "ADT";
                            // $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.passExp=$scope.travellersListByRoom[0].adultTravellersList[a].personaldata.passExp.toLocaleDateString();
                    }
                    $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.trvtype = "ADT";
                    $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.mobcntrycode = $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.mobcntrycodetrobj.name;

                     var adultPassExp = new Date($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.passExp).toLocaleDateString();
                    $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.passExp = ($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.expirydate).trim()+"/"+($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.expirymonth).trim()+"/"+($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.expiryyear).trim();
                    // var adultDob = new Date($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.dob).toLocaleDateString();
                    $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.dob =($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthdate).trim()+"/"+($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthmonth).trim()+"/"+($scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthyear).trim();
                    // adultDob.split('/')[1] + '/' + adultDob.split('/')[0] + '/' + adultDob.split('/')[2];
                    if ( $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.salutation.toLowerCase() == 'mr' ||  $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.salutation.toLowerCase()=='mstr') {
                        $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.gender = 'M'
                    } else {
                        $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.gender = 'F'
                    }
                    // delete $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthdate;
                    // delete $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthmonth;
                    // delete $scope.travellersListByRoom[0].adultTravellersList[a].personaldata.birthyear;

                }

                for (var a = 0; a < $scope.travellersListByRoom[0].childTravellersList.length; a++) {
                    if ($scope.travellersListByRoom[0].childTravellersList[a].personaldata.nationalitycode == undefined) {
                        $scope.travellersListByRoom[0].childTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[0].childTravellersList[a].personaldata.nationalityname;

                    }
                    $scope.travellersListByRoom[0].childTravellersList[a].personaldata.trvtype = "CNN";
                    if( $scope.travellersListByRoom[0].childTravellersList[a].personaldata.mobcntrycode==undefined || $scope.travellersListByRoom[0].childTravellersList[a].personaldata.mobcntrycode=='' || $scope.travellersListByRoom[0].childTravellersList[a].personaldata.mobcntrycode==null){
                        $scope.travellersListByRoom[0].childTravellersList[a].personaldata.mobcntrycode=null; 
                    }else{
                        $scope.travellersListByRoom[0].childTravellersList[a].personaldata.mobcntrycode = $scope.travellersListByRoom[0].childTravellersList[a].personaldata.mobcntrycodetrobj.name;
                    }
                   

                    var chaildPassExp = new Date($scope.travellersListByRoom[0].childTravellersList[a].personaldata.passExp).toLocaleDateString();
                    $scope.travellersListByRoom[0].childTravellersList[a].personaldata.passExp =($scope.travellersListByRoom[0].childTravellersList[a].personaldata.expirydate).trim()+"/"+($scope.travellersListByRoom[0].childTravellersList[a].personaldata.expirymonth).trim()+"/"+($scope.travellersListByRoom[0].childTravellersList[a].personaldata.expiryyear).trim();
                    // var childDob=new Date($scope.travellersListByRoom[0].childTravellersList[a].personaldata.dob).toLocaleDateString();
                    $scope.travellersListByRoom[0].childTravellersList[a].personaldata.dob =($scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthdate).trim()+"/"+($scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthmonth).trim()+"/"+($scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthyear).trim();
                    // childDob.split('/')[1]+'/'+childDob.split('/')[0]+'/'+childDob.split('/')[2];
                    if ( $scope.travellersListByRoom[0].childTravellersList[a].personaldata.salutation.toLowerCase() == 'mr' ||  $scope.travellersListByRoom[0].childTravellersList[a].personaldata.salutation.toLowerCase()=='mstr') {
                        $scope.travellersListByRoom[0].childTravellersList[a].personaldata.gender = 'M'
                    } else {
                        $scope.travellersListByRoom[0].childTravellersList[a].personaldata.gender = 'F'
                    }
                    // delete $scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthdate;
                    // delete $scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthmonth;
                    // delete $scope.travellersListByRoom[0].childTravellersList[a].personaldata.birthyear;
                }
                for (var a = 0; a < $scope.travellersListByRoom[0].infantTravellersList.length; a++) {
                    if ($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.nationalitycode == undefined) {
                        $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.nationalityname;

                    }
                    $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.trvtype = "INF";

                    if( $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycode==undefined ||$scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycode=='' ||$scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycode==null){
                        $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycode=null; 
                    }else{
                        $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycode = $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycodetrobj.name;
                    }

                    // $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycode = $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.mobcntrycodetrobj.name;

                    var infantPassExp = new Date($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.passExp).toLocaleDateString();
                    $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.passExp = ($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.expirydate).trim()+"/"+($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.expirymonth).trim()+"/"+($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.expiryyear).trim();
                    // var infantDOB=new Date($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.dob).toLocaleDateString();
                    $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.dob = ($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.birthdate).trim() + "/" + ($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.birthmonth).trim() + "/" + ($scope.travellersListByRoom[0].infantTravellersList[a].personaldata.birthyear).trim();
                    // infantDOB.split('/')[1]+'/'+infantDOB.split('/')[0]+'/'+infantDOB.split('/')[2];
                    // delete $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.birthdate;
                    // delete $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.birthmonth;
                    // delete $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.birthyear;
                    if ( $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.salutation.toLowerCase() == 'mr' ||  $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.salutation.toLowerCase()=='mstr') {
                        $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.gender = 'M'
                    } else {
                        $scope.travellersListByRoom[0].infantTravellersList[a].personaldata.gender = 'F'
                    }
                
                }
                $scope.passengerInfos = [];
                $scope.bookingRequest.sessionId = JSON.parse(sessionStorage.getItem('InsuranceSearchResult')).sessionId,
                    $scope.bookingRequest.paymentMode = "PAYMENT";
                $scope.bookingRequest.paymentParameters = $scope.paymentDetail

                // $scope.bookingRequest.paymentParameters.successUrl = "http://3.6.54.119:8091/art/paymentRedirector.html",
                //     $scope.bookingRequest.paymentParameters.failureUrl = "http://3.6.54.119:8091/art/paymentRedirector.html",
                //     $scope.bookingRequest.paymentParameters.cancelUrl = "http://3.6.54.119:8091/art/#!/insuranceReview"
              
                if ($scope.selectedAvilplan) {
                    // $scope.bookingRequest.travelPlanId = Number($scope.selectedAvilplan.sessionId.split('#')[$scope.selectedAvilplan.sessionId.split('#').length - 1]);
                    $scope.bookingRequest.travelPlanId = $scope.selectedAvilplan.sessionId;


                    
                }else{
                    $scope.bookingRequest.travelPlanId="";
                }


                $scope.bookingRequest.paymentParameters.successUrl = "paymentRedirector.html",
                    $scope.bookingRequest.paymentParameters.failureUrl = "paymentRedirector.html",
                    $scope.bookingRequest.paymentParameters.cancelUrl = "#!/insuranceReview",


                  
                  
                  
                $scope.tempRiderPlans = [];
                if ($scope.selectedRidersplan) {
                    for (var ride = 0; ride < $scope.selectedRidersplan.length; ride++) {
                        // $scope.tempRiderPlans[ride] = Number($scope.selectedRidersplan[ride].sessionId.split('#').length - 1);
                        $scope.tempRiderPlans[ride] =$scope.selectedRidersplan[ride].sessionId;


                    }
                }

                $scope.bookingRequest.riderPlanIds =  $scope.tempRiderPlans;
                $scope.bookingRequest.passengerInfos = $scope.travellersListByRoom[0].adultTravellersList.concat($scope.travellersListByRoom[0].childTravellersList).concat($scope.travellersListByRoom[0].infantTravellersList);
                sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom));
                insurancePurchaseRQ();
            } else {
                $scope.artloading = false;


                $('html,body').scrollTop(240);
                $scope.errMsg = "Enter All * (Mandatory) Fields";
            }
            $scope.traveller.open = true;
            $scope.artloading = false;
        }
        /* end */

    /* payNow function */



    $scope.payment = {}
    $scope.message = false;
    $scope.showCard = false;
    $scope.payNow = function() {
            var cardId = $('#cardNo').val();
            var firstDigit = cardId.charAt(0)
            var secondDigit = cardId.charAt(1)
            if (firstDigit == 3 && between(secondDigit, 4, 7) && cardId.length == 5) {
                $scope.path = 'american';
                $scope.message = false;
                $scope.showCard = true;
            } else if (firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length == 5) {
                $scope.path = 'dinners';
                $scope.message = false;
                $scope.showCard = true;
            } else if (((firstDigit == 5 && between(secondDigit, 1, 5)) || (firstDigit == 2 && between(secondDigit, 2, 7))) && cardId.length == 5) {
                $scope.path = 'master';
                $scope.message = false;
                $scope.showCard = true;
            } else if ((firstDigit == 5 || firstDigit == 6) && (cardId.length == 5)) {
                $scope.path = "metro";
                $scope.message = false;
                $scope.showCard = true;
            } else if ((firstDigit == 4) && (cardId.length == 5)) {
                $scope.path = 'visa';
                $scope.message = false;
                $scope.showCard = true;
            } else if (cardId.length == 0) {
                $scope.message = true;
                $scope.showCard = false;
            }
        }
        /* end */
    function daysBetween(date1, date2) {
        //Get 1 day in milliseconds
        var one_day = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = date2_ms - date1_ms;

        // Convert back to days and return
        return Math.round(difference_ms / one_day);
    }

    var years;



    function getAge(dateString, dateType, deptDate) {
        //var now = new Date();
        var depdate = deptDate;
        var depdate2exp = depdate.split('/');
        var now = new Date(depdate2exp[2], depdate2exp[1] - 1, depdate2exp[0]);
        var today = new Date(now.getYear(), now.getMonth(), now.getDate());

        var yearNow = now.getYear();
        var monthNow = now.getMonth();
        var dateNow = now.getDate();

        if (dateType == 1)
            var dob = new Date(dateString.substring(0, 4),
                dateString.substring(4, 6) - 1,
                dateString.substring(6, 8));
        else if (dateType == 2)
            var dob = new Date(dateString.substring(0, 2),
                dateString.substring(2, 4) - 1,
                dateString.substring(4, 6));
        else if (dateType == 3)
            var dob = new Date(dateString.substring(6, 10),
                dateString.substring(3, 5) - 1,
                dateString.substring(0, 2));
        else if (dateType == 4)
            var dob = new Date(dateString.substring(6, 8),
                dateString.substring(3, 5) - 1,
                dateString.substring(0, 2));
        else
            return '';

        var yearDob = dob.getYear();
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();

        var yearAge = yearNow - yearDob;

        if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
        else {
            yearAge--;
            var monthAge = 12 + monthNow - monthDob;
        }

        if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
        else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }
        years = yearAge;
        //return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days';
    }

    /* end */

});

function newFunction($scope) {
    if ($scope.selectedAvilplan) {
        $scope.bookingRequest.travelPlanId = Number($scope.selectedAvilplan.sessionId.split('#')[$scope.selectedAvilplan.sessionId.split('#').length - 1]);
    } else {
    }
}

function between(x, min, max) {
    return x >= min && x <= max;
}

ouaApp.controller('flightFareRulesContent', function($scope, $uibModalInstance, $rootScope, stopsService) {

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    /* modelDismiss function */
    $scope.modelDismiss = function() {
            $uibModalInstance.close();
        }
        /* end */
});
ouaApp.controller('flightTermsCtrl', function($scope, $uibModalInstance, $rootScope) {

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    /* modelDismiss function */
    $scope.modelDismiss = function() {
        $uibModalInstance.close();
    }
})