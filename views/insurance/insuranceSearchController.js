ouaApp.controller('insureController', function($scope, $http,refreshService, $location, $uibModal, ConstantService, ServerService, $rootScope, ajaxService, $timeout, customSession) {
    window.scrollTo(0, 0);
    $("#load").show();
    setTimeout(function () {
       $('#load').hide();
    }, 1000);

    var today = new Date();
    today.setDate(today.getDate() + 321 - 1);


    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }

    $("#round").addClass("active");



    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(".refine-search-close").click(function() {
        $(".toggle").slideUp();
    });
   

    $scope.fInfantCount = [0,1,2,3,4,5,6,7,8,9];

	$scope.fAdultChange = function(a) {
        var infArr = [];
        for( var k = 0; k<=a; k++) {
        infArr.push(k);
        }
        $scope.fInfantCount = infArr;
      
        
        }
    if (sessionStorage.getItem('insurenceSearchRQ') != undefined) {
        // && $scope.urlPath=='/flight'
        // if (searchType === 'roundTrip') {
        $scope.insurenceSearchRQ = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));

        if(!$scope.insurenceSearchRQ.infantCount){
            $scope.insurenceSearchRQ.infantCount=0;
        }
        // }else{
        //console.log("data from insurance search", $scope.insurenceSearchRQ)
        $scope.multiCityinsurenceSearchRQ = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));

        // }
        $('#from_place').val($scope.insurenceSearchRQ.origin);
        $('#to_place').val($scope.insurenceSearchRQ.destination);

        if($scope.insurenceSearchRQ.returnDateTime==null){
            $("#oneWay").addClass("active");
            $("#round").removeClass("active");
            $("#oneWay a").addClass("active");
            $("#round a").removeClass("active");
                    
        }else{
            $("#round a").addClass("active");
            $("#round").addClass("active");
            $("#oneWay a").removeClass("active");
            $("#oneWay").removeClass("active");
                    
        }
        $var = $scope.insurenceSearchRQ.onwardDateTime + " - " + $scope.insurenceSearchRQ.returnDateTime
            //console.log('combine', $var);

        // $('#daterange1').val(08 / 10 / 2020 - 10 / 11 / 2020);
        var infArr = [];
        for (var k = 0; k <= $scope.insurenceSearchRQ.adultCount; k++) {
            infArr.push(k);
        }
        $scope.fInfantCount = infArr;
        //  //console.log("ngmo", $scope.fInfantCount)
        //$scope.insurenceSearchRQ.adultCount = ($scope.insurenceSearchRQ.adultCount * 1) - 1;
        //console.log($scope.insurenceSearchRQ.adultCount)
        //console.log($scope.insurenceSearchRQ.childCount)
        //console.log($scope.insurenceSearchRQ.infantCount)
        // $scope.insurenceSearchRQ.adultCount = "2"

        if ($scope.insurenceSearchRQ.returnDateTime === null) {
            $('#daterange1').val($scope.insurenceSearchRQ.onwardDateTime);
            //console.log("oneway");
            $scope.flightop = {
                opens: "center",
                singleDatePicker: true,
                autoApply: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,

                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    },
                    'hide.daterangepicker':function(ev, picker) {
                        $('select[name=naming]').focus();
        
                    }
                }
            }
            $scope.showOnewayDatepicker = true;
        } else {
            $('#daterange1').val($var);

            //console.log("twoway");
            $scope.flightop = {
               opens: "center",

                singleDatePicker: false,
                autoApply: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,
                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: false //$scope.checkboxModel.value
                        // }
                    },
                    'hide.daterangepicker':function(ev, picker) {
                        $('select[name=naming]').focus();
        
                    }
                }
            }

            $scope.showOnewayDatepicker = false;
        }

        ////console.log($scope.searchFlightObjJson);
    } else {
        $scope.insurenceSearchRQ = {

            // currency: "AED",
            adultCount: "1",
            childCount: "0",
            infantCount: "0",
            origin: "",
            destination: "",
            // originCountryCode: "",
            // destinationCountryCode: "",
            // onwardAirlineCode: "",
            // onwardFlightNumber: "",
            onwardDateTime: "",
            returnDateTime: null
            // returnDateTime: "",
            // returnAirlineCode: null,
            // returnFlightNumber: null,
            
        }
        $scope.insurenceSearchRQ.infantCount=0;
        $scope.multiCityinsurenceSearchRQ = {
            /*  frmAirport: "",
        toAirport: "", */
            adtCnt: "1",
            chldCnt: "0",
            infCnt: "0",

            /*  prefAirlineList: [],
             prefNonStop: false,
             multicityInfos: [ $scope.initMultiCityTrip()], */


        }
    }
    $scope.prefAirlineList = []
    $scope.searchTrip = '';
    $scope.flightop = {
       opens: "center",
        singleDatePicker: false,
        autoApply: true,
        autoUpdateInput: true,
        minDate: new Date(),
        maxDate: today,
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                // $scope.flightop = {
                //     singleDatePicker: false //$scope.checkboxModel.value
                // }
            },
            'hide.daterangepicker':function(ev, picker) {
                $('select[name=naming]').focus();

            }
        }
    }
   

    $scope.oneWayAndRoundTrip = function(searchType, checkboxModelvalue) {
        //  $("#oneway").removeClass("active");
        // $("#round").removeClass("active");



        if (searchType == 1) {
            $("#oneWay").addClass("active");
            $("#round").removeClass("active");
            $("#oneWay a").addClass("active");
            $("#round a").removeClass("active");

            $scope.searchTrip = "oneway";
            // $('.daterangepicker.dropdown-menu').css('max-width','fit-content !important');
            $scope.insurenceSearchRQ.onwardDateTime = "";
            $scope.insurenceSearchRQ.returnDateTime = null;
            // $scope.insurenceSearchRQ.childCount = "0";
            // $scope.insurenceSearchRQ.infantCount = "0";
            $scope.insurenceSearchRQ.adultCount = "1";
            $scope.flightop = {
               opens: "center",
                singleDatePicker: true,
                autoApply: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,
                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: true //$scope.checkboxModel.value
                        // }
                    },
                    'hide.daterangepicker':function(ev, picker) {
                        $('select[name=naming]').focus();
        
                    }
                }
            }
            $("#daterange1").val('');
            $scope.showOnewayDatepicker = true;
        } else {
            $("#oneWay").removeClass("active");
            $("#round").addClass("active");
            $("#oneWay a").removeClass("active");
            $("#round a").addClass("active");

            // $('.daterangepicker.dropdown-menu').css('max-width','493px !important');
            $scope.insurenceSearchRQ.onwardDateTime = "";
            $scope.insurenceSearchRQ.returnDateTime = null;
            // $scope.insurenceSearchRQ.childCount = "0";
            // $scope.insurenceSearchRQ.infantCount = "0";
            $scope.insurenceSearchRQ.adultCount = "1";
            $scope.flightop = {
                opens: "center",

                singleDatePicker: false,
                autoApply: true,
                autoUpdateInput: true,
                minDate: new Date(),
                maxDate: today,
                eventHandlers: {
                    'show.daterangepicker': function(ev, picker) {
                        // $scope.flightop = {
                        //     singleDatePicker: false //$scope.checkboxModel.value
                        // }
                    },
                    'hide.daterangepicker':function(ev, picker) {
                        $('select[name=naming]').focus();
        
                    }
                }
            }
            $scope.showOnewayDatepicker = false;
            $("#daterange1").val('');

        }


        var date = new Date();
        var currentDate = date.getDate() + 1;

        if (checkboxModelvalue == true) {

            var dateelement = angular.element('#daterange1');
            var dateval = dateelement.val();
            var dateclass = dateelement.attr("class");
            var datevalsingle = dateval.split(" - ");

            localStorage.setItem("isOneWay", true)
            $("#daterange1").parent().addClass('has-error');
            // $scope.insurenceSearchRQ.strDeptDate = angular.element('#daterange1').val();

        } else if (checkboxModelvalue == false) {
            var dateelement = angular.element('#daterange1');
            dateelement.val('');


            $('input[name=daterange]').focus();
            $("#daterange1").parent().addClass('has-error');
            localStorage.setItem("isOneWay", false)


        }

    }

    $scope.createMultiCityInfo = function() {

        $scope.multiCityinsurenceSearchRQ.multicityInfos.push({
            frmAirport: "",
            toAirport: "",
            strDeptDate: "",
            classType: "Economy",
            prefferedAirlineList: [],
            prefNonStop: false
        });

    }

    $scope.initMultiCityTrip = function() {
        $scope.multiCityinsurenceSearchRQ.multicityInfos = []
        for (var i = 0; i < 2; i++) {
            $scope.createMultiCityInfo();
            $scope.totalSelectedSize++;
        }
        // $scope.intiMultiCityFromAirPortAutoComplete();
        // $scope.intiMultiCityToAirPortAutoComplete()
    }


    $scope.addMultiCityTrip = function() {
        if ($scope.multiCityinsurenceSearchRQ.multicityInfos.length < 4)
            $scope.createMultiCityInfo();
        //$scope.intiMultiCityAutoComplete();
    }
    $scope.removeMultiCityTrip = function() {
        if ($scope.multiCityinsurenceSearchRQ.multicityInfos.length > 2)
            $scope.multiCityinsurenceSearchRQ.multicityInfos.splice(0, 1);
    }






    // if($scope.searchFlightObjJson.prefNonStop){
    //     $scope.prefNonStop = true;
    //     }

    var reminder2 = "";
    jQuery('#contenteditables1').keyup(function(e) {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }

        // //console.log(jQuery(this).val().toUpperCase());
        if (jQuery(this).val().length == 2) {
            $http.get(ServerService.serverPath + 'flight/airlines/' + jQuery(this).val().toUpperCase(), {
                headers: headersOptions
            }).then(function(result, status) {
                var availableTags = [];
                var data = result.data.data;
                for (var i = 0; i < data.length; i++) {
                    var prefcount = 0;
                    var contentcount = 0;
                    var checkCity4 = data[i].shortName;
                    var checkCity1 = checkCity4.toLowerCase();
                    var checkCity2 = data[i].displayname + ' (' + data[i].shortName + ')';
                    if (jQuery('#contenteditables1').val().toLowerCase() == data[i].shortName.toLowerCase()) {

                        jQuery('.contenttags').find('.destination').each(function() {

                            if (data[i].shortName.toLowerCase() == jQuery(this).text().toLowerCase()) {
                                contentcount = contentcount + 1;
                                prefcount = prefcount + 1;
                            }
                            if (prefcount == 0) {
                                if (jQuery('#contenteditables1').val().toLowerCase() != data[i].shortName.toLowerCase()) {
                                    availableTags.push(data[i].displayname + ' (' + data[i].shortName + ')');
                                }
                            }
                        });
                        if (contentcount == 0) {
                            availableTags.push(data[i].displayname + ' (' + data[i].shortName + ')');
                        }
                    }
                }
                for (var i = 0; i < data.length; i++) {
                    var prefcount = 0;
                    jQuery('.contenttags').find('.destination').each(function() {
                        if (data[i].shortName.toLowerCase() == jQuery(this).text().toLowerCase()) {
                            prefcount = prefcount + 1;
                        }
                    });
                    if (prefcount == 0) {
                        if (jQuery('#contenteditables1').val().toLowerCase() != data[i].shortName.toLowerCase()) {
                            availableTags.push(data[i].displayname + ' (' + data[i].shortName + ')');
                        }
                    }
                }
                jQuery("#contenteditables1").autocomplete({
                    source: availableTags,
                    select: function(event, ui) {
                        reminder2 = ui.item.value;
                        contentfunction();
                        $("#contenteditables1").val("");
                        return false;
                    }

                });
                var e = jQuery.Event("keydown");
                e.keyCode = 50;
                $("#contenteditables1").trigger(e);
            })
        }
    });

    function contentfunction() {
        var prefercount = 0;
        var reminders2 = reminder2.split('(');
        var reminders1 = reminders2[1].replace(')', '');
        jQuery('.contenttags').find('.destination').each(function() {
            if (reminders1 == jQuery(this).text()) {
                prefercount = prefercount + 1;
            }
        });
        if (prefercount == 0) {
            jQuery("#contentdiv").after('<span class="contenttags"><span class="airlinenames" style="display:none;color:white">' + reminders2[0] + '</span>  <span class="destination" style="float:left;">' + reminders1 + '</span><span class="xclass">&times;</span><span>');
        }
    }

    jQuery('body').on('click', '.xclass', function() {
        jQuery(this).parent('span').remove();
    });
    jQuery('body').on('mouseover', '.destination', function() {
        jQuery(this).prev('.airlinenames').show();
    });
    jQuery('body').on('mouseout', '.destination', function() {
        jQuery(this).prev('.airlinenames').hide();
    });
    $scope.data = '';













    $scope.removeprefAirlineList = function(airline) {
        $scope.insurenceSearchRQ.prefAirlineList.pop(airline);
        $scope.multiCityinsurenceSearchRQ.prefAirlineList.pop(airline);
    };
    $scope.artloading = false;
     // if ($scope.searchTrip == "oneway") {
    //     $("#oneway").addClass("active");
    //     $("#round").removeClass("active");
    // } else {
    //     $("#round").addClass("active");
    //     $("#oneway").removeClass("active");
    // }
    $scope.insurenceSearch = function(searchType, valid) {


        var date = $('#daterange1').val();
        var date1 = []
            // //console.log("selected date", date);
        date1 = date.split(' ');
        var totalTravllersCount = parseInt($scope.insurenceSearchRQ.adultCount) + parseInt($scope.insurenceSearchRQ.childCount) ;
        // $scope.artloading = true;
        // if(jQuery('#searchForm').valid()){


        if (totalTravllersCount > 9) {
            // //console.log($scope.flightSearchRQ.strDeptDate);

            Lobibox.alert('info', {
                msg: "You can book upto 9 passengers per booking, including adults and children"
            });
            return;
        } else {

            if (searchType === 'roundTrip') {
                if ($scope.searchTrip == '') {

console.log("round")
     $("#round").addClass("active");
        // $("#oneway").removeClass("active");
                    //console.log("check inside the array 0", date1[0]);
                    //console.log("check inside the array 2", date1[2]);hj
                    $scope.insurenceSearchRQ.onwardDateTime = date1[0];
                    $scope.insurenceSearchRQ.returnDateTime = date1[2];
                    //console.log($scope.insurenceSearchRQ);
                    sessionStorage.setItem("insurenceSearchRQ", JSON.stringify($scope.insurenceSearchRQ));
                } else {
                    console.log("oneway")
                         $("#oneway").addClass("active");
        // $("#round").removeClass("active");

                    $scope.insurenceSearchRQ.onwardDateTime = date1[0];
                    // $scope.insurenceSearchRQ.returnDateTime = date1[2];
                    //console.log($scope.insurenceSearchRQ);
                    sessionStorage.setItem("insurenceSearchRQ", JSON.stringify($scope.insurenceSearchRQ));
                }

            }
            //  else if (searchType === 'multiCity') {
            //     sessionStorage.setItem("insurenceSearchRQ", JSON.stringify($scope.multiCityinsurenceSearchRQ));
            // }


            $location.path('/searchingInsurance');
        }
        // }else{
        //     return;
        // }
    }



    $(function() {

        'use strict';
        var f = 0;
        var flag = false;
        // FROM ARIPORT AUTOCOMPELTE
        $('#from_place').autocomplete({
            source: function(request, response) {
                $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                var tokenname = $scope.tokenresult.data;
                //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                var headersOptions = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + tokenname.authToken
                }
                $http.get(ServerService.serverPath + 'flight/airports/' + request.term, {
                    headers: headersOptions
                }).then(function successCallback(data, status) {
                    var success = data.data.success
                    var data = data.data.data;
                    if (success) {
                        f = 0;
                        response(data);
                        //console.log(data);
                    } else {
                        $("#from_place").val('');
                        $("#from_place").focus(0);
                        $('.ui-autocomplete').hide();
                    }
                }, function errorCallback(response) {
					if(response.status == 403){
						var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
						status.then(function(greeting) {
                            $("#from_place").val('');
						});
					}
				  });
            },
            // focus: function(event, ui) {
            //     $("#from_place").val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
            //     return false;
            // },
            change: function(event, ui) {
                if ($("#from_place").val()) {
                    $("#from_place").val($scope.insurenceSearchRQ.origin);
                    if ($("#to_place").val() == $("#from_place").val()) {
                        $("#from_place").val("");
                        return false;
                    }
                }

                /*if(flag==false) {
                $('#from_place').val('');
                $('#from_place').focus(0);
                return false;
                }*/
            },
            autoFocus: true,
            minLength: 3,
            select: function(event, ui) {
                if ($("#from_place").val() == $("#to_place").val()) {
                    $("#from_place").val('');
                    $('.destcheck').show().fadeOut(5000);
                } else {
                    $("#from_place").val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
                    $scope.insurenceSearchRQ.origin = ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")";
                    $scope.$apply();
                    $("#from_place").parent().removeClass('has-error');
                    $scope.errorDisp = false;
                    $('#to_place').focus(0);
                    flag = true;
                }
                return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function(event, ui) {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (f == 0) {
                f++;
                //$scope.insurenceSearchRQ.origin = item.city + ", " + item.countryCode + " (" + item.code + ")";
                return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.countryCode +  "(" + item.code + ")</a>").appendTo(ul);
            } else {
                return $("<li>").append("<a>" + item.city + "," + item.countryCode +  "(" + item.code + ")</a>").appendTo(ul);
            }
        };




    });
    //TO AIRPORT AUTOCOMPLETE


    $('#to_place').autocomplete({
        source: function(request, response) {
            $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
            $http.get(ServerService.serverPath + 'flight/airports/' + request.term, {
                headers: headersOptions
            }).then(function successCallback(data, status) {
                var success = data.data.success
                var data = data.data.data;
                if (success) {
                    f = 0;
                    response(data);
                } else {
                    $("#to_place").val('');
                    $("#to_place").focus(0);
                    $('.ui-autocomplete').hide();
                }
            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                        // $scope.sendMailId();
                    $("#to_place").val('');
                    });
                }
              });
        },
        // focus: function(event, ui) {
        //     $("#to_place").val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
        //     return false;
        // },
        change: function(event, ui) {
            if ($("#to_place").val()) {
                $("#to_place").val($scope.insurenceSearchRQ.destination);
                if ($("#to_place").val() == $("#from_place").val()) {
                    $("#to_place").val("");
                    return false;
                }
            }

            /*if(flag==false) {
            $('#from_place').val('');
            $('#from_place').focus(0);
            return false;
            }*/
        },
        autoFocus: true,
        minLength: 3,
        select: function(event, ui) {
            if ($("#to_place").val() == $("#from_place").val()) {
                $("#to_place").val('');
                $('.destcheck').show().fadeOut(5000);
            } else {
                $("#to_place").val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
                $scope.insurenceSearchRQ.destination = ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")";
                $scope.$apply();
                $("#to_place").parent().removeClass('has-error');
                $scope.errorDisp = false;
                $('#to_place').focus(0);
                flag = true;
            }
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function(event, ui) {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        if (f == 0) {
            f++;
           // $scope.insurenceSearchRQ.destination = item.city + ", " + item.countryCode +  " (" + item.code + ")";
            return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
        } else {
            return $("<li>").append("<a>" + item.city + "," + item.countryCode +  "(" + item.code + ")</a>").appendTo(ul);
        }
    };




    // $("#prefered_airline").autocomplete({
    //     source: function (request, response) {

    //         $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
    //         var tokenname = $scope.tokenresult.data;
    //         var headersOptions = {
    //             'Content-Type': 'application/json',
    //             'Authorization': 'Bearer ' + tokenname.authToken
    //         }

    //         $http.get(ServerService.serverPath + 'flight/airlines/' + request.term, {
    //             headers: headersOptions
    //         }).then(function (data, status) {
    //             var data = data.data.data;
    //             if (data != null && data != '') {
    //                 f = 0;
    //                 response(data);
    //             } else {
    // 		      $('.ui-autocomplete').hide();
    //             }
    //         });
    //     },
    //     focus: function (event, ui) {
    //         $("#prefered_airline").val(ui.item.displayname + ", " + ui.item.displayname + " - " + ui.item.displayname + " (" + ui.item.code + ")");
    //         return false;
    //     },
    //     change: function (event, ui) {

    //     },
    //     autoFocus: true,
    //     minLength: 3,
    //     select: function (event, ui) {

    //         return false;
    //     },
    //     open: function () {
    //         $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    //     },
    //     close: function () {
    //         $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    //     }
    // }).data("ui-autocomplete")._renderItem = function (ul, item) {
    //     if (f == 0) {
    //         f++;
    //         $scope.insurenceSearchRQ.airline = item.displayname + " (" + item.code + ")";
    //         return $("<li>").append("<a class='ui-state-focus'>" + item.displayname + "(" + item.code + ")</a>").appendTo(ul);
    //     } else {
    //         return $("<li>").append("<a>" + item.displayname + "(" + item.code + ")</a>").appendTo(ul);
    //     }

    //     var e = jQuery.Event("keydown");
    //     e.keyCode = 13;
    //     $("#to_place").trigger(e);
    // };



    $scope.intiMultiCityFromAirPortAutoComplete = function(indexId) {

        var count = $scope.multiCityinsurenceSearchRQ.multicityInfos.length;
        for (var i = 0; i < count; i++) {
            $('#multi_flying_from_' + i).autocomplete({
                source: function(request, response) {
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.get(ServerService.serverPath + 'flight/airports/' + request.term, {
                        headers: headersOptions
                    }).then(function(data, status) {
                        var data = data.data.data;
                        if (data != null && data != '') {
                            f = 0;
                            response(data);
                        } else {
                            $("#multi_flying_from_" + i).val('');
                            $("#multi_flying_from_" + i).focus(0);
                            $('.ui-autocomplete').hide();
                        }
                    });
                },
                focus: function(event, ui) {
                    $("#multi_flying_from_" + i).val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
                    return false;
                },
                change: function(event, ui) {
                    $("#multi_flying_from_" + i).val($scope.insurenceSearchRQ.destination);
                    if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to").val()) {
                        $("#multi_flying_from_" + i).val("");
                        return false;
                    }

                    /*if(flag==false) {
                    $('#from_place').val('');
                    $('#from_place').focus(0);
                    return false;
                    }*/
                },
                //autoFocus: true,
                minLength: 3,
                select: function(event, ui) {
                    if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to").val()) {
                        $("#multi_flying_from_" + i).val('');
                        $('.destcheck').show().fadeOut(5000);
                    } else {
                        $("#multi_flying_from_" + i).val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
                        $scope.multiCityinsurenceSearchRQ.multicityInfos[indexId].frmAirport = ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")";
                        $scope.$apply();
                        $("#multi_flying_from_" + i).parent().removeClass('has-error');
                        $scope.errorDisp = false;
                        $('#multi_flying_from_' + i).focus(0);
                        flag = true;
                    }
                    return false;
                },
                open: function() {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function(event, ui) {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                if (f == 0) {
                    f++;
                    $scope.multiCityinsurenceSearchRQ.multicityInfos[indexId].frmAirport = item.city + ", " + item.countryCode +  " (" + item.code + ")";
                    return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.countryCode + + "(" + item.code + ")</a>").appendTo(ul);
                } else {
                    return $("<li>").append("<a>" + item.city + "," + item.countryCode +"(" + item.code + ")</a>").appendTo(ul);
                }
            };
        }
    }

    $scope.intiMultiCityToAirPortAutoComplete = function(indexId) {
        var count = $scope.multiCityinsurenceSearchRQ.multicityInfos.length;
        for (var i = 0; i < count; i++) {
            $('#multi_flying_to_' + i).autocomplete({
                source: function(request, response) {
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.get(ServerService.serverPath + 'flight/airports/' + request.term, {
                        headers: headersOptions
                    }).then(function(data, status) {
                        var data = data.data.data;
                        if (data != null && data != '') {
                            f = 0;
                            response(data);
                        } else {
                            $("#multi_flying_to_" + i).val('');
                            $("#multi_flying_to_" + i).focus(0);
                            $('.ui-autocomplete').hide();
                        }
                    });
                },
                focus: function(event, ui) {
                    $("#multi_flying_to_" + i).val(ui.item.city + ", " + ui.item.countryCode +  " (" + ui.item.code + ")");
                    return false;
                },
                change: function(event, ui) {
                    $("#multi_flying_to_" + i).val($scope.insurenceSearchRQ.toAirport);
                    if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to_" + i).val()) {
                        $("#multi_flying_to_" + i).val("");
                        return false;
                    }

                    /*if(flag==false) {
                    $('#from_place').val('');
                    $('#from_place').focus(0);
                    return false;
                    }*/
                },
                //autoFocus: true,
                minLength: 3,
                select: function(event, ui) {
                    if ($("#multi_flying_from_" + i).val() == $("#multi_flying_to_" + i).val()) {
                        $("#multi_flying_to_" + i).val('');

                        $('.destcheck').show().fadeOut(5000);
                    } else {
                        $("#multi_flying_to_" + i).val(ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")");
                        $scope.multiCityinsurenceSearchRQ.multicityInfos[indexId].toAirport = ui.item.city + ", " + ui.item.countryCode + " (" + ui.item.code + ")";
                        $scope.$apply();
                        $("#multi_flying_to_" + i).parent().removeClass('has-error');
                        $scope.errorDisp = false;
                        $('#multi_flying_to_' + i).focus(0);
                        flag = true;
                    }
                    return false;
                },
                open: function() {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function(event, ui) {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                if (f == 0) {
                    f++;
                    $scope.multiCityinsurenceSearchRQ.multicityInfos[indexId].toAirport = item.city + ", " + item.countryCode +  " (" + item.code + ")";
                    return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
                } else {
                    return $("<li>").append("<a>" + item.city + "," + item.countryCode + "(" + item.code + ")</a>").appendTo(ul);
                }
            };
        }
    }
});