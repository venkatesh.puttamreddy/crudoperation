ouaApp.controller('insuranceResultController', function ($scope,$timeout,$rootScope,$http, renderHTMLFactory, $location, ServerService, customSession) {
    sessionStorage.removeItem('flightplusSearchResp');
    sessionStorage.removeItem('hotelPlusSearchResp');
    sessionStorage.removeItem('repriseResponse');
    sessionStorage.removeItem('roomIndex');
    sessionStorage.removeItem('selectedFliResp');
    sessionStorage.removeItem('selectedHotResp');
	// sessionStorage.removeItem('ridersplan');
	// sessionStorage.removeItem('avilplan')
	$scope.isCollapsed = true;
	$scope.renderHTMLFactory = renderHTMLFactory;
	$('html, body').scrollTop(0);

	
    $(".refine-search").click(function () {
      $(".toggle").slideToggle();
    });
    $(".refine-search-close").click(function () {
      $(".toggle").slideUp();
    });
	$scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
           $('#load').hide();
        }, 1000);
    }
	
	 $scope.Insurance = JSON.parse(sessionStorage.getItem('insurenceSearchRQ')); 
	$scope.InsuranceSearchRequest = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));
	$scope.InsuranceSearchReqItem = $scope.InsuranceSearchRequest ;
	if(!$scope.Insurance.infantCount){
		$scope.Insurance.infantCount=0;
	}
	$scope.passengerCount = Number($scope.Insurance.adultCount) + Number($scope.Insurance.childCount) +Number($scope.Insurance.infantCount)
	
	$scope.InsuranceSearchResult = JSON.parse(sessionStorage.getItem('InsuranceSearchResult'));

	if($scope.InsuranceSearchReqItem.returnDateTime){
		$scope.trip = 'Round trip';
	} else {
		$scope.trip = 'One Way';
		}
		
		$scope.riderPlans = $scope.InsuranceSearchResult.data.upsellPlans
	var plans = $scope.InsuranceSearchResult.data.availablePlans
	
	/* sorting based on totalFare */
	function sorting (array, key){
		return array.sort(function(a, b){
			var x = a[key]; var y = b[key];
			return ( (x < y) ? -1 : ((x > y) ? 1 : 0));
			});
		}
		$scope.availablePlans = sorting(plans, 'totalFare');
	/* end */
	$scope.insurancedate={
		startDate: moment(),
		endDate: moment(),
		locale:{
			format : 'D-MMM-YY'
			}
		}
	$scope.isLifeStylePlanSelected;
/* availablePlans function */
$scope.allInsPlan = [];
$scope.grandTotal = 0;


$scope.calcGrandTotal = function(){
$scope.grandTotal = 0;
if($scope.selectedRaidPlan != null){
	if($scope.selectedRaidPlan.length>0){
		$scope.selectedRaidPlan.forEach(element => {
			$scope.grandTotal += element.totalFare;
		});
	}
}
	if($scope.selectedAvailPlan){
		$scope.grandTotal += $scope.selectedAvailPlan.totalFare;
	}
}
$scope.selectedRiderPlansIndex=[];
$scope.selectedRaidPlan = [];

$scope.selectedAvailPlan = JSON.parse(sessionStorage.getItem('avilplan'));
if($scope.selectedAvailPlan){
	$scope.isLifeStylePlanSelected=true;
}
$scope.selectedRaidPlan = JSON.parse(sessionStorage.getItem('ridersplan'));
if($scope.selectedRaidPlan){
	$scope.isLifeStylePlanSelected=true;
}
if( $scope.selectedRaidPlan!=null){
	$scope.calcGrandTotal(); 
	setTimeout(function(){
	for(var i=0;i<$scope.riderPlans.length;i++){
		$scope.selectedRaidPlan.forEach(plans => {
			if(plans.sessionId == $scope.riderPlans[i].sessionId){
					$("#selectriders_"+i).prop("checked", true);
					$scope.selectedRiderPlansIndex.push(i);
			}
		});
	}
},1000)

}else{
$scope.selectedRaidPlan = [];
}
if($scope.selectedAvailPlan != null){
	$scope.calcGrandTotal(); 
	setTimeout(function(){
		for(var pi=0;pi<$scope.availablePlans.length;pi++){
			if($scope.selectedAvailPlan.sessionId == $scope.availablePlans[pi].sessionId){
				$("#selectplans_"+pi).attr("checked", true);
			}
		}
	},1000)
	
}else{
}

$scope.availPlans = function(index){
	$scope.selectedAvailPlan = $scope.availablePlans[index];
	sessionStorage.setItem('avilplan',JSON.stringify($scope.selectedAvailPlan));
	$scope.isLifeStylePlanSelected=true;
	$scope.calcGrandTotal(); 
}

/* end */

/* ridersPlans function */

$scope.ridersPlans = function(value){
	if($scope.selectedRiderPlansIndex.includes(value)){
		$scope.selectedRaidPlan.pop($scope.riderPlans[value])
		$scope.selectedRiderPlansIndex.pop(value);
		$("#selectriders_"+value).prop("checked", false);

	}else{
		$scope.riderList = $scope.selectedRaidPlan.push($scope.riderPlans[value]);
		$scope.selectedRiderPlansIndex.push(value);
		$scope.isLifeStylePlanSelected=true;

		$("#selectriders_"+value).prop("checked", true);

	}
	sessionStorage.setItem('ridersplan',JSON.stringify($scope.selectedRaidPlan));
	$scope.calcGrandTotal();

}

if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
	$scope.loggedvalue = true;
	$rootScope.bookingHide = false;
	$rootScope.guestLogin = true;
	$rootScope.bookingHide1 = false;
	
	$rootScope.dispName = "";
	sessionStorage.removeItem("guestLoginDatails");
	localStorage.removeItem("loginToken");
	
}



/* end */
/* onBook function */
$scope.onBook = function(){
	// $scope.timeload();
	// $scope.artloading = true;
	//var checked = $('input[name="selectplans"]:checked').val();
	//var radioType = $('#selectplans').prop('checked');
	if(!$scope.isLifeStylePlanSelected){
		// $scope.artloading = false;
		Lobibox.alert('warning', {
			msg: "Please Select At Least One Travel Plan To Process Booking."
		});
		
	}else if(!$scope.isLifeStylePlanSelected){
		// $scope.artloading = false;
		Lobibox.alert('warning', {
			msg: "Please Select At Least One Travel Plan To Process Booking."
		});
	} else {
		// $scope.artloading = false;
		$location.path('/insuranceReview')
		$('html,body').scrollTop(50);
		}
	
}
/* end */
	
console.log($scope.selectedRaidPlan);
});
