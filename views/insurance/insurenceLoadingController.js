ouaApp.controller('insuranceLoadingController', function($scope,$http,refreshService, $location, $timeout, ServerService){
$('html, body').scrollTop(0);
$scope.searching = "insurance";
$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
var tokenname = $scope.tokenresult.data;
var headersOptions = {
	'Content-Type': 'application/json',
	'Authorization': 'Bearer ' + tokenname.authToken
}
$scope.Insurance = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));

if($scope.Insurance.returnDateTime){
	$scope.trip = 'Round trip';
} else {
	$scope.trip = 'One Way';
	}

var orgin=JSON.parse(sessionStorage.getItem('insurenceSearchRQ')).origin.split('(')[1];
var destination=JSON.parse(sessionStorage.getItem('insurenceSearchRQ')).destination.split('(')[1];
// .split('(')[1];
$scope.Insurance.origin=orgin.split(')')[0]
$scope.Insurance.destination=destination.split(')')[0]



// $scope.Insurance.originCountryCode=  "AE",
// $scope.Insurance.destinationCountryCode=  "IN",
// $scope.Insurance.onwardAirlineCode= "J9",
// $scope.Insurance.onwardFlightNumber=  539,
// $scope.Insurance={
//     "currency": "AED",
//     "adultCount": 1,
//     "childCount": 0,
//     "infantCount": 0,
//     "origin": "DXB",
//     "destination": "MAA",
//     "originCountryCode": "AE",
//     "destinationCountryCode": "IN",
//     "onwardAirlineCode": "J9",
//     "onwardFlightNumber": 539,
//     "onwardDateTime": "19/10/2020",
//     "returnAirlineCode": null,
//     "returnFlightNumber": null,
//     "returnDateTime": null
// }
$scope.message = "Searching For Available Insurance";

insuranceSearch = function(){
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
var tokenname = $scope.tokenresult.data;
var headersOptions = {
	'Content-Type': 'application/json',
	'Authorization': 'Bearer ' + tokenname.authToken
}

	

	$http.post(ServerService.serverPath+'insurance/plan', JSON.stringify($scope.Insurance),{
		headers: headersOptions
	}).then(function successCallback(response){

	
		$scope.loading = false;
			$scope.disabled = false;
		sessionStorage.setItem('InsuranceSearchResult',JSON.stringify(response.data));
		// sessionStorage.setItem('insurenceSession', JSON.stringify(response.data));
		sessionStorage.setItem('InsuranceSearchReq',JSON.stringify($scope.Insurance));
		sessionStorage.removeItem('avilplan');
		sessionStorage.removeItem('ridersplan');
		if(response.data.success){
			$location.path('/insuranceResult');
			} else {
		Lobibox.alert('error',{
			msg: response.data.errorMessage
		})
		$timeout(function(){
			$location.path('/insurance')
			}, 2000);
	}
		
}, function errorCallback(response) {
	if(response.status == 403){
		var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
		status.then(function(greeting) {
			insuranceSearch();
		});
	}
  });
}

insuranceSearch();

});