ouaApp.controller('contactEnquiryController', function($scope, $http, $location, refreshService, $uibModal, ConstantService, ServerService, $rootScope){
	//$scope.title = items
	// window.scrollTo(600,600);
		$scope.salutation = [{'values': 'Mr', 'label': 'Mr'},{'values': 'Mrs', 'label': 'Mrs'},{'values': 'Miss', 'label': 'Miss'}]
		$scope.adult = [{'values': '1', 'label': '1'},{'values': '2', 'label': '2'},{'values': '3', 'label': '3'}, {'values': '4', 'label': '4'}, {'values': '5', 'label': '5'}, {'values': '6', 'label': '6'}, {'values': '7', 'label': '7'}, {'values': '8', 'label': '8'}, {'values': '9', 'label': '9'}]
		$scope.child = [{'values': '0', 'label': '0'}, {'values': '1', 'label': '1'},{'values': '2', 'label': '2'},{'values': '3', 'label': '3'}, {'values': '4', 'label': '4'}, {'values': '5', 'label': '5'}, {'values': '6', 'label': '6'}, {'values': '7', 'label': '7'}, {'values': '8', 'label': '8'}]
		$scope.enquiryForms = {}
		$scope.enquiryForms.enquiryType = 'service';
		$scope.enquiryForms.subject = 'Contactus Enquiry Mail';
		$scope.enquiryForms.mode = 'contactus';
	
		$scope.path = $location.absUrl().split('/')[5];
	
		 $scope.inputs = [];
		 $scope.addfield=function(){
		  $scope.inputs.push({})
		 }
		 $scope.removeColumn = function(index) {
			// remove the row specified in index
			$scope.inputs.splice( index, 1);
			// if no rows left in the array create a blank array
			if ( $scope.inputs.length() === 0 || $scope.inputs.length() == null){
			  alert('no rec');
			  $scope.inputs.push = [{"colId":"col1"}];
			}
				
		  
		  };
		$scope.imageUpload = function (event) {
			var files = event.target.files;
	
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				var reader = new FileReader();
				reader.onload = $scope.imageIsLoaded;
				reader.readAsDataURL(file);
			}
		}
	
		$scope.imageIsLoaded = function (e) {
			$scope.$apply(function () {
				$scope.img = e.target.result;            
			});
		}
	
	
		/* for date picker */
		$scope.loginOption = {
		 singleDatePicker: true,
		
	   eventHandlers: {
				'show.daterangepicker': function(ev, picker){
					$scope.loginOption = {
						singleDatePicker: true,
						format: 'DD-MMM-YYYY'
					}
					}
				}
		 }
		/* end */
				$scope.submitted = false;
				$scope.onEnquirySubmit = function(valid){
		$scope.submitted = true;
			if(valid){
				var dates = $('#defaultContactWhen').val();
				
				$scope.enquiryForms.date = dates;
				$scope.enquiryForms.name = $scope.firstName+' '+$scope.lastName;
					$http.post(ServerService.serverPath+'rest/mail', $scope.enquiryForms).then(function(response){
						
						if(response.data.status == 'SUCCESS'){
							Lobibox.alert('success', {
								msg: response.data.message
								})
								
						} else {
							Lobibox.alert('error', {
								msg: response.data.message
								})
							}
						})
					
				}
			}
			$scope.cancel = function(){
				$location.path('/home');
			}
	
			$scope.contact = {
				salutation:'',
				firstName: '',
				lastname:'',
				email: '',
				phonenumber: '',
				subject: '',
				terms: '',
				additionalinfo:''
			};
			
			$scope.submitcontactForm = function () {
				$scope.packagespinner = true;
				var tokenresult = JSON.parse(localStorage.getItem('authentication'));
				var tokenname = tokenresult.data;
				var headersOptions = {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + tokenname.authToken
				}
				
				$http.post(ServerService.packagePath + 'enquiry/mail', $scope.contact, {
					headers: headersOptions
				}).then(function successCallback(response, status) {
					if(response.data.success == true){
						console.log(response);
						$scope.packagespinner = false;
						Lobibox.alert('success', {
							msg: response.data.message
							});
						$scope.contact = {
							salutation:'',
							firstName: '',
							lastname:'',
							email: '',
							phonenumber: '',
							subject: '',
							terms: '',
							additionalinfo:''
						};
					}
				}, function errorCallback(response) {
					$scope.packagespinner = false;
					Lobibox.alert('error', {
						msg: 'Server Issue'
						});
					if(response.status == 403){
						//refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
					}
				  });
	
			};
	
		function getSectionDetail(sectionId){
				$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
					var tokenname = $scope.tokenresult.data;
					var headersOptions = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + tokenname.authToken
					}
				$http.get(ServerService.cmsPath + 'section-details/section-master/'+sectionId, {
					headers: headersOptions
				}).then(function successCallback(response, status) {
					if(response){
						$scope.contactDetaills = response.data.data;
					}
				}, function errorCallback(response) {
					if(response.status == 403){
						//refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
					}
				  });
			}
	
	
			$(document).ready(function () {  
				$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
					var tokenname = $scope.tokenresult.data;
					var headersOptions = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + tokenname.authToken
					}
				$http.get(ServerService.cmsPath + 'pageByName/contactus', {
					headers: headersOptions
				}).then(function successCallback(response, status) {
					if(response){
						var sectionId =  response.data.data.sectionMasterId[0];
						getSectionDetail(sectionId);
					}
				}, function errorCallback(response) {
					if(response.status == 403){
						//refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
					}
				  });
	
			});

			$scope.accordionIndex=0;
			$scope.selectedLocation=function(selectedIndex){
				$scope.accordionIndex=selectedIndex;
			}
	
	
	})