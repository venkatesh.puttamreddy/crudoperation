ouaApp.controller('userTripsCtrl', function($scope, $http,refreshService, ServerService, $location, $uibModal, $timeout, customSession, $rootScope){
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	$scope.logged = JSON.parse(localStorage.getItem('loginName'));
	if($scope.logged!= undefined){
		$scope.role = JSON.parse(localStorage.getItem('authentication'));
		$scope.roleId = $scope.role.userId;
		}
	// $scope.rolename = document.getElementById('rolename').value; 
	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords1 = true;
	$scope.noRecords = false;
	$scope.travelClick = false;
	
	var tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname =tokenresult.data;
	var headersOptions = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer ' + tokenname.authToken
	}
	$scope.prodvalue="FLT";
	$scope.columnFileds=[
		{
		
				field: "bookingRefNo",
			title: "Booking Details"
		},{
			field: "createTime",
			title: "Booking Date"
		},{
			field: "product",
			title: "Product Type"
		}
		,{
			field: "actualAmount",
			title: "Total Fare"
		},{
			field: "status",
			title: "Status"
			
		}
		
	]
	// $scope.bookingRQ = {

	// 	fromBookingDate: "",
	// 	toBookingDate: "",
	// 	fromTravelDate: "",
	// 	toTravelDate: "",
	// 	prodType: ""
	// }


	$scope.checkFlight=function(prodvalue){
		$scope.fCheck = true;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =false;
		$scope.prodvalue=prodvalue;
		$scope.curentpage = 1;
	
		// var datereverse = $('#bookingDateFrom').val();
		// var date = datereverse.split("-").reverse().join("-");
		// var date1reverse = $('#bookingDateTo').val();
		// var date1 = date1reverse.split("-").reverse().join("-");
		// var date2 = $('#journeyDateFrom').val();
		// var date3 = $('#journeyDateTo').val();
		// $scope.bookingRQ.fromBookingDate=date;
		// $scope.bookingRQ.toBookingDate=date1;
		// $scope.bookingRQ.fromTravelDate=date2;
		// $scope.bookingRQ.toTravelDate=date3;
		// 	$scope.bookingRQ.prodType=prodvalue;

			$scope.bookingRQ={
				"page": null,
				"getAllResults": true,
				"filter": {
					"logic": "and",
					"filters": [
						{
							"field":"product",
							"operator":"eq",
							"value":prodvalue
						}
			
					]
				},"sort": [
					{
						"field": "createTime",
						"dir": "desc"
					}
				]
			}

		$scope.artloading = true;
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
		$http.post(ServerService.listPath + 'search/booking', $scope.bookingRQ, {
	
			headers: headersOptions
		}).then(function successCallback(response) {
			$scope.artloading = false;
			if (response.data.success == true) {

				$scope.dataSourcedata=response.data.data.data;
			
				$("#grid").kendoGrid({
					dataSource:$scope.dataSourcedata,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					filterable: true,
		 
					sortable: true,
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 10
					},
					columns: $scope.columnFileds
				});
			}
				}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.checkFlight(prodvalue);
		});
            }
          });

	}
	
	$scope.checkInsurance=function(page,prodvalue){
		$scope.curentpage = 1;
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}

		$scope.fCheck = false;
		$scope.hCheck = false;
		$scope.insCheck =true;
		$scope.atCheck =false;
		$scope.prodvalue=prodvalue
		// var datereverse = $('#bookingDateFrom').val();
		// var date = datereverse.split("-").reverse().join("-");
	
		// var dater1everse = $('#bookingDateTo').val();
		// var date1 = dater1everse.split("-").reverse().join("-");
		// var date2 = $('#journeyDateFrom').val();
		// var date3 = $('#journeyDateTo').val();
		// $scope.bookingRQ.fromBookingDate=date;
		
		// $scope.bookingRQ.toBookingDate=date1;
		// $scope.bookingRQ.fromTravelDate=date2;
		// $scope.bookingRQ.toTravelDate=date3;
		
		// $scope.bookingRQ.prodType=prodvalue;
		
		$scope.bookingRQ={
			"page": null,
			"getAllResults": true,
			"filter": {
				"logic": "and",
				"filters": [
					{
						"field":"product",
						"operator":"eq",
						"value":prodvalue
					}
		
				]
			},"sort": [
				{
					"field": "createTime",
					"dir": "desc"
				}
			]
		}
		$scope.artloading = true;
		$http.post(ServerService.listPath + 'search/booking', $scope.bookingRQ, {
	
			headers: headersOptions
		}).then(function successCallback(response) {
			$scope.artloading = false;
			if (response.data.success == true) {

				$scope.dataSourcedata=response.data.data.data;
			
				$("#grid").kendoGrid({
					dataSource:$scope.dataSourcedata,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					filterable: true,
		 
					sortable: true,
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 10
					},
					columns: $scope.columnFileds
				});
			}
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.checkInsurance(page,prodvalue);
		});
            }
          });
	}
	
	$scope.checkHotel=function(page,prodvalue){
		$scope.curentpage = 1;
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}

		$scope.fCheck = false;
		$scope.hCheck = true;
		$scope.insCheck =false;
		$scope.atCheck =false;
		$scope.prodvalue=prodvalue
		// $scope.field='createBy';
		// var datereverse = $('#bookingDateFrom').val();
		// var date = datereverse.split("-").reverse().join("-");
		// var dater1everse = $('#bookingDateTo').val();
		// var date1 = dater1everse.split("-").reverse().join("-");
		// var date2 = $('#journeyDateFrom').val();
		// var date3 = $('#journeyDateTo').val();
		// $scope.bookingRQ.fromBookingDate=date;
		// $scope.bookingRQ.toBookingDate=date1;
		// $scope.bookingRQ.fromTravelDate=date2;
		// $scope.bookingRQ.toTravelDate=date3;
		
		// $scope.bookingRQ.prodType=prodvalue;
		
		$scope.bookingRQ={
			"page": null,
			"getAllResults": true,
			"filter": {
				"logic": "and",
				"filters": [
					{
						"field":"product",
						"operator":"eq",
						"value":prodvalue
					}
		
				]
			},"sort": [
				{
					"field": "createTime",
					"dir": "desc"
				}
			]
		}
		$scope.artloading = true;
		$http.post(ServerService.listPath + 'search/booking', $scope.bookingRQ, {
	
			headers: headersOptions
		}).then(function successCallback(response) {
			$scope.artloading = false;
			if (response.data.success == true) {

				$scope.dataSourcedata=response.data.data.data;
			
				$("#grid").kendoGrid({
					dataSource:$scope.dataSourcedata,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					filterable: true,
		 
					sortable: true,
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 10
					},
					columns: $scope.columnFileds
				});
			}
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.checkHotel(page,prodvalue);
		});
            }
          });
	}
	$scope.checkAttraction=function(page,prodvalue){
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
		$scope.fCheck = false;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =true;
		// $scope.field='createBy';
		// var datereverse = $('#bookingDateFrom').val();
		// var date = datereverse.split("-").reverse().join("-");
		// var date1reverse = $('#bookingDateTo').val();
		// var date1 = date1reverse.split("-").reverse().join("-");
		// var date2 = $('#journeyDateFrom').val();
		// var date3 = $('#journeyDateTo').val();
		// $scope.bookingRQ.fromBookingDate=date;
		// $scope.bookingRQ.toBookingDate=date1;
		// $scope.bookingRQ.fromTravelDate=date2;
		// $scope.bookingRQ.toTravelDate=date3;
		
		// $scope.bookingRQ.prodType=prodvalue;





		$scope.bookingRQ={
			"page": null,
			"getAllResults": true,
			"filter": {
				"logic": "and",
				"filters": [
					{
						"field":"product",
						"operator":"eq",
						"value":prodvalue
					}
		
				]
			},"sort": [
				{
					"field": "createTime",
					"dir": "desc"
				}
			]
		}

		
		$scope.artloading = true;
		$http.post(ServerService.profilePath + 'search/booking', $scope.bookingRQ, {
	
			headers: headersOptions
		}).then(function (response) {
			$scope.artloading = false;
			if (response.data.success == true) {

				$scope.dataSourcedata=response.data.data.bookingDtlsGridDataBean;
			
				$("#grid").kendoGrid({
					dataSource:$scope.dataSourcedata,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					filterable: true,
		 
					sortable: true,
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 10
					},
					columns: $scope.columnFileds
				});
			}
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.checkAttraction(page,prodvalue);
		});
            }
          });
	}
	





	
	// prepare traveller search request
	// $scope.retriveBookingReq={
	// 	"fromBookingDate":null,
	// 	"toBookingDate":null,
	// 	"fromTravelDate":null,
	// 	"toTravelDate":null
		
	// }

	function retrieveBookingReq(){
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
	$scope.retriveBookingReq={
		"page": null,
		"getAllResults": true,
		"filter": {
			"logic": "and",
			"filters": [
				{
					"field":"product",
					"operator":"eq",
					"value":"FLT"
				}
	
			]
		},"sort": [
			{
				"field": "createTime",
				"dir": "desc"
			}
		]
	}


	
	$http.post(ServerService.listPath + 'search/booking', $scope.retriveBookingReq, {
	
		headers: headersOptions
	}).then(function successCallback(response) {
		
		$scope.loading = false;
		$scope.disabled = false;
		if (response.data.success == true && response.data.data.data.length>0) {
			$scope.dataSourcedata=response.data.data.data;
		
		} else {
	
			$scope.noRecords = true;
			
		}
	
	}, function errorCallback(response) {
		if(response.status == 403){
			var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
			status.then(function(greeting) {
				retrieveBookingReq();
	});
		}
	  });
	}
	retrieveBookingReq();

	$scope.viewDocPnr = function( pnrNumber, prodType){
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
		$('body').addClass("loading");
		
		if(prodType=='FLT'){
			$scope.prodTypevalue="F";
			$rootScope.tabmenuactive = 1;
		}
		if(prodType=='HTL'){
			$scope.prodTypevalue="H";
			$rootScope.tabmenuactive = 2;
		}
		if(prodType=='INS'){
			$scope.prodTypevalue="I";
			$rootScope.tabmenuactive = 4;
		}
	
		
		 $http.get(ServerService.serverPath+'booking-dtls/'+$scope.prodTypevalue+'/'+pnrNumber,{
			headers: headersOptions
		 }	 ).then(function successCallback(response){
		
		 	
			console.log(response +"itinerary");
			if(response.data.status == "success"){
				var jsondata= response.data.data;
				if(jsondata.serviceOrderType == 'FLT'){
					sessionStorage.setItem("bookingItneryResponse",JSON.stringify(response.data));
					sessionStorage.setItem("activeService", 1);
					window.open($location.$$absUrl.replace($location.$$path,'/flightItenary'), '_blank');
					// $location.path('/flightItenary');
					}
					if(jsondata.serviceOrderType == 'HTL'){
						sessionStorage.setItem("activeService", 2);
						sessionStorage.setItem("bookingItneryResponse",JSON.stringify(response.data));
						window.open($location.$$absUrl.replace($location.$$path,'/hotelItenary'), '_blank');
					// $location.path('/hotelItenary');
						}
						if(jsondata.serviceType == 'INSRNC'){
							sessionStorage.setItem("activeService", 4);
						sessionStorage.setItem("bookingItneryResponse",JSON.stringify(response.data));
					// $location.path('/insuranceItenary');
					window.open($location.$$absUrl.replace($location.$$path,'/insuranceItenary'), '_blank');
						}
				
			} else {
			$location.path('/user');
				
				Lobibox.alert('error',
				{
					msg: response.data.errorMessage
				});
			}
			$timeout(function(){
				$('body').removeClass("loading");
				$("html,body").scrollTop(0);
			},1000);
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.viewDocPnr(pnrNumber,prodType);
		});
            }
          });
		 
	}




	$scope.curentpage = 1;
	$scope.retriveBookingData=function(){
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
		var fromBookingDate = $('#bookingDateFrom').val();
		// var fromBookingDate = datereverse.split("-").reverse().join("-");
		if(fromBookingDate){
			var bookingFrom={
				"field":"createTime",
				"operator":"gte",
				"value":fromBookingDate
				}
		}
		var toBookingDate = $('#bookingDateTo').val();
		// var toBookingDate = date1reverse.split("-").reverse().join("-");
		if(toBookingDate){
			var bookingTo={
				"field":"createTime",
				"operator":"lte",
				"value":toBookingDate
				}
		}
		var journyDateFrom = $('#journeyDateFrom').val();
		// var journyDateFrom = date2.split("-").reverse().join("-");
		if(journyDateFrom){
			var journyFrom={
				"field":"journeyDate",
				"operator":"gte",
				"value":journyDateFrom
				}
		}
		var journyDateto = $('#journeyDateTo').val();
		// var journyDateto = date3.split("-").reverse().join("-");
		if(journyDateto){
			var journyto={
				"field":"journeyDate",
				"operator":"lte",
				"value":journyDateto
				}
		}
		// $scope.bookingRQ.fromBookingDate=fromBookingDate;
		// $scope.bookingRQ.toBookingDate=toBookingDate;
		// $scope.bookingRQ.fromTravelDate=journyDateFrom;
		// $scope.bookingRQ.toTravelDate=journyDateto;
		// $scope.bookingRQ.prodType=	$scope.prodvalue;

		$scope.bookingRQ={
			"page": null,
			"getAllResults": true,
			"filter": {
			"logic": "and",
			"filters": [
			{
			"field":"product",
			"operator":"eq",
			"value":$scope.prodvalue
			},
			
			bookingFrom,
			bookingTo,
			journyFrom,
			journyto

			
			]
			},"sort": [
				{
					"field": "createTime",
					"dir": "desc"
				}
			]
			}

			for(var a=0; a<$scope.bookingRQ.filter.filters.length; a++){
				
if($scope.bookingRQ.filter.filters[a]==undefined){
	$scope.bookingRQ.filter.filters.splice(a, a);
}
			}

		
		$scope.artloading = true;
		$http.post(ServerService.listPath + 'search/booking', $scope.bookingRQ, {
	
			headers: headersOptions
		}).then(function successCallback(response) {
			$scope.artloading = false;
			if (response.data.success == true) {
				$scope.dataSourcedata=response.data.data.data;
				$scope.curentpage = 1;
				// $("#grid").kendoGrid({
				// 	dataSource:$scope.dataSourcedata,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
					// },
				// 	height: 550,
				// 	groupable: true,
				// 	filterable: true,
		 
				// 	sortable: true,
				// 	pageable: {
				// 		refresh: true,
				// 		pageSizes: true,
				// 		buttonCount: 10
				// 	},
				// 	columns: $scope.columnFileds
				// });
			}
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.retriveBookingData(pnrNumber,prodType);
		});
            }
          });
		 
	}

	$scope.pageChanged = function(page){
		$scope.curentpage = page;
	}

	$scope.retriveBookingDataClear=function(){
		$('#bookingDateFrom').val('');
		$('#bookingDateTo').val('');
		$('#journeyDateFrom').val('');
		$('#journeyDateTo').val('');
		// $scope.bookingRQ.fromBookingDate="";
		// $scope.bookingRQ.toBookingDate="";
		// $scope.bookingRQ.fromTravelDate="";
		// $scope.bookingRQ.toTravelDate="";
	}

	$scope.travelReq = function(page,count) {
		$scope.userProfileReq.docketNo = null;
		$scope.userProfileReq.paxsName = null;
		$scope.userProfileReq.status = null;
		$scope.userProfileReq.service = null;
		$scope.userProfileReq.docketDate = null;
		$scope.userProfileReq.category = "SELF";
		$scope.userProfileReq.createBy = null;
		$scope.userProfileReq.orgin = null;
		$scope.userProfileReq.destination = null;
		$scope.userProfileReq.gridMode = "child";
		$scope.userProfileReq.searchMode = "grid";
		$scope.userProfileReq.fromIndex = 0;
		$scope.userProfileReq.toIndex = 0;
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	
	$scope.getTraveller = function () {
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname =tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
	var params = "page=1&count=10"
	var paramsObj= {'count': '10', 'page': '1', 'pagination': 'true', 'sortBy': undefined , 'sortOrder': undefined}
	$scope.loading = true;
		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/docket/search',$scope.userProfileReq).then(function successCallback(response) {
			$scope.loading = false;
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.recentBookingData = response.data.data.docketChildList;
				
				$scope.totTrvCount = response.data.data.totalCount;
				//$location.path('/user-trips');
				$location.path('/user');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
			} else {
				$scope.recentBookingData = [];
				$scope.totTrvCount = 0;
				$scope.noRecords = true;
			//	$location.path('/user-trips');
				$location.path('/user');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
			}
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.getTraveller();
		});
            }
          });	
	};
	
	$scope.tripsModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	// $scope.retriveBookingData=function(){
	// 	console.log();
	// }


	
	$scope.logOut = function(){
	sessionStorage.clear();
	localStorage.clear();
	//$location.path('/');
	window.location.href="index.jsf"
		}
		$scope.changePwd = function(size, parentElem){
			  var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'changePwd.html',
			controller : 'passwordChangeController',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
			}
});
ouaApp.controller('passwordChangeController', function($scope,refreshService, $uibModalInstance, $http,urlService,ServerService, $location, $timeout){
	ouaApp.service('urlService', function ($location) {
		//this.relativePath = $location.absUrl().split('/')[6];
		var currentURL = $location.absUrl();
		this.relativePath = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length);
	
	});

	$scope.changePasReq = {}
	$scope.userName = JSON.parse(localStorage.getItem('loginName'));
	//$scope.changePasReq.userName = $scope.userName.userName;
	$scope.changePasReq.userName = $scope.userName.fname;
		//$scope.changePasReq.activationDtlId = $scope.userName.activationDtlId;
	// $scope.changePasReq.activationDtlId = 135;
		$scope.close = function(){
		$uibModalInstance.close();
		}
		$scope.resetPassword = function(){
			var tokenresult = JSON.parse(localStorage.getItem('authentication'));
			var tokenname =tokenresult.data;
			var headersOptions = {
				"g-recaptcha-response" :   $scope.token,
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + tokenname.authToken
			}
			if($scope.isvalid){	
				if($scope.changePasReq.password != $scope.changePasReq.confirmPassword){
					$scope.mismatch = "PassWord And Confirm Passowrd Should be Same";
			} else if($scope.changePasReq.password != '' && $scope.changePasReq.confirmPassword != '' && $scope.changePasReq.userName != '' && $scope.changePasReq.oldPassword != '' && $scope.changePasReq.activationDtlId !=''){
				$scope.mismatch = "";
				console.log($scope.changePasReq);
				var tokenresult = JSON.parse(localStorage.getItem('authentication'));
				var tokenname =tokenresult.data;
				var headersOptions = {
					"g-recaptcha-response" :   $scope.token,
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + tokenname.authToken
				}
				$http.post(ServerService.authPath+'user/change-password', $scope.changePasReq, { headers: headersOptions }).then(function successCallback(response){
					console.log(response);
					if(response == undefined){
						Lobibox.alert('error', {
                            msg: "Invalid old password"
                        })
					}
					if(response.data.success == true){
					$scope.dispsuccMsg = true;$scope.dispErrMsg = false;
						//$scope.successMsg = response.data.message;
						Lobibox.alert('success', {
							msg: response.data.message,
							callback: function(lobibox, type) {
								if (type === 'ok') {
									$timeout(function(){
										$scope.dispsuccMsg = false;
										$scope.dispErrMsg = false;
										$uibModalInstance.dismiss();
										}, 1000)
								
								}
							}
						});
						localStorage.islogged=false;
						$scope.username='';
						if($location.absUrl().split('username=')[1]){
							$scope.username=$location.absUrl().split('username=')[1];
						}else{
							$scope.username=DEFAULT_USER
						}
								var authenticationreq={
							 "username":$scope.username
							 }
							 if (localStorage.authentication == undefined) {

				/* var Serverurl="http://3.6.54.119:8091/ibp/api/v1/"; */
			 $http.post(ServerService.authPath+'guest-login', JSON.stringify(authenticationreq),{ 
				headers: {'Content-Type': 'application/json'}
					   }).then(function(response){
						   if(response.status == 200){
							$location.path('/home');
							window.location.reload();
							localStorage.removeItem('authentication');
							localStorage.removeItem('userBookingdetails');
							localStorage.removeItem('loginName');
							
						   }
				   var authenticationreqres=response;
				   localStorage.setItem('authentication',JSON.stringify(authenticationreqres));
			 }); 
			 
			}
					} else if(response.data.status == "failure" || response.data.status == "error") {
				$scope.dispErrMsg = true; $scope.dispsuccMsg = false;
					$scope.errorMsg = response.data.errorMessage
					}
					
					// $timeout(function(){
					// 	$scope.dispsuccMsg = false;
					// 	$scope.dispErrMsg = false;
					// 	$uibModalInstance.dismiss();
					// 	}, 2000)
					}, function errorCallback(response) {
						if(response.status == 403){
							var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
							status.then(function(greeting) {
								$scope.resetPaswrd(valid);
					});
						}
					  });	
			}
			}
		}


		
		$scope.resetPaswrd = function(isvalid) {
			$scope.recaptchaId = JSON.parse(localStorage.getItem('configKeys'));
			$scope.isvalid=isvalid;
				grecaptcha.ready(function() {
					grecaptcha.execute($scope.recaptchaId.google_recaptcha_public_key, {})
							.then(function(token) {
								// sessionStorage.setItem("captcha",token);
								$scope.token=token;
								$scope.resetPassword();
							});
				});
			
	
		   
	
		}
	
			
			$scope.modelDismiss = function(){
				$uibModalInstance.close();
				}
})