"use strict";
ouaApp.controller( 'userTripsCtrl' , function( $scope, $location, $interval, $http, $timeout,$rootScope, ServerService, ajaxService,$window,NationService,CountryService) {
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	$scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
	var tokenname = $scope.tokenresult.data;
	var headersOptions = {
	  'Content-Type': 'application/json',
	  'Authorization': 'Bearer ' + tokenname.authToken
	}



	$scope.retriveBookingReq={
		"fromBookingDate":"04-11-2020",
		"toBookingDate":"05-11-2020",
		"fromTravelDate":null,
		"toTravelDate":null,
		"prodType":"INSRNC"
	}
	
	
	$http.post(ServerService.profilePath + 'bookings/all', $scope.retriveBookingReq, {
	
		headers: headersOptions
	}).then(function (response) {
		$scope.loading = false;
		$scope.disabled = false;
		if (response.data.status == 'success') {
			sessionStorage.setItem('flightSearchRS', JSON.stringify(response.data));
			
			  $location.path('/flightResult');
		} else {
	
			// Lobibox.alert('error', {
			// 	msg: response.data.errorMessage
			// })
			
		}
	
	});










	// $scope.serviceBaseURl="https://wowsochi.ae/myaccount/b2cprofile/api";
	$scope.serviceBaseURl="http://uat.wowsochi.convergentechnologies.com/b2cprofile/api";
		// $scope.serviceBaseURl="https://wowsochi.ae/b2cprofile/api";
	$scope.init = {'page':1, 'count':10	};
	// $scope.role=document.getElementById('rolename').value;
	$scope.userProfileReq = {};
	$scope.noRecords = false;
	$scope.travelClick = false;
	$scope.fCheck = true;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =false;
		
		// $scope.agencyId=JSON.stringify(JSON.parse(document.getElementById('userId').value));

		$scope.dataTosend= {
			"filter": {
			   "logic": "and",
			   "filters": [
					   {
			  
				   "field": 'createBy',
				   "operator": "eq",
				   "value": $scope.agencyId
					   }
					   ]
				
				 
			 }
		   }
		 
	if($scope.fCheck){
		$scope.field='createBy';
		
	}else if($scope.hCheck){
		$scope.field='createBy';
		$scope.dataTosend= {
			"filter": {
			  "logic": "and",
			  "filters": [
				
				{
				 "field": 'screenType',
				 "operator": "eq",
				 "value": 'HOLIDAYS'
			   },
				{
				    "field": $scope.field,
				  "operator": "eq",
				  "value": $scope.agencyId
			
				}				 
				
			  ]
			}
		}
		
	}else if($scope.atCheck){
		$scope.field='createBy';
		$scope.dataTosend= {
			"filter": {
			  "logic": "and",
			  "filters": [
				
				{
				 "field": 'screenType',
				 "operator": "eq",
				 "value": 'ATTRACTIONS'
			   },
				{
				    "field": $scope.field,
				  "operator": "eq",
				  "value": $scope.agencyId
			
				}				 
				
			  ]
			}
		}
		
	}else if($scope.insCheck){
		$scope.field='createBy';
		$scope.dataTosend= {
			"filter": {
			  "logic": "and",
			  "filters": [
				
				{
				    "field": $scope.field,
				  "operator": "eq",
				  "value": $scope.agencyId
			
				}				 
				
			  ]
			}
		}
	
	}
	

	$scope.dataURL=$scope.serviceBaseURl+"/search/company-flight"
		$scope.columnFileds=[
			{
			
					field: "bookingDetails",
				title: "BookingDetails"
			},{
				// template:"<div class='customer-name'  onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'FLT');'>#: bkngRefNo #</div>",
				field: "bkngRefNo",
				title: "Reference No"
			},{
				field: "bookingDate",
				title: "BookingDate"
			},{
				field: "firstName",
				title: "Passenger"
			}
			,{
				field: "totalFare",
				title: "TotalFare"
			},{
				field: "status",
				title: "Status"
				
			},{
				command: 
			   { text: "View", click: viewFLTPnr }, title: " " 
		   }
			
		]


	$scope.checkFlight = function() {
		$scope.fCheck = true;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =false;
		$scope.dataTosend= {
			"filter": {
			   "logic": "and",
			   "filters": [
					   {
			  
				   "field": 'createBy',
				   "operator": "eq",
				   "value": $scope.agencyId
					   }
					   ]
				
				 
			 }
		   }
		 
		$scope.dataURL=$scope.serviceBaseURl+"/search/company-flight"
		$scope.columnFileds=[
			{
			
					field: "bookingDetails",
				title: "BookingDetails"
			},{
				// template:"<div class='customer-name' onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'FLT');'>#: bkngRefNo #</div>",
				field: "bkngRefNo",
				title: "Reference No"
			},{
				field: "bookingDate",
				title: "BookingDate"
			},{
				field: "firstName",
				title: "Passenger"
			}
			,{
				field: "totalFare",
				title: "TotalFare"
			},{
				field: "status",
				title: "Status"
				
			},{
				command: 
			   { text: "View", click: viewFLTPnr }, title: " "
		    }
			
		]
		$scope.gridBinding();
	
	};
	$scope.checkInsurance = function(page) {
		$scope.fCheck = false;
		$scope.hCheck = false;
		$scope.insCheck =true;
		$scope.atCheck =false;
	$scope.dataTosend= {
			"filter": {
			   "logic": "and",
			   "filters": [
					   {
			  
				   "field": 'createBy',
				   "operator": "eq",
				   "value": $scope.agencyId
					   }
					   ]
				
				 
			 }
		   }
		 
		$scope.dataURL=$scope.serviceBaseURl+"/search/company-insurance"
		$scope.columnFileds=[
			{
			
					field: "bookingDetails",
				title: "BookingDetails"
			},{
				// template:"<div class='customer-name' onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'INSNRC');'>#: bookingRefNo #</div>",
				field: "bookingRefNo",
				title: "Reference No"
			},{
				field: "gdsRefNo",
				title: "GDSRefNo"
			},		
			{
				field: "bookingDate",
				title: "BookingDate"
			},{
				field: "firstName",
				title: "Passenger"
			}
			,{
				field: "totalAmount",
				title: "TotalAmount"
			},{
				field: "status",
				title: "Status"
				
			},{
				command: 
			   { text: "View", click: viewinsPnr }, title: " "
		   }
			
		]
		$scope.gridBinding();
	};

	$scope.checkHoliday = function(page) {
		$scope.fCheck = false;
		$scope.hCheck = true;
		$scope.insCheck =false;
		$scope.atCheck =false;
		$scope.field='createBy';
		$scope.dataTosend= {
			"filter": {
			  "logic": "and",
			  "filters": [
				
				{
				 "field": 'screenType',
				 "operator": "eq",
				 "value": 'HOLIDAYS'
			   },
				{
				    "field": $scope.field,
				  "operator": "eq",
				  "value": $scope.agencyId
			
				}				 
				
			  ]
			}
		}
		$scope.dataURL=$scope.serviceBaseURl+"/search/company-holiday-attraction"
		$scope.columnFileds=[
		{
			// template:"<div class='customer-name' onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'HOLIDAYS');'>#: merchantRef #</div>",
			field: "merchantRef",
			title: "Reference No"
		},{
			field: "bookingTime",
			title: "BookingDate"
		},{
			field: "firstName",
			title: "Passenger"
		},{
			field: "paxType",
			title: "Passenger Type"
		}
		,{
			field: "emailId",
			title: "EmailId"
		},{
			field: "mobileNumber",
			title: "MobileNumber"
			
		},{
			command: 
		   { text: "View", click: viewpacPnr }, title: " "
		}
		]
		$scope.gridBinding();
	};

	$scope.checkAttraction = function(page) {
		$scope.fCheck = false;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =true;
		$scope.field='createBy';
		$scope.dataTosend= {
			"filter": {
			  "logic": "and",
			  "filters": [
				
				{
				 "field": 'screenType',
				 "operator": "eq",
				 "value": 'ATTRACTIONS'
			   },
				{
				    "field": $scope.field,
				  "operator": "eq",
				  "value": $scope.agencyId
			
				}				 
				
			  ]
			}
		}
		$scope.dataURL=$scope.serviceBaseURl+"/search/company-holiday-attraction"
		$scope.columnFileds=[
		{
			// template:"<div class='customer-name' onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'ATTR');'>#: merchantRef #</div>",
			field: "merchantRef",
			title: "Reference No"
		},{
			field: "bookingTime",
			title: "BookingDate"
		},{
			field: "firstName",
			title: "Passenger"
		},{
			field: "paxType",
			title: "Passenger Type"
		}
		,{
			field: "emailId",
			title: "EmailId"
		},{
			field: "mobileNumber",
			title: "MobileNumber"
		},{
			command: 
		   { text: "View", click: viewpacPnr }, title: " "
		   
		}
		]
		$scope.gridBinding();
	};









	$scope.gridBinding = function() {
		$('body').addClass("loading");
		$.ajax({
			url:$scope.dataURL,
			type: "POST",
			  data: JSON.stringify($scope.dataTosend),
			contentType: "application/json; charset=utf-8",

			success: function (resdata, status) {
				if(resdata.data){
					
				}else{
					resdata.data.data=[];
				}
				if($scope.fCheck){
					for(var i=0;i<resdata.data.data.length;i++){
						resdata.data.data[i].bookingDetails=resdata.data.data[i].clientNo+' '+resdata.data.data[i].origin+'->'+resdata.data.data[i].destination
					}
				}else if($scope.insCheck){
					for(var i=0;i<resdata.data.data.length;i++){
						resdata.data.data[i].bookingDetails=resdata.data.data[i].tktRefNo+' '+resdata.data.data[i].origin+'->'+resdata.data.data[i].destination
					}
				}
				
				
				$("#grid").kendoGrid({
					dataSource:resdata.data.data,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					filterable: true,
         
					sortable: true,
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 5
					},
					columns: $scope.columnFileds
				});
				$('body').removeClass("loading");
			}
		});
	}

	function viewFLTPnr(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		// e.preventDefault();
		console.log(dataItem);
				$('body').addClass("loading");
				var docPnrRequest = new Object();
				docPnrRequest.clientRefNo = dataItem.clientNo;
				docPnrRequest.bookingRefNo = dataItem.bkngRefNo;
				docPnrRequest.type = 'DB';
				// if(prodType=='FLT'){
					docPnrRequest.serviceType = 'FLT';
				// }
				
				
				var docPnrRequestJson = JSON.stringify(docPnrRequest);	
				$http({
					method:'POST',				
					data:docPnrRequestJson,
					url:ServerService.serverPath+'rest/retrieve/itinerary',
					}).success(function(data,status){				
					if(data.status=="SUCCESS"){	
						var jsondata=data.data;
						// if(jsondata.serviceOrderType=='FLT'){
							sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
							$location.path('/flight-itinerary');
						// }
					
						} else {
						Lobibox.alert('error',
						{
							msg: data.errorMessage
						});
					}
					$timeout(function(){
						$('body').removeClass("loading");
						$routeSegment.chain[0].reload();
						//location.reload(true);
						$("html,body").scrollTop(0);
					},1000);
						
			
				})
			
	}
	function viewinsPnr(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		// e.preventDefault();
		console.log(dataItem);
				$('body').addClass("loading");
				var docPnrRequest = new Object();
				docPnrRequest.clientRefNo = dataItem.tktRefNo;
				docPnrRequest.bookingRefNo = dataItem.bookingRefNo;
				docPnrRequest.type = 'DB';
				// if(prodType=='FLT'){
					docPnrRequest.serviceType = 'INSRNC';
				// }
				
				
				var docPnrRequestJson = JSON.stringify(docPnrRequest);	
				$http({
					method:'POST',				
					data:docPnrRequestJson,
					url:ServerService.serverPath+'rest/retrieve/itinerary',
					}).success(function(data,status){				
					if(data.status=="SUCCESS"){	
						var jsondata=data.data;
						// if(jsondata.serviceOrderType=='FLT'){
							sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insurance-itinerary');
						// }
					
						} else {
						Lobibox.alert('error',
						{
							msg: data.errorMessage
						});
					}
					$timeout(function(){
						$('body').removeClass("loading");
						$routeSegment.chain[0].reload();
						//location.reload(true);
						$("html,body").scrollTop(0);
					},1000);
						
			
				})
			
	}
	function viewpacPnr(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		// e.preventDefault();
		console.log(dataItem);
				$('body').addClass("loading");
				var docPnrRequest = new Object();
				// docPnrRequest.clientRefNo = dataItem.tktRefNo;
				// docPnrRequest.bookingRefNo = dataItem.bookingRefNo;
				// docPnrRequest.type = 'DB';
				// // if(prodType=='FLT'){
				// 	docPnrRequest.serviceType = 'HOLIDAYS';
				// }
				
				
				// var docPnrRequestJson = JSON.stringify(docPnrRequest);	
				$http({
					method:'GET',				
					// data:docPnrRequestJson,
					url:ServerService.serverPath+'rest/holiday/retrieve/'+dataItem.merchantRef,
					contentType:"application/json; charset=utf-8"
					}).success(function(data,status){				
					if(data.status=="SUCCESS"){	
						var jsondata=data.data;
						// if(jsondata.serviceOrderType=='FLT'){
							sessionStorage.setItem("packageconfirmJson",JSON.stringify(jsondata));
					sessionStorage.setItem('holidayPackagedata',JSON.stringify(jsondata));
					$location.path('/confirmed-package-itinerary');
						// }
					
						} else {
						Lobibox.alert('error',
						{
							msg: data.errorMessage
						});
					}
					$timeout(function(){
						$('body').removeClass("loading");
						$routeSegment.chain[0].reload();
						//location.reload(true);
						$("html,body").scrollTop(0);
					},1000);
						
			
				})
			
	}


	// prepare traveller search request
	
	$scope.travelReq = function(page,count) {
		$scope.userProfileReq.docketNo = null;
		$scope.userProfileReq.paxsName = null;
		$scope.userProfileReq.status = null;
		$scope.userProfileReq.service = null;
		$scope.userProfileReq.docketDate = null;
		$scope.userProfileReq.category = "SELF";
		$scope.userProfileReq.createBy = null;
		$scope.userProfileReq.orgin = null;
		$scope.userProfileReq.destination = null;
		$scope.userProfileReq.gridMode = "child";
		$scope.userProfileReq.searchMode = "grid";
		$scope.userProfileReq.fromIndex = 0;
		$scope.userProfileReq.toIndex = 0;
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	$scope.checkFlight()
	$scope.getTraveller = function (params, paramsObj) {
		$scope.checkFlight()
		// if($scope.travelClick==false){
		// 	$scope.userProfileReq = {};
		// }
		// $scope.travelReq(paramsObj.page,paramsObj.count);
		// $scope.page = paramsObj.page;
		// return $http.post(ServerService.serverPath+'rest/docket/search',$scope.userProfileReq).then(function (response) {
		// 	if(response.data.status=='SUCCESS' && response.data.data!='null') {
		// 		$scope.recentBookingData = response.data.data.docketChildList;
		// 		$scope.totTrvCount = response.data.data.totalCount;
		// 		$location.path('/user-trips');
		// 		return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
		// 		};
		// 	} else {
		// 		$scope.recentBookingData = [];
		// 		$scope.totTrvCount = 0;
		// 		$scope.noRecords = true;
		// 		$location.path('/user-trips');
		// 		return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
		// 		};
		// 	}
		// });	
	};
	
	$scope.tripsModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	function viewDocPnr (clientRefNo, pnrNumber, prodType){
		$('body').addClass("loading");
		var docPnrRequest = new Object();
		docPnrRequest.clientRefNo = clientRefNo;
		docPnrRequest.bookingRefNo = pnrNumber;
		docPnrRequest.type = 'DB';
		if(prodType=='FLT'){
			docPnrRequest.serviceType = 'FLT';
		}
		if(prodType=='HTL'){
			docPnrRequest.serviceType = 'HTL';
		}
		if(prodType=='INSRNC'){
			docPnrRequest.serviceType = 'INSRNC';
		}
		if(prodType=='VISA'){
			docPnrRequest.serviceType = 'VISA';
		}
		var docPnrRequestJson = JSON.stringify(docPnrRequest);	
		$http({
			method:'POST',				
			data:docPnrRequestJson,
			url:ServerService.serverPath+'rest/retrieve/itinerary',
			}).success(function(data,status){				
			if(data.status=="SUCCESS"){	
				var jsondata=data.data;
				if(jsondata.serviceOrderType=='FLT'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
					$location.path('/flight-itinerary');
				}
				if(jsondata.serviceOrderType=='HTL'){
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata));
					$location.path('/hotel-itinerary');
				}
				if(jsondata.serviceOrderType=='INSRNC'){
					sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insurance-itinerary');
				}
				if(jsondata.serviceOrderType=='VISA'){
					sessionStorage.setItem("visaItineraryJson",JSON.stringify(jsondata));
					$location.path('/visa-itinerary');
				}
				if (jsondata.serviceOrderType=='FNH'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
					$location.path('/flight-hotel-itinerary');
				}
				} else {
				Lobibox.alert('error',
				{
					msg: data.errorMessage
				});
			}
			$timeout(function(){
				$('body').removeClass("loading");
				$routeSegment.chain[0].reload();
				//location.reload(true);
				$("html,body").scrollTop(0);
			},1000);
		})
	}
});