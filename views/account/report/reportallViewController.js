ouaApp.controller('userReportCtrls', function($scope, $location,refreshService, $interval, $http, $timeout, ServerService,ajaxService, NationService,CountryService, customSession, $uibModal){
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords = false;
	$scope.travelClick = false;
	
	$scope.checkFlightOption = true;
	$("#checkFlight").addClass("active");
	$scope.checkHotelOption = false;
	$scope.checkInsuranceOption = false;
	if(JSON.parse(localStorage.getItem('authentication'))){
		$scope.role = JSON.parse(localStorage.getItem('authentication'));
		$scope.roleId = $scope.role.userId;
		}
	$scope.serviceData = [{label: 'FLIGHT', value: 'FLT'}, {label: 'HOTEL', value: 'HTL'}, {label: 'HOLIDAY', value: 'FNH'}];
	
	/* traveller search */
	
	$scope.agentUserId = 162;
	var fi;
	$( "#userReportEmail" ).autocomplete({
			source: function( request, response ) {
				ajaxService.ajaxRequest('GET',ServerService.serverPath+'rest/repo/users/'+request.term+'?value='+$scope.agentUserId+'&page=1&size=10','json')
				.then(function successCallback(data,status) {
					if(data.status=="SUCCESS" && data.data!=null) {
						fi=0;
						response( data.data );
						}else{
						$( "#userReportEmail" ).val('');
						$('.ui-autocomplete').hide();
					}
				}, function errorCallback(response) {
					if(response.status == 403){
						var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
						status.then(function(greeting) {
						   });                
					}
				});
			},
			focus: function( event, ui ) {
				$( "#userReportEmail" ).val( ui.item.userName);
				return false;
			},
			change : function(event,ui) {
				$timeout(function(){
					jQuery('.form-group').removeClass('has-error');
				},10);
			},
			//autoFocus: true,
			minLength: 3,
			select: function( event, ui ) {
				$( "#userReportEmail" ).val( ui.item.userName);
				$scope.userCreateById = ui.item.userId;
				return false;
			},
			open: function() {
				$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			},
			close: function(event, ui) {
				$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			}
			});
		
	// 	$scope.travelReq = function(page,count) {
	// 	$scope.userProfileReq.category = "REPORT";
	// 	$scope.userProfileReq.searchMode = 'grid';
	// 	$scope.userProfileReq.gridMode = "child";
	// 	$scope.userProfileReq.page = page;
	// 	$scope.userProfileReq.count = count;
	// };

	
	$scope.checkFlight = function () {
		$scope.checkFlightOption = true;
		$scope.checkHotelOption = false;
		$scope.checkInsuranceOption = false;
		// $scope.toDate = '';
		// $scope.fromDate = '';
		$("#checkFlight").addClass("active");
		$("#checkHotel").removeClass("active");
		$("#checkInsurance").removeClass("active");
		$scope.recentBookingData = '';
	}
	$scope.checkHotel = function () {
		$scope.checkFlightOption = false;
		$scope.checkHotelOption = true;
		$scope.checkInsuranceOption = false;
		// $scope.toDate = '';
		// $scope.fromDate = '';
		$("#checkHotel").addClass("active");
		$("#checkFlight").removeClass("active");
		$("#checkInsurance").removeClass("active");
		$scope.recentBookingData = '';
	}
	$scope.checkInsurance = function () {
		$scope.checkFlightOption = false;
		$scope.checkHotelOption = false;
		$scope.checkInsuranceOption = true;
		// $scope.toDate = '';
		// $scope.fromDate = '';
		$("#checkInsurance").addClass("active");
		$("#checkHotel").removeClass("active");
		$("#checkFlight").removeClass("active");
		$scope.recentBookingData = '';
	}
	
	$scope.downloadBooking = function() {
		$('body').addClass('loading');
		// $scope.recentBookingDataDownload.flightResponseList = $scope.flightResponse;
		// $scope.recentBookingDataDownload.hotelResponseList = $scope.hotelResponse;
		// $scope.recentBookingDataDownload.insuranceResponseList = $scope.insuranceResponse;
		$http({
			url : ServerService.serverPath+'rest/user/allbooking/download',
			method : 'POST',
			data : JSON.stringify($scope.recentBookingDataDownloadmap),
			responseType:'arraybuffer'
			}).then(function(response) {
			$('body').removeClass('loading');
			var file = new Blob([ response.data ], {
				type : 'application/xlsx'
			});
			var fileURL = URL.createObjectURL(file);
			var a         = document.createElement('a');
			a.href        = fileURL; 
			a.target      = '_blank';
			a.download    = 'Your_Booking.xlsx';
			document.body.appendChild(a);
			a.click();
			}).error(function(data, status, headers, config) {
		});
	};
	
	$scope.getTraveller = function () {
	
	var params = "page=1&count=10"
	var paramsObj= {'count': '10', 'page': '1', 'pagination': 'true', 'sortBy': undefined , 'sortOrder': undefined}
	
		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/docket/allbooking/search',$scope.userProfileReq).then(function (response) {
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.recentBookingData = response.data.data.docketChildList;
				$scope.totTrvCount = response.data.data.totalCount;
				$location.path('/report');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/report');
			}
		});	
	};

	$scope.columnFileds=[
		{
			// template:"<div class='customer-name'  onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'ATTR');'>#: merchantRef #</div>",
			field: "Title",
			title: "Title"
		},{
			field: "GivenName",
			title: "GivenName"
		},{
			field: "Surname",
			title: "Surname"
		},{
			field: "Passenger",
			title: "Passenger Type"
		}
		,{
			field: "TicketNo",
			title: "TicketNo"
		},{
			field: "Status",
			title: "Status"
			
		}
		]

		$("#grid").kendoGrid({
			dataSource:[],
			columns:[]
		});

	$("#grid").kendoGrid({
		toolbar: ["excel"],
		excel: {
			fileName: "BookingReport.xlsx",
			proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
			filterable: true
		},
		
		dataSource: [
			{ Title: "MR", GivenName: "Venkatesh",Surname: "Convergent",Passenger: "Adult",TicketNo: "5435647",Status: "TICKETED",},
			{ Title: "MISTER", GivenName: "Raghav",Surname: "Cts",Passenger: "Child",TicketNo: "6547456",Status: "CONFIRMED",}
		  ],
		//  {
			// type: "odata",
			// transport: {
			//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
			// },
			pageSize: 20,
		// },
		height: 550,
		groupable: true,
		sortable: true,
		pageable: {
			refresh: true,
			pageSizes: true,
			buttonCount: 5
		},
		columns: $scope.columnFileds,
		// columns: [
		//   { field: "Title" },
		//   { field: "GivenName" },
		//   { field: "Surname" },
		//   { field: "Passenger" },
		//   { field: "TicketNo" },
		//   { field: "Status" }
		// ],
		
	  }).data("kendoGrid");
	  var grid = $("#grid").data("kendoGrid");
	  var altRows = grid.table.find("tr.k-alt");
	  altRows.css("background", "#afeeee");
	  

	
	$scope.tripsModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	$scope.promocodes = '';
	$scope.search = function () {
		$scope.travelClick = true;
		$scope.noRecords = false;
	
		$scope.userProfileReq.fromDate =$scope.fromDatereport;

		$scope.userProfileReq.toDate = $scope.toDate;
		
		$scope.userProfileReq.promoCode = $scope.promocodes;
		$scope.userProfileReq.serviceType = $scope.servicetype;
		$scope.userProfileReq.city = $scope.CityName;
		$scope.userProfileReq.searchMode='';
		 $scope.userProfileReq.createTime='';
		// $scope.userProfileReq.flyFrom=$scope.cityfrom;
		// $scope.userProfileReq.flyTo=$scope.CityTo;
		$scope.userProfileReq.validRequest=true;
		$scope.recentBookingData = {};
	
		$http.post(ServerService.serverPath+'rest/repo/company/search/reports',$scope.userProfileReq).then(function (response) {
			if(response.statusText=='OK') {
				$scope.statustable=true;
				$scope.flightResponse = response.data;
				$scope.hotelResponse = response.data;
			
				$scope.recentBookingDataDownloadmap = response.data;
				
				
				$scope.totTrvCount = response.data.length;
				$location.path('/reportAll');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/reportAll');
			}
		});
	};
	$scope.serviceTypeFlight=false;
	$scope.serviceTypeHotel=false;
	$scope.serviceTypes=function(servicedata){
		$scope.serviceType=servicedata;
		if($scope.serviceType=='F'){
			$scope.serviceTypeFlight=true;
			$scope.serviceTypeHotel=true;
		}else if($scope.serviceType=='H'){
			$scope.serviceTypeHotel=false;
			$scope.serviceTypeFlight=false;
		}
	}
	$scope.clear = function() {
		$scope.toDate = '';
		$scope.fromDatereport = '';
		$scope.promocodes = '';
		$scope.servicetype = '';
		$scope.CityName = '';

	}
	
	$( "#to_place" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
				
					}else{
					$( "#to_place" ).show();
					$( "#to_place" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},focus: function( event, ui ) {
			$( "#to_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},minLength: 3,
		select: function( event, ui ) {
		
				$( "#to_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.CityName = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$( "#to_place" ).parent().removeClass('has-error');
				$scope.errorDisp = false;
			
				flag = true;
			
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function(event, ui) {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	
	}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.CityName = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	// $( "#from_place" ).autocomplete({
	// 	source: function( request, response ) {
	// 		ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
	// 		.then(function(data,status) {
	// 			if(data!=null && data!='') {
	// 				f=0;
	// 				response( data );
				
	// 				}else{
	// 				$( "#from_place" ).show();
	// 				$( "#from_place" ).focus(0);
	// 				$('.ui-autocomplete').hide();
	// 			}
	// 		});
	// 	},focus: function( event, ui ) {
	// 		$( "#from_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
	// 		return false;
	// 	},minLength: 3,
	// 	select: function( event, ui ) {
		
	// 			$( "#from_place" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
	// 			$scope.cityfrom = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
	// 			$scope.$apply();
	// 			$( "#from_place" ).parent().removeClass('has-error');
	// 			$scope.errorDisp = false;
			
	// 			flag = true;
			
	// 		return false;
	// 	},
	// 	open: function() {
	// 		$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
	// 	},
	// 	close: function(event, ui) {
	// 		$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
	// 	}
	
	// }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	// 	if(f==0) {
	// 		f++;
	// 		$scope.cityfrom = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
	// 		return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
	// 		} else {
	// 		return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
	// 	}
	// };
	// $( "#to_placecity" ).autocomplete({
	// 	source: function( request, response ) {
	// 		ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
	// 		.then(function(data,status) {
	// 			if(data!=null && data!='') {
	// 				f=0;
	// 				response( data );
				
	// 				}else{
	// 				$( "#to_placecity" ).show();
	// 				$( "#to_placecity" ).focus(0);
	// 				$('.ui-autocomplete').hide();
	// 			}
	// 		});
	// 	},focus: function( event, ui ) {
	// 		$( "#to_placecity" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
	// 		return false;
	// 	},minLength: 3,
	// 	select: function( event, ui ) {
		
	// 			$( "#to_placecity" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
	// 			$scope.CityTo = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
	// 			$scope.$apply();
	// 			$( "#to_placecity" ).parent().removeClass('has-error');
	// 			$scope.errorDisp = false;
			
	// 			flag = true;
			
	// 		return false;
	// 	},
	// 	open: function() {
	// 		$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
	// 	},
	// 	close: function(event, ui) {
	// 		$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
	// 	}
	
	// }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	// 	if(f==0) {
	// 		f++;
	// 		$scope.CityTo = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
	// 		return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
	// 		} else {
	// 		return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
	// 	}
	// };


	$('.bookingDate-from').datepicker({ 
		format: 'dd/mm/yyyy',
		changeMonth: true,
		changeYear: true,
		minDate: '-5Y',
		maxDate: 0,
		yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){                                
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});
	$('.bookingDate-to').datepicker({ 
		format: 'dd/mm/yyyy',
		changeMonth: true,
		changeYear: true,
		minDate: '-5Y',
		maxDate: 0,
		yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){                                
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});


	// $('.bookingtime').datepicker({ 
	// 	format: 'dd/mm/yyyy',
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	minDate: '-5Y',
	// 	maxDate: 0,
	// 	yearRange: '1900:'+(new Date).getFullYear(),
	// 	showAnim: 'slideDown',
	// 	onChangeMonthYear:function(y, m, i){                                
	// 		var d = i.selectedDay;
	// 		$(this).datepicker('setDate', new Date(y, m-1, d));
	// 	}
	// });
	$scope.reportViewDocPnr = function(clientRefNo, pnrNumber, prodType){
		$('body').addClass("loading");
		var docPnrRequest = new Object();
		docPnrRequest.clientRefNo = clientRefNo;
		docPnrRequest.bookingRefNo = pnrNumber;
		docPnrRequest.type = 'DB';
		if(prodType=='FLT'){
			docPnrRequest.serviceType = 'FLT';
		}
		if(prodType=='HTL'){
			docPnrRequest.serviceType = 'HTL';
		}
		if(prodType=='INSRNC'){
			docPnrRequest.serviceType = 'INSRNC';
		}
		if(prodType=='VISA'){
			docPnrRequest.serviceType = 'VISA';
		}
		var docPnrRequestJson = JSON.stringify(docPnrRequest);	
		$http({
			method:'POST',				
			data:docPnrRequestJson,
			url:ServerService.serverPath+'rest/retrieve/itinerary',
			}).then(function(data,status){				
			if(data.data.status=="SUCCESS"){	
				var jsondata=data.data.data;
				if(jsondata.serviceOrderType=='FLT'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
					$location.path('/flightItenary');
				}
				if(jsondata.serviceOrderType=='HTL'){
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata));
					$location.path('/hotelItenary');
				}
				if(jsondata.serviceOrderType=='INSRNC'){
					sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insuranceItenary');
				}
				if(jsondata.serviceOrderType=='VISA'){
					sessionStorage.setItem("visaItineraryJson",JSON.stringify(jsondata));
					$location.path('/visa-itinerary');
				}
				if (jsondata.serviceOrderType=='FNH'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
					$location.path('/holidayItenary');
				}
				} else {
				$location.path('/report');
				Lobibox.alert('error',
				{
					msg: data.data.errorMessage
				});
			}
			$timeout(function(){
				$('body').removeClass("loading");
				$("html,body").scrollTop(0);
			},1000);
		})
	}
	
	/* for change password */
	$scope.changePwd = function(size, parentElem){
			  var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'changePwd.html',
			controller : 'passwordChangeController',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
			}
		/* end */
	
	$scope.logOut = function(){
	sessionStorage.clear();
	window.location.href="index.jsf"
		}
	
})