ouaApp.controller('userReportCtrl', function($scope, $location, $interval, $http, $timeout, ServerService, NationService,CountryService, customSession, $uibModal){
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords = false;
	$scope.travelClick = false;
	
	$scope.checkFlightOption = true;
	$("#checkFlight").addClass("active");
	$scope.checkHotelOption = false;
	$scope.checkInsuranceOption = false;
	if(JSON.parse(localStorage.getItem('authentication'))){
		$scope.role = JSON.parse(localStorage.getItem('authentication'));
		$scope.roleId = $scope.role.userId;
		}
	$scope.serviceData = [{label: 'FLIGHT', value: 'FLT'}, {label: 'HOTEL', value: 'HTL'}, {label: 'HOLIDAY', value: 'FNH'}];
	
	/* traveller search */
	
	$scope.agentUserId = 162;
	var fi;
	$( "#userReportEmail" ).autocomplete({
			source: function( request, response ) {
				ajaxService.ajaxRequest('GET',ServerService.serverPath+'rest/repo/users/'+request.term+'?value='+$scope.agentUserId+'&page=1&size=10','json')
				.then(function(data,status) {
					if(data.status=="SUCCESS" && data.data!=null) {
						fi=0;
						response( data.data );
						}else{
						$( "#userReportEmail" ).val('');
						$('.ui-autocomplete').hide();
					}
				});
			},
			focus: function( event, ui ) {
				$( "#userReportEmail" ).val( ui.item.userName);
				return false;
			},
			change : function(event,ui) {
				$timeout(function(){
					jQuery('.form-group').removeClass('has-error');
				},10);
			},
			//autoFocus: true,
			minLength: 3,
			select: function( event, ui ) {
				$( "#userReportEmail" ).val( ui.item.userName);
				$scope.userCreateById = ui.item.userId;
				return false;
			},
			open: function() {
				$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			},
			close: function(event, ui) {
				$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			}
			});
		
		$scope.travelReq = function(page,count) {
		$scope.userProfileReq.category = "REPORT";
		$scope.userProfileReq.searchMode = 'grid';
		$scope.userProfileReq.gridMode = "child";
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	$scope.checkFlight = function () {
		$scope.checkFlightOption = true;
		$scope.checkHotelOption = false;
		$scope.checkInsuranceOption = false;
		// $scope.toDate = '';
		// $scope.fromDate = '';
		$("#checkFlight").addClass("active");
		$("#checkHotel").removeClass("active");
		$("#checkInsurance").removeClass("active");
		$scope.recentBookingData = '';
	}
	$scope.checkHotel = function () {
		$scope.checkFlightOption = false;
		$scope.checkHotelOption = true;
		$scope.checkInsuranceOption = false;
		// $scope.toDate = '';
		// $scope.fromDate = '';
		$("#checkHotel").addClass("active");
		$("#checkFlight").removeClass("active");
		$("#checkInsurance").removeClass("active");
		$scope.recentBookingData = '';
	}
	$scope.checkInsurance = function () {
		$scope.checkFlightOption = false;
		$scope.checkHotelOption = false;
		$scope.checkInsuranceOption = true;
		// $scope.toDate = '';
		// $scope.fromDate = '';
		$("#checkInsurance").addClass("active");
		$("#checkHotel").removeClass("active");
		$("#checkFlight").removeClass("active");
		$scope.recentBookingData = '';
	}
	
	$scope.downloadBooking = function() {
		$('body').addClass('loading');
		// $scope.recentBookingDataDownload.flightResponseList = $scope.flightResponse;
		// $scope.recentBookingDataDownload.hotelResponseList = $scope.hotelResponse;
		// $scope.recentBookingDataDownload.insuranceResponseList = $scope.insuranceResponse;
		$http({
			url : ServerService.serverPath+'rest/user/allbooking/download',
			method : 'POST',
			data : JSON.stringify($scope.recentBookingDataDownloadmap),
			responseType:'arraybuffer'
			}).then(function(response) {
			$('body').removeClass('loading');
			var file = new Blob([ response.data ], {
				type : 'application/xlsx'
			});
			var fileURL = URL.createObjectURL(file);
			var a         = document.createElement('a');
			a.href        = fileURL; 
			a.target      = '_blank';
			a.download    = 'Your_Booking.xlsx';
			document.body.appendChild(a);
			a.click();
			}).error(function(data, status, headers, config) {
		});
	};
	
	$scope.getTraveller = function () {
	
	var params = "page=1&count=10"
	var paramsObj= {'count': '10', 'page': '1', 'pagination': 'true', 'sortBy': undefined , 'sortOrder': undefined}
	
		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/docket/allbooking/search',$scope.userProfileReq).then(function (response) {
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.recentBookingData = response.data.data.docketChildList;
				$scope.totTrvCount = response.data.data.totalCount;
				$location.path('/report');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/report');
			}
		});	
	};
	
	$scope.tripsModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	$scope.search = function () {
		$scope.travelClick = true;
		$scope.noRecords = false;
		$scope.travelReq(1,10);
		$scope.page = 1;
		$scope.userProfileReq.searchMode = 'grid';
		$scope.userProfileReq.gridMode = "child";
		if($( "#userReportEmail" ).val()){
			$scope.userProfileReq.createBy = $scope.userCreateById;
		} else {
			$scope.userProfileReq.createBy = null;
		}
		$scope.userProfileReq.service = null;
		if($scope.checkFlightOption == true){
			$scope.userProfileReq.service = 'FLT';
		}else if($scope.checkHotelOption == true){
			$scope.userProfileReq.service = 'HTL';
		}else if($scope.checkInsuranceOption == true){
			$scope.userProfileReq.service = 'INS';
		}
		
		$scope.userProfileReq.fromDate = $scope.fromDate;
		$scope.userProfileReq.toDate = $scope.toDate;
		$scope.recentBookingData = {};
		$http.post(ServerService.serverPath+'rest/docket/allbooking/search',$scope.userProfileReq).then(function (response) {
			if(response.data.data!=null && response.data.status=='SUCCESS') {
				$scope.recentBookingData = angular.copy(response.data.data);
				$scope.flightResponse = angular.copy(response.data.data.flightResponse);
				$scope.hotelResponse = angular.copy(response.data.data.hotelResponse);
				$scope.insuranceResponse = angular.copy(response.data.data.insuranceResponse);
				$scope.recentBookingDataDownloadmap = response.data.data;
				
				for(var i=0; i<$scope.flightResponse.length;i++){
					response.data.data.flightResponse[i].discountPercent = ($scope.flightResponse[i].discount/$scope.flightResponse[i].baseFare) * 100;
					$scope.flightResponse[i].discountPercent = ($scope.flightResponse[i].discount/$scope.flightResponse[i].baseFare) * 100;
				}
				
				for(var i=0; i<$scope.hotelResponse.length;i++){
					response.data.data.hotelResponse[i].discountPercent = ($scope.hotelResponse[i].dtAmount/$scope.hotelResponse[i].roomFare) * 100;
					$scope.hotelResponse[i].discountPercent = ($scope.hotelResponse[i].dtAmount/$scope.hotelResponse[i].roomFare) * 100;
				}
				
				for(var i=0; i<$scope.insuranceResponse.length;i++){
					response.data.data.insuranceResponse[i].discountPercent = ($scope.insuranceResponse[i].dtAmount/$scope.insuranceResponse[i].roomFare) * 100;
					$scope.insuranceResponse[i].discountPercent = ($scope.insuranceResponse[i].dtAmount/$scope.insuranceResponse[i].roomFare) * 100;
					$scope.insuranceResponse[i].promoCode = '--';
				}
				
				$scope.totTrvCount = angular.copy(response.data.data.totalCount);
				$location.path('/report');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/report');
			}
		});
	};
	
	$scope.clear = function() {
		$scope.toDate = '';
		$scope.fromDate = '';
	}
	
	$('.bookingDate-from').datepicker({ 
		dateFormat:'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		minDate: '-5Y',
		maxDate: 0,
		//yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){                                
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});
	$('.bookingDate-to').datepicker({ 
		dateFormat:'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		minDate: '-5Y',
		maxDate: 0,
		//yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){                                
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});
	
	$scope.reportViewDocPnr = function(clientRefNo, pnrNumber, prodType){
		$('body').addClass("loading");
		var docPnrRequest = new Object();
		docPnrRequest.clientRefNo = clientRefNo;
		docPnrRequest.bookingRefNo = pnrNumber;
		docPnrRequest.type = 'DB';
		if(prodType=='FLT'){
			docPnrRequest.serviceType = 'FLT';
		}
		if(prodType=='HTL'){
			docPnrRequest.serviceType = 'HTL';
		}
		if(prodType=='INSRNC'){
			docPnrRequest.serviceType = 'INSRNC';
		}
		if(prodType=='VISA'){
			docPnrRequest.serviceType = 'VISA';
		}
		var docPnrRequestJson = JSON.stringify(docPnrRequest);	
		$http({
			method:'POST',				
			data:docPnrRequestJson,
			url:ServerService.serverPath+'rest/retrieve/itinerary',
			}).then(function(data,status){				
			if(data.data.status=="SUCCESS"){	
				var jsondata=data.data.data;
				if(jsondata.serviceOrderType=='FLT'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
					$location.path('/flightItenary');
				}
				if(jsondata.serviceOrderType=='HTL'){
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata));
					$location.path('/hotelItenary');
				}
				if(jsondata.serviceOrderType=='INSRNC'){
					sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insuranceItenary');
				}
				if(jsondata.serviceOrderType=='VISA'){
					sessionStorage.setItem("visaItineraryJson",JSON.stringify(jsondata));
					$location.path('/visa-itinerary');
				}
				if (jsondata.serviceOrderType=='FNH'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
					$location.path('/holidayItenary');
				}
				} else {
				$location.path('/report');
				Lobibox.alert('error',
				{
					msg: data.data.errorMessage
				});
			}
			$timeout(function(){
				$('body').removeClass("loading");
				$("html,body").scrollTop(0);
			},1000);
		})
	}
	
	/* for change password */
	$scope.changePwd = function(size, parentElem){
			  var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'changePwd.html',
			controller : 'passwordChangeController',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
			}
		/* end */
	
	$scope.logOut = function(){
	sessionStorage.clear();
	window.location.href="index.jsf"
		}
	
})