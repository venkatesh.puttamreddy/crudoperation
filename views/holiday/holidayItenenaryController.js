ouaApp.controller('holidayItenenaryController', function($http, $scope, $location, ServerService, customSession){
	
	/**
	Final Data
	*/
	$scope.flightconfirmJson =  JSON.parse(sessionStorage.getItem("flightconfirmJson"));
	$scope.hotelconfirmJson =  JSON.parse(sessionStorage.getItem("hotelBookJson"));
	
	if($scope.flightconfirmJson){
	$scope.passengListArray = new Array;
	$scope.passengListArray = $scope.flightconfirmJson.ssrDetailList;
	
	if($scope.passengListArray > 0){
	for(var i=0; i<$scope.passengListArray.length; i++){
		$scope.splSerDataLst = $scope.passengListArray[i].ssrDetailList;
	}
	}
	
	$scope.printRequest = {};
	$scope.printTkt = function(i){
		
		//sessionStorage.removeItem('hotelBookJson');
		$scope.printRequest.bookingRefNo = $scope.flightconfirmJson.pnrNo;
		$scope.printRequest.serviceType = $scope.flightconfirmJson.serviceOrderType;
		$scope.printRequest.clientRefNo = $scope.flightconfirmJson.clientRefNo;
		$scope.printRequest.type = "DB";
		$http({
			method:'POST',
			data:$scope.printRequest,
			url:ServerService.serverPath+'rest/retrieve/itinerary'
			}).then(function(response,status){
			if(response.data.status=="SUCCESS") {
				var nav_url= window.location.hostname + window.location.pathname + "#!/terms_conditions"
				sessionStorage.setItem("navUrl",nav_url );
				response.data.data.tIndex = i;
				var jsondata=response.data.data;
				
				if (jsondata.serviceOrderType=='FNH'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
				}
				window.open("print_pages/holiday/printHolidayItenenary.html",'_blank');
				} else {
				Lobibox.alert('error',
				{
					msg: data.errorMessage
				});
			}
			
		})
	} 
	} else {
		$location.path('/user')
		}
		
		$scope.loginCheck = false;
	$scope.logged = JSON.parse(localStorage.getItem('loginName'));
	if($scope.logged != null){
		$scope.loginCheck = true;
		}
		
		$scope.tripsDetail = function(){
			$location.path('/user');
			}
})