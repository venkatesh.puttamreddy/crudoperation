ouaApp.controller('holidayReviewController', function ($scope, $http, $rootScope, renderHTMLFactory, $uibModal, $location, customSession, ConstantService) {
	$('html, body').scrollTop(0);
	$scope.currency = ConstantService.currency;
	$scope.repriceResp = JSON.parse(sessionStorage.getItem('repriseResponse'));
	$scope.repriceResp1 = JSON.stringify($scope.repriceResp);
	
	$scope.hotelResp = JSON.parse(sessionStorage.getItem('selectedHotResp'));
	$scope.hotelRespreprice = $scope.hotelResp.searchResults[0];
	$scope.flightResp = JSON.parse(sessionStorage.getItem('selectedFliResp'));
	
	$scope.flightReview = $scope.flightResp.selectedResult;
	$scope.flightReview1 = $scope.flightResp.selectedResult.flightSearchResults[0].segGrpList[0].segList[0];
	$scope.flightReview2 = $scope.flightResp.selectedResult.flightSearchResults[0].segGrpList[1].segList[0];
	$scope.flighthotelfare=$scope.flightReview.flightSearchResults[0].grndTotal+$scope.repriceResp.repriceResponse.totalFare;//+parseInt($scope.repriceResp.repriceResponse.markupAmount)
	$scope.InsuranceDetails = $scope.flightResp.insurancePlans
	
	
	$scope.renderHTMLFactory = renderHTMLFactory;
	
	$scope.totalFare = parseInt($scope.repriceResp.repriceResponse.totalFare) + parseInt($scope.flightResp.selectedResult.flightSearchResults[0].grndTotal)
	sessionStorage.removeItem('selectedInsurancePlanId')
   /* insurance*/
   $scope.extras = 0;
   $scope.getSelectedPlanId = function (selectedPlanId) {
	   if (selectedPlanId != null) {
		   sessionStorage.setItem('selectedInsurancePlanId', selectedPlanId);
		   $rootScope.value = selectedPlanId;
		   $scope.palanContent = $scope.InsuranceDetails.availablePlans[selectedPlanId]
		   $scope.extras = $scope.InsuranceDetails.availablePlans[selectedPlanId].totalFare
	   }else{
		   $scope.extras=0;	
		   sessionStorage.removeItem('selectedInsurancePlanId')
	   }
   }
   $scope.selectPlan = function (values, size, parentSelector) {
	
	   $rootScope.value = values;
	   var parentElem = parentSelector ?
		   angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
	   //$rootScope.selectedIndex = index;
	   $scope.animationEnabled = true;
	   var modalInstance = $uibModal.open({
		   animation: $scope.animationEnabled,
		   ariaLabelledBy: 'modal-title',
		   ariaDescribedBy: 'modal-body',
		   templateUrl: 'aboutInsurance.html',
		   controller: 'insuranceCtrl',
		   backdrop: true,
		   size: size,
		   appendTo: parentElem,
		   resolve: {
			   items: function () {
				   return $scope.items
			   }
		   }
	   })

   }
   /*end*/
	$scope.goBack = function(path){
		$location.path(path);
	}
    $scope.getNumber = function (num) {
        return new Array(num);
    }
	/* openFlightInfo function */
	$scope.openFlightInfo = function(size, parentSelector){
		var parentElem = parentSelector ?
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightInfo.html',
            controller: 'flightHolidayReviewDetailsCtrl',
            size: size,
            appendTo: parentElem
		});
	}
}).controller('flightHolidayReviewDetailsCtrl', function($scope, $uibModalInstance, $rootScope, stopsService){
	$scope.flightDetails = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
	$scope.cabinClass = $scope.flightDetails.clasType;
	$scope.flightDetails1 = JSON.parse(sessionStorage.getItem('newSelectedFliResp'));
	if($scope.flightDetails1 == undefined){
		$scope.flightDetails1 = JSON.parse(sessionStorage.getItem('selectedFliResp')).selectedResult.flightSearchResults[0];
	}
	// sessionStorage.setItem('flightplusSearchResp')
	var value = $rootScope.selectedIndex;
	$scope.flightDetails2 = $scope.flightDetails.data
	//$scope.flightDetails1 = $scope.$parent.flightResultList1
	$scope.filter = $rootScope.sCheckId
	$scope.airlineCode = $rootScope.airlineCodes;
	var flag = false;
	var filterDatas = {};
	if($scope.filter != null && $scope.airlineCode != null ){
	
		filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
	} else if(!flag){
		if($scope.filter != null || $scope.filter != undefined){
			filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
		} else if($scope.airlineCode != null || $scope.airlineCode != undefined){
			filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
			}
		}
	/* if($scope.filter != null || $scope.filter != undefined){
		var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
	} */
	
	$scope.waitingDuration=[];	
	var count=0;
	$scope.getWaitingDuration = function(a,b,ind,x,key){		 
		//$scope.waitingDuration;	      
		if(a!=undefined && b!==undefined ){
			var startDate = new Date(a);   
			var endDate   = new Date(b);
			var d =(endDate.getTime() - startDate.getTime()) / 1000;	
			 d = Number(d);
			    var h = Math.floor(d / 3600);  	  
			    var m = Math.floor(d % 3600 / 60);
			    var hDisplay = h +"h";	    
			    var mDisplay = m +"m";
			    $scope.waitingDuration[parseInt(ind+""+x+""+key)]= hDisplay +" "+ mDisplay;	   	 	  	  	       	                
			
		} 
		
	}		
	
	$scope.totalDuration = [];
	$scope.getTotalDuration =function(a,b,i){
		$scope.totalDuration[i]  = timeConvert(parseInt(a)+parseInt(b));  		
		
	} 
	
	function timeConvert(n) {
		var num = n;
		var hours = (num / 60);
		var rhours = Math.floor(hours);
		var minutes = (hours - rhours) * 60;
		var rminutes = Math.round(minutes);
		return rhours + "h " + rminutes + " m";	      
		}

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}).controller('insuranceCtrl', function ($http, $scope, $location, $uibModalInstance, $rootScope, renderHTMLFactory, urlService) {

	var urlService = $location.absUrl().substring($location.absUrl().lastIndexOf("/") + 1, $location.absUrl().length);
	$scope.renderHTMLFactory = renderHTMLFactory;
	var index = $rootScope.value
	if (urlService == "flightDetails") {
		$scope.isureFlight = JSON.parse(sessionStorage.getItem('flightPriceData'));
		$scope.avaliablePlan = $scope.isureFlight.insurancePlans.availablePlans[index];
	} else if (urlService == "holidayReview") {
		$scope.holidaylightInst = JSON.parse(sessionStorage.getItem('selectedFliResp'));
		$scope.avaliablePlan = $scope.holidaylightInst.insurancePlans.availablePlans[index];
	}


	$scope.ok = function () {
		$uibModalInstance.close($scope.selected.item);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});
