ouaApp.controller('holidayBookController', function($scope, $http, $interval, $uibModal, $window, $log, $document, Month, Year, ServerService, $timeout, $location, httpService, $state, customSession, ConstantService) {
    $('html, body').scrollTop(0);
    $scope.currency = ConstantService.currency;
    $scope.hotelResp = JSON.parse(sessionStorage.getItem('selectedHotResp'));
    $scope.flightResp = JSON.parse(sessionStorage.getItem('selectedFliResp'));
    $scope.reprice = JSON.parse(sessionStorage.getItem('repriseResponse'));
    $scope.hotelResp1 = $scope.hotelResp.searchResults[0];
    $scope.flightResult = $scope.flightResp.selectedResult;
    $scope.selectedHotelRooms = JSON.parse(sessionStorage.getItem('selectedHotels'));
    $scope.flightDetail = JSON.parse(sessionStorage.getItem('selectedFliResp'));
    $scope.selectedHotel = $scope.hotelResp.searchResults[0];
    $scope.hotelFare = $scope.reprice.repriceResponse.totalFare + $scope.reprice.repriceResponse.markupAmount;
    $scope.flightFare = $scope.flightResp.selectedResult.flightSearchResults[0].grndTotal
    $scope.totalFare = parseFloat($scope.hotelFare) + parseFloat($scope.flightFare);
    $scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
    $scope.nationality = JSON.stringify($scope.nationality1);

    $scope.month = Month;
    $scope.year = Year;
    $scope.AllFare = $scope.flightResult.flightSearchResults[0].grndTotal + $scope.reprice.repriceResponse.totalFare
    $scope.AllFareInsurence = $scope.AllFare + $scope.insuranceTotalFare
    $scope.onCredit = function() {

        // $scope.cardType='CREDIT'
        $scope.msg = "Please click on the below PAY NOW button to continue"
            //  $scope.paynw=""
    }
    $scope.onDebit = function() {

        //$scope.cardType='DEBIT'
        $scope.msg = "Please click on the below PAY NOW button to continue"
    }



    /* open FareInfo */
    $scope.openFareInfo = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            //	$rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightFareinfoContent.html',
                controller: 'flightFareinfoContent',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        //	return $scope.items
                    }
                }
            })
        }
        /* end */

    /**
    @author Raghava Muramreddy
    to showing payment error message
    */
    if (sessionStorage.getItem("paymentError")) {
        var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
        Lobibox.alert('error', {
            msg: paymentError.errorMessage,
            callback: function(lobibox, type) {
                if (type === 'ok') {
                    sessionStorage.removeItem("paymentError");
                }
            }
        });

    }

    var depDate = $scope.flightResult.deptDate;






    /**
         //@author Raghava Murmareddy
    	get Selected Insurance Details which we select at Review Time
    	
    */

    $scope.insuranceTotalFare = 0;
    $scope.insuranceTotalFares = 0;
    $scope.selectedInsurancePlanId = sessionStorage.getItem('selectedInsurancePlanId');
    if ($scope.selectedInsurancePlanId != null) {
        $scope.withInsurance = true;
        $scope.selectedInsurancePlan = $scope.flightResp.insurancePlans.availablePlans[$scope.selectedInsurancePlanId];
        $scope.insuranceTotalFare = $scope.selectedInsurancePlan.totalFare;
        $scope.insuranceTotalFares = $scope.insuranceTotalFare;
    }

    /**
    Selected Insurance Details end
    */

    $scope.adultCount = 0;
    $scope.childCount = 0;

    $scope.selectedHotelRooms.forEach(function(roomCount) {
        $scope.adultCount = $scope.adultCount + parseInt(roomCount.adultCount);
        $scope.childCount = $scope.childCount + parseInt(roomCount.childCount);

    })

    /*check user Already login end*/
    $scope.salutations = ["Mr", "Mrs", "Miss"];
    $scope.childsalutations = ["Miss", "Mstr"];

    var personaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = loginUser.mobile;
        this.email = loginUser.email;
        this.nationalityname = null;
        this.passportNo = null;
    }
    var childPersonalData = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = loginUser.mobile;
        this.email = loginUser.email;
        this.nationalityname = null;
        this.passportNo = null;
    }
    var infantPersonaldata = function() {
        this.gender = "M";
        this.salutation = "Inft";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = 0;
        this.mobile = 0;
        this.email = '';
        this.nationalityname = '';
        this.passportNo = null;
    }
    var traveller = function() {
        this.personaldata = new personaldata();
        this.agntid = 0;
        this.profileid = 0;
        this.travellerRefRPH = 'A1';
    }
    var childTraveller = function() {
        this.personaldata = new childPersonalData();
        this.agntid = 0;
        this.profileid = 0;
        this.travellerRefRPH = 'A1';
    }
    var infantTraveller = function() {
        this.personaldata = new childPersonalData();
        this.agntid = 0;
        this.profileid = 0;
        this.travellerRefRPH = 'A1';
    }
    var travellerByRoom = function() {
        this.adultTravellersList = [];
        this.childTravellersList = [];
        this.infantTravellersList = [];
    }

    /* contact number not start with zero */
    $("#contact").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    $("#acontact_0").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    /* contact number not start with zero */
    var loginUser = "";
    $scope.user = {};
    $scope.isValidUser = false;
    /**We have to maintain same Order below until "update Traveller Details end" */
    $scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
    $scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))
    if ($scope.userDetails != null) {
        $scope.user = $scope.userDetails;
    }
    /* check user Already login*/


    if (localStorage.getItem('loginName') && localStorage.getItem('loginName') != "null") {
        $scope.isValidUser = true;

        loginUser = JSON.parse(localStorage.getItem('loginName'));
        $scope.user.email = loginUser.email;
        $scope.user.contactNumber = parseInt(loginUser.contactNumber);
        $scope.user.mobcntrycode = loginUser.mobcntrycode;

        $scope.userDetails = JSON.parse(sessionStorage.getItem('userDetails'));
        $scope.isUserCheck = JSON.parse(sessionStorage.getItem('isUserCheck'));

        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];

            for (i = 0; i < $scope.selectedHotel.occGroups.length; i++) {
                var travellerByRoomObj = new travellerByRoom();
                for (j = 0; j < $scope.selectedHotel.occGroups[i].roomGrpBean.adultCount; j++) {
                    var travellerObj = new traveller();
                    if (i == 0 && $scope.isUserCheck == true) {
                        if (j == 0) {
                            travellerByRoomObj.adultTravellersList[j] = travellerObj;
                            travellerObj.personaldata = $scope.userDetails;
                            travellerByRoomObj.adultTravellersList[0] = travellerObj;
                        } else {
                            travellerByRoomObj.adultTravellersList.push(travellerObj);
                            travellerObj.personaldata.email = $scope.user.email;
                            travellerObj.personaldata.mobile = $scope.user.contactNumber;
                            travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
                        }
                    } else {
                        travellerByRoomObj.adultTravellersList.push(travellerObj);
                        travellerObj.personaldata.email = $scope.user.email;
                        travellerObj.personaldata.mobile = $scope.user.contactNumber;
                        travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
                    }


                }
                for (j = 0; j < $scope.selectedHotel.occGroups[i].roomGrpBean.childCount; j++) {
                    var travellerObj = new childTraveller();
                    travellerByRoomObj.childTravellersList.push(travellerObj);
                    travellerObj.personaldata.email = $scope.user.email;
                    travellerObj.personaldata.mobile = $scope.user.contactNumber;
                    travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
                }
                for (j = 0; j < $scope.selectedHotel.occGroups[i].roomGrpBean.infantCount; j++) {
                    var travellerObj = new infantTraveller();
                    travellerByRoomObj.infantTravellersList.push(travellerObj);
                    travellerObj.personaldata.email = $scope.user.email;
                    travellerObj.personaldata.mobile = $scope.user.contactNumber;
                }
                $scope.travellersListByRoom.push(travellerByRoomObj);
            }

        }
        //$scope.user.contactNumber = loginUser.mobile;
    } else {
        /**update Traveller Details  */

        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];

            for (i = 0; i < $scope.selectedHotel.occGroups.length; i++) {
                var travellerByRoomObj = new travellerByRoom();
                for (j = 0; j < $scope.selectedHotel.occGroups[i].roomGrpBean.adultCount; j++) {
                    var travellerObj = new traveller();
                    travellerByRoomObj.adultTravellersList.push(travellerObj);
                }
                for (j = 0; j < $scope.selectedHotel.occGroups[i].roomGrpBean.childCount; j++) {
                    var travellerObj = new childTraveller();
                    travellerByRoomObj.childTravellersList.push(travellerObj);
                }
                for (j = 0; j < $scope.selectedHotel.occGroups[i].roomGrpBean.infantCount; j++) {
                    var travellerObj = new infantTraveller();
                    travellerByRoomObj.infantTravellersList.push(travellerObj);
                }
                $scope.travellersListByRoom.push(travellerByRoomObj);
            }

        }
    }
    /**update Traveller Details end */


    $scope.showSignIn = false;

    var loginCredential;

    var booking = {};
    $scope.login = function() {

        if ($scope.showSignIn && $scope.user.email != null && $scope.user.password != null) {
            $scope.loadingsignIn = true;
            var url = 'rest/user/signin';
            var datas = '{ "userName" : "' + $scope.user.email + '", "password" : "' + $scope.user.password + '"}'
            $http.post(ServerService.serverPath + url, datas).then(function(response) {
                $scope.loadingsignIn = false;
                if (response.data.status == "SUCCESS") {
                    Lobibox.alert('success', {
                        msg: response.data.message
                    });
                    $scope.loginResp = response.data.data;
                    $scope.user.email = response.data.data.loginId;
                    $scope.user.fname = response.data.data.profileData.personaldata.fname;
                    $scope.user.contactNumber = response.data.data.profileData.personaldata.mobile;
                    $scope.user.mobcntrycode = response.data.data.profileData.personaldata.mobcntrycode;
                    $scope.$parent.bookingHide1 = true;
                    booking.bookingHide1 = true;
                    booking.bookingHide = true;
                    localStorage.setItem('authentication', JSON.stringify(response));
                    localStorage.setItem('loginName', JSON.stringify($scope.user));
                    localStorage.setItem('userBookingdetails', JSON.stringify(booking))
                    window.location.reload()
                } else {
                    Lobibox.alert('error', {
                        msg: response.data.errorMessage
                    });
                    $scope.showSignIn = true;
                }
            });
        } else {
            if ($("#pass").val() != "") {
                $("#pass").parent().removeClass('has-error');
            } else {
                $("#pass").parent().addClass('has-error');
            }
        }

    }

    /* userChange function */
    $scope.travelby = {}
    $scope.userChange = function(value) {

        sessionStorage.setItem('isUserCheck', value)
        if (value == true) {
            $scope.travelby.count = 10;
            $scope.travelby.country = null;
            $scope.travelby.dateOnProcess = "15/01/2019";
            $scope.travelby.operation = "PROFILE";
            $scope.travelby.page = 1;
            $http.post(ServerService.serverPath + 'rest/search/profile', $scope.travelby).then(function(response) {

                if (response.data.status == 'FAILURE') {
                    Lobibox.alert('error', {
                        msg: response.data.errorMessage
                    })
                    $scope.isUserCheck = false;
                } else if (response.data.status == 'SUCCESS') {
                    $scope.userDetails = response.data.data.travellerBeanList[0].personaldata
                    sessionStorage.setItem('userDetails', JSON.stringify($scope.userDetails));
                    //$scope.userDetails = JSON.parse(sessionStorage.getItem("userDetails"));
                    if ($scope.travellersListByRoom == 'undefined') {
                        $scope.travellersListByRoom.push(travellerByRoomObj);
                        if (travellersListByRoom.adultTravellersList.length == 0 || travellersListByRoom.adultTravellersList == 'undefined') {
                            var travellerObj = new traveller();
                            travellersListByRoom.adultTravellersList.push(travellerObj);
                        }
                    }

                    $scope.travellersListByRoom[0].adultTravellersList[0].personaldata = $scope.userDetails;
                    $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalityname = $scope.userDetails.nationalitycode;
                    //window.location.reload();
                    /* var travellerObj = new traveller() ;
                    	
                    travellerByRoomObj.adultTravellersList[0] = travellerObj;
                    travellerObj.personaldata = $scope.userDetails
                    travellerObj.personaldata.salutation = $scope.userDetails.salutation
					
                    $scope.salutations[0] = $scope.userDetails.salutation; */
                    //	$scope.nationality1[0] = $scope.userDetails.salutation;

                    /* angular.element('input[name = "afname0"]').attr('readonly', true);
                    angular.element('input[name = "alname0"]').attr('readonly', true);
                    angular.element('#adateOfBirth_0').prop('readonly', true);
                    angular.element('input[name = "acontact0"]').attr('readonly', true);
                    angular.element('input[name = "amail0"]').attr('readonly', true);
                    angular.element('input[name = "anationalityVal0"]').prop('readonly', true);
                    angular.element('input[name = "apassport0"]').attr('readonly', true); */
                }
            })
        } else if (value == false) {
            //window.location.reload();
            if (window.performance) {
                console.info("window.performance works fine on this browser");
                //	$scope.userDetails = JSON.parse(sessionStorage.getItem("userDetails"));
                $scope.useDetails = {}

                sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom));
                $scope.travellersByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
                $scope.travellersListByRoom[0].adultTravellersList[0] = $scope.useDetails;
                //$scope.travellersListByRoom.push(userDetails);

            }



        }

        /* var travellerObj = traveller;
	
        					$scope.userDetails = {}
        					
        				travellerByRoomObj.adultTravellersList[0] = travellerObj;
        				
        				travellerObj.personaldata = $scope.userDetails
        				$scope.salutations[0] = "Mr"; */
    }



    if (sessionStorage.getItem('userDetails') != null) {
        $scope.isUserCheck = JSON.parse(sessionStorage.getItem('isUserCheck'));
        if ($scope.isUserCheck == true) {
            $scope.userDetails = JSON.parse(sessionStorage.getItem('userDetails'));

        }

    }


    /* end */

    /*assign guestUserDetails to Travellers */
    $scope.updateGustUserDetailsToTravller = function() {

            $scope.travellersListByRoom.forEach(function(travellersList) {
                travellersList.adultTravellersList.forEach(function(adultTraveller) {
                    adultTraveller.personaldata.email = jQuery('#email').val();
                    adultTraveller.personaldata.mobile = parseInt(jQuery('#contact').val());
                    adultTraveller.personaldata.mobcntrycode = jQuery('#userCountryCode').val();
                });
            })
            $scope.travellersListByRoom.forEach(function(travellersList) {
                travellersList.childTravellersList.forEach(function(childTravellers) {
                    childTravellers.personaldata.email = jQuery('#email').val();
                    childTravellers.personaldata.mobile = parseInt(jQuery('#contact').val());
                    childTravellers.personaldata.mobcntrycode = jQuery('#userCountryCode').val();
                });
            })
            $scope.travellersListByRoom.forEach(function(travellersList) {
                travellersList.infantTravellersList.forEach(function(infantTravellers) {
                    infantTravellers.personaldata.email = jQuery('#email').val();
                    infantTravellers.personaldata.mobile = parseInt(jQuery('#contact').val());
                    infantTravellers.personaldata.mobcntrycode = jQuery('#userCountryCode').val();
                });
            })
        }
        /*assign guestUserDetails to Travellers end*/

    $scope.goBack = function(path) {
        $location.path(path);
    }

    var roomIteration = $scope.reprice.repriceResponse.roomDetails;
    $scope.nonRefundFlag = false;
    for (var j = 0; j < roomIteration.length; j++) {
        $scope.nonRefund = roomIteration[j].nonRefundable;
        if ($scope.nonRefund == 'yes') {
            $scope.nonRefundFlag = true;
        }
    }

    /* signIn function */
    $scope.showSignInText = "Sign in to book faster";
    $scope.showSignIn = false;
    $scope.signIn = function() {
        $scope.showSignIn = $scope.showSignIn ? false : true;
        var buttonText = "Sign in to book faster";
        if ($scope.showSignIn == true) { var buttonText = "For Continue As Guest"; } else { var buttonText = "Sign in to book faster"; }

        $scope.showSignInText = buttonText;
    }

    /* end */

    /* AgencyTermsOfBusiness*/
    $scope.openAgencyTermsOfBusiness = function(size, parentSelector) {

        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'agencyTermsOfBusiness.html',
            controller: 'flightFareRulesContent',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    /* AgencyTermsOfBusiness End*/



    /* multiple date */

    var flyhoteldate = $scope.hotelResp.checkOutDate;
    var depDateSplt = flyhoteldate.split('-');
    var minDepDate = new Date(depDateSplt[1] + '-' + depDateSplt[0] + '-' + depDateSplt[2]);
    /* var dt = daysBetween(new Date(),minDepDate)-1;
     var chldMinDate = daysBetween(new Date(),minDepDate)+2; 
     */
    $timeout(function() {
        $('.adultTrvlr').each(function(index) {
            jQuery(this).attr('id', 'adultTrvlr_' + (index + 1));
        });
        $('.childTrvlr').each(function(index) {
            jQuery(this).attr('id', 'childTrvlr_' + (index + 1));
        });
        $('.infantTrvlr').each(function(index) {
            jQuery(this).attr('id', 'infantTrvlr_' + (index + 1));
        });
        $('.adatepicker').each(function(index) {
            jQuery(this).attr('id', 'adatepicker' + (index + 1));
        });
        $('.cdateOfBirth').each(function(index) {
            jQuery(this).attr('id', 'cdateOfBirth' + (index + 1));
        });
        $('.idatepicker').each(function(index) {
            jQuery(this).attr('id', 'idatepicker' + (index + 1));
        });
        var minAdultYear = parseInt(minDepDate.getFullYear()) - 12;

        var maxAdultEndYear = parseInt(minDepDate.getFullYear()) - 113;
        $('[id*=adatepicker]').datepicker({
            dateFormat: 'dd-mm-yy',
            singleDatePicker: true,
            showDropdowns: true,
            changeYear: true,
            changeMonth: true,
            singleDatePicker: true,
            yearRange: '1905:' + (new Date).getFullYear(),
            /*  minYear: 1901,
             maxYear: parseInt(moment().format('YYYY'),10) */
            startDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),
            minDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate()),
            maxDate: new Date(minAdultYear, minDepDate.getMonth(), minDepDate.getDate()),
            endDate: new Date(maxAdultEndYear, minDepDate.getMonth(), minDepDate.getDate())
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');
            $scope.age = years;
            jQuery(this).attr('data-age', years);
        });
        var childMaxYear = parseInt(minDepDate.getFullYear()) - 2;
        var childMinYear = parseInt(minDepDate.getFullYear()) - 12;
        $('[id*=cdateOfBirth]').datepicker({
            dateFormat: 'dd-mm-yy',
            changeYear: true,
            changeMonth: true,
            singleDatePicker: true,
            showDropdowns: true,
            yearRange: '1905:' + (new Date).getFullYear(),
            /* minYear: 1901, */
            minDate: new Date(childMinYear, minDepDate.getMonth(), minDepDate.getDate() + 1),
            maxDate: new Date(childMaxYear, minDepDate.getMonth(), minDepDate.getDate()),
            maxYear: parseInt(moment().format('YYYY'), 10)
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');

            jQuery(this).attr('data-age', years);
        });

        $('[id*=idateOfBirth]').daterangepicker({

            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10)
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');

            jQuery(this).attr('data-age', years);
        });

    }, 2000);


    /* end */


    /* payNow function */
    $scope.payment = {}
    $scope.message = false;
    $scope.showCard = false;
    $scope.payNow = function() {
            var cardId = $('#cardNo').val();
            var firstDigit = cardId.charAt(0)
            var secondDigit = cardId.charAt(1)
            if (firstDigit == 3 && between(secondDigit, 4, 7) && cardId.length == 5) {
                $scope.path = 'american';
                $scope.message = false;
                $scope.showCard = true;
            } else if (firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length == 5) {
                $scope.path = 'dinners';
                $scope.message = false;
                $scope.showCard = true;
            } else if (((firstDigit == 5 && between(secondDigit, 1, 5)) || (firstDigit == 2 && between(secondDigit, 2, 7))) && cardId.length == 5) {
                $scope.path = 'master';
                $scope.message = false;
                $scope.showCard = true;
            } else if ((firstDigit == 5 || firstDigit == 6) && (cardId.length == 5)) {
                $scope.path = "metro";
                $scope.message = false;
                $scope.showCard = true;
            } else if ((firstDigit == 4) && (cardId.length == 5)) {
                $scope.path = 'visa';
                $scope.message = false;
                $scope.showCard = true;
            } else if (cardId.length == 0) {
                $scope.message = true;
                $scope.showCard = false;
            }
        }
        /* end */

    /* room based pass count */
    var occupancy = $scope.hotelResp.searchResults[0].occGroups
    $scope.adltCount = [];
    $scope.childCount = [];
    for (var i = 0; i < occupancy.length; i++) {
        var roombean = occupancy[i].roomGrpBean.adultCount
        $scope.adltCount.push(roombean);

    }

    $scope.getNumber = function(num) {
            return new Array(num);
        }
        /* end */



    /* $interval(function(){
    	$('.adultTrvlr').each(function(index){
    		jQuery(this).attr('id','adultTrvlr_'+(index+1));
    	})
    	
    	 $('.adatepicker').each(function(index){
    		jQuery(this).attr('id','adatepicker'+(index+1));
    	}); 
    	$('[id*=adatepicker]').focus(function(){
    		$('.adatepicker').each(function(index){
    			jQuery(this).attr('id','adatepicker'+(index+1));
    		});
    		if(jQuery(this).parents('#pasadd').find('.modal').is(":hidden"))
    		{
    			$('.ui-datepicker').appendTo($('body'));
    		}
    		
    	});
    	
    	$('[id*=adatepicker]').daterangepicker({
    	singleDatePicker: true,
    	showDropdowns: true,
    	format: 'dd/mm/yyyy',
    	minYear: 1901,
    	maxYear: parseInt(moment(),10)
    	}, function(start, end, label) {
    	var years = moment().diff(start, 'years');
    	alert("You are " + years + " years old!");
    	})
    	}, 2000)   */



    /* end */

    $scope.roomIndex = JSON.parse(sessionStorage.getItem('roomIndex'));
    var parentIndex = $scope.roomIndex.parentIndex;
    var childIndex = $scope.roomIndex.childIndex;

    /* payment function */
    $scope.selectDoc = {}
    $scope.bookingReq = {}
    $scope.specialServiceRequest = {}
    $scope.selectDoc.dockNo = null;

    var traveller = {};
    var expireDate = {}
    $scope.paymentDetail = {}

    $scope.validateCardDetails = function() {
        var flage = true;
        if ($scope.paymentDetail.cardNo == null) {
            $('#cardNo').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#cardNo').css({ 'background-color': '' });
        }
        if ($scope.paymentDetail.expireMonth == null) {
            $('#expiremonth').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#expiremonth').css({ 'background-color': '' });
        }
        if ($scope.paymentDetail.expireYear == null) {
            $('#expireYrs').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#expireYrs').css({ 'background-color': '' });
        }
        if ($scope.paymentDetail.cvv == null) {
            $('#cardName').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#cardName').css({ 'background-color': '' });
        }

        if ($scope.paymentDetail.cardName == null) {
            $('#cardNo').css({ 'background-color': 'red' });
            flage = false;
        } else {
            $('#cardNo').css({ 'background-color': '' });
        }
        return flage;
    }

    $scope.holidayPayment = function(bookingWay, valid) {

        // if($scope.cardType==='CREDIT'){
        // 	valid=$scope.validateCardDetails();

        // }

        $scope.submitted = true;
        if (valid) {
            var cardId = $('#cardNo').val();
            $scope.loading = true;
            $scope.disabled = true;
            expireDate = $scope.paymentDetail.expireYear + $scope.paymentDetail.expireMonth
            sessionStorage.setItem("contactDetails", JSON.stringify($scope.user))
            $scope.travellerdatalist = [];
            $('.adultTrvlr').each(function(index) {
                jQuery(this).attr('id', 'adultTrvlr_' + (index + 1))
            });

            $scope.travellerdatalist = [];
            $('.adultTrvlr').each(function(index) {
                jQuery(this).attr('id', 'adultTrvlr_' + (index + 1))
            });
            $('.childTrvlr').each(function(index) {
                jQuery(this).attr('id', 'childTrvlr_' + (index + 1))
            });
            // if (bookingWay == 'Payment' && $scope.cardType==='CREDIT') {

            // 	if(cardId.length!=16){

            // 		Lobibox.alert('error',
            // 						{
            // 							msg: "Please check CardNumber"
            // 						});
            // 						return false;
            // 	}
            // 	$scope.bookingReq.payfortTxnData = {
            // 		"3ds_url": null,
            // 		"access_code": null,
            // 		"amount": null,
            // 		"authorization_code": null,
            // 		"card_bin": null,
            // 		"card_number": $scope.paymentDetail.cardNo,
            // 		"card_security_code": $scope.paymentDetail.cvv,
            // 		"command": null,
            // 		"currency": 'OMR',
            // 		"customer_email": null,
            // 		"customer_ip": null,
            // 		"customer_name": $scope.paymentDetail.cardName,
            // 		"eci": null,
            // 		"expiry_date": expireDate,
            // 		"fort_id": null,
            // 		"language": null,
            // 		"merchant_identifier": null,
            // 		"merchant_reference": null,
            // 		"order_description": null,
            // 		"payment_option": $scope.path,
            // 		"response_code": null,
            // 		"response_message": null,
            // 		"return_url": null,
            // 		"service_command": "TOKENIZATION",
            // 		"device_fingerprint": $('#device_fingerprint').val(),
            // 		"signature": null,
            // 		"status": null,
            // 		"token_name": null,
            // 		"postUrl": null
            // 	}
            // } else if (bookingWay == 'NonPayment') {
            // 	$scope.bookingReq.payfortTxnData = [];
            // }
            $scope.bookingReq.ccDetails = btoa(JSON.stringify($scope.bookingReq.payfortTxnData));
            $scope.bookingReq.payfortTxnData = null;
            /* FOR HOTEL */
            var bookingReqObj = new Object();
            if ($scope.nonRefundFlag == false) {
                bookingReqObj.reqCommand = 3;
            } else if ($scope.nonRefundFlag == true) {
                bookingReqObj.reqCommand = 1;
            }
            bookingReqObj.htlName = $scope.selectedHotel.name;
            bookingReqObj.htlCode = $scope.selectedHotel.htlCode;
            bookingReqObj.zcode = $scope.selectedHotel.zcode;
            bookingReqObj.address = $scope.selectedHotel.address;
            bookingReqObj.city = $scope.hotelResp.city;
            bookingReqObj.cityCode = $scope.selectedHotel.cityCode;
            bookingReqObj.country = $scope.selectedHotel.country;
            bookingReqObj.phNo = $scope.selectedHotel.phNo;
            bookingReqObj.htlDesc = $scope.selectedHotel.htlDesc;
            bookingReqObj.starRating = $scope.selectedHotel.starRating;
            bookingReqObj.provider = $scope.selectedHotel.provider;
            //	bookingReqObj.isBindingPrice = $scope.selectedHotelResp1.isBindingPrice;
            bookingReqObj.latitude = $scope.selectedHotel.latitude;
            bookingReqObj.longitude = $scope.selectedHotel.longitude;
            bookingReqObj.currency = $scope.selectedHotel.currency;
            bookingReqObj.comments = $scope.selectedHotel.comments;
            bookingReqObj.status = $scope.selectedHotel.status;
            bookingReqObj.checkInTime = $scope.selectedHotel.checkInTime;
            bookingReqObj.checkOutTime = $scope.selectedHotel.checkOutTime;
            bookingReqObj.totalRooms = $scope.hotelResp.totalRooms;
            bookingReqObj.totalPax = $scope.hotelResp.totalPaxs;
            bookingReqObj.providerCur = $scope.selectedHotel.providerCur;
            bookingReqObj.convRate = $scope.selectedHotel.convRate;
            bookingReqObj.grossFare = $scope.hotelFare;
            bookingReqObj.GetpackagePrice = true;
            bookingReqObj.checkIn = $scope.hotelResp.checkInDate;
            bookingReqObj.checkOut = $scope.hotelResp.checkOutDate;
            bookingReqObj.offers = $scope.selectedHotel.offer;
            bookingReqObj.offerDesc = $scope.selectedHotel.offerDesc;
            bookingReqObj.offerDis = $scope.selectedHotel.offerDis;
            bookingReqObj.nationality = $scope.selectedHotel.nationality;
            bookingReqObj.totalFare = $scope.reprice.repriceResponse.totalFare;
            bookingReqObj.duration = $scope.selectedHotel.duration;
            bookingReqObj.servChrg = $scope.selectedHotel.servChrg;
            bookingReqObj.discount = $scope.selectedHotel.discount;
            bookingReqObj.markupAmount = $scope.selectedHotel.markupAmount;
            bookingReqObj.appMode = 2;
            bookingReqObj.paymentMode = $scope.cardType;
            bookingReqObj.sessionId = $scope.reprice.repriceResponse.sessionId;
            // bookingReqObj.enableNewsLetter=$scope.receiveInfo.value;
            var roomCategories = new Array();
            for (i = 0; i < $scope.selectedHotelRooms.length; i++) {
                var roomObj = new Object();
                roomObj.status = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].status;
                roomObj.roomRefNo = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomRefNo;
                roomObj.roomCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomCode;
                roomObj.roomName = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomName;
                roomObj.roomCheckIn = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomCheckIn;
                roomObj.isBindingPrice = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].isBindingPrice;
                roomObj.netCost = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].netCost;
                roomObj.grossCost = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].grossCost;
                roomObj.roomCheckOut = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomCheckOut;
                roomObj.roomType = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomType;
                roomObj.roomTypeChar = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomTypeChar;
                roomObj.rateBasis = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].rateBasis;
                roomObj.rateBasisCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].rateBasisCode;
                roomObj.mealBoard = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].mealBoard;
                roomObj.mealBoardDesc = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].mealBoardDesc;
                roomObj.adultCount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].adultCount;
                roomObj.childCount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].childCount;
                roomObj.infantCount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].infantCount;
                roomObj.xtrBeds = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].xtrBeds;
                roomObj.roomFare = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomFare;
                roomObj.servChrg = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrg;
                roomObj.discount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].discount;
                roomObj.childAges = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].childAges;
                roomObj.cancellationPolicy = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].cancellationPolicy;

                if ($scope.reprice.repriceResponse.roomDetails[i].psngrNamesRequird != 0) {
                    roomObj.psngrNamesRequird = $scope.reprice.repriceResponse.roomDetails[i].psngrNamesRequird;
                }

                var validRoomOccupancy = new Object();
                if ($scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy != null) {
                    validRoomOccupancy.runno = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.runno;
                    validRoomOccupancy.roomCode = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.roomCode;
                    validRoomOccupancy.adult = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.adult;
                    validRoomOccupancy.children = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.children;
                    validRoomOccupancy.childrenAges = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.childrenAges;
                    validRoomOccupancy.extraBed = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.extraBed;
                    validRoomOccupancy.extraBedOccupant = $scope.reprice.repriceResponse.roomDetails[i].validRoomOccupancy.extraBedOccupant;
                    roomObj.validRoomOccupancy = validRoomOccupancy;
                }
                /* else {

			validRoomOccupancy.runno = 'null';
			validRoomOccupancy.roomCode = 'null';
			validRoomOccupancy.children = 'null';
			validRoomOccupancy.childrenAges = 'null';
			validRoomOccupancy.extraBed = 'null';
			validRoomOccupancy.extraBedOccupant = 'null';

			roomObj.validRoomOccupancy = validRoomOccupancy;
		} */

                roomObj.allocations = $scope.reprice.repriceResponse.roomDetails[i].allocations;
                roomObj.amendmentPolicy = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].amendmentPolicy;
                roomObj.uniqueId = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].uniqueId;
                roomObj.alertMsg = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].alertMsg;
                roomObj.shrdBdng = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].sharedBdng;
                roomObj.grossWithOutDis = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].grsWithOutDiscount;
                roomObj.offerCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].offerCode;
                roomObj.comments = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomComments;
                roomObj.bkngId = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].bkngId;
                roomObj.cancellationChrgCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].cancellationChrgCode;
                roomObj.priceCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].priceCode;
                roomObj.offerDis = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].offerDiscount;
                roomObj.pymntGrndBy = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].paymentGuaranteedBy;
                roomObj.srvChrgType = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgType;
                roomObj.discChrgType = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].discChrgType;
                roomObj.markupAmount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].markupAmount;
                roomObj.seqNo = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].seqNo;
                if ($scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgKeys != null) {
                    var servChrgKeys = new Array();
                    var servChrgKeyLength = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgKeys.length;
                    for (var s = 0; s < servChrgKeyLength; s++) {
                        servChrgKeys.push($scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgKeys[s]);
                    }
                    roomObj.servChrgKeys = servChrgKeys;
                }

                for (var a = 0; a < $scope.travellersListByRoom[i].adultTravellersList.length; a++) {
                    if ($scope.travellersListByRoom[i].adultTravellersList[a].personaldata.nationalitycode == undefined) {
                        $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[i].adultTravellersList[a].personaldata.nationalityname;

                    }
                }
                for (var a = 0; a < $scope.travellersListByRoom[i].childTravellersList.length; a++) {
                    if ($scope.travellersListByRoom[i].childTravellersList[a].personaldata.nationalitycode == undefined) {
                        $scope.travellersListByRoom[i].childTravellersList[a].personaldata.nationalitycode = $scope.travellersListByRoom[i].childTravellersList[a].personaldata.nationalityname;

                    }
                }
                //roomObj.paxDtls = [];
                // roomObj.paxDtls[0] = $scope.travellersListByRoom[i].adultTravellersList.concat($scope.travellersListByRoom[i].childTravellersList);
                //roomObj.paxDtls[0] = $scope.travellersListByRoom[0].adultTravellersList.concat($scope.travellersListByRoom[0].childTravellersList).concat($scope.travellersListByRoom[0].infantTravellersList);
                roomObj.paxDtls = $scope.travellersListByRoom[i].adultTravellersList.concat($scope.travellersListByRoom[i].childTravellersList).concat($scope.travellersListByRoom[i].infantTravellersList);
                if ($scope.travellerdatalist.length == 0) {
                    $scope.travellerdatalist = roomObj.paxDtls;
                } else {
                    $scope.travellerdatalist.concat(roomObj.paxDtls);
                }
                roomCategories.push(roomObj);
            }
            bookingReqObj.docketNo = $scope.selectDoc.dockNo;
            bookingReqObj.roomCategories = roomCategories;

            /* END */

            $scope.bookingReq.docketNo = $scope.selectDoc.dockNo;
            $scope.bookingReq.travellerdatalist = $scope.travellerdatalist;
            $scope.bookingReq.selectedInsurancePlan = $scope.selectedInsurancePlan
            $scope.bookingReq.flightsearchresult = $scope.flightResp.selectedResult;
            bookingReqObj.payfortTxnData = {
                "3ds_url": null,
                "access_code": null,
                "amount": null,
                "authorization_code": null,
                "card_bin": null,
                "card_number": $scope.paymentDetail.cardNo,
                "card_security_code": $scope.paymentDetail.cvv,
                "command": null,
                "currency": 'OMR',
                "customer_email": null,
                "customer_ip": null,
                "customer_name": $scope.paymentDetail.cardName,
                "eci": null,
                "expiry_date": expireDate,
                "fort_id": null,
                "language": null,
                "merchant_identifier": null,
                "merchant_reference": null,
                "order_description": null,
                "payment_option": $scope.path,
                "response_code": null,
                "response_message": null,
                "return_url": null,
                "service_command": "TOKENIZATION",
                "device_fingerprint": $('#device_fingerprint').val(),
                "signature": null,
                "status": null,
                "token_name": null,
                "postUrl": null
            };
            $scope.bookingReq.specialServiceRequest = $scope.specialServiceRequest;
            //bookingReqObj.ccDetails = btoa(JSON.stringify(bookingReqObj.payfortTxnData));
            bookingReqObj.payfortTxnData = null;
            $scope.bookingReq.paymentMode = $scope.cardType;
            // $scope.bookingReq.enableNewsLetter=$scope.receiveInfo.value;
            $scope.flightHotelReq = {}
            $scope.flightHotelReq.grandTotal = $scope.totalFare;
            $scope.flightHotelReq.flightBookCriteria = $scope.bookingReq;
            $scope.flightHotelReq.hotelBookCriteria = bookingReqObj;
            $scope.flightHotelReq.ccDetails = btoa(JSON.stringify(bookingReqObj.payfortTxnData));
            $scope.flightHotelReq.payfortTxnData = null;
            // $scope.flightHotelReq.enableNewsLetter=$scope.receiveInfo.value;
            //$scope.flightHotelReq.paymentMode=$scope.cardType;
            /* $scope.flightHotelReq.cardName = $scope.paymentDetail.cardName;
            $scope.flightHotelReq.cardNo = $scope.paymentDetail.cardNo;
            $scope.flightHotelReq.cvv = $scope.paymentDetail.cvv;
            $scope.flightHotelReq.expireMonth = $scope.paymentDetail.expireMonth;
            $scope.flightHotelReq.expireYear = $scope.paymentDetail.expireYear; */
            //$scope.paymentDetail=null;
            $http.get(ServerService.serverPath + 'rest/validate/session').then(function(response) {
                if (response.data.isSessionValid == true) {
                    sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom));
                    sessionStorage.setItem("user", $scope.user);
                    /* $http.post(ServerService.serverPath + 'rest/fltNhtl/booking/confirm', JSON.stringify($scope.flightHotelReq), { */
                    if ($scope.bookingReq.paymentMode == null || undefined) {
                        Lobibox.alert('warning', {
                            msg: "Please Select any one Payment Section.."
                        });
                        $scope.loading = false;
                        $scope.disabled = false;
                    } else {
                        $http.post(ServerService.serverPath + 'rest/fltNhtl/booking/confirm', angular.toJson($scope.flightHotelReq), {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
                        }).then(function(response) {
                            $scope.loading = false;
                            $scope.disabled = false;
                            if (response.data.status == 'SUCCESS') {

                                if ($scope.bookingReq.paymentMode === "DEBIT" || $scope.bookingReq.paymentMode === "CREDIT") {
                                    $window.location.href = response.data.data;
                                    return;
                                } else {


                                    //	window.location.href = response.data.returnUrl;
                                    var data = response.data;
                                    var expiry_date = $scope.paymentDetail.expireYear + $scope.paymentDetail.expireMonth
                                    $('#hdn_access_code').attr('name', 'access_code');
                                    $('#hdn_access_code').attr('value', data.data.access_code);
                                    $('#hdn_language').attr('name', 'language');
                                    $('#hdn_language').attr('value', data.data.language);
                                    $('#hdn_merchant_id').attr('name', 'merchant_identifier');
                                    $('#hdn_merchant_id').attr('value', data.data.merchant_identifier);
                                    $('#hdn_merchant_ref').attr('name', 'merchant_reference');
                                    $('#hdn_merchant_ref').attr('value', data.data.merchant_reference);
                                    $('#hdn_merchant_ref').attr('value', data.data.merchant_reference);
                                    $('#hdn_return_url').attr('name', 'return_url');
                                    $('#hdn_return_url').attr('value', data.data.return_url);
                                    $('#hdn_serv_command').attr('name', 'service_command');
                                    $('#hdn_serv_command').attr('value', data.data.service_command);
                                    $('#hdn_signature').attr('name', 'signature');
                                    $('#hdn_signature').attr('value', data.data.signature);
                                    $('#hdn_tok_name').attr('name', 'token_name');
                                    $('#hdn_tok_name').attr('value', data.data.token_name);
                                    $('#paymentPage').attr('action', data.data.postUrl);
                                    $('#hdn_card_security_code').attr('name', 'card_security_code');
                                    $('#hdn_card_security_code').attr('value', $scope.paymentDetail.cvv);
                                    $('#hdn_card_holder_name').attr('name', 'card_holder_name');
                                    $('#hdn_card_holder_name').attr('value', $scope.paymentDetail.cardName);
                                    $('#hdn_card_number').attr('name', 'card_number');
                                    $('#hdn_card_number').attr('value', $scope.paymentDetail.cardNo);
                                    $('#hdn_expiry_date').attr('name', 'expiry_date');
                                    $('#hdn_expiry_date').attr('value', expiry_date);
                                    $('#paymentPage').submit();
                                }
                            } else if (response.data.status == 'FAILURE') {

                                Lobibox.alert('error', {
                                    msg: response.data.message
                                });
                            } else {

                                Lobibox.alert('error', {
                                    msg: 'Unexpected Error Occurred, Please Try Again Later'
                                });
                            }

                        })
                    }
                }
            })

        } else {
            $('html,body').scrollTop(240);
            $scope.errMsg = "Enter All * (Mandatory) Fields";
        }
    }



    $scope.getNumber = function(num) {
        return new Array(num);
    }

    /*  $http.get("json/flightReview.json")
     .then(function (response) {
    	 $scope.flightList = response.data;
     });
     $http.get("json/hotelResultFilter.json")
     .then(function (response) {
    	 $scope.filterOptions = response.data;
     });
     $http.get("json/hotelResultList.json")
     .then(function (response) {
    $scope.hotelList = response.data; */
    //});
    $scope.hotelDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
    $scope.range = function(n) {
        n = Number(n);
        return new Array(n);
    };

    $scope.myInterval = 5000;
    $scope.thumbnailSize = 5;
    $scope.thumbnailPage = 1;
    $scope.slides = [{
        image: "images/hotel/h1.jpeg"
    }, {
        image: "images/hotel/h2.jpeg"
    }, {
        image: "images/hotel/h3.jpeg"
    }, {
        image: "images/hotel/h4.jpeg"
    }, {
        image: "images/hotel/h5.jpeg"
    }, {
        image: "images/hotel/h6.jpeg"
    }, {
        image: "images/hotel/h7.jpeg"
    }, {
        image: "images/hotel/h8.jpeg"
    }];

    $scope.prevPage = function() {
        if ($scope.thumbnailPage > 1) {
            $scope.thumbnailPage--;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.nextPage = function() {
        if ($scope.thumbnailPage <= Math.floor($scope.slides.length / $scope.thumbnailSize)) {
            $scope.thumbnailPage++;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    $scope.setActive = function(idx) {
        $scope.slides[idx].active = true;
    }

    $scope.reviews = [{
        "comment": "Good in general",
        "rating": "7.8",
        "rateValue": "Very Good",
        "reviewer": "Hisham",
        "location": "Shj"
    }, {
        "comment": "Good in general",
        "rating": "7.2",
        "rateValue": "Very Good",
        "reviewer": "Nijamuddin",
        "location": "Chennai"
    }];

    $scope.roomsLists = [{
        "title": "Luxury Room",
        "img": "images/hotel/13461.jpg",
        "description": ["Luxury Rooms are warm and elegant accommodations boasting views of the city.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "1,888.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "1,935.00"
        }]
    }, {
        "title": "Luxury Room Park View",
        "img": "images/hotel/13462.jpg",
        "description": ["Luxury Rooms are warm and elegant high floor accommodations showcasing views of the park.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "2,165.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "2,211.00"
        }]
    }];
    //hotelRoomsList
    var hotelRoomsList = angular.element(document.getElementById('hotelRoomsList'));
    $scope.toHotelRoomsList = function() {
        $document.scrollTo(hotelRoomsList, 30, 1000);
    }

    /* openFlightInfo function */
    $scope.openFlightInfo = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightInfo.html',
                controller: 'flightHolidayBookDetailsCtrl',
                size: size,
                appendTo: parentElem
            });
        }
        /* end */
});
ouaApp.controller('flightInfoCtrl', function($http, $scope, $uibModalInstance) {
    $scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('selectedFliResp'));
    $scope.flightsearchResdetails = $scope.flightsearchResInfo.selectedResult.flightSearchResults[0];


    $scope.ok = function() {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
ouaApp.controller('flightHolidayBookDetailsCtrl', function($scope, $uibModalInstance, $rootScope, stopsService) {
    $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
    $scope.cabinClass = $scope.flightDetails.clasType;
    $scope.flightDetails1 = JSON.parse(sessionStorage.getItem('newSelectedFliResp'));
    if ($scope.flightDetails1 == undefined) {
        $scope.flightDetails1 = JSON.parse(sessionStorage.getItem('selectedFliResp')).selectedResult.flightSearchResults[0];
    }
    // sessionStorage.setItem('flightplusSearchResp')
    var value = $rootScope.selectedIndex;
    $scope.flightDetails2 = $scope.flightDetails.data
        //$scope.flightDetails1 = $scope.$parent.flightResultList1
    $scope.filter = $rootScope.sCheckId
    $scope.airlineCode = $rootScope.airlineCodes;
    var flag = false;
    var filterDatas = {};
    if ($scope.filter != null && $scope.airlineCode != null) {

        filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
        $scope.flightDetails1 = filterDatas[value];
    } else if (!flag) {
        if ($scope.filter != null || $scope.filter != undefined) {
            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        } else if ($scope.airlineCode != null || $scope.airlineCode != undefined) {
            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        }
    }
    /* if($scope.filter != null || $scope.filter != undefined){
    	var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    	$scope.flightDetails1 = filterDatas[value];
    } */

    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        //$scope.waitingDuration;	      
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }

    $scope.totalDuration = [];
    $scope.getTotalDuration = function(a, b, i) {
        //	console.log(i);
        $scope.totalDuration[i] = timeConvert(parseInt(a) + parseInt(b));
        //	console.log($scope.totalDuration);
    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

ouaApp.controller('flightFareinfoContent', function($scope, ConstantService, $uibModalInstance, $rootScope) {
    //console.log(" flightFareinfoContent ");
    $scope.currency = ConstantService.currency;
    if (sessionStorage.getItem('selectedFliResp')) {
        //var index = $rootScope.selectedFareIndex
        $scope.flightDetail = JSON.parse(sessionStorage.getItem('selectedFliResp'));
        $scope.flightDetail1 = $scope.flightDetail.selectedResult.flightSearchResults[0];
        $scope.flightDetail2 = $scope.flightDetail1.fareslst;
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };


    /* modelDismiss function */
    $scope.modelDismiss = function() {
            $uibModalInstance.close();
        }
        /* end */
})