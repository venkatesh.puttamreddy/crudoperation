ouaApp.controller('holidayDetailsController', function($scope, $http, $uibModal, $log, $document, $location, urlService, renderHTMLFactory, ServerService, customSession, ConstantService, $uibModal) {
    $('html, body').scrollTop(0);
    $scope.hotelResp = JSON.parse(sessionStorage.getItem('selectedHotResp'));
    $scope.flightResp = JSON.parse(sessionStorage.getItem('selectedFliResp'));
    $scope.currency = ConstantService.currency
    $scope.flightResult = $scope.flightResp.selectedResult;
    $scope.hotelPlusSearchResp = JSON.parse(sessionStorage.getItem('hotelPlusSearchResp'));
    //$scope.hotelPlusSearchResp1 = $scope.hotelPlusSearchResp.searchResults;  
    $scope.selectedHotelRooms = JSON.parse(sessionStorage.getItem('selectedHotels'));
    $scope.urlService = urlService.relativePath;

    $scope.renderHTMLFactory = renderHTMLFactory;
    $scope.totalRoomFares = 0.00;
    $scope.hotelResult = $scope.hotelResp.searchResults[0];


    $scope.goBack = function(path) {
        $location.path(path);
    }

    /* for mutiroom select */
    $scope.defaultSelect = function() {

        $('.roomset').each(function(index) {
            $('input[type=radio]:first', this).prop("checked", true)
        })

    }


    $scope.roundUp = function(value, mult, dir) {
        dir = dir || 'nearest';
        mult = mult || 1;
        value = !value ? 0 : Number(value);
        if (dir === 'up') {
            return Math.ceil(value / mult) * mult;
        } else if (dir === 'down') {
            return Math.floor(value / mult) * mult;
        } else {
            return Math.round(value / mult) * mult;
        }
    }


    $scope.selectedHotelRoomsIndex = JSON.parse(sessionStorage.getItem('selectedHotelRoomsIndex'));
    if ($scope.selectedHotelRoomsIndex == undefined) {
        $scope.selectedHotelRoomsIndex = [];
        $scope.selectedHotelRooms = [];
    } else {
        for (i = 0; i < $scope.hotelResult.occGroups.length; i++) {
            for (j = 0; j < $scope.hotelResult.occGroups[i].roomInfos.length; j++) {
                $scope.hotelResult.occGroups[i].roomInfos[j].isSelected = false;
                $scope.hotelResult.occGroups[i].roomInfos[j].rawRoomFare = $scope.hotelResult.occGroups[i].roomInfos[j].rawRoomFare;
            }
        }
        $scope.selectedHotelRooms = [];
        for (i = 0; i < $scope.selectedHotelRoomsIndex.length; i++) {
            $scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].isSelected = true;
            $scope.totalRoomFares = $scope.totalRoomFares + $scope.roundUp($scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].roomFare, '', 'up');
            $scope.selectedHotelRooms.push($scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]]);

        }
    }



    $scope.roomFare = [];

    if ($scope.selectedHotelRooms.length == 0) {
        for (i = 0; i < $scope.hotelResult.occGroups.length; i++) {
            for (j = 0; j < $scope.hotelResult.occGroups[i].roomInfos.length; j++) {
                if (j == 0) {
                    $scope.hotelResult.occGroups[i].roomInfos[j].isSelected = true;
                    $scope.selectedHotelRooms.push($scope.hotelResult.occGroups[i].roomInfos[j]);
                    $scope.hotelResult.occGroups[i].roomInfos[j].rawRoomFare = $scope.hotelResult.occGroups[i].roomInfos[j].roomFare;
                    // 
                    $scope.totalRoomFares = $scope.totalRoomFares + ($scope.hotelResult.occGroups[i].roomInfos[j].roomFare);
                    $scope.selectedHotelRoomsIndex.push(j);

                } else {
                    $scope.hotelResult.occGroups[i].roomInfos[j].rawRoomFare = $scope.hotelResult.occGroups[i].roomInfos[j].roomFare;
                    $scope.hotelResult.occGroups[i].roomInfos[j].isSelected = false;
                }
            }
        }
    }



    $scope.calculateTotal = function(parentIndex, index) {
            $scope.totalRoomFares = 0.00;
            $scope.selectedHotelRoomsIndex[parentIndex] = index;
            for (i = 0; i < $scope.hotelResult.occGroups.length; i++) {
                for (j = 0; j < $scope.hotelResult.occGroups[i].roomInfos.length; j++) {
                    $scope.hotelResult.occGroups[i].roomInfos[j].isSelected = false;
                }
            }
            $scope.selectedHotelRooms = [];
            for (i = 0; i < $scope.selectedHotelRoomsIndex.length; i++) {
                $scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].isSelected = true;
                var tempRoomFare = ($scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].roomFare);
                //$scope.totalRoomFares=$scope.totalRoomFares+parseInt($scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]].rawRoomFare);
                $scope.totalRoomFares = $scope.totalRoomFares + tempRoomFare;
                $scope.selectedHotelRooms.push($scope.hotelResult.occGroups[i].roomInfos[$scope.selectedHotelRoomsIndex[i]]);

            }
        }
        /* end */



    $scope.checkValue = function(val, size, parentSelector) {
        // if(val=="Non Refundable"){
        // 	val="A non-refundable rate is usually the hotel’s/provider’s best rate. Full payment is needed at the time of booking and if you decide to cancel, it will be a 100% loss, so no refund would be due. As this is a promotional rate, no changes at all can be made to your booking once it is confirmed.";
        // }else if(val.startsWith("If cancelled")){
        // 	val = val.replace("<b>","");
        // 	val = val.replace("</b>","");
        // 	val = val.replace("<br/>","");
        // 	val = val.replace("<b>","");
        // 	val = val.replace("</b>","");
        // 	val = val.replace("<br/>","");
        // }
        // sessionStorage.setItem("hypeMsg", val);
        var cancelPolicyInfo = [];
        if (val == "Non Refundable") {
            var tempCancelValue = { string: "A non-refundable rate is usually the hotel’s/provider’s best rate. Full payment is needed at the time of booking and if you decide to cancel, it will be a 100% loss, so no refund would be due. As this is a promotional rate, no changes at all can be made to your booking once it is confirmed." };
            cancelPolicyInfo[0] = tempCancelValue;
        } else {
            cancelPolicyInfo = val;
        }
        sessionStorage.setItem("hypeMsg", JSON.stringify(cancelPolicyInfo));
        /* sessionStorage.setItem("hypeMsg", val); */

        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myHypeMessagehf.html',
            controller: 'myHypeMessagehfCtrl',
            backdrop: true,
            size: 'lg',
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.checkMsg = function(val) {
            if (val.startsWith("If cancelled")) {
                return false;
            } else if (val == "Non Refundable") {
                return true;
            }
        }
        /* openFlightInfo function */
    $scope.openFlightInfo = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightInfo.html',
                controller: 'flightHolidayDetailsCtrl',
                size: size,
                appendTo: parentElem
            });
        }
        /* end */
        //(date2 - date1) / 1000 / 60 / 60 / 24
    var noNight = parseInt($scope.hotelPlusSearchResp.checkOutDate) - parseInt($scope.hotelPlusSearchResp.checkInDate)
        /* onSelect function */
    $scope.loading = false;
    $scope.disabled = false;
    var roonList = [];
    var repriceObj = new Object();
    $scope.clsObj = {}

    $scope.onSelect = function() {
        $scope.loading = true;
        $scope.disabled = true;
        $http.get(ServerService.serverPath + 'rest/validate/session').then(function(response) {
            if (response.data.isSessionValid == true) {
                for (i = 0; i < $scope.selectedHotelRoomsIndex.length; i++) {
                    child = $scope.selectedHotelRoomsIndex[i];
                    parent = i;
                    var clsObj = {};
                    var cls = $(this).attr('class');
                    $scope.clsObj.parentIndex = parent;
                    $scope.clsObj.childIndex = child;
                    sessionStorage.setItem('roomIndex', JSON.stringify($scope.clsObj));
                    repriceObj.city = $scope.hotelResult.cityCode;
                    repriceObj.hotelCode = $scope.hotelResult.htlCode;
                    repriceObj.checkIn = $scope.hotelResp.checkInDate;
                    repriceObj.checkOut = $scope.hotelResp.checkOutDate;
                    repriceObj.duration = noNight;
                    repriceObj.provider = $scope.hotelResult.provider;
                    repriceObj.currency = $scope.hotelResp.currency;
                    repriceObj.convRate = $scope.hotelResult.convRate;
                    repriceObj.servChrg = $scope.hotelResult.servChrg;
                    repriceObj.discount = $scope.hotelResult.discount;
                    repriceObj.getPackagePrice = true;
                    repriceObj.markupAmount = $scope.hotelResult.markupAmount;
                    var rmdtlsObj = {};
                    rmdtlsObj.roomId = $scope.hotelResult.occGroups[parent].roomInfos[child].roomRefNo;
                    rmdtlsObj.roomCode = $scope.hotelResult.occGroups[parent].roomInfos[child].roomCode;
                    rmdtlsObj.noOfRooms = $scope.hotelResult.occGroups[parent].roomGrpBean.roomCount;
                    rmdtlsObj.adultCount = $scope.hotelResult.occGroups[parent].roomGrpBean.adultCount;
                    rmdtlsObj.childCount = $scope.hotelResult.occGroups[parent].roomGrpBean.childCount + $scope.hotelResult.occGroups[parent].roomGrpBean.infantCount;
                    rmdtlsObj.xtraBed = $scope.hotelResult.occGroups[parent].roomInfos[child].xtraBeds;
                    rmdtlsObj.rateBasis = $scope.hotelResult.occGroups[parent].roomInfos[child].rateBasisCode;
                    rmdtlsObj.allocations = $scope.hotelResult.occGroups[parent].roomInfos[child].allocations;
                    rmdtlsObj.netCost = $scope.hotelResult.occGroups[parent].roomInfos[child].netCost;
                    rmdtlsObj.grossCost = $scope.hotelResult.occGroups[parent].roomInfos[child].grossCost;
                    rmdtlsObj.roomFare = $scope.hotelResult.occGroups[parent].roomInfos[child].roomFare;
                    rmdtlsObj.cancellationChrgCode = $scope.hotelResult.occGroups[parent].roomInfos[child].cancellationChrgCode;
                    if ($scope.hotelResult.occGroups[parent].roomInfos[child].servChrgKeys) {
                        var servChrgKeys = new Array();
                        var servChrgKeyLength = $scope.hotelResult.occGroups[parent].roomInfos[child].servChrgKeys.length
                        for (var s = 0; s < servChrgKeyLength; s++) {
                            servChrgKeys.push($scope.hotelResult.occGroups[parent].roomInfos[child].servChrgKeys[s]);
                        }
                        rmdtlsObj.servChrgKeys = servChrgKeys;
                    }
                    /*	if($scope.hotelResult.occGroups[parent].roomGrpBean.childAges!=null){
                    		var childAges = new Array();*/
                    if ($scope.hotelResult.occGroups[parent].roomGrpBean.childAges != null) {
                        rmdtlsObj.childAges = JSON.parse($scope.hotelResult.occGroups[parent].roomGrpBean.childAges);
                        /*	var childAgeLength;
                        	if($scope.hotelResult.occGroups[parent].roomGrpBean.childAges.length){
                        		childAgeLength = $scope.hotelResult.occGroups[parent].roomGrpBean.childAges.split(',').length;
                        		}else {
                        		childAgeLength = $scope.hotelResult.occGroups[parent].roomGrpBean.childAges.length;
                        	}
                        	for(var c=0; c<childAgeLength; c++){
                        		childAges.push($scope.hotelResult.occGroups[parent].roomGrpBean.childAges.split(',')[c]);
                        	}
                        	rmdtlsObj.childAges = childAges;*/
                    }

                    roonList.push(rmdtlsObj);
                    repriceObj.roomDtls = roonList;
                    $scope.repriceObj = repriceObj;
                }

                $http.post(ServerService.serverPath + 'rest/hotel/reprice', JSON.stringify($scope.repriceObj), {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
                }).then(function(response) {
                    //console.log(response);
                    $scope.loading = false;
                    $scope.disabled = false;
                    if (response.data.status == 'SUCCESS') {
                        $scope.repriceResp = response.data.data;
                        //console.log($scope.repriceResp);
                        sessionStorage.setItem('repriseResponse', JSON.stringify($scope.repriceResp));
                        sessionStorage.setItem('selectedHotels', JSON.stringify($scope.selectedHotelRooms));
                        sessionStorage.setItem('selectedHotelRoomsIndex', JSON.stringify($scope.selectedHotelRoomsIndex));
                        $location.path('/holidayReview');
                    } else {
                        Lobibox.alert('error', {
                            msg: response.data.errorMessage
                        })
                    }


                })


            }
        })
    }

    /* $scope.onSelect = function(parent, child){
    	$scope.loading = true;
    	$scope.disabled = true;
    	//console.log('parent '+parent)
    	//console.log('child '+child)
    	$scope.clsObj.parentIndex = parent;
    	$scope.clsObj.childIndex = child;
    	sessionStorage.setItem('roomIndex', JSON.stringify($scope.clsObj));
    	$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
    		
    		if(response.data.isSessionValid == true){
    			
    			repriceObj.city = $scope.hotelResp.searchResults[0].cityCode;
    			repriceObj.hotelCode = $scope.hotelResp.searchResults[0].htlCode;
    			repriceObj.checkIn = $scope.hotelResp.checkInDate;
    			repriceObj.checkOut = $scope.hotelResp.checkOutDate;
    			repriceObj.duration = noNight;
    			repriceObj.provider = $scope.hotelResp.searchResults[0].provider;
    			repriceObj.currency = $scope.hotelResp.currency; 
    			repriceObj.convRate = $scope.hotelResp.searchResults[0].convRate; 
    			repriceObj.servChrg = $scope.hotelResp.searchResults[0].servChrg; 
    			repriceObj.discount = $scope.hotelResp.searchResults[0].discount; 
    			repriceObj.markupAmount = $scope.hotelResp.searchResults[0].markupAmount;
    			var rmdtlsObj = {};
    			rmdtlsObj.roomId = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].roomRefNo;
    			rmdtlsObj.roomCode = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].roomCode;
    			rmdtlsObj.noOfRooms = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.roomCount;
    			rmdtlsObj.adultCount = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.adultCount;
    			rmdtlsObj.childCount = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childCount+$scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.infantCount;
    			rmdtlsObj.xtraBed = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].xtraBeds;
    			rmdtlsObj.rateBasis = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].rateBasisCode;
    			rmdtlsObj.allocations = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].allocations;
    			rmdtlsObj.roomFare = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].roomFare;
    			rmdtlsObj.cancellationChrgCode = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].cancellationChrgCode;
    			
    			if($scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].servChrgKeys){
    				var servChrgKeys = new Array();
    				var servChrgKeyLength = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].servChrgKeys.length
    				for(var s=0; s<servChrgKeyLength; s++){
    					servChrgKeys.push($scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].servChrgKeys[i]);
    				}
    				rmdtlsObj.servChrgKeys = servChrgKeys;
    			}
    			
    			if($scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges!=null){
    				var childAges = new Array();
    				var childAgeLength;
    				if($scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.length){
    					childAgeLength = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.split(',').length;
    					}else {
    					childAgeLength = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.length;
    				}
    				for(var c=0; c<childAgeLength; c++){
    					childAges.push($scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.split(',')[c]);
    				}
    				rmdtlsObj.childAges = childAges;
    			}
    			
    			roonList.push(rmdtlsObj);
    			repriceObj.roomDtls = roonList;
    			$scope.repriceObj = repriceObj;
    			
    			
    			$http.post(ServerService.serverPath+'rest/hotel/reprice',$scope.repriceObj, {
    				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    				}).then(function(response){
    				
    				$scope.loading = false;
    				$scope.disabled = false;
    				$scope.repriseResponse = response.data.data;
    				sessionStorage.setItem('repriseResponse',JSON.stringify($scope.repriseResponse));
    				$location.path('/holidayReview');
    			})
    			
    		} 
    	})
    	
	
	
    } */
    /*ReadMore About Hotel */
    $scope.hotelDetailInfo = function(obj) {
        var currntEl = obj.target.attributes.id.value;

        if (jQuery('#' + currntEl).text() == 'More Info') {
            jQuery('#' + currntEl).text('Less Info');
            jQuery('#' + currntEl).prev().css('height', 'auto');

        } else {
            jQuery('#' + currntEl).text('More Info');
            jQuery('#' + currntEl).prev().css('height', '80px');
            jQuery('#' + currntEl).prev().css('overflow', 'hidden');
        }

    }
    $scope.hotelDetailInfoInit = function() {
        // var currntEl = $('#hotelIndex').innerHTML;
        var hotelmoreInfo = "#hotelMoreInfo";
        var currntEl = $(hotelmoreInfo).text();

        if (currntEl == 'More Info') {
            jQuery(hotelmoreInfo).text('Less Info');
            jQuery(hotelmoreInfo).prev().css('height', 'auto');

        } else {
            jQuery(hotelmoreInfo).text('More Info');
            jQuery(hotelmoreInfo).prev().css('height', '80px');
            jQuery(hotelmoreInfo).prev().css('overflow', 'hidden');
        }

    }


    /*ReadMore About hotel end*/

    /* end */

    $http.get("json/flightReview.json")
        .then(function(response) {
            $scope.flightList = response.data;
        });
    $http.get("json/hotelResultFilter.json")
        .then(function(response) {
            $scope.filterOptions = response.data;
        });
    $http.get("json/hotelResultList.json")
        .then(function(response) {
            $scope.hotelList = response.data;
        });
    $scope.hotelDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
    $scope.range = function(n) {
        n = Number(n);
        return new Array(n);
    };

    $scope.myInterval = 5000;
    $scope.thumbnailSize = 5;
    $scope.thumbnailPage = 1;
    $scope.slides = [{
        image: "images/hotel/h1.jpeg"
    }, {
        image: "images/hotel/h2.jpeg"
    }, {
        image: "images/hotel/h3.jpeg"
    }, {
        image: "images/hotel/h4.jpeg"
    }, {
        image: "images/hotel/h5.jpeg"
    }, {
        image: "images/hotel/h6.jpeg"
    }, {
        image: "images/hotel/h7.jpeg"
    }, {
        image: "images/hotel/h8.jpeg"
    }];

    $scope.prevPage = function() {
        if ($scope.thumbnailPage > 1) {
            $scope.thumbnailPage--;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.nextPage = function() {
        if ($scope.thumbnailPage <= Math.floor($scope.slides.length / $scope.thumbnailSize)) {
            $scope.thumbnailPage++;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }

    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    $scope.setActive = function(idx) {
        $scope.slides[idx].active = true;
    }

    $scope.reviews = [{
        "comment": "Good in general",
        "rating": "7.8",
        "rateValue": "Very Good",
        "reviewer": "Hisham",
        "location": "Shj"
    }, {
        "comment": "Good in general",
        "rating": "7.2",
        "rateValue": "Very Good",
        "reviewer": "Nijamuddin",
        "location": "Chennai"
    }];

    $scope.roomsLists = [{
        "title": "Luxury Room",
        "img": "images/hotel/13461.jpg",
        "description": ["Luxury Rooms are warm and elegant accommodations boasting views of the city.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "1,888.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "1,935.00"
        }]
    }, {
        "title": "Luxury Room Park View",
        "img": "images/hotel/13462.jpg",
        "description": ["Luxury Rooms are warm and elegant high floor accommodations showcasing views of the park.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "2,165.00"
        }, {
            "name": "Bed And Breakfast",
            "fare": "2,211.00"
        }]
    }];
    //hotelRoomsList
    var hotelRoomsList = angular.element(document.getElementById('hotelRoomsList'));
    $scope.toHotelRoomsList = function() {
        $document.scrollTo(hotelRoomsList, 30, 1000);
    }

    /* open FareInfo */
    $scope.openFareInfo = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            //	$rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightFareinfoContent.html',
                controller: 'flightFareinfoContent',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        //	return $scope.items
                    }
                }
            })
        }
        /* end */

}).controller('flightHolidayDetailsCtrl', function($scope, $uibModalInstance, $rootScope, stopsService) {

    $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
    $scope.cabinClass = $scope.flightDetails.clasType;
    $scope.flightDetails1 = JSON.parse(sessionStorage.getItem('newSelectedFliResp'));
    if ($scope.flightDetails1 == undefined) {
        $scope.flightDetails1 = JSON.parse(sessionStorage.getItem('selectedFliResp')).selectedResult.flightSearchResults[0];
    }
    // sessionStorage.setItem('flightplusSearchResp')
    var value = $rootScope.selectedIndex;
    $scope.flightDetails2 = $scope.flightDetails.data
        //$scope.flightDetails1 = $scope.$parent.flightResultList1
    $scope.filter = $rootScope.sCheckId
    $scope.airlineCode = $rootScope.airlineCodes;
    var flag = false;
    var filterDatas = {};
    if ($scope.filter != null && $scope.airlineCode != null) {

        filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
        $scope.flightDetails1 = filterDatas[value];
    } else if (!flag) {
        if ($scope.filter != null || $scope.filter != undefined) {
            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        } else if ($scope.airlineCode != null || $scope.airlineCode != undefined) {
            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        }
    }
    /* if($scope.filter != null || $scope.filter != undefined){
    	var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    	$scope.flightDetails1 = filterDatas[value];
    } */

    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        //$scope.waitingDuration;	      
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }

    $scope.totalDuration = [];
    $scope.getTotalDuration = function(a, b, i) {
        //console.log(i);
        $scope.totalDuration[i] = timeConvert(parseInt(a) + parseInt(b));
        //console.log($scope.totalDuration);
    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
ouaApp.controller('myHypeMessagehfCtrl', function($scope, $rootScope, $uibModalInstance) {
    $scope.myHypeMsg = JSON.parse(sessionStorage.getItem("hypeMsg"));
    /* $scope.myHypeMsg = sessionStorage.getItem("hypeMsg"); */
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.removeBR = function(string) {
        string = string.replace(". <br/>", ".");
        string = string.replace("<br/>", ".");
        string = string.replace("</b>", "");
        string = string.replace("<b>", "");
        return string;

    }
});

ouaApp.controller('flightFareinfoContent', function($scope, ConstantService, $uibModalInstance, $rootScope) {
        //console.log(" flightFareinfoContent ");
        $scope.currency = ConstantService.currency;
        if (sessionStorage.getItem('selectedFliResp')) {
            //var index = $rootScope.selectedFareIndex
            $scope.flightDetail = JSON.parse(sessionStorage.getItem('selectedFliResp'));
            $scope.flightDetail1 = $scope.flightDetail.selectedResult.flightSearchResults[0];
            $scope.flightDetail2 = $scope.flightDetail1.fareslst;
        }

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    })
    /* ouaApp.controller('flightInfoCtrl', function ($http, $scope, $uibModalInstance) {
	$scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('selectedFliResp'));
	$scope.flightsearchResdetails = $scope.flightsearchResInfo.selectedResult.flightSearchResults[0];
	
	
    $scope.ok = function () {
	$uibModalInstance.close($scope.selected.item);
    };
	
    $scope.cancel = function () {
	$uibModalInstance.dismiss('cancel');
    };
	});	 */
    // ouaApp.controller('flightInfoCtrl', function($scope, $uibModalInstance, $rootScope, stopsService){
    // $scope.cabinClass = localStorage.getItem('cabinClass');
    // $scope.flightDetails = JSON.parse(sessionStorage.getItem('selectedFliResp'))
    // $scope.flightDetails1 = $rootScope.flightDetails;
    // var value = $rootScope.selectedIndex;
    // $scope.flightDetails2 = $scope.flightDetails.data
    // $scope.flightDetails1 = $scope.$parent.flightResultList1
    // $scope.filter = $rootScope.sCheckId
    // $scope.airlineCode = $rootScope.airlineCodes;
    // var flag = false;
    // var filterDatas = {};
    // if($scope.filter != null && $scope.airlineCode != null ){

// filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
// $scope.flightDetails1 = filterDatas[value];
// } else if(!flag){
// if($scope.filter != null || $scope.filter != undefined){
// filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
// $scope.flightDetails1 = filterDatas[value];
// } else if($scope.airlineCode != null || $scope.airlineCode != undefined){
// filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
// $scope.flightDetails1 = filterDatas[value];
// }
// }
// /* if($scope.filter != null || $scope.filter != undefined){
// var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
// $scope.flightDetails1 = filterDatas[value];
// } */

// $scope.waitingDuration=[];	
// var count=0;
// $scope.getWaitingDuration = function(a,b,ind,x,key){		 
// $scope.waitingDuration;	      
// if(a!=undefined && b!==undefined ){
// var startDate = new Date(a);   
// var endDate   = new Date(b);
// var d =(endDate.getTime() - startDate.getTime()) / 1000;	
// d = Number(d);
// var h = Math.floor(d / 3600);  	  
// var m = Math.floor(d % 3600 / 60);
// var hDisplay = h +"h";	    
// var mDisplay = m +"m";
// $scope.waitingDuration[parseInt(ind+""+x+""+key)]= hDisplay +" "+ mDisplay;	   	 	  	  	       	                

// } 

// }		

// $scope.totalDuration = [];
// $scope.getTotalDuration =function(a,b,i){
// //console.log(i);
// $scope.totalDuration[i]  = timeConvert(parseInt(a)+parseInt(b));  		
// //console.log($scope.totalDuration);
// } 

// function timeConvert(n) {
// var num = n;
// var hours = (num / 60);
// var rhours = Math.floor(hours);
// var minutes = (hours - rhours) * 60;
// var rminutes = Math.round(minutes);
// return rhours + "h " + rminutes + " m";	      
// }

// $scope.cancel = function () {
// $uibModalInstance.dismiss('cancel');
// };
// });