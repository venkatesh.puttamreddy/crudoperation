var currentURL = window.location.href;
 var relativePath = currentURL.substring(0, currentURL.lastIndexOf("/") + 1);

function loadCMS(){
    var sessionAuthData = JSON.parse(localStorage.getItem('authentication'));
    if(sessionAuthData==null){
        var authenticationreq = { "username": DEFAULT_USER };
			$.ajaxSetup({
				headers: {
					'Content-Type': "application/json"
				}
			});
        $.ajax({
            type: 'POST',
            url: API_URL+'auth/api/v1/guest-login',
            data: JSON.stringify(authenticationreq),
            success: function (response) {
                if (response) {
                    var token = response.authToken;
                    $.ajaxSetup({
                        headers: {
                            'Content-Type': "application/json",
                            'Authorization': "Bearer " + token
                        }
                    });
                        getHeadines();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
               
            }
        });

        
    }else{
        $.ajaxSetup({
            headers: {
                'Content-Type': "application/json",
                'Authorization': "Bearer " + sessionAuthData.data.authToken
            }
        });
            getHeadines();
    }

   
    
}

function getHeadines() {
    var sectionmasterids = JSON.parse(localStorage.getItem('cmsHeaderSectionMasterID'));
    if(sectionmasterids == null){
    $.ajax({
        type: 'GET',
        url: API_URL + 'cms/api/v1/pageByName/headline',
        success: function (response) {
            if (response.success == true) {
                headlineID = response.data.sectionMasterId[0];

                var secMasterid = [{
                    headlineID: response.data.sectionMasterId[0],
                }];
                localStorage.setItem('cmsHeaderSectionMasterID',JSON.stringify(secMasterid));
                headlineDetails(headlineID); 
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            if (xhr.status === 403) {
               
            }
        }
    });
    }else{
        headlineID = sectionmasterids[0].headlineID;
        headlineDetails(headlineID); 
    }
}

function headlineDetails(HeadlineID){
    $.ajax({
        type: 'GET',
        url: API_URL + 'cms/api/v1/section-details/published/section-master/' + HeadlineID,
        success: function (response) {
            if (response.data.length != 0) {
                $("#headlineContent").html('');
                $('.ticker-text1').html('');
                $("#newsTicker ").html('');
                $.each(response.data, function (index, value) {
                    if(response.data.length > 1){
                        $("#headlineContent").html('');
                        $('.ticker-text1').html('');
                        $("#newsTicker ").html('');
                        for (i = 0; i < response.data.length; i++) {
                            var tickresponse = response.data;
                            var tickerHeads= response.data[i].content.banner.headline;
                            tickerHeads = tickerHeads.replace('<p>','');
                            tickerHeads = tickerHeads.replace('</p>',"");

                                $("#newsTicker ").append(
                                    tickerHeads
                                  );
                          }
                      
            }
                if(response.data.length === 1){
                    $('#headlineContent').css('display','none');
                    $('.ticker-text1').append(value.content.banner.headline);
                   }
                });
            }

           

        },
        error: function (xhr, textStatus, errorThrown) { 
            if (xhr.status === 403) {

            }
        }
    });
}

